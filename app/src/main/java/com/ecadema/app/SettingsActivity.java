package com.ecadema.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import java.util.Locale;

public class SettingsActivity extends AppCompatActivity {

    Context context;
    SharedPreferences sharedPreferences;
    RadioButton radioenglish,radioarabic;
    ImageView close;
    SharedPreferences.Editor editor;
    LinearLayout languageLayout,passwordLayout,notificationLayout,myProfile,myTrainers,groupSessions,terms_conditions,privacyPolicy,logout;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);
        context=this;
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        editor=sharedPreferences.edit();
        close=findViewById(R.id.close);

        languageLayout=findViewById(R.id.languageLayout);
        passwordLayout=findViewById(R.id.passwordLayout);
        notificationLayout=findViewById(R.id.notificationLayout);
        myProfile=findViewById(R.id.myProfile);
        myTrainers=findViewById(R.id.myTrainers);
        terms_conditions=findViewById(R.id.terms_conditions);
        groupSessions=findViewById(R.id.groupSessions);
        privacyPolicy=findViewById(R.id.privacyPolicy);
        logout=findViewById(R.id.logout);

        close.setOnClickListener(v-> finish());

        languageLayout.setOnClickListener(v-> {
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                    inflater1.inflate(R.layout.language_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            CardView update=dialogView.findViewById(R.id.update);
            radioenglish=dialogView.findViewById(R.id.radioenglish);
            radioarabic=dialogView.findViewById(R.id.radioarabic);

            update.setOnClickListener(v1->{
                if (radioenglish.isChecked()){
                    setAppLocale("en");
                    editor.putString("lang","en");
                    editor.apply();
                }
                else{
                    setAppLocale("ar");
                    editor.putString("lang","ar");
                    editor.apply();
                }
                dialog.dismiss();
            });


            close.setOnClickListener(v1->dialog.dismiss());
            dialog.create();
            dialog.show();
        });
        passwordLayout.setOnClickListener(v-> {
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                    inflater1.inflate(R.layout.password_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            CardView update=dialogView.findViewById(R.id.update);

            update.setOnClickListener(v1->{
                dialog.dismiss();
            });

            close.setOnClickListener(v1->dialog.dismiss());
            dialog.create();
            dialog.show();
        });
        notificationLayout.setOnClickListener(v-> {
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                    inflater1.inflate(R.layout.notification_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            CardView update=dialogView.findViewById(R.id.update);

            update.setOnClickListener(v1->{
                dialog.dismiss();
            });

            close.setOnClickListener(v1->dialog.dismiss());
            dialog.create();
            dialog.show();
        });
        myProfile.setOnClickListener(v-> {
            Intent intent=new Intent(this,MyProfile.class);
            startActivity(intent);
        });
        myTrainers.setOnClickListener(v-> {
            Intent intent=new Intent(this,MyTrainers.class);
            startActivity(intent);
        });
        terms_conditions.setOnClickListener(v-> {
            Uri uri = Uri.parse("https://ecadema.com/terms"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        });
        groupSessions.setOnClickListener(v-> {
            Intent intent=new Intent(this,MyGroupSessions.class);
            startActivity(intent);
        });
        privacyPolicy.setOnClickListener(v-> {
            Uri uri = Uri.parse("https://ecadema.com/privacy"); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        });
        logout.setOnClickListener(v-> {

            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                    inflater1.inflate(R.layout.logout_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            TextView cancel=dialogView.findViewById(R.id.cancel);
            CardView logout=dialogView.findViewById(R.id.logout);

            logout.setOnClickListener(v1->{
                sharedPreferences.edit().remove("userID").apply();
                Intent intent=new Intent("logout");
                sendBroadcast(intent);
                dialog.dismiss();
                finish();
            });

            close.setOnClickListener(v1->dialog.dismiss());
            cancel.setOnClickListener(v1->dialog.dismiss());

            dialog.create();
            dialog.show();
        });

    }

    private void setAppLocale(String LocaleCode)
    {
        Resources res=getResources();
        DisplayMetrics dr=res.getDisplayMetrics();
        Configuration configuration=res.getConfiguration();
        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
            configuration.setLocale(new Locale(LocaleCode.toLowerCase()));
        }
        else
        {
            configuration.locale=new Locale(LocaleCode.toLowerCase());
        }
        res.updateConfiguration(configuration,dr);

        Intent intent=new Intent(SettingsActivity.this,SplashActivity.class);
        TaskStackBuilder.create(SettingsActivity.this).addNextIntentWithParentStack(intent).startActivities();
        finish();
    }

}
/*try{
                    GoogleSignInOptions gso = new GoogleSignInOptions.
                            Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                            build();

                    GoogleSignInClient googleSignInClient= GoogleSignIn.getClient(context,gso);
                    googleSignInClient.signOut();
                }catch (Exception e){
                    e.printStackTrace();
                }

                try{
                    FacebookSdk.sdkInitialize(getApplicationContext());
                    if (AccessToken.getCurrentAccessToken() != null) {
                        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/",
                                null, HttpMethod.DELETE, response -> LoginManager.getInstance().logOut()).executeAsync();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                FirebaseAuth.getInstance().signOut();
                Map<String,String> params=new HashMap<>();
                params.put("customer_id",sharedPreferences.getString("customer_id",""));
                params.put("device_type","A");
                Log.e("logoutParam",params.toString());

                new PostMethod(Api.logout,params,context).startPostMethod(new ResponseData() {
                    @Override
                    public void response(String data) {
                        Log.e("logoutResp",data);
                        sharedPreferences.edit().remove("AccessToken").apply();
                        sharedPreferences.edit().remove("customer_id").apply();
                        sharedPreferences.edit().remove("profilePic").apply();
                        sharedPreferences.edit().remove("email").apply();
                        sharedPreferences.edit().remove("social_image").apply();
                        sharedPreferences.edit().putString("socialLogin","-1").apply();
                        Intent intent=new Intent(context,MainActivity.class);
                        TaskStackBuilder.create(MyAcoountActivity.this).addNextIntentWithParentStack
                                (intent).startActivities();
                        finish();
                    }

                    @Override
                    public void error(VolleyError error) {
                        error.printStackTrace();
                    }
                });*/

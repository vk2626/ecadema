package com.ecadema.app;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Size;
import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.stripe.android.EphemeralKeyProvider;
import com.stripe.android.EphemeralKeyUpdateListener;
import io.reactivex.disposables.CompositeDisposable;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class MyEphemeralKeyProvider implements EphemeralKeyProvider {

  private final String stripeCustomerId;

  public MyEphemeralKeyProvider(final String stripeCustomerId) {
    this.stripeCustomerId=stripeCustomerId;
  }

  final CompositeDisposable compositeDisposable = new CompositeDisposable();

  @Override
  public void createEphemeralKey(

      @NonNull @Size(min = 4) String apiVersion,
      @NonNull final EphemeralKeyUpdateListener keyUpdateListener) {

    Map<String,String> param = new HashMap<>();
    param.put("stripe_version","2020-08-27");
    param.put("customer",stripeCustomerId);

    Log.e("param",""+param);
    new PostMethod(Api.StripePayment,param,getApplicationContext()).startPostMethod(
        new ResponseData() {
          @Override
          public void response(final String data) {
            try {
              JSONObject jsonObject = new JSONObject(data);
              if (jsonObject.optBoolean("status")){
                JSONObject dataObj = jsonObject.optJSONObject("data");
                keyUpdateListener.onKeyUpdate(data);
              }
            }catch (Exception w){
              w.printStackTrace();
            }
            Log.e("StripeCustID",data);

          }

          @Override
          public void error(final VolleyError error) {
            error.printStackTrace();
          }
        });


    // Using RxJava2 for handling asynchronous responses
  }
}
package com.ecadema.app;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.VolleyError;
import com.ecadema.EmojiFilter1;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    TextView signUp,login,forgotPass;
    EditText password,email,EnterEmail;
    Context context;
    SharedPreferences sharedPreferences;
    ImageView close;
    ProgressDialog progressDialog;
    Dialog dialog;
    LinearLayout emailLayout,confirmationLayout,fbLogin,gmailLogin;
    TextView title,sendMail,ok,textMessage;
    CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    String socialName,socialEmail="",socialPhone,socialID,socialProfilePic,deviceToken,socialProvider;
    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_activity);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        context = this;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        FacebookSdk.sdkInitialize(context);
        callbackManager = CallbackManager.Factory.create();
        mAuth = FirebaseAuth.getInstance();

        progressDialog=new ProgressDialog(context);
        gmailLogin = findViewById(R.id.gmailLogin);
        forgotPass = findViewById(R.id.forgotPass);
        fbLogin = findViewById(R.id.fbLogin);
        close = findViewById(R.id.close);
        signUp = findViewById(R.id.signUp);
        login = findViewById(R.id.login);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        email.setFilters(EmojiFilter1.getFilter());
        password.setFilters(EmojiFilter1.getFilter());
        final String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z]+(\\.[A-Za-z]+)*(\\.[A-Za-z]{2,})$";

        close.setOnClickListener(v -> finish());
        signUp.setOnClickListener(v -> {
            Intent intent = new Intent(context, SignUp.class);
            startActivity(intent);
        });

        login.setOnClickListener(v -> {
            if (!email.getText().toString().matches(emailPattern)) {
                email.setError(getResources().getString(R.string.entervalidemail));
            } else if (email.getText().toString().trim().matches("")) {
                email.setError(getResources().getString(R.string.fieldRequired));
            } else if (password.getText().toString().trim().matches("")) {
                password.setError(getResources().getString(R.string.fieldRequired));
            } else {
                senddata();
            }
        });
        fbLogin.setOnClickListener(view -> facebookLoginInit());
        forgotPass.setOnClickListener(view1 -> {
            AlertDialog.Builder builder=new AlertDialog.Builder(context);

            LayoutInflater inflater = getLayoutInflater();
            @SuppressLint("InflateParams") final View dialogView =
                    inflater.inflate(R.layout.email_layout, null);
            builder.setView(dialogView);
            dialog=builder.create();

            confirmationLayout=dialogView.findViewById(R.id.confirmationLayout);
            emailLayout=dialogView.findViewById(R.id.emailLayout);
            textMessage=dialogView.findViewById(R.id.textMessage);

            title=dialogView.findViewById(R.id.title);
            sendMail=dialogView.findViewById(R.id.sendMail);
            EditText email=dialogView.findViewById(R.id.email);
            email.setFilters(EmojiFilter1.getFilter());
            sendMail.setOnClickListener(view -> {
                if(email.getText().toString().trim().matches("")) {
                    email.setError(getResources().getString(R.string.enter_email_id));
                    return;
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
                    email.setError(getResources().getString(R.string.entervalidemail));
                    return;

                }
                forgotPasswordMethod(email.getText().toString());
            });
            ok=dialogView.findViewById(R.id.ok);
            ok.setOnClickListener(view -> dialog.dismiss());
            title.setOnClickListener(view -> dialog.dismiss());
            dialog.show();
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        gmailLogin.setOnClickListener(view -> {
            progressDialog.setTitle(context.getResources().getString(R.string.processing));
            progressDialog.show();
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, 100);
        });

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w("TAG", "Fetching FCM registration token failed", task.getException());
                        return;
                    }
                    deviceToken = task.getResult();
                    sharedPreferences.edit().putString("deviceToken",task.getResult()).apply();
                });

        /*FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this,
                instanceIdResult -> {
                    String newToken = instanceIdResult.getToken();
                    Log.e("newToken", newToken);
                    deviceToken = newToken;
                    sharedPreferences.edit().putString("deviceToken",newToken).apply();
                });*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode!=RESULT_OK){
            progressDialog.cancel();
            //Toast.makeText(context, "There is something went wrong. Please try again later.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (requestCode == 100) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
            return;
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> result){
        try {
            GoogleSignInAccount account = result.getResult(ApiException.class);
            socialName=account.getDisplayName();
            socialEmail=""+account.getEmail();
            socialID=account.getId();
            socialPhone="";
            socialProfilePic=""+account.getPhotoUrl();
            socialProvider="Google";
            CheckSocialSignUp();
            Log.e("socialProfilePic",""+socialProfilePic);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("TAG", "signInResult:failed code=" + e.getStatusCode());
        }

    }

    private void facebookLoginInit() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }
        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email","public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i("MainActivity", "@@@onSuccess:"+loginResult.getAccessToken().getToken());
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        (object, response) -> {
                            try {
                                handleFacebookAccessToken(loginResult.getAccessToken());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,phone");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i("MainActivity", "@@@onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("MainActivity", "@@@onError: "+ error.getMessage());
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        });

    }

    private void handleFacebookAccessToken(AccessToken token) {
        //Log.d("TAG", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("TAG", "signInWithCredential:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        socialName=user.getDisplayName();
                        socialEmail=""+user.getEmail();
                        socialPhone=user.getPhoneNumber();
                        socialID=user.getUid();
                        socialProfilePic= String.valueOf(user.getPhotoUrl());
                        socialProvider="Facebook";
                        Log.e("socialProfilePic",""+socialProfilePic);

                        CheckSocialSignUp();

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("TAG", "signInWithCredential:failure", task.getException());
                        Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                    }

                });
    }

    private void CheckSocialSignUp() {
       //

        Map<String, String> param=new HashMap<>();
        param.put("name",socialName);
        param.put("device_token",deviceToken);
        param.put("device_type","A");
        param.put("oauth_uid",socialID);
        param.put("oauth_provider",socialProvider);
        param.put("email",socialEmail);
        param.put("device_id",sharedPreferences.getString("device_id",""));
        Log.e("socialSignParam",""+ param);

        new PostMethod(Api.SocialLogin,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();
                Log.e("socialSignResponse",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if (jsonObject.optBoolean("status")) {
                        Toast.makeText(context, jsonObject.optJSONObject("message").optString("success"), Toast.LENGTH_SHORT).show();
                        JSONObject dataObject=jsonObject.getJSONObject("data");
                        sharedPreferences.edit().putString("userID",dataObject.optString("id")).apply();
                        sharedPreferences.edit().putString("firstName",dataObject.optString("first_name")).apply();
                        sharedPreferences.edit().putString("lastName",dataObject.optString("last_name")).apply();
                        sharedPreferences.edit().putString("email",dataObject.optString("email")).apply();
                        sharedPreferences.edit().putString("contact_no",dataObject.optString("contact_no")).apply();
                        sharedPreferences.edit().putString("country",dataObject.optString("country")).apply();
                        sharedPreferences.edit().putString("profile_image",dataObject.optString("profile_image")).apply();

                        if(dataObject.optString("user_types").equalsIgnoreCase("c"))
                            sharedPreferences.edit().putString("user_type","2").apply();

                        else if(dataObject.optString("user_types").equalsIgnoreCase("t"))
                            sharedPreferences.edit().putString("user_type","3").apply();
                        else
                            sharedPreferences.edit().putString("user_type","1").apply();
                        finish();
                    }
                    else {
                        //progressDialog.cancel();;
                        JSONObject jsonObject1=jsonObject.getJSONObject("message");
                        if (jsonObject1.has("email")) {
                            {
                                if(null==socialEmail || socialEmail.equalsIgnoreCase("null")) {
                                    AlertDialog.Builder builder=new AlertDialog.Builder(context);

                                    LayoutInflater inflater = getLayoutInflater();
                                    @SuppressLint("InflateParams") final View dialogView =
                                            inflater.inflate(R.layout.email_dialog, null);
                                    builder.setView(dialogView);
                                    dialog=builder.create();

                                    confirmationLayout=dialogView.findViewById(R.id.confirmationLayout);
                                    emailLayout=dialogView.findViewById(R.id.emailLayout);
                                    textMessage=dialogView.findViewById(R.id.textMessage);

                                    title=dialogView.findViewById(R.id.title);
                                    sendMail=dialogView.findViewById(R.id.sendMail);
                                    EnterEmail=dialogView.findViewById(R.id.email);
                                    sendMail.setOnClickListener(view -> {
                                        if(EnterEmail.getText().toString().trim().matches("")) {
                                            EnterEmail.setError(getResources().getString(R.string.enter_email_id));
                                            return;
                                        }
                                        if (!Patterns.EMAIL_ADDRESS.matcher(EnterEmail.getText().toString()).matches()) {
                                            EnterEmail.setError(getResources().getString(R.string.entervalidemail));
                                            return;
                                        }
                                        socialEmail=EnterEmail.getText().toString();
                                        progressDialog.setTitle(context.getResources().getString(R.string.processing));
                                        progressDialog.show();
                                        CheckSocialSignUp();
                                    });
                                    ok=dialogView.findViewById(R.id.ok);
                                    ok.setOnClickListener(view -> dialog.dismiss());
                                    title.setOnClickListener(view -> dialog.dismiss());
                                    dialog.show();
                                }else {
                                    try{
                                        mGoogleSignInClient.signOut()
                                                .addOnCompleteListener(LoginActivity.this, task -> {
                                                    // ...
                                                });

                                        mGoogleSignInClient.revokeAccess()
                                                .addOnCompleteListener(LoginActivity.this, task -> {
                                                    // ...
                                                });
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    Toast.makeText(context, jsonObject1.optString("email"), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    }
                } catch (Exception e) {
                    progressDialog.cancel();
                    e.printStackTrace();

                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();
                error.printStackTrace();
            }
        });
    }

    private void forgotPasswordMethod(String email) {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("email",email);

        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        new PostMethod(Api.ForgetPassword,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        emailLayout.setVisibility(View.GONE);
                        confirmationLayout.setVisibility(View.VISIBLE);
                        textMessage.setText(jsonObject.optString("message"));
                    }else {
                        dialog.dismiss();
                        Toast.makeText(LoginActivity.this, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();
            }
        });

    }

    private void senddata() {

        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();

        Map<String, String> param=new HashMap<>();
        param.put("email",email.getText().toString());
        param.put("password",password.getText().toString());
        param.put("device_token",deviceToken);
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("device_type","A");
        param.put("device_id",sharedPreferences.getString("device_id",""));

        Log.e("loginparam",""+ param);

        new PostMethod(Api.Login,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();
                Log.e("loginresponse",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        JSONObject dataObject=jsonObject.getJSONObject("data");
                        sharedPreferences.edit().putString("userID",dataObject.optString("id")).apply();
                        sharedPreferences.edit().putString("firstName",dataObject.optString("first_name")).apply();
                        sharedPreferences.edit().putString("lastName",dataObject.optString("last_name")).apply();
                        sharedPreferences.edit().putString("email",dataObject.optString("email")).apply();
                        sharedPreferences.edit().putString("contact_no",dataObject.optString("contact_no")).apply();
                        sharedPreferences.edit().putString("country",dataObject.optString("country")).apply();
                        sharedPreferences.edit().putString("profile_image",dataObject.optString("profile_image")).apply();

                        if(dataObject.optString("user_types").equalsIgnoreCase("c"))
                            sharedPreferences.edit().putString("user_type","2").apply();

                        else if(dataObject.optString("user_types").equalsIgnoreCase("t"))
                            sharedPreferences.edit().putString("user_type","3").apply();
                        else
                            sharedPreferences.edit().putString("user_type","1").apply();

                        finish();
                    }
                    else {
                        Snackbar.make(login,jsonObject.optString("message"),Snackbar.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();
                error.printStackTrace();
            }
        });
    }
}

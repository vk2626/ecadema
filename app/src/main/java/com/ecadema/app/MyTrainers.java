package com.ecadema.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.fragments.ViewProfile;
import com.ecadema.modal.TrainerModal;

import java.util.ArrayList;

public class MyTrainers extends AppCompatActivity {

    Context context;
    SharedPreferences sharedPreferences;
    RecyclerView recyclerView;
    ArrayList<TrainerModal> trainerModalArrayList=new ArrayList<>();
    MyTrainerAdapter myTrainerAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_trainer);
        context=this;
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        trainerModalArrayList.clear();
        for (int ar=0;ar<5;ar++){
            TrainerModal trainerModal=new TrainerModal();
            trainerModal.setID(""+ar);
            trainerModal.setImage("");
            trainerModal.setName("Name "+(ar+1));
            trainerModal.setRatings((ar+1));

            trainerModalArrayList.add(trainerModal);
        }
        myTrainerAdapter=new MyTrainerAdapter(context,trainerModalArrayList);
        recyclerView.setAdapter(myTrainerAdapter);
    }

    private class MyTrainerAdapter extends RecyclerView.Adapter<MyTrainerAdapter.RowHolder>{
        private final Context context;
        private final ArrayList<TrainerModal> trainerModalArrayList;

        public MyTrainerAdapter(Context context, ArrayList<TrainerModal> trainerModalArrayList) {
            this.context=context;
            this.trainerModalArrayList=trainerModalArrayList;
        }

        @NonNull
        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(context).inflate(R.layout.my_trainer_adapter,null);
            return new RowHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RowHolder holder, int position) {
            TrainerModal trainerModal=trainerModalArrayList.get(position);
            holder.name.setText(trainerModal.getName());
            holder.rating.setRating(trainerModal.getRatings());

            holder.viewProfile.setOnClickListener(v->{
                Bundle bundle=new Bundle();
                bundle.putString("trainerID",trainerModal.getTeacherID());
                ViewProfile viewProfile=new ViewProfile();
                viewProfile.setArguments(bundle);
                ((MainActivity) context).addFragment2(viewProfile, true, false, 0);
            });
        }

        @Override
        public int getItemCount() {
            return trainerModalArrayList.size();
        }

        public class RowHolder extends RecyclerView.ViewHolder {
            TextView name,rate,viewProfile;
            RatingBar rating;

            public RowHolder(@NonNull View itemView) {
                super(itemView);
                name=itemView.findViewById(R.id.name);
                rating=itemView.findViewById(R.id.ratings);
                viewProfile=itemView.findViewById(R.id.viewProfile);
                rate=itemView.findViewById(R.id.rate);
            }
        }
    }
}

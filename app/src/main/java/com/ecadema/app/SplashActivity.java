package com.ecadema.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.core.app.TaskStackBuilder;

import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Locale;

import pl.droidsonroids.gif.GifImageView;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT =3200;
    GifImageView im;
    SharedPreferences sharedPreferences;
    Handler splashHandler;

    CardView card1,card2,card3,card4,card5,card6,card7;
    ImageView itsTime;
    int count = 0;
    private String language;
    SharedPreferences.Editor editor;
    android.app.AlertDialog b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Log.e("redirect_URL",String.format("%s://paypalpay", BuildConfig.APPLICATION_ID));
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
        editor=sharedPreferences.edit();
        language=sharedPreferences.getString("lang","");
        if(!language.equalsIgnoreCase("ar")&&!language.equalsIgnoreCase("en")){

            android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.select_language, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            LinearLayout en=dialogView.findViewById(R.id.en);
            LinearLayout ru=dialogView.findViewById(R.id.ru);


            en.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    b.dismiss();
                    language="en";
                    setAppLocale(language);
                    editor.putString("lang","en").apply();
                    Intent intent=new Intent(SplashActivity.this,SplashActivity.class);
                    TaskStackBuilder.create(SplashActivity.this).addNextIntentWithParentStack
                            (intent).startActivities();
                    finish();
                }
            });
            ru.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editor.putString("lang","ar").apply();
                    language="ar";
                    setAppLocale(language);
                    Intent intent=new Intent(SplashActivity.this,SplashActivity.class);
                    TaskStackBuilder.create(SplashActivity.this).addNextIntentWithParentStack
                            (intent).startActivities();
                    finish();
                }
            });

            b = dialogBuilder.create();
            b.show();
        }
        else {
            setAppLocale(language);
            moveNext();
        }
    }

    private void moveNext() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        setContentView(R.layout.activity_splash);

        card1 = findViewById(R.id.card1);
        card2 = findViewById(R.id.card2);
        card3 = findViewById(R.id.card3);
        card4 = findViewById(R.id.card4);
        card5 = findViewById(R.id.card5);
        card6 = findViewById(R.id.card6);
        card7 = findViewById(R.id.card7);
        itsTime = findViewById(R.id.itsTime);

        /*sharedPreferences.edit().remove("user_type").apply();
        sharedPreferences.edit().remove("userID").apply();*/
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SplashActivity.this,
                instanceIdResult -> {
                    String newToken = instanceIdResult.getToken();
                    Log.e("deviceToken",newToken);
                    sharedPreferences.edit().putString("deviceToken",newToken).apply();
                });

        new CountDownTimer(9000, 500){
            @Override
            public void onTick(long millisUntilFinished) {
                Log.e("milisecond",""+millisUntilFinished);
                if (count==1)
                    card1.setVisibility(View.VISIBLE);
                if (count==2)
                    card2.setVisibility(View.VISIBLE);
                if (count==3)
                    card3.setVisibility(View.VISIBLE);
                if (count==4)
                    card4.setVisibility(View.VISIBLE);
                if (count==5)
                    card5.setVisibility(View.VISIBLE);
                if (count==6)
                    card6.setVisibility(View.VISIBLE);
                if (count==7) {
                    card7.setVisibility(View.VISIBLE);
                    itsTime.setVisibility(View.VISIBLE);
                    itsTime.startAnimation(AnimationUtils.loadAnimation(SplashActivity.this, R.anim.fade_in));
                    splashHandler=new Handler();
                    splashHandler.postDelayed(() -> {
                        if(!sharedPreferences.getString("userID","").isEmpty() ||
                                !sharedPreferences.getString("userID","").equalsIgnoreCase("")){
                            Intent intent=new Intent(SplashActivity.this,MainActivity.class);
                            startActivity(intent);
                        }
                        else
                        {
                            sharedPreferences.edit().putString("userID","-1").apply();
                            Intent intent=new Intent(SplashActivity.this,MainActivity.class);
                            TaskStackBuilder.create(SplashActivity.this).addNextIntentWithParentStack
                                    (intent).startActivities();
                        }
                        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
                        finish();

                        }, 900);
                }

                count++;
            }

            @Override
            public void onFinish() {
                card7.setVisibility(View.VISIBLE);

            }
        }.start();

    }

    private void setAppLocale(String LocaleCode) {
        Resources res=getResources();
        DisplayMetrics dr=res.getDisplayMetrics();
        Configuration configuration=res.getConfiguration();
        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
            configuration.setLocale(new Locale(LocaleCode.toLowerCase()));
        }
        else
        {
            configuration.locale=new Locale(LocaleCode.toLowerCase());
        }
        res.updateConfiguration(configuration,dr);
    }

}
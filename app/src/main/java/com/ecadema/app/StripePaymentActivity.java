package com.ecadema.app;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.adapter.SavedCardsAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.modal.SavedCardModal;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.CardBrand;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class StripePaymentActivity extends Fragment {

  private String paymentIntentClientSecret;
  private Stripe stripe;
  public CardInputWidget cardInputWidget;
  TextView title, amount;
  String stripeServerKey;
  SharedPreferences sharedPreferences;
  String card_number;
  String cvc;
  CardBrand brand;
  String lastFour;
  int exp_month,exp_year;
  ImageView close;
  RecyclerView recyclerView;
  ArrayList<SavedCardModal>savedCardModalArrayList = new ArrayList<>();
  ProgressDialog progressDialog;
  Context context;
  View root;

  @Nullable
  @Override
  public View onCreateView(@NonNull final LayoutInflater inflater,
      @Nullable final ViewGroup container,
      @Nullable final Bundle savedInstanceState) {
    root = inflater.inflate(R.layout.stripe_payment_activity, container, false);
    context=getActivity();
    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

    progressDialog = new ProgressDialog(context);

    close = root.findViewById(R.id.close);
    title = root.findViewById(R.id.title);
    amount = root.findViewById(R.id.amount);
    recyclerView = root.findViewById(R.id.recyclerView);
    cardInputWidget = root.findViewById(R.id.cardInputWidget);
    cardInputWidget.setPostalCodeEnabled(false);

    getAllSaveCards();
    close.setOnClickListener(v->{
      getParentFragmentManager().popBackStack();
    });
    //stripeServerKey = Api.testStripeKey; /** Testing Sever **/
    stripeServerKey = Api. liveStripeKey; /** Live Sever **/

    amount.setText(getResources().getString(R.string.amount)+" : $ "+getArguments().getString("amount"));
    startCheckout();
    return root;
  }

  private void getAllSaveCards() {
    Map<String,String> param = new HashMap<>();
    param.put("lang",sharedPreferences.getString("lang",""));
    param.put("user_id",sharedPreferences.getString("userID",""));

    Log.e("allCards",""+param);
    new PostMethod(Api.GetUserCardInfo,param,context).startPostMethod(
        new ResponseData() {
          @Override
          public void response(final String data) {
            Log.e("saveInfo",data);
            try {
              savedCardModalArrayList.clear();
              JSONObject jsonObject = new JSONObject(data);
              if (jsonObject.optBoolean("status")){
                JSONArray array = jsonObject.optJSONArray("data");
                for (int ar=0 ; ar<array.length(); ar++){
                  SavedCardModal savedCardModal = new SavedCardModal();
                  JSONObject object = array.getJSONObject(ar);
                  savedCardModal.setLastFour(object.optString("card_number").substring(object.optString("card_number").length() - 4));
                  savedCardModal.setCardNumber(object.optString("card_number"));
                  savedCardModal.setMonth(object.optString("month"));
                  savedCardModal.setYear(object.optString("year"));
                  savedCardModal.setCVC(object.optString("cvc"));
                  savedCardModal.setBrand(object.optString("brand"));
                  savedCardModalArrayList.add(savedCardModal);
                }
                SavedCardsAdapter savedCardsAdapter = new SavedCardsAdapter(StripePaymentActivity.this, savedCardModalArrayList,context);
                recyclerView.setAdapter(savedCardsAdapter);
              }
            }catch (Exception e){
              e.printStackTrace();
            }
          }

          @Override
          public void error(final VolleyError error) {
            error.printStackTrace();
          }
        });
  }

  private void startCheckout() {
    double temp = (Double.parseDouble(getArguments().getString("amount"))*100);

    Map<String,String> param = new HashMap<>();
    param.put("currency","usd");
    param.put("amount",""+Math.round(temp));

    new PostMethod(Api.StripeApiKey,param,context).startPostMethod(
        new ResponseData() {
          @Override
          public void response(final String data) {
            Log.e("StrpeData",data);
            try {
              JSONObject jsonObject = new JSONObject(data);
              paymentIntentClientSecret = jsonObject.optString("data");

              stripe = new Stripe(
                  context,
                  Objects.requireNonNull(stripeServerKey));

            }catch (Exception e){
              e.printStackTrace();
            }
          }

          @Override
          public void error(final VolleyError error) {
            progressDialog.dismiss();
            Log.e("Error",error.toString());
            Toast.makeText(
                context, "Error: " + error.toString(), Toast.LENGTH_LONG
            ).show();

          }
        });
    Button payButton = root.findViewById(R.id.payButton);

    payButton.setOnClickListener((View view) -> {

      cardInputWidget = root.findViewById(R.id.cardInputWidget);
      PaymentMethodCreateParams params = cardInputWidget.getPaymentMethodCreateParams();
      if (params != null) {
        progressDialog.setTitle(getResources().getString(R.string.processing));
        progressDialog.setMessage(getResources().getString(R.string.processing));
        progressDialog.setCancelable(false);
        progressDialog.show();

        card_number = cardInputWidget.getCard().getNumber();
        exp_month = cardInputWidget.getCard().getExpMonth();
        exp_year = cardInputWidget.getCard().getExpYear();
        cvc = cardInputWidget.getCard().getCvc();
        brand = cardInputWidget.getCard().getBrand();
        lastFour = cardInputWidget.getCard().getLast4();

        ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams
            .createWithPaymentMethodCreateParams(params, paymentIntentClientSecret);
        stripe.confirmPayment(this, confirmParams);
      }
    });
  }

  private void saveCardDetails() {
    Map<String,String> param = new HashMap<>();
    param.put("card_number",card_number);
    param.put("brand",""+brand);
    param.put("lastFour",lastFour);
    param.put("cvc",cvc);
    param.put("exp_month",""+exp_month);
    param.put("exp_year",""+exp_year);
    param.put("user_id",sharedPreferences.getString("userID",""));

    Log.e("saveCard",""+param);
    new PostMethod(Api.GetStripeCardInfo,param,context).startPostMethod(
        new ResponseData() {
          @Override
          public void response(final String data) {
            Log.e("saveInfo",data);
          }

          @Override
          public void error(final VolleyError error) {
            error.printStackTrace();
          }
        });
  }

  private void displayAlert(@NonNull String title,
      @Nullable String message, boolean restartDemo) {
    if (!restartDemo) {
      progressDialog.dismiss();
      AlertDialog.Builder builder = new AlertDialog.Builder(context)
          .setIcon(R.mipmap.app_icon)
          .setCancelable(false)
          .setMessage(title);
      builder.setPositiveButton(getResources().getString(R.string.retry),
          (DialogInterface dialog, int index) -> {
            cardInputWidget.clear();
            startCheckout();
          });
      builder.create().show();
    } else {
      if (getArguments().getString("from").equalsIgnoreCase("credit")){
        updatePayment(Api.WalletCheckOut);
      }else {
        if(getArguments().getString("sessionType").equalsIgnoreCase(getResources().getString(R.string.workshop))
            || getArguments().getString("sessionType").equalsIgnoreCase(getResources().getString(R.string.Course)))
          updatePayment(Api.CheckOutWorkshop);
        else
          updatePayment(Api.OneToOneCheckOut);
      }
    }
  }

  private void updatePayment(String api) {
    /*ProgressDialog progressDialog = new ProgressDialog(context);
    progressDialog.setCancelable(false);
    progressDialog.setMessage(getResources().getString(R.string.processing));
    progressDialog.show();*/

    Map<String,String> param = new HashMap<>();
    param.put("lang_token",sharedPreferences.getString("lang",""));
    param.put("trans_id",getArguments().getString("trans_id"));
    param.put("transaction_info","Payment Done");
    if (getArguments().getString("from").equalsIgnoreCase("booking")){
      param.put("user_id",sharedPreferences.getString("userID",""));
      param.put("timezone",getArguments().getString("timezone"));
      if(api.contains("OneToOneCheckOut")){
        param.put("teacher_id",getArguments().getString("trainerID"));
      }
      param.put("pay_by_wallet",getArguments().getString("pay_by_wallet"));

      param.put("amount",getArguments().getString("totalAmount"));
      param.put("amount_modify",getArguments().getString("amount_modify"));


    }

    Log.e("updatePaymentParam",""+param);
    new PostMethod(api,param,context).startPostMethod(new ResponseData() {
      @Override
      public void response(String data) {
        progressDialog.cancel();;
        Log.e("updatePayment",data);
        try {
          JSONObject jsonObject = new JSONObject(data);
          if(jsonObject.optBoolean("status")){
            Intent intent = new Intent("performAction");
            if (getArguments().getString("from").equalsIgnoreCase("booking")){
              Toast.makeText(context, getResources().getString(R.string.bookingSuccesfully), Toast.LENGTH_SHORT).show();
              intent.putExtra("to","navigationClass");
            }
            else {
              intent.putExtra("to","popUp");
              Toast.makeText(context, getResources().getString(R.string.creditRefill), Toast.LENGTH_SHORT).show();
            }
            context.sendBroadcast(intent);
            getParentFragmentManager().popBackStackImmediate();
          }else
            Toast.makeText(context,jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
        }catch (Exception e){
          e.printStackTrace();
        }
      }

      @Override
      public void error(VolleyError error) {
        progressDialog.cancel();;
        error.printStackTrace();
      }
    });
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    // Handle the result of stripe.confirmPayment
    stripe.onPaymentResult(requestCode, data, new PaymentResultCallback());
  }

  private final class PaymentResultCallback implements ApiResultCallback<PaymentIntentResult> {

    @Override
    public void onSuccess(@NonNull PaymentIntentResult result) {
      if (getApplicationContext() == null) {
        return;
      }
      PaymentIntent paymentIntent = result.getIntent();
      PaymentIntent.Status status = paymentIntent.getStatus();
      if (status == PaymentIntent.Status.Succeeded) {
        // Payment completed successfully
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String response = gson.toJson(paymentIntent);
        Log.e("Response",response);
        progressDialog.dismiss();
        try {
          saveCardDetails();
          //progressDialog.showProgress(context.getResources().getString(R.string.processing));
          if (getArguments().getString("from").equalsIgnoreCase("credit")){
            updatePayment(Api.WalletCheckOut);
          }else {
            if(getArguments().getString("sessionType").equalsIgnoreCase(context.getResources().getString(R.string.workshop))
                || getArguments().getString("sessionType").equalsIgnoreCase(context.getResources().getString(R.string.Course)))
              updatePayment(Api.CheckOutWorkshop);
            else
              updatePayment(Api.OneToOneCheckOut);
          }

        }catch (Exception e){
          progressDialog.dismiss();
          e.printStackTrace();
        }

      } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
        // Payment failed – allow retrying using a different payment method
        progressDialog.dismiss();
        Objects.requireNonNull(paymentIntent.getLastPaymentError()).getMessage();
        Toast.makeText(getApplicationContext(), Objects.requireNonNull(paymentIntent.getLastPaymentError()).getMessage(),
            Toast.LENGTH_SHORT).show();
      }
    }

    @Override
    public void onError(@NonNull Exception e) {
      if (getApplicationContext() == null) {
        return;
      }
      progressDialog.dismiss();
      Log.e("Error",e.toString());
      Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
      // Payment request failed – allow retrying using the same payment method
    }
  }
}

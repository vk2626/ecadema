package com.ecadema.app;

import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

public class ViewImages extends AppCompatActivity {

    ImageView imageView,close;
    TextView header;
    WebView webView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_image);

        header=findViewById(R.id.header);
        close=findViewById(R.id.close);
        imageView=findViewById(R.id.imageView);
        webView=findViewById(R.id.webView);

        header.setText(getIntent().getStringExtra("title"));
        Glide.with(this).load(getIntent().getStringExtra("image")).into(imageView);

        close.setOnClickListener(v->finish());
        Log.e("Image",getIntent().getStringExtra("image"));

        //webView.loadUrl(getIntent().getStringExtra("image"));
        String doc="<iframe src='http://docs.google.com/viewer?    url="+getIntent().getStringExtra("image")+"&embedded=true'"+
                " width='100%' height='100%' style='border: none;'></iframe>";
        //webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(true);
        //wv.loadUrl(doc);
        webView.loadData( doc , "text/html",  "UTF-8");
    }

}

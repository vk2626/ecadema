package com.ecadema.app;

import static android.content.ContentValues.TAG;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.provider.FontRequest;
import androidx.emoji.text.EmojiCompat;
import androidx.emoji.text.FontRequestEmojiCompatConfig;
import androidx.emoji.widget.EmojiEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.ecadema.adapter.ChatAdapter;
import com.ecadema.adapter.CoursePriceAdapter;
import com.ecadema.adapter.SessionAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.modal.ChatRoomModal;
import com.ecadema.modal.CoursePriceModal;
import com.ecadema.modal.SessionModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatRoom extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    Context context;

    ArrayList<ChatRoomModal> chatRoomModalArrayList=new ArrayList<>();
    ChatAdapter chatAdapter;
    RecyclerView recyclerView;
    CardView send,bookCard;
    EmojiEditText message;
    String chatID, userName,receiverID,imageURL;
    String courseID="",ticketPrice="";

    CardView submit;
    TextView header,book,notActive,block,noChat;
    Calendar cal;
    CircleImageView agentPic;
    ImageView back,report;
    ArrayList<SessionModal> sessionModalArrayList=new ArrayList<>();
    ArrayList<CoursePriceModal> coursePriceModalArrayList=new ArrayList<>();
    RecyclerView allSessions,priceRecyclerView;
    Dialog dialog;
    JSONArray jsonArray;
    String userType = "student";
    LinearLayout sendMessageLL,mainLayout;
    String reportType ="Report", checkUserType="";

    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat format5 = new SimpleDateFormat("EEE, dd MMM, yyyy");
    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FontRequest fontRequest = new FontRequest("com.example.fontprovider",
                "com.example.fontprovider", "my font", R.array.com_google_android_gms_fonts_certs);
        EmojiCompat.Config config = new FontRequestEmojiCompatConfig(this, fontRequest);
        EmojiCompat.init(new FontRequestEmojiCompatConfig(getApplicationContext(), fontRequest)
                .setReplaceAll(true)
                .registerInitCallback(new EmojiCompat.InitCallback() {
                    @Override
                    public void onInitialized() {
                        Log.i(TAG, "EmojiCompat initialized");
                    }

                    @Override
                    public void onFailed(@Nullable Throwable throwable) {
                        Log.e(TAG, "EmojiCompat initialization failed", throwable);
                    }
                }));

        setContentView(R.layout.chat_room);
        context=this;
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        noChat=findViewById(R.id.noChat);
        submit=findViewById(R.id.submit);
        mainLayout=findViewById(R.id.mainLayout);
        block=findViewById(R.id.block);
        sendMessageLL=findViewById(R.id.sendMessageLL);
        notActive=findViewById(R.id.notActive);
        book=findViewById(R.id.book);
        bookCard=findViewById(R.id.bookCard);
        report=findViewById(R.id.report);
        back=findViewById(R.id.back);
        header=findViewById(R.id.header);
        agentPic=findViewById(R.id.agentPic);
        recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        message=findViewById(R.id.message);
        cal = Calendar.getInstance();

        try {
            userType = getIntent().getStringExtra("type");
        }catch (Exception e){
            e.printStackTrace();
        }
        book.setVisibility(View.VISIBLE);

        try {
            //chatID=getIntent().getStringExtra("chatID");
            userName=getIntent().getStringExtra("userName");
            receiverID=getIntent().getStringExtra("receiverID");
            if(sharedPreferences.getString("user_type","").equalsIgnoreCase("3") &&
                    sharedPreferences.getString("userID","").equalsIgnoreCase(receiverID))
                book.setVisibility(View.GONE);

            Glide.with(context).load(getIntent().getStringExtra("userImage")).into(agentPic);
            //Log.e("profile",getIntent().getStringExtra("userImage"));
            header.setText(userName);

            try {
                jsonArray = new JSONArray(getIntent().getStringExtra("courses"));
                if (jsonArray.length()==0)
                    book.setVisibility(View.GONE);

                for (int i=0;i<jsonArray.length();i++){
                    JSONObject Object=jsonArray.getJSONObject(i);
                    SessionModal sessionModal=new SessionModal();
                    sessionModal.setCourseId(Object.optString("id"));
                    sessionModal.setSubId(Object.optString("subject_id"));
                    sessionModal.setSubject(Object.optString("subject"));
                    sessionModal.setCoursePrices(""+Object.optJSONArray("course_price"));
                    sessionModalArrayList.add(sessionModal);

                    if(i==0){
                        ticketPrice=Object.optJSONArray("course_price").optJSONObject(0).optString("price_per_hour");
                        Intent intent=new Intent("courseSelected");
                        intent.putExtra("coursePrice", ""+Object.optJSONArray("course_price"));
                        intent.putExtra("courseID", Object.optString("id"));
                        context.sendBroadcast(intent);
                    }
                }

                courseDialog();

                book.setOnClickListener(v->{
                    try {
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject Object=jsonArray.getJSONObject(i);
                            if(i==0){
                                ticketPrice=Object.optJSONArray("course_price").optJSONObject(0).optString("price_per_hour");
                                Intent intent=new Intent("courseSelected");
                                intent.putExtra("coursePrice", ""+Object.optJSONArray("course_price"));
                                intent.putExtra("courseID", Object.optString("id"));
                                context.sendBroadcast(intent);
                            }
                        }

                    }catch (Exception w){
                        w.printStackTrace();
                    }

                    dialog.show();
                });
            }catch (Exception e){
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        send=findViewById(R.id.send);
        back=findViewById(R.id.back);

        back.setOnClickListener(v->{
            finish();
        });
        send.setOnClickListener(v -> {
            if(!message.getText().toString().trim().matches("")){
                try {
                    String mystring = URLEncoder.encode(message.getText().toString(),"UTF-8");
                    sendMessage(mystring);

                } catch (UnsupportedEncodingException e) {
                    sendMessage(message.getText().toString());
                    e.printStackTrace();
                }
                noChat.setVisibility(View.GONE);

                ChatRoomModal maliciousModel=new ChatRoomModal();
                maliciousModel.setMessage(message.getText().toString());
                maliciousModel.setUserId(sharedPreferences.getString("userID",""));
                maliciousModel.setDate(""+format5.format(Calendar.getInstance().getTime()));
                maliciousModel.setTime(""+timeFormat2.format(Calendar.getInstance().getTime()));
                //sendData(message.getText().toString(),sharedpreferences.getString("userId",""));
                chatRoomModalArrayList.add(maliciousModel);
                chatAdapter=new ChatAdapter(context,chatRoomModalArrayList);
                chatAdapter.notifyItemInserted(chatAdapter.getItemCount() - 1);
                recyclerView.scrollToPosition(chatRoomModalArrayList.size()-1);
                recyclerView.setAdapter(chatAdapter);
            }
            message.setText("");
        });

        report.setOnClickListener(v->{
            /*android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
            builder.setMessage(context.getResources().getString(R.string.reportUser));
            builder.setPositiveButton(context.getResources().getString(R.string.report),
                    (dialogInterface, i) -> Report());
            builder.setNegativeButton(context.getResources().getString(R.string.cancel),
                    (dialogInterface, i) -> dialogInterface.dismiss());

            builder.create().show();*/

            AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.block_report, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            TextView header = dialogView.findViewById(R.id.header);
            RadioGroup radioGroup = dialogView.findViewById(R.id.radioGroup);
            RadioButton report = dialogView.findViewById(R.id.report);
            RadioButton block = dialogView.findViewById(R.id.block);
            RadioButton report_block = dialogView.findViewById(R.id.report_block);
            EditText reason = dialogView.findViewById(R.id.reason);
            ImageView close = dialogView.findViewById(R.id.close);
            CardView cancel = dialogView.findViewById(R.id.cancel);
            TextView submit = dialogView.findViewById(R.id.submit);

            radioGroup.setOnCheckedChangeListener((radioGroup1, checkedId) -> {
                if (checkedId == R.id.report) {
                   header.setText(getResources().getString(R.string.reportText));
                   submit.setText(getResources().getString(R.string.report));
                    reportType ="Report";
                } else if (checkedId == R.id.block) {
                    header.setText(getResources().getString(R.string.blockText));
                    submit.setText(getResources().getString(R.string.block));
                    reportType ="Block";
                }else if (checkedId == R.id.report_block) {
                    header.setText(getResources().getString(R.string.reportBlockText));
                    submit.setText(getResources().getString(R.string.reportBlock));
                    reportType ="Report_Block";
                }
            });

            cancel.setOnClickListener(v1-> dialog.dismiss());
            close.setOnClickListener(v1-> dialog.dismiss());
            submit.setOnClickListener(v1->{
                if (reason.getText().toString().trim().matches("")) {
                    reason.setError(getResources().getString(R.string.fieldRequired));
                    return;
                }
                Report(reason.getText().toString(),dialog);
            });

            dialog.show();
            //;
        });
        getChat();

        try {
            if(!getIntent().getStringExtra("accountActive").equalsIgnoreCase("1")){
                sendMessageLL.setVisibility(View.GONE);
                notActive.setVisibility(View.VISIBLE);
                bookCard.setVisibility(View.GONE);
                book.setVisibility(View.GONE);
                report.setVisibility(View.GONE);
            }else {
                header.setOnClickListener(v->{
                    if(checkUserType.equalsIgnoreCase("t")){
                        Intent intent = new Intent(context,MainActivity.class);
                        intent.putExtra("open","profile");
                        intent.putExtra("trainerID",receiverID);
                        startActivity(intent);
                    }
                });

            }
        }catch (Exception e){
            e.printStackTrace();
            header.setOnClickListener(v->{
                Intent intent = new Intent(context,MainActivity.class);
                intent.putExtra("open","profile");
                intent.putExtra("trainerID",receiverID);
                startActivity(intent);
            });
        }

        submit.setOnClickListener(v->{
            ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(context.getResources().getString(R.string.processing));
            progressDialog.show();
            Map<String,String> param = new HashMap<>();
            param.put("report_id",receiverID);
            param.put("user_id",sharedPreferences.getString("userID",""));
            param.put("lang_token",sharedPreferences.getString("lang",""));

            Log.e("unblockParam",""+param);

            new PostMethod(Api.UnblockUser,param,context).startPostMethod(new ResponseData() {
                @Override
                public void response(String data) {
                    progressDialog.cancel();;
                    Log.e("unblockResp",data);

                    try {
                        JSONObject jsonObject = new JSONObject(data);
                        if(jsonObject.optBoolean("status")){

                            block.setVisibility(View.GONE);
                            submit.setVisibility(View.GONE);
                            sendMessageLL.setVisibility(View.VISIBLE);
                            bookCard.setVisibility(View.VISIBLE);
                            getChat();
                            Toast.makeText(context, jsonObject.optJSONObject("message").optString("success"), Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception w){
                        w.printStackTrace();
                    }
                }

                @Override
                public void error(VolleyError error) {
                    progressDialog.cancel();;
                    error.printStackTrace();
                }
            });
        });
    }

    private void Report(final String reason, final Dialog dialog) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param = new HashMap<>();
        param.put("block_msg",reason);
        param.put("report_type",reportType);
        param.put("report_id",receiverID);
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));

        Log.e("reportParam",""+param);

        new PostMethod(Api.reporttouser,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("reportResp",data);

                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){

                        if (!reportType.equalsIgnoreCase("Report")){
                            block.setVisibility(View.VISIBLE);
                            submit.setVisibility(View.VISIBLE);
                            sendMessageLL.setVisibility(View.GONE);
                            bookCard.setVisibility(View.GONE);
                        }
                        dialog.dismiss();
                        reportType="Report";
                        Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception w){
                    w.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    private void courseDialog() {
        androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
        LayoutInflater inflater1 =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") final View dialogView =
                inflater1.inflate(R.layout.course_dialog, null);
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(false);
        dialog=alertDialog.create();
        allSessions = dialogView.findViewById(R.id.allSessions);
        priceRecyclerView = dialogView.findViewById(R.id.priceRecyclerView);
        TextView bookNow = dialogView.findViewById(R.id.bookNow);
        TextView title = dialogView.findViewById(R.id.title);

        title.setOnClickListener(v2->dialog.dismiss());

        allSessions.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        priceRecyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        allSessions.setAdapter(new SessionAdapter(context,sessionModalArrayList));

        bookNow.setOnClickListener(v->{
            dialog.dismiss();
            Intent intent = new Intent(context,MainActivity.class);
            intent.putExtra("open","booking");
            intent.putExtra("trainerID",receiverID);
            intent.putExtra("name",userName);
            intent.putExtra("userImage",getIntent().getStringExtra("userImage"));
            intent.putExtra("courseID",courseID);
            intent.putExtra("ticketPrice",ticketPrice);
            intent.putExtra("from","booking");
            startActivity(intent);
        });
    }

    private void getChat() {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        progressDialog.setCancelable(false);
        Map<String,String> param = new HashMap<>();
        param.put("to_user_id",receiverID);
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));

        Log.e("chatParam",""+param);

        new PostMethod(Api.UserChatHistory,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("chatDetailResp",data);

                try {
                    JSONObject jsonObject = new JSONObject(data);
                    mainLayout.setVisibility(View.VISIBLE);
                    checkUserType=jsonObject.optString("user_type");

                    if(checkUserType.equalsIgnoreCase("t"))
                        bookCard.setVisibility(View.VISIBLE);
                    else
                        bookCard.setVisibility(View.GONE);

                    if(jsonObject.optBoolean("status")){
                        JSONObject youBlocked = jsonObject.optJSONObject("you_blocked");
                        JSONObject blockedBy = jsonObject.optJSONObject("blocked_by");
                        if (!youBlocked.optString("status").equalsIgnoreCase("1")){
                            block.setText(youBlocked.optString("msg"));
                            block.setVisibility(View.VISIBLE);
                            sendMessageLL.setVisibility(View.GONE);
                            submit.setVisibility(View.VISIBLE);
                            bookCard.setVisibility(View.GONE);
                        }
                        else if (!blockedBy.optString("status").equalsIgnoreCase("1")){
                            block.setText(blockedBy.optString("msg"));
                            block.setVisibility(View.VISIBLE);
                            sendMessageLL.setVisibility(View.GONE);
                            bookCard.setVisibility(View.GONE);
                        }

                        JSONArray dataArray = jsonObject.optJSONArray("data");
                        for(int ar=0;ar<dataArray.length();ar++){
                            format.setTimeZone(TimeZone.getTimeZone("UTC"));
                            format5.setTimeZone(TimeZone.getDefault());
                            timeFormat2.setTimeZone(TimeZone.getDefault());
                            timeFormat1.setTimeZone(TimeZone.getDefault());

                            JSONObject object = dataArray.getJSONObject(ar);
                            ChatRoomModal maliciousModel=new ChatRoomModal();
                            Date tempDate1  = format.parse(object.optString("timestamp"));
                            //Log.e("tempDate1",""+tempDate1);
                            Date tempDate  = timeFormat1.parse(timeFormat1.format(tempDate1));
                            //Log.e("tempDate",""+tempDate);
                            try {
                                String Title = URLDecoder.decode(object.optString("chat_message"), "UTF-8");
                                maliciousModel.setMessage(Title);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            maliciousModel.setUserId(object.optString("from_user_id"));
                            //if(((ar+1)*3)%2==0)
                            maliciousModel.setDate(format5.format(tempDate));
                            maliciousModel.setTime(timeFormat2.format(tempDate1));
                            maliciousModel.setImage(getIntent().getStringExtra("userImage"));
                            chatRoomModalArrayList.add(maliciousModel);
                        }

                        setAdapter();
                    }
                    else {
                        noChat.setVisibility(View.VISIBLE);
                    }
                }catch (Exception w){
                    w.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                mainLayout.setVisibility(View.VISIBLE);
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    public void setAdapter() {
        chatAdapter=new ChatAdapter(context,chatRoomModalArrayList);
        recyclerView.scrollToPosition(chatRoomModalArrayList.size()-1);
        recyclerView.setAdapter(chatAdapter);
    }


    public void sendMessage(String message) {
        Map<String,String> param = new HashMap<>();
        param.put("to_user_id",receiverID);
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("chat_message",message);
        param.put("lang_token",sharedPreferences.getString("lang",""));

        Log.e("chatParam",""+param);

        new PostMethod(Api.AddChat,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("chatResp",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        Intent intent = new Intent("update");
                        intent.putExtra("success","1");
                        context.sendBroadcast(intent);
                    }else {
                        Intent intent = new Intent("update");
                        intent.putExtra("success","0");
                        context.sendBroadcast(intent);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        context.unregisterReceiver(checkMessage);
        sharedPreferences.edit().putString("chatRoom","0").apply();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sharedPreferences.edit().putString("chatRoom","0").apply();
    }

    /*@Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        context.unregisterReceiver(checkMessage);
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences.edit().putString("chatRoom","1").apply();
        registerReceiver(courseSelected,new IntentFilter("courseSelected"));
        registerReceiver(checkMessage, new IntentFilter("newMessage"));
    }


    BroadcastReceiver courseSelected=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String coursePrice=intent.getStringExtra("coursePrice");
                Log.e("coursePrice",coursePrice);
                courseID=intent.getStringExtra("courseID");
                coursePriceModalArrayList.clear();
                JSONArray jsonArray=new JSONArray(coursePrice);
                for (int ar=0;ar<jsonArray.length();ar++){
                    JSONObject object=jsonArray.optJSONObject(ar);
                    CoursePriceModal coursePriceModal=new CoursePriceModal();
                    coursePriceModal.setHours(object.optString("hours")+" "+context.getResources().getString(R.string.hours));
                    coursePriceModal.setPrice("$ "+object.optString("price_per_hour")+" /"+context.getResources().getString(R.string.hour));

                    coursePriceModalArrayList.add(coursePriceModal);
                }
                priceRecyclerView.setAdapter(new CoursePriceAdapter(context,coursePriceModalArrayList));
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    };


    BroadcastReceiver checkMessage =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent intent1 = new Intent("update");
            context.sendBroadcast(intent1);
            
            Map<String,String> param = new HashMap<>();
            param.put("to_user_id",receiverID);
            param.put("user_id",sharedPreferences.getString("userID",""));
            param.put("lang_token",sharedPreferences.getString("lang",""));

            Log.e("chatBroadCastParam",""+param);

            new PostMethod(Api.UserChatHistory,param,context).startPostMethod(new ResponseData() {
                @Override
                public void response(String data) {
                    Log.e("chatBroadCastResp",data);

                    try {
                        JSONObject jsonObject = new JSONObject(data);
                        if(jsonObject.optBoolean("status")){
                            chatRoomModalArrayList.clear();
                            JSONArray dataArray = jsonObject.optJSONArray("data");
                            for(int ar=0;ar<dataArray.length();ar++){
                                format.setTimeZone(TimeZone.getTimeZone("UTC"));
                                format5.setTimeZone(TimeZone.getDefault());
                                timeFormat2.setTimeZone(TimeZone.getDefault());
                                timeFormat1.setTimeZone(TimeZone.getDefault());

                                JSONObject object = dataArray.getJSONObject(ar);
                                ChatRoomModal maliciousModel=new ChatRoomModal();
                                Date tempDate1  = format.parse(object.optString("timestamp"));
                                Date tempDate  = timeFormat1.parse(timeFormat1.format(tempDate1));
                                try {
                                    String Title = URLDecoder.decode(object.optString("chat_message"), "UTF-8");
                                    maliciousModel.setMessage(Title);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                maliciousModel.setUserId(object.optString("from_user_id"));
                                maliciousModel.setImage(getIntent().getStringExtra("userImage"));
                                //if(((ar+1)*3)%2==0)
                                maliciousModel.setDate(format5.format(tempDate));
                                maliciousModel.setTime(timeFormat2.format(tempDate1));
                                chatRoomModalArrayList.add(maliciousModel);
                            }
                            setAdapter();
                        }
                    }catch (Exception w){
                        w.printStackTrace();
                    }
                }

                @Override
                public void error(VolleyError error) {
                    error.printStackTrace();
                }
            });
        }
    };
}

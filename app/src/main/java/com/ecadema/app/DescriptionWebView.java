package com.ecadema.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.GetMethod;
import com.ecadema.apiMethods.ResponseData;

import org.json.JSONObject;

public class DescriptionWebView extends AppCompatActivity {

    LinearLayout header;
    WebView webView;
    ProgressDialog progressDialog;
    Context context;
    ImageView close,homeIcon;
    TextView text_home;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.web_view);

        context = this;
        header = findViewById(R.id.header);
        webView = findViewById(R.id.webView);
        close = findViewById(R.id.close);
        text_home = findViewById(R.id.text_home);
        homeIcon = findViewById(R.id.homeIcon);

        close.setOnClickListener(v-> finish());
        homeIcon.setOnClickListener(v-> {
            Intent intent = new Intent(context,MainActivity.class);
            startActivity(intent);
            finish();
        });

        header.setVisibility(View.VISIBLE);
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        new GetMethod(getIntent().getStringExtra("link").replace("http","https"),context).startmethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject = new JSONObject(data);

                    webView.getSettings().setAllowFileAccess(true);
                    //wv.loadUrl(doc);
                    webView.getSettings().setMinimumFontSize(40);

                    webView.getSettings().setLoadWithOverviewMode(true);
                    webView.getSettings().setUseWideViewPort(true);
                    text_home.setText(jsonObject.optJSONObject("title").optString("rendered"));
                    //webView.loadData(jsonObject.optJSONObject("content").optString("rendered") , "text/html; charset=UTF-8", null);
                    webView.loadDataWithBaseURL(null,jsonObject.optJSONObject("content").optString("rendered") ,
                            "text/html", null,null);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }
}

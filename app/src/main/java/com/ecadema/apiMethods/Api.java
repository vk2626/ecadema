package com.ecadema.apiMethods;

public class Api {

    /** User roles
     * 1 => Trainee
     * c => 2 => Corporate
     * t => 3 => Trainer **/

    public static String BASE_URL = "https://api.ecadema.com/api/student/"; //Live
    //public static String BASE_URL = "https://apidev.ecadema.com/api/student/"; // Test

    /** Sign up **/
    public static String TraineeSignup = BASE_URL+"TraineeSignup";
    public static String SocialLogin = BASE_URL+"SocialLogin";//Param: name,device_token,device_type,oauth_uid,oauth_provider,email,lang_token

    /** Login **/
    public static String Login = BASE_URL+"Login";

    /** LogOut **/
    public static String LogOut = BASE_URL+"LogOut"; //Param: lang_token, user_id

    /** Country List **/
    public static String CountriesList = BASE_URL+"CountriesList";

    /** Lang List **/
    public static String SpokenLanguages = BASE_URL+"SpokenLanguages";

    /** BrowseCourses **/
    public static String BrowseCourses = BASE_URL+"BrowseCourses"; //Param: lang_token

    /** Profile APIs **/
    public static String TrainerProfile = BASE_URL+"TrainerProfile"; //Param: lang_token,trainer_id
    public static String TraineeProfile = BASE_URL+"TraineeProfile"; //Param: lang_token,trainee_id
    public static String TraineeProfileUpdate = BASE_URL+"TraineeProfileUpdate"; //Param: first_name,last_name,contact_no,country,trainee_id,profile_photo_edit

    /** Password API **/
    public static String ForgetPassword = BASE_URL+"ForgetPassword"; //Param: lang_token,email
    public static String ChangePassword = BASE_URL+"ChangePassword"; //Param: lang_token,old_password,new_password,cpassword,user_id

    /** Up coming group session **/
    public static String UpGroupSessions = BASE_URL+"UpcomingGroupSessions?lang=";

    /** Past group session **/
    public static String PastGroupSessions = BASE_URL+"PastGroupSessions?lang=";

    /** ViewWorkshop **/
    public static String ViewWorkshop = BASE_URL+"ViewWorkshop"; //Param: lang_token,workshop_id

    /** TrainersList **/
    public static String TrainersList = BASE_URL+"TrainersList";/* param: lang_token:ar, langg, gender, cat_id, min_price, max_price, country, teacher_name, subject_id*/

    /** ViewProfile **/
    public static String ViewProfile = BASE_URL+"ViewProfile";/* param: lang_token,trainer*/

    /** Credit Api **/
    public static String EcademaCredit=BASE_URL+"EcademaCredit"; //user_id,lang_token
    public static String AddRefill=BASE_URL+"AddRefill"; //user_id,lang_token,refill_amount
    public static String WalletCheckOut=BASE_URL+"WalletCheckOut"; //trans_id,lang_token,transaction_info

    /** Corporate Apis **/
    public static String RemoveTeammates=BASE_URL+"RemoveTeammates"; //removed_id,lang_token,status
    public static String WorkshopInvitation=BASE_URL+"WorkshopInvitation"; //user_id,lang_token,email
    public static String CorporateBillingList=BASE_URL+"CorporateBillingList"; //corporate_id,lang_token
    public static String CorporateDeactive=BASE_URL+"CorporateDeactive"; //user_id,lang_token
    public static String CorporatePastSessions=BASE_URL+"CorporatePastSessions"; //corporate_id,lang_token
    public static String CorporateUpcomingSessions=BASE_URL+"CorporateUpcomingSessions"; //corporate_id,lang_token
    public static String ChatCorporateList=BASE_URL+"ChatCorporateList"; //user_id,lang_token
    public static String EmployeesList=BASE_URL+"EmployeesList"; //user_id,lang_token
    public static String Teammates=BASE_URL+"Teammates"; //user_id,lang_token
    public static String CorporateProfileDetails=BASE_URL+"CorporateProfileDetails"; //user_id,lang_token
    public static String UpdateCorporate=BASE_URL+"UpdateCorporate"; /*user_id,lang_token,contact_no,language,business_name,website_url,tel_number,country,
    first_name,last_name,attachment_edit,profile_image_edit,registration_no*/


    /** Trainee Apis **/
    public static String TraineeOneToOnePastBooking=BASE_URL+"TraineeOneToOnePastBooking"; //trainee_id,lang_token
    public static String TraineeOneToOneUpcomingBooking=BASE_URL+"TraineeOneToOneUpcomingBooking"; //trainee_id,lang_token
    public static String TraineeCancelSession=BASE_URL+"TraineeCancelSession"; //booking_date,lang_token,booking_id,reason
    public static String TraineePastSessions=BASE_URL+"TraineePastSessions"; //lang_token,trainee_id
    public static String TraineeUpcomingSessions=BASE_URL+"TraineeUpcomingSessions"; //lang_token,trainee_id
    public static String TraineeBillingList=BASE_URL+"TraineeBillingList"; //lang_token,trainee_id
    public static String MyTrainers=BASE_URL+"MyTrainers"; //lang_token,trainee_id
    public static String CheckingToSendPdfStudent=BASE_URL+"CheckingToSendPdfStudent"; //lang_token,booking_id
    public static String CommunityList=BASE_URL+"CommunityList?lang=";
    public static String CommunityDetails=BASE_URL+"CommunityDetails";//community_id,lang

    /** Trainer Apis **/
    public static String TrainerBillingList=BASE_URL+"TrainerBillingList"; //trainer_id,lang_token
    public static String TrainerUpdateSlotes=BASE_URL+"TrainerUpdateSlotes"; //trainer_id,lang_token,timezone,slotes,timezone
    public static String TrainerAvailability=BASE_URL+"TrainerAvailability"; //trainer_id,lang_token,timezone
    public static String TrainerUpcomingSessions=BASE_URL+"TrainerUpcomingSessions"; //trainer_id,lang_token
    public static String TrainerPastSessions=BASE_URL+"TrainerPastSessions"; //trainer_id,lang_token,timezone
    public static String TrainerSubmittedSessions=BASE_URL+"TrainerSubmittedSessions"; //trainer_id,lang_token
    public static String TrainerOneToOnePastBooking=BASE_URL+"TrainerOneToOnePastBooking"; //trainer_id,lang_token,timezone
    public static String TrainerOneToOneUpcomingBooking=BASE_URL+"TrainerOneToOneUpcomingBooking"; //trainer_id,lang_token
    public static String RescheduleByTeacher=BASE_URL+"RescheduleByTeacher"; //teacher_id,lang_token,booking_id, reason,student_id
    public static String TrainerPersonalDetailsUpdate=BASE_URL+"TraineerPersonalDetailsUpdate"; //traineer_id,lang_token,contact_no,languages
    public static String TrainerBiographyUpdate=BASE_URL+"TraineerBiographyUpdate"; //traineer_id,lang_token,about
    public static String AddEducationByTrainer=BASE_URL+"AddEducationByTraineer"; //date_from,lang_token,date_to,designation,traineer_id,upload_certificate
    public static String TrainerEducationUpdate=BASE_URL+"TraineerEducationUpdate"; //date_from,lang_token,date_to,designation,traineer_id,upload_certificate,education_id,upload_certificate_edit
    public static String AddExperienceByTrainer=BASE_URL+"AddExperienceByTraineer"; //date_from,lang_token,date_to,designation,traineer_id,upload_certificate,organisation,upload_certificate
    public static String TrainerExperienceUpdate=BASE_URL+"TraineerExperienceUpdate"; //date_from,lang_token,date_to,designation,traineer_id,upload_certificate,organisation,upload_certificate,upload_certificate_edit,experience_id
    public static String TrainerAttachmentsUpdate=BASE_URL+"TraineerAttachmentsUpdate"; //edit_resume,edit_id_proof,lang_token,traineer_id,upload_resume,upload_photo_id
    public static String CheckingToSendPdfMentor=BASE_URL+"CheckingToSendPdfMentor"; //lang_token,booking_id
    public static String AddCommunity = BASE_URL+"AddCommunity";//Param: trainer_id, title, blog, categories, keyword, video_link, upload_image
    public static String GetTrainerPublicSubmitted =BASE_URL+"GetTrainerPublicSubmitted"; //lang, trainer_id,status = 0 (submitted) & 1 (Published)

    /**  Notification **/
    public  static String NotificationStatus=BASE_URL+"NotificationStatus";//Param: message_notification, session_reminders,other_notification,lang_token,user_id
    public  static String UserNotificationStatus=BASE_URL+"UserNotificationStatus";//Param: lang_token,user_id

    /**  Promocode **/
    public  static String CheckingPromocode=BASE_URL+"CheckingPromocode";//Param: user_id, promocode,trans_id,lang_token,amount

    /**  TraineerSkills **/
    public  static String TraineerSkills=BASE_URL+"TraineerSkills";

    /** Add Review **/
    public  static String AddReviews=BASE_URL+"AddReviews";

    /** TimeSlots **/
    public  static String AvailabilityCalendarList=BASE_URL+"AvailabilityCalendarList";//lang_token,course_id,timezone,day_availability

    /** Report **/
    public  static String reporttouser=BASE_URL+"reporttouser";//lang_token,user_id,report_id

    /** BillingPDF **/
    public  static String BillingPdf=BASE_URL+"BillingPdf";//lang_token,user_id,report_id

    /** TraineeAcceptedOrRescheduleBooking **/
    public  static String TraineeAcceptedOrRescheduleBooking=BASE_URL+"TraineeAcceptedOrRescheduleBooking";/*Param: lang_token,booking_id,booking_slots_id,bookedslots,timezone,old_booking_date:2020-12-26 09:30:00*/

    /** Reviews **/
    public  static String ReviewAjax=BASE_URL+"ReviewAjax";/*Param: teacher_id,pages_no,lang_token*/

    /** Articles **/
    public  static String ArticleAjax=BASE_URL+"ArticleAjax";/*Param: teacher_id,pages_no,lang_token*/

    /** Like_Dislike **/
    public  static String GetLikeDislike=BASE_URL+"GetLikeDislike";/* Param: teacher_id,user_id,community_id, like_dislike_value */

    /** UnblockUser **/
    public  static String UnblockUser=BASE_URL+"UnblockUser";/* Param: user_id,report_id,lang_token*/

    /** Earning API **/
    public  static String TrainerEarningHistory=BASE_URL+"TrainerEarningHistory";//Param: lang_token, trainer_id
    public  static String TrainerEarningShow=BASE_URL+"TrainerEarningShow";//Param: lang_token, trainer_id
    public  static String Withdraw=BASE_URL+"Withdraw";//Param: lang_token,trainer_id,paypal_email,payoneer_email,pay_mode,amount


    /**  Booking api **/
    public  static String SaveWorkshopBooking=BASE_URL+"SaveWorkshopBooking";//user_id,,workshop_id,emails[],rate,,timezone,lang_token,teacher_id
    public  static String CheckOutWorkshop=BASE_URL+"CheckOutWorkshop";//user_id,trans_id,amount,amount_modify,transaction_info,,timezone,lang_token,pay_by_wallet
    public  static String CheckOutWallet=BASE_URL+"CheckOutWallet";//user_id,trans_id,amount,lang_token
    public  static String SaveBooking=BASE_URL+"SaveBooking";//user_id,timezone:Asia,course,lang_token,bookedslots,teacher_id
    public  static String OneToOneCheckOut=BASE_URL+"OneToOneCheckOut";//user_id,trans_id,amount,amount_modify,transaction_info,,timezone,lang_token,pay_by_wallet,teacher_id
    public  static String updateCorporateUserId=BASE_URL+"GetCorporateBooking";//user_id,trans_id,amount,amount_modify,transaction_info,,timezone,lang_token,pay_by_wallet,teacher_id

    /** Chat APIs **/
    public static String UserUnReadMsgCount=BASE_URL+"UserUnReadMsgCount";/*user_id,lang_token*/
    public static String AddChat=BASE_URL+"AddChat";/*to_user_id,user_id,hat_message,lang_token*/
    public static String ChatTrainerList=BASE_URL+"ChatTrainerList";/*user_id,lang_token*/
    public static String UserChatHistory=BASE_URL+"UserChatHistory";/*user_id,lang_token,to_user_id*/
    public static String ChatTraineeList=BASE_URL+"ChatTraineeList";/*user_id,lang_token*/

    /** Deactivate API **/
    public static String StudentDeactivate = BASE_URL+"StudentDeactive";//Param: lang_token, user_id
    public static String TeacherDeactivate = BASE_URL+"TeacherDeactive";//Param: lang_token, user_id

    /** Paypal Braintree API **/
    public static String sendNonceKey = "http://167.172.24.97/ecadema_braintree_setup/pay.php";//Param: paymentMethodNonce, deviceData, amount

    /** Stripe API **/
    public static String StripeApiKey = BASE_URL+"StripeApiKey";//Param: amount, currency
    public static String GetStripeCardInfo = BASE_URL+"GetStripeCardInfo";//Param: amount, currency
    public static String GetUserCardInfo = BASE_URL+"GetUserCardInfo";//Param: user_id, lang
    public static String RemoveStripeCardById = BASE_URL+"RemoveStripeCardById";//Param: card_id, lang_token
    public static String StripePayment = BASE_URL+"StripePayment";//Param: customer, stripe_version="2020-08-27
    public static String StripeCustomers = BASE_URL+"StripeCustomers";//Param: customer, stripe_version="2020-08-27

    public static String liveStripeKey = "pk_live_2tuCQmkzSBtHg0SeuDOix84N00qNV7JYaZ";//Param: customer, stripe_version="2020-08-27
    public static String testStripeKey = "pk_test_51JeDSGSAsol1gfVNdowtXsP6wkI486v398LfzMO0JJkwA8nyN7P59VrOduKBX9ahxDvD4kYe0YutByzs90fzafRd006bzPikM1";//Param: customer, stripe_version="2020-08-27


    /** Remove Upcoming Bookings API **/
    public static String RemoveExpiredBookingOneToOne = BASE_URL+"RemoveExpiredBookingOneToOne";//Param: lang_token, booking_slots_id
    public static String RemoveExpiredBookingWorkshop = BASE_URL+"RemoveExpiredBookingWorkshop";//Param: lang_token, booked_date

    /** Create Session API **/
    public static String WorkshopStartTime = BASE_URL+"WorkshopStartTime";//Param: lang, timezone
    public static String WorkshopEndTime = BASE_URL+"WorkshopEndTime";//Param: start_time, lang, timezone
    public static String getWorkshopTimeValidate = BASE_URL+"getWorkshopTimeValidate";//Param: start_time, lang, end_time,user_id,no_day,timezone
    public static String getWorkshopHours = BASE_URL+"getWorkshopHours";//Param: start_time, end_time, lang
    public static String AddWorkshop = BASE_URL+"AddWorkshop";//Param: trainer_id,languages,status_type,type,about,overview,workshope_webinar_time,
    //hours,ticket_cost,who_should_attend,why_attend,max_participants, start_time, end_time

    /** MentorById **/
    public static String getMentorById = BASE_URL+"getMentorById";//Param: trainer_id

    /** UpdateLastActivityOfUser **/
    public static String UpdateLastActivityOfUser = BASE_URL+"UpdateLastActivityOfUser";//Param: user_id

    /** CommunityApis**/
    public static String deletePost = BASE_URL+"CommunityDelete";//Param: removed_id
    public static String getAllFilterTagsTeachersCommunity = BASE_URL+"getAllFilterTagsTeachersCommunity?lang=";

    /** Filters **/
    public static String TrainerCountriesList = BASE_URL+"TrainerCountriesList";//Param: lang_token:en
    public static String allTags = BASE_URL+"getAllTag?lang=";
    public static String allTrainerTags = BASE_URL+"getAllFilterTagsTeachers?lang=";
    public static String allSessionsTags = BASE_URL+"getAllGroupSessionTagsFilter?lang=";
    public static String langFilter = BASE_URL+"getSpokenLanguagesDelegateSessionFilter"; //Param: lang_token:en, page_filter:one-to-one/group-session

    /** Employees Graph APIs **/
    public static String employeesWeeklyGraph = BASE_URL+"employeesWeeklyGraph";//Param: user_id,lang_token,week
    public static String employeesMonthlyGraph = BASE_URL+"employeesMonthlyGraph";//Param: user_id,lang_token,month

    /** Bookings Graph APIs **/
    public static String getNoBookingWeeklyGraph = BASE_URL+"getNoBookingWeeklyGraph";//Param: user_id,lang_token,week
    public static String getNoBookingMonthlyGraph = BASE_URL+"getNoBookingMonthlyGraph";//Param: user_id,lang_token,month

    /** Credit Graph APIs **/
    public static String getNoCreditWeeklyGraph = BASE_URL+"getNoCreditWeeklyGraph";//Param: user_id,lang_token,week
    public static String getNoCreditMonthlyGraph = BASE_URL+"getNoCreditMonthlyGraph";//Param: user_id,lang_token,month

    /** Investment Graph APIs **/
    public static String getInvestWeeklyGraph=BASE_URL+"getInvestWeeklyGraph"; //user_id,lang_token,week
    public static String getInvestMonthlyGraph=BASE_URL+"getInvestMonthlyGraph"; //user_id,lang_token,month

    /** Send Request to Company APIs **/
    public static String TeammatesInvitation=BASE_URL+"TeammatesInvitation"; //user_id,lang_token,email
    public static String TeammatesRequest=BASE_URL+"TeammatesRequest"; //user_id,lang_token,requested_id,department,seniority

    public static String RequestForWorkshop=BASE_URL+"RequestForWorkshop"; //user_id, about, workshope_webinar_time,language,budget,details_about_your_request,max_participants
    public static String RequestedSessions=BASE_URL+"RequestedSessions"; //user_id

}
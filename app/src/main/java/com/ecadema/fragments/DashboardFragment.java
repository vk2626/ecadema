package com.ecadema.fragments;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DashboardFragment extends Fragment {
    ImageView close;
    LineChart lineChart,creditBalanceChart;

    BarChart employeesBarChart,bookingsBarChart;
    BarData barData;
    BarDataSet barDataSet;
    ArrayList<BarEntry> barEntries;

    AutoCompleteTextView employeesMonth,bookingsMonth,investmentMonth,creditMonth;
    ArrayList<String> optionsList = new ArrayList<>();

    TextView numberOfEmp,noOfBookings,investmentAmount,creditBalance;
    ArrayList<String> weeks = new ArrayList<>();
    ArrayList<String> threeMonthArrayList = new ArrayList<>();
    ArrayList<String> sixMonthArrayList = new ArrayList<>();
    ArrayList<String> _12MonthArrayList = new ArrayList<>();
    String employeeApi = Api.employeesWeeklyGraph;
    String bookingApi = Api.getNoBookingWeeklyGraph;
    String investmentApi = Api.getInvestWeeklyGraph;
    String creditBalanceApi = Api.getNoCreditWeeklyGraph;
    SharedPreferences sharedPreferences;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.dashboard_frag, container, false);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        close=root.findViewById(R.id.close);
        close.setOnClickListener(v-> getParentFragmentManager().popBackStackImmediate());

        numberOfEmp = root.findViewById(R.id.numberOfEmp);
        creditBalance = root.findViewById(R.id.creditBalance);
        noOfBookings = root.findViewById(R.id.noOfBookings);
        investmentAmount = root.findViewById(R.id.investmentAmount);

        optionsList.add(getContext().getResources().getString(R.string.last_30_days));
        optionsList.add(getContext().getResources().getString(R.string.last_3_months));
        optionsList.add(getContext().getResources().getString(R.string.last_6_months));
        optionsList.add(getContext().getResources().getString(R.string.last_12_months));

        weeks.add("Week 1");
        weeks.add("Week 2");
        weeks.add("Week 3");
        weeks.add("Week 4");

        Calendar c = new GregorianCalendar();
        c.setTime(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");
        for (int i=0; i<3;i++){
            if (i !=0) {
                c.add(Calendar.MONTH, -1);
            }
            threeMonthArrayList.add(sdf.format(c.getTime()));
        }
        Collections.reverse(threeMonthArrayList);

        Calendar c1 = new GregorianCalendar();
        c1.setTime(new Date());
        sdf = new SimpleDateFormat("MMM yyyy");
        for (int i=0; i<6;i++){
            if (i !=0) {
                c1.add(Calendar.MONTH, -1);
            }
            sixMonthArrayList.add(sdf.format(c1.getTime()));
        }
        Collections.reverse(sixMonthArrayList);

        Calendar c2 = new GregorianCalendar();
        c2.setTime(new Date());
        for (int i=0; i<12;i++){
            if (i !=0) {
                c2.add(Calendar.MONTH, -1);
            }
            _12MonthArrayList.add(sdf.format(c2.getTime()));
        }
        Collections.reverse(_12MonthArrayList);

        creditBalanceChart = root.findViewById(R.id.creditBalanceChart);
        creditMonth = root.findViewById(R.id.creditMonth);
        bookingsMonth = root.findViewById(R.id.bookingsMonth);
        investmentMonth = root.findViewById(R.id.investmentMonth);
        employeesMonth = root.findViewById(R.id.employeesMonth);
        lineChart = root.findViewById(R.id.reportingChart);
        employeesBarChart = root.findViewById(R.id.employeesBarChart);
        bookingsBarChart = root.findViewById(R.id.bookingsBarChart);
        initEmpBarChart(0);
        initBookingBarChart(0);
        initInvestmentLineChart(0);
        initCreditLineChart(0);

        employeesMonth.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, optionsList));
        employeesMonth.setOnTouchListener((@SuppressLint("ClickableViewAccessibility") View view, MotionEvent motionEvent) -> {
            if(motionEvent.getAction()== MotionEvent.ACTION_DOWN)
               employeesMonth.showDropDown();
            return true;
        });
        employeesMonth.setOnItemClickListener((parent, view, position, id) -> {
            initEmpBarChart(position);
        });

        creditMonth.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, optionsList));
        creditMonth.setOnTouchListener((@SuppressLint("ClickableViewAccessibility") View view, MotionEvent motionEvent) -> {
            if(motionEvent.getAction()== MotionEvent.ACTION_DOWN)
                creditMonth.showDropDown();
            return true;
        });
        creditMonth.setOnItemClickListener((parent, view, position, id) -> {
            initCreditLineChart(position);
        });

        bookingsMonth.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, optionsList));
        bookingsMonth.setOnTouchListener((@SuppressLint("ClickableViewAccessibility") View view, MotionEvent motionEvent) -> {
            if(motionEvent.getAction()== MotionEvent.ACTION_DOWN)
                bookingsMonth.showDropDown();
            return true;
        });
        bookingsMonth.setOnItemClickListener((parent, view, position, id) -> {
            initBookingBarChart(position);
        });

        investmentMonth.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, optionsList));
        investmentMonth.setOnTouchListener((@SuppressLint("ClickableViewAccessibility") View view, MotionEvent motionEvent) -> {
            if(motionEvent.getAction()== MotionEvent.ACTION_DOWN)
                investmentMonth.showDropDown();
            return true;
        });
        investmentMonth.setOnItemClickListener((parent, view, position, id) -> {
            initInvestmentLineChart(position);
        });

        //getEmployeesData("week","4");
        return root;
    }

    private void initCreditLineChart(int position) {
        creditBalanceChart.setTouchEnabled(true);
        creditBalanceChart.setPinchZoom(true);
        creditBalanceChart.setDescription(null);
        creditBalanceChart.getLegend().setEnabled(false);
        creditBalanceChart.getXAxis().setAvoidFirstLastClipping(true);
        creditBalanceChart.getXAxis().setLabelsToSkip(0);
        creditBalanceChart.getXAxis().setLabelRotationAngle(325);

        if (position==0){
            creditBalanceApi = Api.getNoCreditWeeklyGraph;
            getCreditData("week","4",weeks);

        }
        if (position==1){
            creditBalanceApi = Api.getNoCreditMonthlyGraph;
            getCreditData("month","3",threeMonthArrayList);
        }
        if (position==2) {
            creditBalanceApi = Api.getNoCreditMonthlyGraph;
            getCreditData("month","6",sixMonthArrayList);
        }
        if (position==3) {
            creditBalanceApi = Api.getNoCreditMonthlyGraph;
            getCreditData("month","12",_12MonthArrayList);
        }
    }

    private void initInvestmentLineChart(int position) {
        lineChart.setTouchEnabled(true);
        lineChart.setPinchZoom(true);
        lineChart.setDescription(null);
        lineChart.getLegend().setEnabled(false);
        lineChart.getXAxis().setAvoidFirstLastClipping(true);
        lineChart.getXAxis().setLabelsToSkip(0);
        lineChart.getXAxis().setLabelRotationAngle(325);


        if (position==0){
            bookingApi = Api.getInvestWeeklyGraph;
            getInvestmentData("week","4",weeks);

        }
        if (position==1){
            bookingApi = Api.getInvestMonthlyGraph;
            getInvestmentData("month","3",threeMonthArrayList);
        }
        if (position==2) {
            bookingApi = Api.getInvestMonthlyGraph;
            getInvestmentData("month","6",sixMonthArrayList);
        }
        if (position==3) {
            bookingApi = Api.getInvestMonthlyGraph;
            getInvestmentData("month","12",_12MonthArrayList);
        }
    }

    private void initBookingBarChart(int position) {
        barEntries = new ArrayList<>();
        bookingsBarChart.getXAxis().setLabelsToSkip(0);
        bookingsBarChart.getXAxis().setLabelRotationAngle(325);
        if (position==0){
            bookingApi = Api.getNoBookingWeeklyGraph;
            getBookingData("week","4",weeks);

        }
        if (position==1){
            bookingApi = Api.getNoBookingMonthlyGraph;
            getBookingData("month","3",threeMonthArrayList);
        }
        if (position==2) {
            bookingApi = Api.getNoBookingMonthlyGraph;
            getBookingData("month","6",sixMonthArrayList);
        }
        if (position==3) {
            bookingApi = Api.getNoBookingMonthlyGraph;
            getBookingData("month","12",_12MonthArrayList);
        }
    }

    private void initEmpBarChart(int position) {
        barEntries = new ArrayList<>();
        employeesBarChart.getXAxis().setLabelsToSkip(0);
        employeesBarChart.getXAxis().setLabelRotationAngle(325);
        if (position==0){
            employeeApi = Api.employeesWeeklyGraph;
            getEmployeesData("week","4",weeks);

        }
        if (position==1){
            employeeApi = Api.employeesMonthlyGraph;
            getEmployeesData("month","3",threeMonthArrayList);
        }
        if (position==2) {
            employeeApi = Api.employeesMonthlyGraph;
            getEmployeesData("month","6",sixMonthArrayList);
        }
        if (position==3) {
            employeeApi = Api.employeesMonthlyGraph;
            getEmployeesData("month","12",_12MonthArrayList);
        }
    }

    private void getCreditData(String key, String value, ArrayList<String> labelList) {
        Map<String,String> param = new HashMap<>();
        param.put(key,value);
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));
        Log.e("CreditDataParam",""+param);

        new PostMethod(creditBalanceApi,param,getContext()).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    Log.e("CreditDataResp",""+data);
                    List<Entry> lineEntries = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(data);
                    creditBalance.setText(": $"+jsonObject.optString("total"));
                    JSONArray dataArray = jsonObject.optJSONArray("data");
                    for (int i=0; i<dataArray.length(); i++){

                        lineEntries.add(new BarEntry(dataArray.optInt(i), i));
                    }
                    LineDataSet lineDataSet = new LineDataSet(lineEntries, "");
                    LineData lineData = new LineData(labelList,lineDataSet);

                    lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
                    lineDataSet.setLineWidth(2);
                    lineDataSet.setDrawValues(false);
                    lineDataSet.setDrawHighlightIndicators(true);
                    lineDataSet.setHighlightEnabled(true);
                    lineDataSet.setHighLightColor(Color.CYAN);
                    lineDataSet.setValueTextSize(12);
                    lineDataSet.setValueTextColor(Color.DKGRAY);

                    lineDataSet.setDrawCircles(true);
                    lineDataSet.enableDashedLine(10f, 0f, 0f);
                    lineDataSet.enableDashedHighlightLine(10f, 0f, 0f);
                    lineDataSet.setColor(getResources().getColor(R.color.colorPrimary));
                    lineDataSet.setCircleColor(getResources().getColor(R.color.red));
                    lineDataSet.setCircleRadius(5f);
                    lineDataSet.setDrawCircleHole(true);
                    creditBalanceChart.animateY(1000);
                    creditBalanceChart.setData(lineData);

                    // Setup X Axis
                    XAxis xAxis = creditBalanceChart.getXAxis();
                    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                    xAxis.enableGridDashedLine(2f, 7f, 0f);

                    // Setup Y Axis
                    YAxis yAxis = creditBalanceChart.getAxisLeft();
                    yAxis.setGranularity(1f);
                    yAxis.enableGridDashedLine(10f, 10f, 0f);

                    creditBalanceChart.getAxisRight().setEnabled(false);
                    creditBalanceChart.invalidate();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }
    private void getInvestmentData(String key, String value, ArrayList<String> labelList) {

        Map<String,String> param = new HashMap<>();
        param.put(key,value);
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));
        Log.e("InvestmentDataParam",""+param);

        new PostMethod(investmentApi,param,getContext()).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    Log.e("InvestmentDataResp",""+data);
                    List<Entry> lineEntries = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(data);
                    investmentAmount.setText(": $"+jsonObject.optString("total"));
                    JSONArray dataArray = jsonObject.optJSONArray("data");
                    for (int i=0; i<dataArray.length(); i++){

                        lineEntries.add(new BarEntry(dataArray.optInt(i), i));
                    }
                    LineDataSet lineDataSet = new LineDataSet(lineEntries, "");
                    LineData lineData = new LineData(labelList,lineDataSet);

                    lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
                    lineDataSet.setLineWidth(2);
                    lineDataSet.setDrawValues(false);
                    lineDataSet.setDrawHighlightIndicators(true);
                    lineDataSet.setHighlightEnabled(true);
                    lineDataSet.setHighLightColor(Color.CYAN);
                    lineDataSet.setValueTextSize(12);
                    lineDataSet.setValueTextColor(Color.DKGRAY);

                    lineDataSet.setDrawCircles(true);
                    lineDataSet.enableDashedLine(10f, 0f, 0f);
                    lineDataSet.enableDashedHighlightLine(10f, 0f, 0f);
                    lineDataSet.setColor(getResources().getColor(R.color.colorPrimary));
                    lineDataSet.setCircleColor(getResources().getColor(R.color.red));
                    lineDataSet.setCircleRadius(5f);
                    lineDataSet.setDrawCircleHole(true);
                    lineChart.animateY(1000);
                    lineChart.setData(lineData);

                    // Setup X Axis
                    XAxis xAxis = lineChart.getXAxis();
                    xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                    xAxis.enableGridDashedLine(2f, 7f, 0f);

                    // Setup Y Axis
                    YAxis yAxis = lineChart.getAxisLeft();
                    yAxis.setGranularity(1f);
                    yAxis.enableGridDashedLine(10f, 10f, 0f);

                    lineChart.getAxisRight().setEnabled(false);
                    lineChart.invalidate();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }
    private void getBookingData(String key, String value, ArrayList<String> labelList) {
        Map<String,String> param = new HashMap<>();
        param.put(key,value);
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));
        Log.e("BookingDataParam",""+param);

        new PostMethod(bookingApi,param,getContext()).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    Log.e("BookingDataResp",""+data);
                    ArrayList<BarEntry> barEntries = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(data);
                    noOfBookings.setText(" : "+jsonObject.optString("total"));
                    JSONArray dataArray = jsonObject.optJSONArray("data");
                    for (int i=0; i<dataArray.length(); i++){

                        barEntries.add(new BarEntry(dataArray.optInt(i), i));
                    }
                    BarDataSet barDataSet = new BarDataSet(barEntries, null);
                    BarData barData = new BarData(labelList,barDataSet);
                    bookingsBarChart.setData(barData);

                    barDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
                    barDataSet.setValueTextColor(Color.BLACK);
                    barDataSet.setValueTextSize(18f);
                    barDataSet.setDrawValues(false);

                    bookingsBarChart.getAxisRight().setEnabled(false);
                    bookingsBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                    bookingsBarChart.animateY(1000);
                    bookingsBarChart.getXAxis().setAvoidFirstLastClipping(true);
                    bookingsBarChart.setDescription(null);
                    bookingsBarChart.getLegend().setEnabled(false);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }
    private void getEmployeesData(String key, String value, ArrayList<String> labelList) {
        Map<String,String> param = new HashMap<>();
        param.put(key,value);
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));
        Log.e("EmpDataParam",""+param);

        new PostMethod(employeeApi,param,getContext()).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.optJSONArray("data");
                    numberOfEmp.setText(" : "+jsonObject.optString("total"));
                    for (int i=0; i<dataArray.length(); i++){

                        barEntries.add(new BarEntry(dataArray.optInt(i), i));
                    }
                    barDataSet = new BarDataSet(barEntries, null);
                    barData = new BarData(labelList,barDataSet);
                    employeesBarChart.setData(barData);

                    barDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
                    barDataSet.setValueTextColor(Color.BLACK);
                    barDataSet.setValueTextSize(18f);
                    barDataSet.setDrawValues(false);

                    employeesBarChart.getAxisRight().setEnabled(false);
                    employeesBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                    employeesBarChart.animateY(1000);
                    employeesBarChart.getXAxis().setAvoidFirstLastClipping(true);
                    employeesBarChart.setDescription(null);
                    employeesBarChart.getLegend().setEnabled(false);
                }catch (Exception e){
                    e.printStackTrace();
                }
                Log.e("EmpDataResp",""+data);
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });

    }
}

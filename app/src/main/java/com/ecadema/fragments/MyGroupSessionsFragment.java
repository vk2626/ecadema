package com.ecadema.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.VerticalSpaceItemDecoration;
import com.ecadema.app.R;
import com.ecadema.adapter.MyPastAdapter;
import com.ecadema.adapter.MyUpcomingAdapter;
import com.ecadema.modal.PastModal;
import com.ecadema.modal.UpcomingModal;

import java.util.ArrayList;

public class MyGroupSessionsFragment extends Fragment {

    RecyclerView upcomingRecyclerView,pastRecyclerView;
    MyUpcomingAdapter upcomingAdapter;
    MyPastAdapter pastAdapter;

    ArrayList<UpcomingModal> upcomingModalArrayList=new ArrayList<>();
    ArrayList<PastModal> pastModalArrayList=new ArrayList<>();

    Context context;
    SharedPreferences sharedPreferences;
    TextView upcoming,past,noPastText,noUpcomingText;

    RelativeLayout upcomingSessionView,pastSessionView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.my_group_session_frag, container, false);

        context=getActivity();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

        upcomingSessionView= root.findViewById(R.id.upcomingSessionView);
        pastSessionView= root.findViewById(R.id.pastSessionView);
        noUpcomingText= root.findViewById(R.id.noUpcomingText);
        noPastText= root.findViewById(R.id.noPastText);
        upcoming= root.findViewById(R.id.upcoming);
        past= root.findViewById(R.id.past);

        upcomingRecyclerView= root.findViewById(R.id.upcomingRecyclerView);
        pastRecyclerView= root.findViewById(R.id.pastRecyclerView);

        upcomingRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        pastRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        VerticalSpaceItemDecoration verticalSpaceItemDecoration=new VerticalSpaceItemDecoration(0,0,30,10);
        upcomingRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(0,0,20,10));
        pastRecyclerView.addItemDecoration(verticalSpaceItemDecoration);

        upcomingModalArrayList.clear();
        for (int ar=0;ar<5;ar++){
            UpcomingModal upcomingModal=new UpcomingModal();
            upcomingModal.setID(""+ar);
            upcomingModal.setImage("");
            upcomingModal.setName("Name "+(ar+1));
            upcomingModal.setSessionType("Session Type "+(ar+1));
            upcomingModal.setAbout("About "+(ar+1));
            upcomingModal.setTicket("$ "+(ar+1)+"0");

            upcomingModalArrayList.add(upcomingModal);
        }
        /*upcomingAdapter=new MyUpcomingAdapter(context,upcomingModalArrayList, MyClassesFragment.this);
        upcomingRecyclerView.setAdapter(upcomingAdapter);*/

        upcoming.setOnClickListener(view -> {
            upcoming.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            past.setTextColor(context.getResources().getColor(R.color.dark_grey));
            pastSessionView.setVisibility(View.GONE);
            upcomingSessionView.setVisibility(View.VISIBLE);
        });

        pastModalArrayList.clear();
        for (int ar=0;ar<5;ar++){
            PastModal pastModal=new PastModal();
            pastModal.setID(""+ar);
            pastModal.setImage("");
            pastModal.setName("Name "+(ar+1));
            pastModal.setSessionType("Session Type "+(ar+1));
            pastModal.setAbout("About "+(ar+1));
            pastModal.setTicket("$ "+(ar+1)+"0");

            pastModalArrayList.add(pastModal);
        }
        pastAdapter=new MyPastAdapter(context,pastModalArrayList);
        pastRecyclerView.setAdapter(pastAdapter);
        past.setOnClickListener(view -> {
            past.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            upcoming.setTextColor(context.getResources().getColor(R.color.dark_grey));
            pastSessionView.setVisibility(View.VISIBLE);
            upcomingSessionView.setVisibility(View.GONE);
        });

        return root;
    }

}

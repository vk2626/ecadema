package com.ecadema.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.ecadema.EmojiFilter1;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.JsonPostRequest;
import com.ecadema.apiMethods.JsonResponseData;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.app.StripePaymentActivity;
import com.ecadema.modal.TeammateModal;
import com.google.firebase.installations.FirebaseInstallations;
import com.stripe.android.PaymentConfiguration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class CheckOut extends Fragment {

    Context context;
    SharedPreferences sharedPreferences;
    String trans_id,total_price;
    CardView payPal,stripe;
    public static final int PAYPAL_REQUEST_CODE = 123;
    CircleImageView profile_pic;
    TextView name,totalPrice,item,quantity,price,totalprice,apply,creditAmount,discountedAmount,creditBalance,payableAmount;
    CardView picCard,ec;
    EditText promocode;
    ImageView close;

    LinearLayout itemViewLayout,priceLayout,mainLayout,cbLayout;

    //Paypal Configuration Object
    //PayPalConfiguration config ;
    TimeZone tz = TimeZone.getDefault();
    CheckBox checkbox;
    ProgressDialog progressDialog;
    LinearLayout linear,discountLayout,promoLayout;
    CardView creditCheckout;
    double discountedValue=0;
    String stripeTempAmount="",stripeCustomerId="Payment Done";
    String stripeServerKey;
    String deviceToken="",transaction_info="";

    AutoCompleteTextView autoComplete;
    LinearLayout teammateLayout,parentLayout;
    CircleImageView profile_image;
    TextView teammateName;
    ArrayList<String> nameArrayList;
    ArrayList<TeammateModal> teammateModalArrayList;
    //PaymentButton payPalButton;
    //AsyncHttpClient client;
    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_checkout, container, false);

        context= getContext();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        ((MainActivity)context).showBottomMenu();

        progressDialog = new ProgressDialog(context);

        //stripeServerKey = Api.testStripeKey; /** Testing Sever **/
        stripeServerKey = Api.liveStripeKey; /** Live Sever **/

        PaymentConfiguration.init(context, stripeServerKey);

        parentLayout=root.findViewById(R.id.parentLayout);
        teammateLayout=root.findViewById(R.id.teammateLayout);
        teammateName=root.findViewById(R.id.teammateName);
        profile_image=root.findViewById(R.id.profile_image);
        autoComplete=root.findViewById(R.id.autoComplete);
        stripe=root.findViewById(R.id.stripe);

        promoLayout=root.findViewById(R.id.promoLayout);
        discountedAmount=root.findViewById(R.id.discountedAmount);
        discountLayout=root.findViewById(R.id.discountLayout);

        close=root.findViewById(R.id.close);
        promocode=root.findViewById(R.id.promocode);
        promocode.setFilters(EmojiFilter1.getFilter());

        apply=root.findViewById(R.id.apply);
        mainLayout=root.findViewById(R.id.mainLayout);
        creditCheckout=root.findViewById(R.id.creditCheckout);
        profile_pic=root.findViewById(R.id.profile_pic);
        item=root.findViewById(R.id.item);
        checkbox=root.findViewById(R.id.checkbox);
        linear=root.findViewById(R.id.linear);
        quantity=root.findViewById(R.id.quantity);
        price=root.findViewById(R.id.price);
        itemViewLayout=root.findViewById(R.id.itemViewLayout);
        priceLayout=root.findViewById(R.id.priceLayout);
        payPal=root.findViewById(R.id.payPal);
        name=root.findViewById(R.id.name);
        picCard=root.findViewById(R.id.picCard);
        ec=root.findViewById(R.id.ec);
        totalPrice=root.findViewById(R.id.totalPrice);
        totalprice=root.findViewById(R.id.totalprice);
        creditAmount=root.findViewById(R.id.creditAmount);
        creditBalance=root.findViewById(R.id.creditBalance);
        payableAmount=root.findViewById(R.id.payableAmount);
        cbLayout=root.findViewById(R.id.cbLayout);

        trans_id = getArguments().getString("trans_id");
        total_price = getArguments().getString("price");
        Log.e("trans_id",trans_id);

        apply.setOnClickListener(v->{
            if(apply.getText().toString().equalsIgnoreCase(getResources().getString(R.string.apply))){
                if(promocode.getText().toString().trim().matches("")){
                    promocode.setError(getResources().getString(R.string.pleaseEnterPromocode));
                    return;
                }
                applyPromocode();
            }
            else {
                promocode.setText("");
                promocode.setFocusableInTouchMode(true);
                apply.setText(getResources().getString(R.string.apply));
                discountedValue=0;
                discountLayout.setVisibility(View.GONE);
                discountedAmount.setText("");

                if(checkbox.isChecked()){
                    if(getArguments().getString("walletAmount").equalsIgnoreCase("0")){
                        creditAmount.setText("$ 0");
                        payableAmount.setText("$ "+totalprice.getText().toString().replace("$ ","").replace(",",""));
                        return;
                    }
                    if(Double.parseDouble(getArguments().getString("walletAmount"))
                        >Double.parseDouble(totalprice.getText().toString().replace("$ ","").replace(",",""))){
                        creditCheckout.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        payPal.setVisibility(View.GONE);
                        creditAmount.setText(String.format("- $ %.2f", Double.parseDouble(getArguments().getString("walletAmount"))));
                        payableAmount.setText("$ 0");
                    }
                    else {
                        creditAmount.setText(String.format("- $ %.2f", Double.parseDouble(getArguments().getString("walletAmount"))));

                        payableAmount.setText("$ "+(Double.parseDouble(totalprice.getText().toString().replace("$ ","").replace(",",""))
                                - Double.parseDouble(creditAmount.getText().toString().replace("- $ ",""))));
                    }
                }
            }
        });
        checkbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b){
                linear.setVisibility(View.VISIBLE);
                if(getArguments().getString("walletAmount").equalsIgnoreCase("0")){
                    creditAmount.setText("$ 0");
                    payableAmount.setText("$ "+totalprice.getText().toString().replace("$ ",""));
                    return;
                }

                if(Double.parseDouble(getArguments().getString("walletAmount"))
                        >Double.parseDouble(totalprice.getText().toString().replace("$ ","").replace(",",""))){
                    creditCheckout.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    stripe.setVisibility(View.GONE);
                    payPal.setVisibility(View.GONE);
                    if(discountedValue>0){
                        apply.setText(getResources().getString(R.string.remove));
                        promocode.setFocusable(false);
                        discountedAmount.setText("- $ "+discountedValue);
                        discountLayout.setVisibility(View.VISIBLE);
                        creditAmount.setText("- $ "+(Double.parseDouble(getArguments().getString("walletAmount"))-discountedValue));
                    }
                    else
                        creditAmount.setText("- $ "+totalprice.getText().toString().replace("$ ","").replace(",",""));
                        //creditAmount.setText("- $ "+Double.parseDouble(getArguments().getString("walletAmount")));

                    payableAmount.setText("$ 0");
                }
                else {
                    if(discountedValue>0){
                        apply.setText(getResources().getString(R.string.remove));
                        discountedAmount.setText("- $ "+discountedValue);
                        promocode.setFocusable(false);
                        discountLayout.setVisibility(View.VISIBLE);
                        creditAmount.setText("- $ "+(Double.parseDouble(getArguments().getString("walletAmount"))-discountedValue));
                    }
                    else
                        creditAmount.setText("- $ "+totalprice.getText().toString().replace("$ ","").replace(",",""));
                        //creditAmount.setText("- $ "+Double.parseDouble(getArguments().getString("walletAmount")));

                    payableAmount.setText("$ "+(Double.parseDouble(totalprice.getText().toString().replace("$ ","").replace(",",""))
                            - Double.parseDouble(creditAmount.getText().toString().replace("- $ ",""))));
                }

                if (sharedPreferences.getString("user_type","").equalsIgnoreCase("2")){
                    if (autoComplete.getText().toString().trim().matches("")){
                        creditCheckout.setCardBackgroundColor(getResources().getColor(R.color.fade));
                        stripe.setVisibility(View.GONE);
                        payPal.setVisibility(View.GONE);
                    }

                }
            }
            else {
                creditCheckout.setCardBackgroundColor(getResources().getColor(R.color.fade));
                linear.setVisibility(View.GONE);
                if (sharedPreferences.getString("user_type","").equalsIgnoreCase("2")){
                    if (autoComplete.getText().toString().trim().matches("")){
                        creditCheckout.setCardBackgroundColor(getResources().getColor(R.color.fade));
                        stripe.setVisibility(View.GONE);
                        payPal.setVisibility(View.GONE);
                        return;
                    }
                    stripe.setVisibility(View.VISIBLE);

                }
                stripe.setVisibility(View.VISIBLE);
                //payPal.setVisibility(View.VISIBLE);
            }
        });

        try {
            if (getArguments().getString("from").equalsIgnoreCase("credit")){
                name.setText(context.getResources().getString(R.string.ecademaCredit));
                totalPrice.setText("$ "+getArguments().getString("price"));
                picCard.setVisibility(View.GONE);
                itemViewLayout.setVisibility(View.GONE);
                cbLayout.setVisibility(View.GONE);
                ec.setVisibility(View.VISIBLE);
                priceLayout.setVisibility(View.VISIBLE);
            }
            else {

                if (getArguments().getString("sessionType").equalsIgnoreCase(getResources().getString(R.string.oneToOneType)))
                    promoLayout.setVisibility(View.GONE);
                else
                    promoLayout.setVisibility(View.VISIBLE);

                creditBalance.setText(getResources().getString(R.string.availableFor)+" $ "+getArguments().getString("walletAmount"));
                name.setText(getArguments().getString("name"));
                totalprice.setText("$ "+getArguments().getString("price"));
                price.setText("$ "+getArguments().getString("price"));
                item.setText(getArguments().getString("about")+" ["+getArguments().getString("sessionType")+"]");
                Glide.with(context).load(getArguments().getString("image")).into(profile_pic);

                quantity.setText(""+getArguments().getInt("qty"));
                if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2"))
                    totalprice.setText("$ "+(Float.parseFloat(quantity.getText().toString())*Float.parseFloat(getArguments().getString("payable_amount"))));
                else
                    totalprice.setText("$ "+getArguments().getString("payable_amount"));
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }

        /** PayPal Native code **/
        /*payPalButton= root.findViewById(R.id.payPalButton);
        payPalButton.setup(
            createOrderActions -> {
                String tempAmount="";

                if(checkbox.isChecked()){
                    tempAmount=payableAmount.getText().toString().replace("$ ","");
                }
                else if((getArguments().getString("from").equalsIgnoreCase("credit"))){
                    tempAmount=totalPrice.getText().toString().replace("$ ","");
                }else
                    tempAmount=totalprice.getText().toString().replace("$ ","");

                ArrayList purchaseUnits = new ArrayList<>();
                purchaseUnits.add(
                    new PurchaseUnit.Builder()
                        .amount(
                            new Amount.Builder()
                                .currencyCode(CurrencyCode.USD)
                                .value(tempAmount)
                                .build()
                        )
                        .build()
                );
                Order order = new Order(
                    OrderIntent.CAPTURE,
                    new AppContext.Builder()
                        .userAction(UserAction.PAY_NOW)
                        .build(),
                    purchaseUnits
                );
                createOrderActions.create(order, (CreateOrderActions.OnOrderCreated) null);
                //mainLayout.setVisibility(View.GONE);
                progressDialog.setMessage(context.getResources().getString(R.string.processing));
                progressDialog.show();
                progressDialog.setCancelable(false);
            },

            approval -> approval.getOrderActions().capture(result -> {
                transaction_info=result.toString();

                if (getArguments().getString("from").equalsIgnoreCase("credit")){
                    updatePayment(Api.WalletCheckOut);
                }else {
                    if(getArguments().getString("sessionType").equalsIgnoreCase(context.getResources().getString(R.string.workshop))
                        || getArguments().getString("sessionType").equalsIgnoreCase(context.getResources().getString(R.string.Course)))
                        updatePayment(Api.CheckOutWorkshop);
                    else
                        updatePayment(Api.OneToOneCheckOut);
                }
                Log.i("CaptureOrder", String.format("CaptureOrderResult: %s", result));
            }),
            () -> {
                progressDialog.dismiss();
                mainLayout.setVisibility(View.VISIBLE);
                Log.d("OnCancel", "Buyer cancelled the PayPal experience.");
            },
            errorInfo -> {
                progressDialog.dismiss();
                mainLayout.setVisibility(View.VISIBLE);
                Log.d("OnError", String.format("Error: %s", errorInfo));
            }
        );*/

        /** ============= END Native PayPal Code================ **/

        /** PayPal old code **/
        /*config = new PayPalConfiguration()
                // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                // or live (ENVIRONMENT_PRODUCTION)
                .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                .clientId(PayPalConfig.PAYPAL_CLIENT_ID_SandBox);

        Intent intent = new Intent(context, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        context.startService(intent);*/
        /** ============= END ================ **/

        payPal.setVisibility(View.GONE);
        payPal.setOnClickListener(v->{

            /** Braintree Code **/

            /*DropInRequest dropInRequest = new DropInRequest().clientToken("sandbox_38gfwdr8_96bq8pdbs6xq5n27");
            startActivityForResult(dropInRequest.getIntent((MainActivity)context), 512);*/

            /** =================END================ **/

            //myTokenizePayPalAccountWithCheckoutMethod();

            //getClientToken();

            /** PayPal Code **/
            /*String tempAmount="";

            if(checkbox.isChecked()){
                tempAmount=payableAmount.getText().toString().replace("$ ","");
            }
            else if((getArguments().getString("from").equalsIgnoreCase("credit"))){
                tempAmount=totalPrice.getText().toString().replace("$ ","");
            }else
                tempAmount=totalprice.getText().toString().replace("$ ","");

            PayPalPayment payment = new PayPalPayment(new BigDecimal(tempAmount), "USD",
                    context.getResources().getString(R.string.app_name), PayPalPayment.PAYMENT_INTENT_SALE);

            //Creating Paypal Payment activity intent
            Intent intent1 = new Intent(context, PaymentActivity.class);

            //putting the paypal configuration to the intent
            intent1.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

            //Puting paypal payment to the intent
            intent1.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

            //Starting the intent activity for result
            //the request code will be used on the method onActivityResult
            startActivityForResult(intent1, PAYPAL_REQUEST_CODE);*/
            /** =================END================ **/
        });

        stripe.setOnClickListener(v->{
            if(checkbox.isChecked()){
                stripeTempAmount=payableAmount.getText().toString().replace("$ ","");
            }
            else if((getArguments().getString("from").equalsIgnoreCase("credit"))){
                stripeTempAmount=totalPrice.getText().toString().replace("$ ","");
            }else
                stripeTempAmount=totalprice.getText().toString().replace("$ ","");

            Bundle intent1=new Bundle();
            intent1.putString("amount",stripeTempAmount);

            if((getArguments().getString("from").equalsIgnoreCase("credit"))){
                intent1.putString("totalAmount",totalPrice.getText().toString().replace("$ ",""));
            }else
                intent1.putString("totalAmount",totalprice.getText().toString().replace("$ ",""));

            intent1.putString("from",getArguments().getString("from"));
            intent1.putString("sessionType",getArguments().getString("sessionType"));
            intent1.putString("trans_id",trans_id);
            intent1.putString("timezone",""+tz.getID());
            intent1.putString("teacher_id",getArguments().getString("trainerID"));

            if(checkbox.isChecked())
                intent1.putString("pay_by_wallet","pay_by_wallet");
            else
                intent1.putString("pay_by_wallet","");
            if(discountedValue>0)
                intent1.putString("amount_modify",""+(Double.parseDouble(totalprice.getText().toString().replace("$ ",""))-discountedValue));
            else
                intent1.putString("amount_modify",totalprice.getText().toString().replace("$ ",""));

            StripePaymentActivity stripePaymentActivity=new StripePaymentActivity();
            stripePaymentActivity.setArguments(intent1);
            ((MainActivity) context).addFragment2(stripePaymentActivity, true, false, 0);

            //getStripKey(Math.round(temp));
        });
        creditCheckout.setOnClickListener(v->{
            if(checkbox.isChecked()){
                if (getArguments().getString("from").equalsIgnoreCase("credit")){
                    return;
                }
                if (sharedPreferences.getString("user_type","").equalsIgnoreCase("2")){
                    if (autoComplete.getText().toString().trim().matches("")){
                       return;
                    }
                }
                if(Double.parseDouble(payableAmount.getText().toString().replace("$ ",""))==0){
                    if(Double.parseDouble(creditAmount.getText().toString().replace("- $ ",""))>0
                        && Double.parseDouble(payableAmount.getText().toString().replace("$ ",""))==0){
                        if(getArguments().getString("sessionType").equalsIgnoreCase(context.getResources().getString(R.string.workshop))
                                || getArguments().getString("sessionType").equalsIgnoreCase(context.getResources().getString(R.string.Course)))
                            updatePayment(Api.CheckOutWorkshop);
                        else
                            updatePayment(Api.OneToOneCheckOut);

                    // walletCheckout();
                    }
                }else
                    Toast.makeText(context, getResources().getString(R.string.dontHaveSufficient), Toast.LENGTH_SHORT).show();
            }
        });

        close.setOnClickListener(v-> getParentFragmentManager().popBackStackImmediate());

        if (sharedPreferences.getString("user_type","").equalsIgnoreCase("2")){
            stripe.setVisibility(View.GONE);
            payPal.setVisibility(View.GONE);
            teammateLayout.setVisibility(View.VISIBLE);

            autoComplete.setOnTouchListener((view, motionEvent) -> {
                if(motionEvent.getAction()== MotionEvent.ACTION_DOWN)
                    autoComplete.showDropDown();
                return true;
            });

            autoComplete.setOnItemClickListener((parent, view, i, id) -> {
                Glide.with(context).load(teammateModalArrayList.get(i).getImage()).into(profile_image);
                teammateName.setText(teammateModalArrayList.get(i).getName());
                updateUserDetail(teammateModalArrayList.get(i).getId());
                parentLayout.setVisibility(View.VISIBLE);
                if (checkbox.isChecked()) {
                    creditCheckout.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    stripe.setVisibility(View.GONE);
                }else {
                    stripe.setVisibility(View.VISIBLE);
                    creditCheckout.setCardBackgroundColor(getResources().getColor(R.color.fade));
                }
                //payPal.setVisibility(View.VISIBLE);
            });
            getTeamMates();
        }
        if (ContextCompat.checkSelfPermission(getActivity(),
            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
            // REQUEST_CODE_LOCATION should be defined on your app level
            ActivityCompat.requestPermissions(getActivity(), permissions, 100);
        }
        return root;
    }

    private void updateUserDetail(String teammateId) {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("user_id",teammateId);
        param.put("trans_id",trans_id);

        new PostMethod(Api.updateCorporateUserId,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("updateRes",data);
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void getTeamMates() {

        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("user_id",sharedPreferences.getString("userID",""));

        new PostMethod(Api.Teammates,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("Teammates",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        teammateModalArrayList=new ArrayList<>();
                        nameArrayList=new ArrayList<>();
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        for(int ar=0;ar<jsonArray.length();ar++){
                            JSONObject response = jsonArray.optJSONObject(ar);
                            TeammateModal teammateModal=new TeammateModal();
                            teammateModal.setId(response.optString("user_id"));
                            teammateModal.setTeammateId(response.optString("id"));
                            teammateModal.setImage(response.optString("profile_image_url"));
                            teammateModal.setName(response.optString("name"));
                            teammateModal.setMobile(response.optString("contact_no"));
                            teammateModal.setEmail(response.optString("email"));
                            teammateModal.setCourse(response.optJSONArray("courses"));
                            teammateModal.setUserType(response.optString("user_type"));
                            nameArrayList.add(response.optString("name")+" ("+ response.optString("email")+")");
                            teammateModalArrayList.add(teammateModal);
                        }
                        autoComplete.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_dropdown_item_1line, nameArrayList));

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    public static int getScreenWidth(Context context) {
        Point size = new Point();
        ((MainActivity) context).getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 100 && grantResults.length > 0
            && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            throw new RuntimeException("Location services are required in order to " +
                "connect to a reader.");
        }
    }
    private void walletCheckout() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param = new HashMap<>();
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("trans_id",trans_id);
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("amount",totalprice.getText().toString().replace("$ ",""));

        Log.e("walletCheckParam",""+param);
        new PostMethod(Api.CheckOutWallet,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("walletCheckOut",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        Toast.makeText(context, getResources().getString(R.string.bookingSuccesfully), Toast.LENGTH_SHORT).show();
                        ((MainActivity)context).bottom.setSelectedItemId(R.id.navigation_classes);
                        ((MainActivity)context).showBottomMenu();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
            }
        });
    }

    private void applyPromocode() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param = new HashMap<>();
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("promocode",promocode.getText().toString());
        param.put("trans_id",trans_id);
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("amount",totalprice.getText().toString().replace("$ ",""));

        Log.e("promocodeParam",""+param);
        new PostMethod(Api.CheckingPromocode,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("promocodeResponse",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.optBoolean("status")){
                        apply.setText(getResources().getString(R.string.remove));
                        promocode.setFocusable(false);
                        Toast.makeText(context, jsonObject.optJSONObject("message").optString("success"), Toast.LENGTH_SHORT).show();
                        discountedAmount.setText("- $ "+jsonObject.optString("discount_amount"));
                        discountLayout.setVisibility(View.VISIBLE);
                        discountedValue= Double.parseDouble(jsonObject.optString("discount_amount"));

                        if(getArguments().getString("walletAmount").equalsIgnoreCase("0")){
                            creditAmount.setText("$ 0");
                            payableAmount.setText("$ "+totalprice.getText().toString().replace("$ ","").replace(",",""));
                            return;
                        }
                        if(Double.parseDouble(getArguments().getString("walletAmount"))
                                >Double.parseDouble(totalprice.getText().toString().replace("$ ","").replace(",",""))){
                            creditCheckout.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            payPal.setVisibility(View.GONE);
                            if(discountedValue>0){
                                creditAmount.setText("- $ "+(Double.parseDouble(getArguments().getString("walletAmount"))-discountedValue));
                            }
                            else
                                creditAmount.setText("- $ "+Double.parseDouble(getArguments().getString("walletAmount")));

                            payableAmount.setText("$ 0");
                        }
                        else {
                            if(discountedValue>0){
                                creditAmount.setText("- $ "+(Double.parseDouble(getArguments().getString("walletAmount"))-discountedValue));
                            }
                            else
                                creditAmount.setText("- $ "+Double.parseDouble(getArguments().getString("walletAmount")));

                            payableAmount.setText("$ "+(Double.parseDouble(totalprice.getText().toString().replace("$ ","").replace(",",""))
                                    - (Double.parseDouble(getArguments().getString("walletAmount"))-discountedValue)));
                        }
                    }else
                        Toast.makeText(context, jsonObject.optJSONObject("message").optString("failed"), Toast.LENGTH_SHORT).show();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @RequiresApi(api = VERSION_CODES.Q)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //If the result is from paypal
        /** Braintree Code **/
/*
        if (requestCode == 512) {
            if (resultCode == Activity.RESULT_OK) {

                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                String nNonce = result.getPaymentMethodNonce().getNonce();
                String payment_type = result.getPaymentMethodType().getCanonicalName();


                if (nNonce != null && payment_type != null) {
                    System.out.print("Payment done, Send this nonce to server");
                    Log.e("nNonce",nNonce);
                    mainLayout.setVisibility(View.GONE);
                    sendNonce(nNonce);
                    //postNonceToYourServer(nNonce,payment_type,user_id);
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                mainLayout.setVisibility(View.VISIBLE);
                Toast.makeText(context, "Payment cancelled by user, go back to previous activity", Toast.LENGTH_SHORT).show();
                System.out.print("Payment cancelled by user, go back to previous activity");

            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                mainLayout.setVisibility(View.VISIBLE);
                Toast.makeText(context, "Get some unknown error, go back to previous activity",
                    Toast.LENGTH_SHORT).show();
                System.out.print("Get some unknown error, go back to previous activity");
            }
        }
*/
        /** ==============END============= **/

        /** PayPal Code **/
        /*if (requestCode == PAYPAL_REQUEST_CODE) {

            //If the result is OK i.e. user has not canceled the payment
            if (resultCode == Activity.RESULT_OK) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);
                        try {
                            JSONObject jsonDetails = new JSONObject(paymentDetails);
                            mainLayout.setVisibility(View.GONE);
                            //progressDialog.showProgress(context.getResources().getString(R.string.processing));
                            if (getArguments().getString("from").equalsIgnoreCase("credit")){
                                updatePayment(Api.WalletCheckOut);
                            }else {
                                if(getArguments().getString("sessionType").equalsIgnoreCase(context.getResources().getString(R.string.workshop))
                                    || getArguments().getString("sessionType").equalsIgnoreCase(context.getResources().getString(R.string.Course)))
                                    updatePayment(Api.CheckOutWorkshop);
                                else
                                    updatePayment(Api.OneToOneCheckOut);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }*/
        /** ==============END============= **/

    }

    private void sendNonce(final String nNonce) {
        FirebaseInstallations.getInstance().getToken(true).addOnSuccessListener(
            installationTokenResult -> {
                deviceToken=installationTokenResult.getToken();
            });

        try {
            JSONObject param = new JSONObject();
            param.put("paymentMethodNonce",nNonce);
            param.put("deviceData",deviceToken);

            if(checkbox.isChecked()){
                param.put("amount",payableAmount.getText().toString().replace("$ ",""));
            }
            else if((getArguments().getString("from").equalsIgnoreCase("credit"))){
                param.put("amount",totalPrice.getText().toString().replace("$ ",""));
            }else
                param.put("amount",totalprice.getText().toString().replace("$ ",""));

            Log.e("nonceParam",""+param);
            new JsonPostRequest(Api.sendNonceKey,param,context).startPostMethod(new JsonResponseData() {
                @Override
                public void responseObject(final JSONObject response) {
                    Log.e("OverrideResp",""+response);
                    if (response.optBoolean("status")){
                        //progressDialog.showProgress(context.getResources().getString(R.string.processing));
                        if (getArguments().getString("from").equalsIgnoreCase("credit")){
                            updatePayment(Api.WalletCheckOut);
                        }else {
                            if(getArguments().getString("sessionType").equalsIgnoreCase(context.getResources().getString(R.string.workshop))
                                || getArguments().getString("sessionType").equalsIgnoreCase(context.getResources().getString(R.string.Course)))
                                updatePayment(Api.CheckOutWorkshop);
                            else
                                updatePayment(Api.OneToOneCheckOut);
                        }
                    }

                }

                @Override
                public void error(final VolleyError error) {
                    error.printStackTrace();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void updatePayment(String api) {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param = new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("trans_id",trans_id);
        param.put("transaction_info",transaction_info);
        if (getArguments().getString("from").equalsIgnoreCase("booking")){
            param.put("user_id",sharedPreferences.getString("userID",""));
            param.put("timezone",tz.getID());
            if(api.contains("OneToOneCheckOut")){
                param.put("teacher_id",getArguments().getString("trainerID"));
            }
            param.put("timezone",tz.getID());
            if(checkbox.isChecked())
                param.put("pay_by_wallet","pay_by_wallet");
            else
                param.put("pay_by_wallet","");

            if((getArguments().getString("from").equalsIgnoreCase("credit"))){
                param.put("amount",totalPrice.getText().toString().replace("$ ",""));
            }else
                param.put("amount",totalprice.getText().toString().replace("$ ",""));

            if(discountedValue>0)
                param.put("amount_modify",""+(Double.parseDouble(totalprice.getText().toString().replace("$ ",""))-discountedValue));
            else
                param.put("amount_modify",totalprice.getText().toString().replace("$ ",""));

        }

        Log.e("updatePaymentParam",""+param);
        new PostMethod(api,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("updatePayment",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        if (getArguments().getString("from").equalsIgnoreCase("booking")){
                            Toast.makeText(context, getResources().getString(R.string.bookingSuccesfully), Toast.LENGTH_SHORT).show();
                            ((MainActivity)context).bottom.setSelectedItemId(R.id.navigation_classes);
                            ((MainActivity)context).showBottomMenu();
                        }
                        else {
                            getParentFragmentManager().popBackStackImmediate();
                            Toast.makeText(context, getResources().getString(R.string.creditRefill), Toast.LENGTH_SHORT).show();
                        }
                    }else
                        Toast.makeText(context,jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(performAction, new IntentFilter("performAction"));
        /*braintreeClient = new BraintreeClient(context, "sandbox_38gfwdr8_96bq8pdbs6xq5n27");
        payPalClient = new PayPalClient(braintreeClient);

        BrowserSwitchResult browserSwitchResult = braintreeClient.deliverBrowserSwitchResult((MainActivity)context);
        if (browserSwitchResult != null) {
            payPalClient.onBrowserSwitchResult(browserSwitchResult, (payPalAccountNonce, error) -> {
                // send paypalAccountNonce.getString() to server
                Log.e("payPalAccountNonce",""+payPalAccountNonce);
            });
        }*/
    }

    BroadcastReceiver performAction = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (intent.getStringExtra("to").equalsIgnoreCase("navigationClass")){
                ((MainActivity)context).bottom.setSelectedItemId(R.id.navigation_classes);
                ((MainActivity)context).showBottomMenu();
            }
            else if (intent.getStringExtra("to").equalsIgnoreCase("popUp")){
                ((MainActivity)context).onBackPressed();
            }
        }
    };
}

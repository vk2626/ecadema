package com.ecadema.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.Utility;
import com.ecadema.VerticalSpaceItemDecoration;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.adapter.CalendarAdapter;
import com.ecadema.adapter.TextAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.modal.TimeSlotModal;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;

public class CalendarFragment extends Fragment {

    TextView bookNow,noTimeSlot;
    Context context;
    SharedPreferences sharedPreferences;
    HorizontalCalendar horizontalCalendar;
    TextView tvdate;
    GridView gridview;
    TextAdapter textAdapter;
    String dayOfTheWeek,trainerID,courseID,selectedDate,from,bookingSlotID,bookingID;
    ProgressDialog progressDialog;
    ArrayList<TimeSlotModal> timeSlotModalArrayList = new ArrayList<>();
    ArrayList<String> selectedTimeDate = new ArrayList<>();
    RecyclerView recyclerView;
    ImageView close;

    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat format5 = new SimpleDateFormat("EEE, dd MMM, yyyy");
    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");

    @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat1 = new SimpleDateFormat("yyyy-MM-dd");
    @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a");
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_calendar, container, false);

        context = getContext();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        progressDialog = new ProgressDialog(context);
        ((MainActivity) context).hideBottomMenu();

        courseID=getArguments().getString("courseID");
        trainerID=getArguments().getString("trainerID");
        from=getArguments().getString("from");
        bookingID=getArguments().getString("bookingID");
        bookingSlotID=getArguments().getString("bookingSlotID");

        close=root.findViewById(R.id.close);
        recyclerView=root.findViewById(R.id.recyclerView);
        bookNow=root.findViewById(R.id.bookNow);
        noTimeSlot=root.findViewById(R.id.noTimeSlot);
        tvdate=root.findViewById(R.id.tvdate);
        gridview = root.findViewById(R.id.gridview);

        int mNoOfColumns = Utility.calculateNoOfColumns(context,120);
        recyclerView.setLayoutManager(new GridLayoutManager(context,mNoOfColumns));
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(0,0,0,15));

        Calendar calendar = Calendar.getInstance();
        /** end after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 3);

        /** start before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, 0);
        horizontalCalendar = new HorizontalCalendar.Builder(root, R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .datesNumberOnScreen(5)
                .dayFormat("EEE")
                .dayNumberFormat("dd")
                .textColor(Color.BLACK, getResources().getColor(R.color.colorAccent))
                .selectedDateBackground(Color.TRANSPARENT)
                .build();

        tvdate.setText(format5.format(calendar.getTime()));
        selectedDate= timeFormat1.format(calendar.getTime());
        dayOfTheWeek = (String) DateFormat.format("EEE", calendar.getTime()); // Thursday
        Log.e("dayOfTheWeek",dayOfTheWeek);
        getTimeSlots(dayOfTheWeek);
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
                tvdate.setText(format5.format(date));
                dayOfTheWeek = (String) DateFormat.format("EEE", date); // Thursday
                Log.e("dayOfTheWeek",dayOfTheWeek);
                selectedDate= timeFormat1.format(date);
                getTimeSlots(dayOfTheWeek);
                // Toast.makeText(getApplicationContext(), DateFormat.getDateInstance().format(date) + " is selected!", Toast.LENGTH_SHORT).show();
            }
        });

        if(!from.equalsIgnoreCase("Booking")){
            bookNow.setText(context.getResources().getString(R.string.rescheduleSession));
        }
        bookNow.setOnClickListener(v1->{
            //Log.e("selectedTimeDate",""+selectedTimeDate);
            if(selectedTimeDate.size()>0)
                saveBooking();
            else
                Snackbar.make(v1,context.getResources().getString(R.string.pleaseSelectTImeSlot),Snackbar.LENGTH_LONG).show();
        });

        close.setOnClickListener(v-> getParentFragmentManager().popBackStackImmediate());
        return root;
    }

    private void saveBooking() {
        String api="";
        String selectedTimeSlots = android.text.TextUtils.join(",", selectedTimeDate);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        /*Param: ,,,bookedslots,,:2020-12-26 09:30:00*/
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("course",getArguments().getString("courseID"));
        param.put("timezone", TimeZone.getDefault().getID());
        param.put("bookedslots",selectedTimeSlots);
        param.put("teacher_id", trainerID);
        param.put("user_id",sharedPreferences.getString("userID",""));

        if (sharedPreferences.getString("user_type","").equalsIgnoreCase("2")){
            param.put("user_types","c");
        }
        if(!from.equalsIgnoreCase("Booking")){
            param.put("booking_id",bookingSlotID);
            param.put("booking_slots_id",bookingSlotID);
            param.put("old_booking_date",getArguments().getString("oldDate"));
            api=Api.TraineeAcceptedOrRescheduleBooking;
        }else
            api=Api.SaveBooking;

        Log.e("SaveBookingPara",api+"\n"+param);

        new PostMethod(api,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();

                Log.e("SaveBookingResp",""+data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        if(!from.equalsIgnoreCase("Booking")){
                            Toast.makeText(context, jsonObject.optString("success"), Toast.LENGTH_SHORT).show();
                            getParentFragmentManager().popBackStackImmediate();
                            return;
                        }
                        Bundle bundle=new Bundle();
                        bundle.putInt("qty",selectedTimeDate.size());
                        bundle.putString("trainerID",trainerID);
                        bundle.putString("sessionType",context.getResources().getString(R.string.oneToOneType));
                        bundle.putString("about",context.getResources().getString(R.string.oneToOne));
                        bundle.putString("name",getArguments().getString("name"));
                        bundle.putString("image",getArguments().getString("imageurl"));
                        bundle.putString("from","booking");
                        bundle.putString("walletAmount",jsonObject.optString("wallet_amount"));
                        bundle.putString("trans_id",jsonObject.optString("booking_code"));
                        bundle.putString("payable_amount",jsonObject.optString("payable_amount"));
                        bundle.putString("price",jsonObject.optString("price"));
                        CheckOut checkOut=new CheckOut();
                        checkOut.setArguments(bundle);
                        FragmentManager fragmentManager=((MainActivity)context).getSupportFragmentManager();
                        FragmentTransaction ft = fragmentManager.beginTransaction();
                        ft.replace(R.id.frag_container, checkOut, checkOut.getClass().getSimpleName());
                        ft.addToBackStack(null);
                        ft.commit();
                        //7((MainActivity) context).addFragment2(checkOut, false, false, 0);
                        //Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();
                error.printStackTrace();
            }
        });
    }

    private void getTimeSlots(String dayOfTheWeek) {

        //lang_token,course_id,timezone,day_availability
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("course_id",getArguments().getString("courseID"));
        param.put("timezone", TimeZone.getDefault().getID());
        param.put("day_availability",dayOfTheWeek);
        param.put("date", selectedDate);
        Log.e("CalPara",""+param);

        new PostMethod(Api.AvailabilityCalendarList,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("calResp",data);
                progressDialog.cancel();
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    timeSlotModalArrayList.clear();
                    if(jsonObject.optBoolean("status")){
                        noTimeSlot.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        for (int ar=0;ar<jsonArray.length();ar++){
                            TimeSlotModal timeSlotModal = new TimeSlotModal();
                            JSONObject object = jsonArray.getJSONObject(ar);
                            timeSlotModal.setID(object.optString("timeslot_id"));
                            timeSlotModal.setTimeSlot(format.format(format.parse(object.optString("start"))));
                            timeSlotModal.setDate(object.optString("timeslot_id")+"##"+timeFormat1.format(format.parse(object.optString("start"))));
                            String start=timeFormat2.format(format.parse(object.optString("start")));
                            String end = timeFormat2.format(format.parse(object.optString("end")));
                            timeSlotModal.setTime(start+" - "+end);
                            timeSlotModalArrayList.add(timeSlotModal);
                        }
                        recyclerView.setAdapter(new CalendarAdapter(context,timeSlotModalArrayList,selectedTimeDate,from));
                        //gridview.setAdapter(new TextAdapter(context,timeSlotModalArrayList,selectedTimeDate,from));

                    }
                    else{
                        noTimeSlot.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(selectedTime,new IntentFilter("timeSlotArray"));
    }

    BroadcastReceiver selectedTime= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(from.equalsIgnoreCase("Booking")){
                if(intent.getStringExtra("type").equalsIgnoreCase("add"))
                    selectedTimeDate.add(intent.getStringExtra("selectedTime"));
                else
                    selectedTimeDate.remove(intent.getStringExtra("selectedTime"));
            }
            else {
                selectedTimeDate.clear();
                selectedTimeDate.add(intent.getStringExtra("selectedTime"));
            }

            if (selectedTimeDate.size()<0) {
                bookNow.setEnabled(false);
                bookNow.setBackgroundColor(getResources().getColor(R.color.fade));
            }
            else {
                bookNow.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                bookNow.setEnabled(true);
            }

        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        context.unregisterReceiver(selectedTime);
    }
}

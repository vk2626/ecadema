package com.ecadema.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.app.R;
import com.ecadema.adapter.EarningAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.modal.EarningModal;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TrainerEarningFragment extends Fragment {

    ScrollView earningScroll;
    RelativeLayout historyLL;
    TextView earning, history,noHistory;
    Context context;
    SharedPreferences sharedPreferences;
    ProgressDialog progressDialog;

    TextView iMade,iTook,stillHave,willMake,withdraw;
    EditText payPalEmail,payoneerEmail,amount;
    LinearLayout payPal,payoneer;
    RecyclerView historyRecyclerView;
    ArrayList<EarningModal>earningModalArrayList=new ArrayList<>();
    String count="-1",paymentType="-1";
    ImageView close;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.earning_fragment, container, false);

        context=getActivity();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        progressDialog = new ProgressDialog(context);

        close=root.findViewById(R.id.close);
        amount=root.findViewById(R.id.amount);
        withdraw=root.findViewById(R.id.withdraw);
        iMade=root.findViewById(R.id.iMade);
        iTook=root.findViewById(R.id.iTook);
        stillHave=root.findViewById(R.id.stillHave);
        willMake=root.findViewById(R.id.willMake);
        payPalEmail=root.findViewById(R.id.payPalEmail);
        payoneerEmail=root.findViewById(R.id.payoneerEmail);
        payoneer=root.findViewById(R.id.payoneer);
        payPal=root.findViewById(R.id.payPal);

        earningScroll=root.findViewById(R.id.earningScroll);
        historyLL=root.findViewById(R.id.historyLL);
        noHistory=root.findViewById(R.id.noHistory);
        earning=root.findViewById(R.id.earning);
        history=root.findViewById(R.id.history);
        historyRecyclerView=root.findViewById(R.id.historyRecyclerView);
        historyRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        getEarningDetails();
        earning.setOnClickListener(v->{
            getEarningDetails();
            historyLL.setVisibility(View.GONE);
            earningScroll.setVisibility(View.VISIBLE);

            earning.setTextColor(Color.WHITE);
            earning.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

            history.setBackgroundColor(context.getResources().getColor(R.color.white));
            history.setTextColor(context.getResources().getColor(R.color.dark_grey));
        });
        history.setOnClickListener(v->{
            getEarningHistory();
            historyLL.setVisibility(View.VISIBLE);
            earningScroll.setVisibility(View.GONE);

            history.setTextColor(Color.WHITE);
            history.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

            earning.setBackgroundColor(context.getResources().getColor(R.color.white));
            earning.setTextColor(context.getResources().getColor(R.color.dark_grey));
        });

        final String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z]+(\\.[A-Za-z]+)*(\\.[A-Za-z]{2,})$";
        payPal.setOnClickListener(v->{
            paymentType="PayPal";
            payPal.setBackground(getResources().getDrawable(R.drawable.bg_round_corner2));
            payoneer.setBackground(getResources().getDrawable(R.drawable.bg_round_corner3));

            if(!count.equalsIgnoreCase("2"))
                payPalEmail.setFocusableInTouchMode(true);

            payoneerEmail.setFocusable(false);
        });
        payoneer.setOnClickListener(v->{
            paymentType="Payoneer";
            payoneer.setBackground(getResources().getDrawable(R.drawable.bg_round_corner2));
            payPal.setBackground(getResources().getDrawable(R.drawable.bg_round_corner3));

            if(!count.equalsIgnoreCase("2"))
                payoneerEmail.setFocusableInTouchMode(true);

            payPalEmail.setFocusable(false);
        });

        withdraw.setOnClickListener(v->{
            if(paymentType.equalsIgnoreCase("-1")){
                Snackbar.make(v,context.getResources().getString(R.string.enterPaymentMethod),Snackbar.LENGTH_LONG).show();
                return;
            }
            if(!count.equalsIgnoreCase("2")){
                if(paymentType.equalsIgnoreCase("payPal")){
                    if (!payPalEmail.getText().toString().matches(emailPattern)) {
                        payPalEmail.setError(context.getResources().getString(R.string.entervalidemail));
                    } else if (payPalEmail.getText().toString().trim().matches("")) {
                        payPalEmail.setError(context.getResources().getString(R.string.fieldCannotBeEmpty));
                    }
                    return;
                }
                if(paymentType.equalsIgnoreCase("payoneer")){
                    if (!payoneerEmail.getText().toString().matches(emailPattern)) {
                        payoneerEmail.setError(context.getResources().getString(R.string.entervalidemail));
                    } else if (payoneerEmail.getText().toString().trim().matches("")) {
                        payoneerEmail.setError(context.getResources().getString(R.string.fieldCannotBeEmpty));
                    }
                    return;
                }
            }

            if(amount.getText().toString().trim().matches("")){
                amount.setError(context.getResources().getString(R.string.fieldRequired));
                return;
            }
            if(Integer.parseInt(amount.getText().toString())>Integer.parseInt(stillHave.getText().toString().replace("$ ",""))){
                amount.setError(getResources().getString(R.string.amountNotBeGreaterThen));
                return;
            }
            if(Integer.parseInt(amount.getText().toString())<50){
                amount.setError(getResources().getString(R.string.minimumAmount));
                return;
            }
            if(paymentType.equalsIgnoreCase("PayPal") && payPalEmail.getText().toString().trim().matches("")){
                Snackbar.make(v,getResources().getString(R.string.emailAndPayment),Snackbar.LENGTH_LONG).show();
                return;
            }
            if(paymentType.equalsIgnoreCase("Payoneer") && payoneerEmail.getText().toString().trim().matches("")){
                Snackbar.make(v,getResources().getString(R.string.emailAndPayment),Snackbar.LENGTH_LONG).show();
                return;
            }
            withdrawAmount();
        });

        close.setOnClickListener(v->{
            getParentFragmentManager().popBackStackImmediate();
        });
        return root;
    }

    private void withdrawAmount() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();

        //lang_token,trainer_id,paypal_email,payoneer_email,pay_mode,amount
        Map<String,String> param = new HashMap<>();
        param.put("trainer_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("payoneer_email",payoneerEmail.getText().toString());
        param.put("paypal_email",payPalEmail.getText().toString());
        param.put("pay_mode",paymentType);
        param.put("amount",amount.getText().toString());

        Log.e("param",""+param);

        new PostMethod(Api.Withdraw,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("withdrawResp",data);
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        payPal.setBackground(getResources().getDrawable(R.drawable.bg_round_corner3));
                        payoneer.setBackground(getResources().getDrawable(R.drawable.bg_round_corner3));

                        payoneerEmail.setFocusable(false);
                        payPalEmail.setFocusable(false);
                        amount.setText("");
                    }else {
                        Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    private void getEarningDetails() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param = new HashMap<>();
        param.put("trainer_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));

        new PostMethod(Api.TrainerEarningShow,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("Earning Resp",data);
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        JSONObject response = jsonObject.optJSONObject("data");
                        iMade.setText("$ "+response.optString("I_MADE"));
                        iTook.setText("$ "+response.optString("I_TOOK"));
                        stillHave.setText("$ "+response.optString("STILL_HAVE"));
                        willMake.setText("$ "+response.optString("WILL_MAKE"));

                        JSONObject bankInfo = response.optJSONObject("Bank_Info");
                        count=bankInfo.optString("count");
                        if(bankInfo.optString("count").equalsIgnoreCase("2")){
                            payoneerEmail.setFocusable(false);
                            payPalEmail.setFocusable(false);
                        }
                        if(bankInfo.optString("payoneer_email").equalsIgnoreCase("null") || null==bankInfo.optString("payoneer_email"))
                            payoneerEmail.setText("");
                        else
                            payoneerEmail.setText(bankInfo.optString("payoneer_email"));

                        if(bankInfo.optString("paypal_email").equalsIgnoreCase("null") || null==bankInfo.optString("paypal_email"))
                            payPalEmail.setText("");
                        else
                            payPalEmail.setText(bankInfo.optString("paypal_email"));


                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    private void getEarningHistory() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param = new HashMap<>();
        param.put("trainer_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));

        new PostMethod(Api.TrainerEarningHistory,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("History Resp",data);
                progressDialog.cancel();;
                try {
                    JSONObject response = new JSONObject(data);
                    if(response.optBoolean("status")){
                        JSONArray array = response.optJSONArray("data");
                        earningModalArrayList.clear();
                        for (int ar=0;ar<array.length();ar++){
                            JSONObject object = array.getJSONObject(ar);
                            EarningModal earningModal = new EarningModal();
                            earningModal.setId(object.optString("id"));
                            earningModal.setMethod(object.optString("payment_mode"));
                            earningModal.setStatus(object.optString("status_type"));
                            earningModal.setDateTime(object.optString("created_on"));
                            earningModal.setAmount("$ "+object.optString("payable_amount"));

                            earningModalArrayList.add(earningModal);
                        }
                        historyRecyclerView.setAdapter(new EarningAdapter(context,earningModalArrayList));
                    }
                    else {
                        noHistory.setVisibility(View.VISIBLE);
                        historyRecyclerView.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }
}

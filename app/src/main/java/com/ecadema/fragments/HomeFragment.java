package com.ecadema.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.MyService;
import com.ecadema.adapter.GSAdapter;
import com.ecadema.adapter.TrainerAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.GetMethod;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.modal.GSModal;
import com.ecadema.modal.TrainerModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class HomeFragment extends Fragment {

    RecyclerView groupSessionRecyclerView, trainerRecyclerView;
    GSAdapter gsAdapter;
    TrainerAdapter trainerAdapter;

    ArrayList<GSModal> gsModalArrayList = new ArrayList<>();
    ArrayList<TrainerModal> trainerModalArrayList = new ArrayList<>();

    Context context;
    SharedPreferences sharedPreferences;
    TextView viewAllSessions, viewAllTrainer;
    ProgressDialog progressDialog;

    @RequiresApi(api = Build.VERSION_CODES.M)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        context = getContext();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        setAppLocale(sharedPreferences.getString("lang", ""));

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.text_home);

        progressDialog = new ProgressDialog(context);

        ((MainActivity) context).showBottomMenu();
        groupSessionRecyclerView = root.findViewById(R.id.groupSessionRecyclerView);
        trainerRecyclerView = root.findViewById(R.id.trainerRecyclerView);
        viewAllTrainer = root.findViewById(R.id.viewAllTrainer);
        viewAllSessions = root.findViewById(R.id.viewAllSessions);

        groupSessionRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        trainerRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        groupSessions();

        trainerList();

        viewAllSessions.setOnClickListener(view -> {
            AllSessionFrag allSessionFragment = new AllSessionFrag();
            ((MainActivity) context).addFragment2(allSessionFragment, true, false, 0);
        });

        viewAllTrainer.setOnClickListener(view -> {
            AllTrainerFrag allTrainerFrag = new AllTrainerFrag();
            ((MainActivity) context).addFragment2(allTrainerFrag, true, false, 0);
        });
        if (!sharedPreferences.getString("userID", "").equalsIgnoreCase("-1")){
            if (checkAndRequestPermissions())
                context.startService(new Intent(context, MyService.class));

        }
        return root;
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean checkAndRequestPermissions() {
        int cameraPermission = ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA);
        int gps = ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int writeExternal = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }

        if (gps != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (writeExternal != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 123);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals( android.Manifest.permission.CAMERA)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {}
                    }
                    else if (permissions[i].equals(android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {

                        }
                    }
                    else if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            context.startService(new Intent(context, MyService.class));
                        }
                    }
                }
            }
        }
    }

    private void trainerList() {
        Map<String, String> param = new HashMap<>();
        param.put("lang_token", sharedPreferences.getString("lang", ""));

        new PostMethod(Api.TrainersList, param, context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("Trainer List", data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.optBoolean("status")) {
                        trainerModalArrayList.clear();
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        int len;
                        if (jsonObject.optJSONArray("data").length() > 6)
                            len = 6;
                        else
                            len = jsonObject.optJSONArray("data").length();
                        for (int ar = 0; ar < len; ar++) {
                            JSONObject object = jsonArray.getJSONObject(ar);
                            TrainerModal trainerModal = new TrainerModal();
                            trainerModal.setTeacherID(object.optString("teacher_id"));
                            trainerModal.setImage(object.optString("profile_image"));
                            trainerModal.setName(object.optString("teacher_name"));
                            trainerModal.setRatings(object.optInt("average_rating"));
                            trainerModal.setIsMentor(object.optString("is_mentor"));
                            trainerModal.setRates("$ " + object.optString("price_per_hour") + "/" + context.getResources().getString(R.string.hour));
                            trainerModalArrayList.add(trainerModal);

                        }
                        trainerAdapter = new TrainerAdapter(context, trainerModalArrayList);
                        trainerRecyclerView.setAdapter(trainerAdapter);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void groupSessions() {

        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        new GetMethod(Api.UpGroupSessions + sharedPreferences.getString("lang", ""), getContext()).startmethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    if (progressDialog!=null)
                        progressDialog.cancel();

                    JSONObject jsonObject = new JSONObject(data);
                    Log.e("UpGroupSessionsZ", data);

                    if (jsonObject.optBoolean("status")) {
                        gsModalArrayList.clear();
                        int len;
                        if (jsonObject.optJSONArray("data").length() > 6)
                            len = 6;
                        else
                            len = jsonObject.optJSONArray("data").length();

                        for (int ar = 0; ar < len; ar++) {
                            JSONObject object = jsonObject.optJSONArray("data").getJSONObject(ar);
                            GSModal gsModal = new GSModal();
                            gsModal.setID(object.optString("id"));
                            gsModal.setImage(object.optString("profile_image"));
                            gsModal.setTrainerId(object.optString("teacher_id"));
                            gsModal.setName(object.optString("teacher_name"));
                            gsModal.setSessionType(object.optString("type"));
                            gsModal.setAbout(object.optString("about"));
                            gsModal.setBanner(object.optString("image"));
                            gsModal.setTicket("$ " + object.optString("ticket_cost"));

                            gsModalArrayList.add(gsModal);
                        }
                        gsAdapter = new GSAdapter(context, gsModalArrayList);
                        groupSessionRecyclerView.setAdapter(gsAdapter);

                    }
                } catch (Exception e) {
                    if (progressDialog!=null)
                        progressDialog.cancel();
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                if (progressDialog!=null)
                    progressDialog.cancel();
                error.printStackTrace();
            }
        });
    }

    private void setAppLocale(String LocaleCode) {
        Resources res = getResources();
        DisplayMetrics dr = res.getDisplayMetrics();
        Configuration configuration = res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(new Locale(LocaleCode.toLowerCase()));
        } else {
            configuration.locale = new Locale(LocaleCode.toLowerCase());
        }
        res.updateConfiguration(configuration, dr);
    }

}
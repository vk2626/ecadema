package com.ecadema.fragments;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.ecadema.MultipartUtility;
import com.ecadema.RealPathUtil;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.BuildConfig;
import com.ecadema.app.FilePath;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.modal.CountryModal;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfileFragment extends Fragment {

    Context context;
    SharedPreferences sharedPreferences;
    View root;
    ProgressDialog progressDialog;
    EditText firstName,lastName,contactNumber,email,youTubeURL;
    AutoCompleteTextView country;
    ImageView userImage,close;
    TextView submit,savePic;
    CardView cardView;
    ImageView editProfilePic;
    String countyID="-1";
    ArrayList<CountryModal> countryModalArrayList=new ArrayList<>();
    CircleImageView profile_image;

    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;
    private AlertDialog dialog;
    Uri mImageCaptureUri;
    File photoFile,file;
    public String photoFileName="image_" + System.currentTimeMillis()+ ".jpg",profilePicName="";
    Bitmap thumbnail,rotatedBitmap;
    private Bitmap bitmap;
    ArrayList<String> countryArray=new ArrayList<>();
    RelativeLayout relativeLayout;
    ScrollView mScrollView;
    private String realPath;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.activity_my_profile, container, false);

        context=getActivity();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

        savePic=root.findViewById(R.id.savePic);
        mScrollView=root.findViewById(R.id.scrollView);
        youTubeURL=root.findViewById(R.id.youTubeURL);
        relativeLayout=root.findViewById(R.id.relativeLayout);
        editProfilePic=root.findViewById(R.id.editProfilePic);
        submit=root.findViewById(R.id.submit);
        cardView=root.findViewById(R.id.cardView);
        close=root.findViewById(R.id.close);
        profile_image=root.findViewById(R.id.profile_image);
        userImage=root.findViewById(R.id.userImage);
        firstName=root.findViewById(R.id.firstName);
        lastName=root.findViewById(R.id.lastName);
        contactNumber=root.findViewById(R.id.contactNumber);
        email=root.findViewById(R.id.email);
        country=root.findViewById(R.id.country);
        close.setOnClickListener(v->getParentFragmentManager().popBackStackImmediate());

        captureImageInitialization();
        checkAndRequestPermissions();
        getProfile();

       /* */

        getCountryList();
        submit.setOnClickListener(v->{
            if(submit.getText().toString().equalsIgnoreCase(getResources().getString(R.string.editProfile))){
                submit.setText(context.getResources().getString(R.string.save));
                cardView.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                //editProfilePic.setVisibility(View.VISIBLE);
                firstName.setEnabled(true);
                lastName.setEnabled(true);
                contactNumber.setEnabled(true);
                contactNumber.setCursorVisible(true);
                //email.setEnabled(true);
                country.setEnabled(true);

                firstName.setFocusableInTouchMode(true);
                lastName.setFocusableInTouchMode(true);
                contactNumber.setFocusableInTouchMode(true);
                //email.setFocusableInTouchMode(true);
                country.setFocusableInTouchMode(true);

                firstName.setSelection(firstName.getText().length());
                lastName.setSelection(lastName.getText().length());
                contactNumber.setSelection(contactNumber.getText().length());
                //email.setSelection(email.getText().length());

                country.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, countryArray));
            }else {
                if(firstName.getText().toString().trim().matches("")){
                    firstName.setError(getResources().getString(R.string.fieldRequired));
                    return;
                }
                if(lastName.getText().toString().trim().matches("")){
                    lastName.setError(getResources().getString(R.string.fieldRequired));
                    return;
                }
                if(countyID.equalsIgnoreCase("-1")) {
                    Toast.makeText(context, getResources().getString(R.string.selectountry), Toast.LENGTH_LONG).show();
                    return;
                }
                if(contactNumber.getText().toString().trim().matches("")){
                    contactNumber.setError(getResources().getString(R.string.fieldRequired));
                    return;
                }
                if(contactNumber.getText().toString().trim().length()<10){
                    contactNumber.setError(getResources().getString(R.string.numberMustBe));
                    return;
                }

                updateProfile();

                ArrayList<String> countryArray=new ArrayList<>();
                country.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, countryArray));
            }
        });

        savePic.setOnClickListener(v->{
            updateProfile();

            ArrayList<String> countryArray=new ArrayList<>();
            country.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, countryArray));
        });
        editProfilePic.setOnClickListener(v->{
            if(checkAndRequestPermissions()) {
                dialog.show();
            }
        });

        country.setOnTouchListener((v, event) -> {
            country.showDropDown();
            return true;
        });

        country.setOnItemClickListener((parent, view, position, id) -> {
            String selection = (String) parent.getItemAtPosition(position);
            int pos = -1;
            for (int i = 0; i < countryModalArrayList.size(); i++) {
                if (selection.contains(countryModalArrayList.get(i).getCountryName())) {
                    pos = i;
                    break;
                }
            }
            countyID=countryModalArrayList.get(pos).getId();
            Log.e("countyID",countyID);
        });
        return root;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean checkAndRequestPermissions() {
        int cameraPermission = ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA);
        int gps = ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeExternal = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }

        if (gps != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (writeExternal != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 123);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals( android.Manifest.permission.CAMERA)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {}
                    }
                    else if (permissions[i].equals(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {}
                    }
                    else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {}
                    }
                }
            }
        }
    }

    private void captureImageInitialization() {
        /**
         * a selector dialog to display two image source options, from camera
         * ‘Take from camera’ and from existing files ‘Select from gallery’
         */
        final String[] items = new String[]{getResources().getString(R.string.Takefromcamera),getResources().getString(R.string.Selectfromgallery)};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(getResources().getString(R.string.SelectImage));
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) { // pick from
                // camera
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    photoFile= getPhotoFileUri(photoFileName);
                    mImageCaptureUri= FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,mImageCaptureUri);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    //mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));
                    //intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

                    try {
                        intent.putExtra("return-data", true);
                        startActivityForResult(intent, PICK_FROM_CAMERA);

                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                } else {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_FROM_FILE);
                }
            }
        });

        dialog = builder.create();
    }

    public File getPhotoFileUri(String fileName) {
        File mediaStorageDir = new File(String.valueOf(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)));
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
            Log.d("Error", "failed to create directory");
        }
        // Return the file target for the photo based on filename
        this.file = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return this.file;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode !=RESULT_OK)
            return;

        switch (requestCode) {
            case PICK_FROM_CAMERA:
                //uri=data.getData();
                /**
                 * After taking a picture, do the crop
                 */
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    /*options.inPreferredConfig = Bitmap.Config.RGB_565;
                    options.inSampleSize = 8;*/
                    thumbnail = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(mImageCaptureUri), null, options);
                    //getPicOrientation();
                    CropImage();;

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;

            case PICK_FROM_FILE:
                /**
                 * After selecting image from files, save the selected path
                 */

                mImageCaptureUri = data.getData();
                CropImage();;

                break;

            case UCrop.REQUEST_CROP:
                handleUCropResult(data);
                break;

        }
    }

    private void handleUCropResult(Intent data) {
        if (data == null) {
            setResultCancelled();
            return;
        }
        final Uri resultUri = UCrop.getOutput(data);
        setResultOk(resultUri);
    }

    private void setResultOk(Uri imagePath) {
        Intent intent = new Intent();
        intent.putExtra("path", imagePath);
        ((MainActivity)context).setResult(Activity.RESULT_OK, intent);
        mImageCaptureUri=imagePath;
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(((MainActivity)context).getContentResolver(), imagePath);
            profile_image.setImageBitmap(bitmap);
            savePic.setVisibility(View.VISIBLE);
            editProfilePic.setVisibility(View.GONE);

            try {
                // SDK < API11
                if (Build.VERSION.SDK_INT < 11)
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(context, imagePath);

                    // SDK >= 11 && SDK < 19
                else if (Build.VERSION.SDK_INT < 19)
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(context, imagePath);

                    // SDK > 19 (Android 4.4)
                else
                    realPath = RealPathUtil.getRealPathFromURI_API19(context,imagePath);

                Log.e("Real Path", "" + realPath);

                String PdfPathHolder = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    try {
                        PdfPathHolder = FilePath.getPath(context, mImageCaptureUri);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                assert PdfPathHolder != null;
                if (realPath == null)
                    photoFile = new File(PdfPathHolder);
                else
                    photoFile = new File(realPath);

            }
            catch (Exception e){
                e.printStackTrace();
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void setResultCancelled() {

    }
    private static String queryName(ContentResolver resolver, Uri uri) {
        Cursor returnCursor =
            resolver.query(uri, null, null, null, null);
        assert returnCursor != null;
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        returnCursor.close();
        return name;
    }
    private void CropImage() {
        try {
            Uri destinationUri = Uri.fromFile(new File(((MainActivity)context).getCacheDir(),
                queryName(((MainActivity)context).getContentResolver(), mImageCaptureUri)));
            UCrop.Options options = new UCrop.Options();
            options.setCompressionQuality(80);
            options.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary));
            options.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimary));
            options.setActiveWidgetColor(ContextCompat.getColor(context, R.color.colorPrimary));
            options.withAspectRatio(4, 4);
            options.withMaxResultSize(500, 500);

            UCrop.of(mImageCaptureUri, destinationUri)
                .withOptions(options)
                .start(getActivity());

        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(context, "Your device doesn't support the crop action!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getPicOrientation() {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(String.valueOf(getPhotoFileUri(photoFileName)));
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

            Log.e("orientation",""+orientation);
            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(thumbnail, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(thumbnail, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(thumbnail, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                    rotatedBitmap = rotateImage(thumbnail, 0);
                    break;
                default:
                    rotatedBitmap = thumbnail;

            }
            profile_image.setImageBitmap(rotatedBitmap);
            userImage.setImageBitmap(rotatedBitmap);
            photoFile = new File(context.getCacheDir(), photoFileName);
            photoFile.createNewFile();

//Convert bitmap to byte array
            Bitmap bitmap =rotatedBitmap;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(photoFile);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private void updateProfile() {
        progressDialog.setTitle(getResources().getString(R.string.processing));
        progressDialog.show();
        new Thread(() -> {
            try {
                //first_name,last_name,contact_no,country,trainee_id,profile_photo_edit
                MultipartUtility multipart = new MultipartUtility(Api.TraineeProfileUpdate, "UTF-8");
                multipart.addFormField("first_name", firstName.getText().toString());
                multipart.addFormField("last_name", lastName.getText().toString());
                multipart.addFormField("contact_no", contactNumber.getText().toString());
                multipart.addFormField("country", countyID);
                multipart.addFormField("trainee_id", sharedPreferences.getString("userID",""));
                multipart.addFormField("lang_token", sharedPreferences.getString("lang",""));
                multipart.addFormField("profile_photo_edit", profilePicName);
                if(photoFile!=null) {
                    multipart.addFilePart("profile_photo", photoFile);
                }else
                    multipart.addFormField("profile_photo", "");
                Log.e("photoFile",""+photoFile);

                String response = multipart.finish();// response from server
                Log.e("Multipart Response",response);
                try {
                    final JSONObject jsonObject1 = new JSONObject(response);
                    boolean status = jsonObject1.optBoolean("status");
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();
                        // Stuff that updates the UI
                    });
                    if(status){
                        progressDialog.cancel();
                        ((MainActivity)context).runOnUiThread(() -> {
                            mScrollView.fullScroll(View.FOCUS_UP);
                            cardView.setCardBackgroundColor(getResources().getColor(R.color.dark_grey));
                            submit.setText(context.getResources().getString(R.string.editProfile));
                            firstName.setFocusable(false);
                            lastName.setFocusable(false);
                            contactNumber.setFocusable(false);
                            email.setFocusable(false);
                            country.setFocusable(false);

                            editProfilePic.setVisibility(View.VISIBLE);
                            savePic.setVisibility(View.GONE);

                            sharedPreferences.edit().putString("profile_image",jsonObject1.optJSONObject("data").optString("profile_image_url")).apply();
                            ((MainActivity)context).showProfilePic();
                            Toast.makeText(context, jsonObject1.optString("message"), Toast.LENGTH_SHORT).show();
                            // Stuff that updates the UI
                        });
                    }
                    else {
                        Toast.makeText(context, jsonObject1.optString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();
                        // Stuff that updates the UI
                    });
                    e.printStackTrace();
                }
            } catch (IOException e) {
                ((MainActivity)context).runOnUiThread(() -> {
                    progressDialog.cancel();
                    // Stuff that updates the UI
                });
                e.printStackTrace();
            }
        }).start();

    }

    private void getCountryList() {
        Map<String, String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));

        Log.e("countryParam",""+param.toString());

        new PostMethod(Api.CountriesList,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("countryList",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if (jsonObject.optBoolean("status")) {
                        JSONArray countryList=jsonObject.optJSONArray("data");
                        for (int ar=0;ar<countryList.length();ar++){
                            JSONObject jsonObject1=countryList.getJSONObject(ar);
                            CountryModal countryModal=new CountryModal();
                            countryModal.setId(jsonObject1.optString("id"));
                            countryModal.setCountryName(jsonObject1.optString("country_name"));

                            countryArray.add(jsonObject1.optString("country_name"));
                            countryModalArrayList.add(countryModal);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void getProfile() {
        progressDialog=new ProgressDialog(context);
        progressDialog.setTitle(getResources().getString(R.string.processing));
        progressDialog.show();

        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("trainee_id",sharedPreferences.getString("userID",""));

        Log.e("profileParam",""+param);

        new PostMethod(Api.TraineeProfile,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("ProfileResp",data);
                progressDialog.cancel();
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        JSONObject dataObject=jsonObject.optJSONObject("data");
                        firstName.setText(dataObject.optString("first_name").trim());
                        lastName.setText(dataObject.optString("last_name").trim());
                        if(dataObject.optString("contact_no").equalsIgnoreCase("null"))
                            contactNumber.setText("-");
                        else
                            contactNumber.setText(dataObject.optString("contact_no").trim());

                        if(dataObject.optString("email").equalsIgnoreCase("null"))
                            email.setText("-");
                        else
                            email.setText(dataObject.optString("email").trim());

                        if(dataObject.optString("country_name").equalsIgnoreCase("null"))
                            country.setText("");
                        else
                            country.setText(dataObject.optString("country_name"));

                        countyID=dataObject.getString("country");
                        profilePicName=dataObject.getString("profile_image");

                        Picasso.with(context).load(dataObject.optString("profile_image_url")).noFade().into(profile_image);
                        Picasso.with(context).load(dataObject.optString("profile_image_url")).into(userImage);
                        sharedPreferences.edit().putString("profile_image",dataObject.optString("profile_image_url")).apply();
                        ((MainActivity)context).showProfilePic();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();
            }
        });
    }
}

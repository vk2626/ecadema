package com.ecadema.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.LoginActivity;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.squareup.picasso.Picasso;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class BlogDetailsFrag extends Fragment {

  Context context;
  SharedPreferences sharedPreferences;
  ProgressDialog progressDialog;

  public static final String DEVELOPER_KEY = "AIzaSyA0dSJ53Fvxhg7T4FSmgeKHkNuK5EM3Pnc";

  // YouTube video id
  public String teacher_id="";
  YouTubePlayer YPlayer;
  private YouTubePlayerSupportFragment youTubePlayerFragment;

  ImageView profile_pic,image,share;
  TextView name, date, views, title, description, likeCount, dislikeCount, catName,tag_cat_text;
  LinearLayout likeLayout,disLikeLayout;
  View root;
  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    context = getContext();

    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    progressDialog = new ProgressDialog(context);

    Resources res = getResources();
    DisplayMetrics dr = res.getDisplayMetrics();
    Configuration configuration = res.getConfiguration();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
      configuration.setLocale(new Locale(sharedPreferences.getString("lang", "").toLowerCase()));
    } else {
      configuration.locale = new Locale(sharedPreferences.getString("lang", "").toLowerCase());
    }
    res.updateConfiguration(configuration, dr);
    root = inflater.inflate(R.layout.blog_details_frag, container, false);
    root.findViewById(R.id.frame).setVisibility(View.GONE);

    youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
    FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
    transaction.replace(R.id.frame, youTubePlayerFragment);
    transaction.commit();

    profile_pic = root.findViewById(R.id.profile_pic);

    tag_cat_text = root.findViewById(R.id.tag_cat_text);
    disLikeLayout = root.findViewById(R.id.disLikeLayout);
    likeLayout = root.findViewById(R.id.likeLayout);
    share = root.findViewById(R.id.share);
    image = root.findViewById(R.id.image);
    name = root.findViewById(R.id.name);
    date = root.findViewById(R.id.date);
    views = root.findViewById(R.id.view);
    title = root.findViewById(R.id.title);
    description = root.findViewById(R.id.description);
    likeCount = root.findViewById(R.id.likeCount);
    dislikeCount = root.findViewById(R.id.dislikeCount);
    catName = root.findViewById(R.id.catName);

    getDetails(getArguments().getString("id"));

    profile_pic.setOnClickListener(v->{
      Bundle bundle=new Bundle();
      bundle.putString("trainerID",teacher_id);
      ViewProfile viewProfile=new ViewProfile();
      viewProfile.setArguments(bundle);
      ((MainActivity) context).addFragment2(viewProfile, true, false, 0);
    });
    likeLayout.setOnClickListener(v->{
      likeDisLike("1");
    });

    disLikeLayout.setOnClickListener(v->{
      likeDisLike("0");
    });

    return root;
  }

  private void likeDisLike(final String like_dislike) {
    if (sharedPreferences.getString("userID", "").equalsIgnoreCase("-1")) {
      Intent intent = new Intent(context, LoginActivity.class);
      startActivity(intent);
      return;
    }
    progressDialog.show();

    Map<String,String> param = new HashMap<>();
    param.put("trainer_id",teacher_id);
    param.put("user_id",sharedPreferences.getString("userID",""));
    param.put("community_id",getArguments().getString("id"));
    param.put("like_dislike_value",like_dislike);
    param.put("lang",sharedPreferences.getString("lang",""));

    Log.e("GetLikeDislikeParam",""+param);
    new PostMethod(Api.GetLikeDislike, param, context).startPostMethod(new ResponseData() {
      @Override
      public void response(final String data) {
        Log.e("GetLikeDislike",data);
        progressDialog.dismiss();
        try {
          JSONObject jsonObject = new JSONObject(data);
          Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
          likeCount.setText(jsonObject.optString("like"));
          dislikeCount.setText(jsonObject.optString("dislike"));
        }catch (Exception e){
          e.printStackTrace();
        }
      }

      @Override
      public void error(final VolleyError error) {
        progressDialog.dismiss();
        error.printStackTrace();
      }
    });
  }

  private void getDetails(final String id) {
    progressDialog.show();

    Map<String,String> param = new HashMap<>();
    param.put("community_id",id);
    param.put("lang",sharedPreferences.getString("lang",""));

    Log.e("BlogDetail",""+param);
    new PostMethod(Api.CommunityDetails, param, context).startPostMethod(new ResponseData() {
      @Override
      public void response(final String data) {
        Log.e("detail",data);
        progressDialog.dismiss();
        try {
          JSONObject jsonObject = new JSONObject(data);
          if (jsonObject.optBoolean("status"))
          {
            JSONArray jsonArray = jsonObject.optJSONArray("data");
            teacher_id = jsonArray.optJSONObject(0).optString("teacher_id");
            name.setText(jsonArray.optJSONObject(0).optString("teacher_name"));
            date.setText(jsonArray.optJSONObject(0).optString("create_date"));
            views.setText(context.getResources().getString(R.string.views)+" "+jsonArray.optJSONObject(0).optString("views"));
            title.setText(jsonArray.optJSONObject(0).optString("title"));
            description.setText(Html.fromHtml(jsonArray.optJSONObject(0).optString("short_description")));
            likeCount.setText(jsonArray.optJSONObject(0).optString("likes"));
            dislikeCount.setText(jsonArray.optJSONObject(0).optString("dislikes"));
            //catName.setText(jsonArray.optJSONObject(0).optString("categories_name"));

            if (jsonArray.optJSONObject(0).optString("tags_name").equalsIgnoreCase("")) {
              catName.setText(jsonArray.optJSONObject(0).optString("categories_name"));
              tag_cat_text.setText(context.getResources().getString(R.string.categories));
            }
            else {
              catName.setText(jsonArray.optJSONObject(0).optString("tags_name"));
              tag_cat_text.setText(context.getResources().getString(R.string.tags1));
            }


            try {
              Picasso.with(context).load(jsonArray.optJSONObject(0).optString("profile_image")).into(profile_pic);
            }catch (Exception e){
              e.printStackTrace();
            }
            try {
              Picasso.with(context).load(jsonArray.optJSONObject(0).optString("image")).into(image);
            }catch (Exception e){
              image.setVisibility(View.GONE);
              e.printStackTrace();
            }

            share.setOnClickListener(v->{
              Intent sendIntent = new Intent();
              sendIntent.setAction(Intent.ACTION_SEND);
              sendIntent.putExtra(Intent.EXTRA_TEXT,jsonArray.optJSONObject(0).optString("share_url"));
              sendIntent.setType("text/plain");

              Intent shareIntent = Intent.createChooser(sendIntent, null);
              startActivity(shareIntent);
            });
            if (jsonArray.optJSONObject(0).optString("url").equalsIgnoreCase("")
                || jsonArray.optJSONObject(0).optString("url")==null
                || jsonArray.optJSONObject(0).optString("url").equalsIgnoreCase("null")){
              root.findViewById(R.id.frame).setVisibility(View.GONE);
            }else {
              root.findViewById(R.id.frame).setVisibility(View.VISIBLE);
              youTubePlayerFragment.initialize(DEVELOPER_KEY, new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider arg0, YouTubePlayer youTubePlayer, boolean b) {
                  if (!b) {
                    Log.e("youtube",URLUtil.guessFileName(jsonArray.optJSONObject(0).optString("url"),
                        null, null).replace(".bin",""));
                    YPlayer = youTubePlayer;
                    YPlayer.setFullscreen(false);
                    YPlayer.cueVideo(URLUtil.guessFileName(jsonArray.optJSONObject(0).optString("url"),
                        null, null).replace(".bin",""));
                    YPlayer.setFullscreenControlFlags(0);
                    YPlayer.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
                      @Override
                      public void onFullscreen(boolean b) {
                        YPlayer.setFullscreen(false);
                        Toast.makeText(getContext(), "Landscape mode is not supported.",
                            Toast.LENGTH_SHORT).show();
                      }
                    });
                    //YPlayer.play();
                  }
                }

                @Override
                public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) {
                  // TODO Auto-generated method stub

                }
              });

            }

          }

        }catch (Exception e){
          e.printStackTrace();
        }
      }

      @Override
      public void error(final VolleyError error) {
        progressDialog.dismiss();
        error.printStackTrace();
      }
    });
  }

}

package com.ecadema.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.adapter.TeammateAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.R;
import com.ecadema.modal.TeammateModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TeamMateFragment extends Fragment {

    Context context;
    SharedPreferences sharedPreferences;
    LinearLayout registerView;
    TextView teammate,registerTeammate,noTeammate,request,removed;
    RecyclerView teammateRecyclerView;

    ArrayList<TeammateModal> teammateModalArrayList;
    ArrayList<TeammateModal> requestedModalArrayList;
    ArrayList<TeammateModal> removedModalArrayList;
    TeammateAdapter teammateAdapter;
    ProgressDialog progressDialog;
    ImageView close;
    TeamMateFragment teamMateFragment;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.teammate_frag, container, false);

        context=getActivity();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

        teamMateFragment = this;
        progressDialog=new ProgressDialog(context);
        request=root.findViewById(R.id.request);
        removed=root.findViewById(R.id.removed);
        close=root.findViewById(R.id.close);
        noTeammate=root.findViewById(R.id.noTeammate);
        registerView=root.findViewById(R.id.registerView);
        registerTeammate=root.findViewById(R.id.registerTeammate);
        teammate=root.findViewById(R.id.teammate);
        teammateRecyclerView=root.findViewById(R.id.teammateRecyclerView);

        close.setOnClickListener(v-> getParentFragmentManager().popBackStackImmediate());
        teammate.setOnClickListener(view -> {
            teammate.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            registerTeammate.setTextColor(context.getResources().getColor(R.color.dark_grey));
            removed.setTextColor(context.getResources().getColor(R.color.dark_grey));
            request.setTextColor(context.getResources().getColor(R.color.dark_grey));
            //teammateRecyclerView.setVisibility(View.VISIBLE);
            registerView.setVisibility(View.GONE);
            getTeamMates(1);
        });

        /** Make visible after **/
        removed.setVisibility(View.GONE);
        request.setVisibility(View.GONE);
        /** Make visible after **/

        removed.setOnClickListener(view -> {
            removed.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            registerTeammate.setTextColor(context.getResources().getColor(R.color.dark_grey));
            teammate.setTextColor(context.getResources().getColor(R.color.dark_grey));
            request.setTextColor(context.getResources().getColor(R.color.dark_grey));
            //teammateRecyclerView.setVisibility(View.VISIBLE);
            registerView.setVisibility(View.GONE);
            getTeamMates(3);
        });
        request.setOnClickListener(view -> {
            request.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            registerTeammate.setTextColor(context.getResources().getColor(R.color.dark_grey));
            removed.setTextColor(context.getResources().getColor(R.color.dark_grey));
            teammate.setTextColor(context.getResources().getColor(R.color.dark_grey));
            //teammateRecyclerView.setVisibility(View.VISIBLE);
            registerView.setVisibility(View.GONE);
            getTeamMates(2);
        });
        registerTeammate.setOnClickListener(view -> {
            String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

            AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.register_teammate_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            TextView sendInvitation=dialogView.findViewById(R.id.sendInvitation);
            EditText email=dialogView.findViewById(R.id.email);

            close.setOnClickListener(v1->dialog.dismiss());
            sendInvitation.setOnClickListener(v1->{
                if(email.getText().toString().matches("")) {
                    email.setError(context.getResources().getString(R.string.fieldRequired));
                    return;
                }
                if (!email.getText().toString().matches(emailPattern)){
                    email.setError(context.getResources().getString(R.string.entervalidemail));
                    return;
                }
                sendInvitation(email.getText().toString(),dialog);
            });

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dialog.create();
            }
            dialog.show();

            /*registerTeammate.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            teammate.setTextColor(context.getResources().getColor(R.color.dark_grey));
            teammateRecyclerView.setVisibility(View.GONE);
            registerView.setVisibility(View.VISIBLE);*/
        });

        getTeamMates(1);
        return root;

    }

    private void sendInvitation(String email, Dialog dialog) {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("email",email);

        new PostMethod(Api.WorkshopInvitation,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("Teammates",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        dialog.dismiss();
                    }
                    Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    public void getTeamMates(int type) {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("user_id",sharedPreferences.getString("userID",""));

        /** Replace teammate with employeelist**/
        //new PostMethod(Api.EmployeesList,param,context).startPostMethod(new ResponseData() {
        new PostMethod(Api.Teammates,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("Teammates",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        teammateModalArrayList=new ArrayList<>();
                        requestedModalArrayList=new ArrayList<>();
                        removedModalArrayList=new ArrayList<>();
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        for(int ar=0;ar<jsonArray.length();ar++){
                            JSONObject response = jsonArray.optJSONObject(ar);

                            /** Make visible after **/
                            /*if (response.optString("status").equalsIgnoreCase("1")){
                                TeammateModal teammateModal=new TeammateModal();
                                teammateModal.setId(response.optString("user_id"));
                                teammateModal.setTeammateId(response.optString("id"));
                                teammateModal.setImage(response.optString("profile_image_url"));
                                teammateModal.setName(response.optString("name"));
                                teammateModal.setMobile(response.optString("contact_no"));
                                teammateModal.setEmail(response.optString("email"));
                                teammateModal.setCourse(response.optJSONArray("courses"));
                                teammateModal.setUserType(response.optString("user_type"));
                                teammateModal.setDepartment(response.optString("department"));
                                teammateModal.setSeniority(response.optString("seniority"));
                                teammateModal.setStatus(response.optString("status"));
                                teammateModalArrayList.add(teammateModal);
                            }
                            if (response.optString("status").equalsIgnoreCase("0")){
                                TeammateModal teammateModal=new TeammateModal();
                                teammateModal.setId(response.optString("user_id"));
                                teammateModal.setTeammateId(response.optString("id"));
                                teammateModal.setImage(response.optString("profile_image_url"));
                                teammateModal.setName(response.optString("name"));
                                teammateModal.setMobile(response.optString("contact_no"));
                                teammateModal.setEmail(response.optString("email"));
                                teammateModal.setCourse(response.optJSONArray("courses"));
                                teammateModal.setUserType(response.optString("user_type"));
                                teammateModal.setDepartment(response.optString("department"));
                                teammateModal.setSeniority(response.optString("seniority"));
                                teammateModal.setStatus(response.optString("status"));
                                requestedModalArrayList.add(teammateModal);
                            }
                            if (response.optString("status").equalsIgnoreCase("2")){
                                TeammateModal teammateModal=new TeammateModal();
                                teammateModal.setId(response.optString("user_id"));
                                teammateModal.setTeammateId(response.optString("id"));
                                teammateModal.setImage(response.optString("profile_image_url"));
                                teammateModal.setName(response.optString("name"));
                                teammateModal.setMobile(response.optString("contact_no"));
                                teammateModal.setEmail(response.optString("email"));
                                teammateModal.setCourse(response.optJSONArray("courses"));
                                teammateModal.setUserType(response.optString("user_type"));
                                teammateModal.setDepartment(response.optString("department"));
                                teammateModal.setSeniority(response.optString("seniority"));
                                teammateModal.setStatus(response.optString("status"));
                                removedModalArrayList.add(teammateModal);
                            }*/
                            /** Make visible after **/

                            TeammateModal teammateModal=new TeammateModal();
                            teammateModal.setId(response.optString("user_id"));
                            teammateModal.setTeammateId(response.optString("id"));
                            teammateModal.setImage(response.optString("profile_image_url"));
                            teammateModal.setName(response.optString("name"));
                            teammateModal.setMobile(response.optString("contact_no"));
                            teammateModal.setEmail(response.optString("email"));
                            teammateModal.setCourse(response.optJSONArray("courses"));
                            teammateModal.setUserType(response.optString("user_type"));
                            teammateModal.setDepartment(response.optString("department"));
                            teammateModal.setSeniority(response.optString("seniority"));
                            teammateModal.setStatus(response.optString("status"));
                            teammateModalArrayList.add(teammateModal);
                        }
                        teammateRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        if (teammateModalArrayList.size()>0 ){
                            teammateRecyclerView.setAdapter(new TeammateAdapter(context,teammateModalArrayList));
                            noTeammate.setVisibility(View.GONE);
                            teammateRecyclerView.setVisibility(View.VISIBLE);
                        }
                        else{
                            noTeammate.setVisibility(View.VISIBLE);
                            teammateRecyclerView.setVisibility(View.GONE);
                        }
                        /*if (teammateModalArrayList.size()>0 && type ==1){
                            teammateRecyclerView.setAdapter(new TeammateAdapter(context,teammateModalArrayList));
                            noTeammate.setVisibility(View.GONE);
                            teammateRecyclerView.setVisibility(View.VISIBLE);
                        }
                        else if (requestedModalArrayList.size()>0 && type ==2){
                            teammateRecyclerView.setAdapter(new EmployeesRequestListAdapter(context,requestedModalArrayList,teamMateFragment));
                            noTeammate.setVisibility(View.GONE);
                            teammateRecyclerView.setVisibility(View.VISIBLE);
                        }
                        else if (removedModalArrayList.size()>0 && type ==3){
                            teammateRecyclerView.setAdapter(new EmployeesRemovedListAdapter(context,removedModalArrayList,teamMateFragment));
                            noTeammate.setVisibility(View.GONE);
                            teammateRecyclerView.setVisibility(View.VISIBLE);
                        }
                        else{
                            noTeammate.setVisibility(View.VISIBLE);
                            teammateRecyclerView.setVisibility(View.GONE);
                        }*/

                    }
                    else{
                        noTeammate.setVisibility(View.VISIBLE);
                        teammateRecyclerView.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }
}

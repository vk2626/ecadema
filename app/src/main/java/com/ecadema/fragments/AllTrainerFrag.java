package com.ecadema.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.appyvet.materialrangebar.RangeBar;
import com.ecadema.VerticalSpaceItemDecoration;
import com.ecadema.adapter.AllTrainersAdapter;
import com.ecadema.adapter.CoursesAdapter;
import com.ecadema.adapter.LanguageAdapter;
import com.ecadema.adapter.LocationAdapter;
import com.ecadema.adapter.TagsAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.GetMethod;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.modal.AllTrainersModal;
import com.ecadema.modal.CoursesModal;
import com.ecadema.modal.LanguageModal;
import com.ecadema.modal.LocationModal;
import com.ecadema.modal.SubCategModal;
import com.ecadema.modal.TagsModal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AllTrainerFrag extends Fragment {

    Context context;
    SharedPreferences sharedPreferences;

    RecyclerView recyclerView,browseRecyclerView,locationRecyclerView,languageRecyclerView;
    ArrayList<AllTrainersModal> allTrainersModalArrayList =new ArrayList<>();
    ArrayList<CoursesModal> coursesModalArrayList =new ArrayList<>();

    ArrayList<String> trainsFor;
    AllTrainersAdapter allTrainersAdapter;
    CoursesAdapter coursesAdapter;

    TextView filterText,browseCourseText,availabilityText,selectedCourse,clearFilter,done,noTrainer,selectedRange,rateText,genderText,languageText,
            locationText,tagText,noCourse;
    LinearLayout filter,browseCourse,availability,browseLayout,availabilityLayout,searchTrainer,rate,gender,location,trainerSpeaks,
            searchTrainerETLayout,rangeBarLayout,selectGenderLayout,locationLayout,languageLayout,tags;
    CardView allFiltersLayout,filterLayout,headerLayout;
    EditText search,searchTrainerET;

    ArrayList<String> selectedDays=new ArrayList<>();
    ArrayList<LanguageModal> languageList=new ArrayList<>();
    ArrayList<LocationModal> locationList=new ArrayList<>();

    LanguageAdapter languageAdapter;
    LocationAdapter locationAdapter;

    ImageView mor_sun,mor_mon,mor_tue,mor_wed,mor_thu,mor_fri,mor_sat,aft_sun,aft_mon,aft_tue,aft_wed,aft_thu,aft_fri,aft_sat,eve_sun,eve_mon,eve_tue,eve_wed,
            eve_thu,eve_fri,eve_sat,close;

    RangeBar rangeBar;
    int rangBarRange=1000;
    RadioGroup radioGroup;
    String filters="",langFilter="",tagFilter="",genderFilter="",catIdFilter="",subIdFilter="",maxPriceFilter="",countryFilter="",available_time="";
    ProgressDialog progressDialog;
    ArrayList<String> availabilityList=new ArrayList<>();
    RecyclerView tagsRecyclerView;
    ArrayList<TagsModal> tagsModalArrayList=new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_all_trainers, container, false);

        context= getContext();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        ((MainActivity)context).showBottomMenu();

        progressDialog=new ProgressDialog(context);
        progressDialog.setTitle(getResources().getString(R.string.processing));
        progressDialog.setCancelable(false);
        progressDialog.show();

        tagsRecyclerView=root.findViewById(R.id.tagsRecyclerView);
        tags=root.findViewById(R.id.tags);
        noCourse=root.findViewById(R.id.noCourse);
        tagText=root.findViewById(R.id.tagText);
        languageRecyclerView=root.findViewById(R.id.languageRecyclerView);
        locationRecyclerView=root.findViewById(R.id.locationRecyclerView);
        languageText=root.findViewById(R.id.languageText);
        locationText=root.findViewById(R.id.locationText);
        locationLayout=root.findViewById(R.id.locationLayout);
        languageLayout=root.findViewById(R.id.languageLayout);

        radioGroup=root.findViewById(R.id.radioGroup);
        genderText=root.findViewById(R.id.genderText);
        selectGenderLayout=root.findViewById(R.id.selectGenderLayout);
        rateText=root.findViewById(R.id.rateText);
        rangeBarLayout=root.findViewById(R.id.rangeBarLayout);
        selectedRange=root.findViewById(R.id.selectedRange);
        rangeBar=root.findViewById(R.id.rangeBar);
        close=root.findViewById(R.id.close);
        headerLayout=root.findViewById(R.id.headerLayout);
        searchTrainerET=root.findViewById(R.id.searchTrainerET);
        searchTrainerETLayout=root.findViewById(R.id.searchTrainerETLayout);
        searchTrainer=root.findViewById(R.id.searchTrainer);
        rate=root.findViewById(R.id.rate);
        gender=root.findViewById(R.id.gender);
        location=root.findViewById(R.id.location);
        trainerSpeaks=root.findViewById(R.id.trainerSpeaks);
        noTrainer=root.findViewById(R.id.noTrainer);

        mor_sun=root.findViewById(R.id.mor_sun);
        mor_mon=root.findViewById(R.id.mor_mon);
        mor_tue=root.findViewById(R.id.mor_tue);
        mor_wed=root.findViewById(R.id.mor_wed);
        mor_thu=root.findViewById(R.id.mor_thu);
        mor_fri=root.findViewById(R.id.mor_fri);
        mor_sat=root.findViewById(R.id.mor_sat);
        aft_sun=root.findViewById(R.id.aft_sun);
        aft_mon=root.findViewById(R.id.aft_mon);
        aft_tue=root.findViewById(R.id.aft_tue);
        aft_wed=root.findViewById(R.id.aft_wed);
        aft_thu=root.findViewById(R.id.aft_thu);
        aft_fri=root.findViewById(R.id.aft_fri);
        aft_sat=root.findViewById(R.id.aft_sat);
        eve_sun=root.findViewById(R.id.eve_sun);
        eve_mon=root.findViewById(R.id.eve_mon);
        eve_tue=root.findViewById(R.id.eve_tue);
        eve_wed=root.findViewById(R.id.eve_wed);
        eve_thu=root.findViewById(R.id.eve_thu);
        eve_fri=root.findViewById(R.id.eve_fri);
        eve_sat=root.findViewById(R.id.eve_sat);


        filterLayout=root.findViewById(R.id.filterLayout);
        done=root.findViewById(R.id.done);
        availabilityLayout=root.findViewById(R.id.availabilityLayout);
        browseLayout=root.findViewById(R.id.browseLayout);
        clearFilter=root.findViewById(R.id.clearFilter);
        search=root.findViewById(R.id.search);
        allFiltersLayout=root.findViewById(R.id.allFiltersLayout);
        selectedCourse=root.findViewById(R.id.selectedCourse);
        browseRecyclerView=root.findViewById(R.id.browseRecyclerView);
        filterText=root.findViewById(R.id.filterText);
        browseCourseText=root.findViewById(R.id.browseCourseText);
        availabilityText=root.findViewById(R.id.availabilityText);
        filter=root.findViewById(R.id.filter);
        browseCourse=root.findViewById(R.id.browseCourse);
        availability=root.findViewById(R.id.availability);

        recyclerView=root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(0,0,20,10));
        browseRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        locationRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        languageRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        getAllLocations();
        getLanguages();
        getAllCourses();
        getAllTags();

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(null!=coursesModalArrayList){
                    if(search.getText().toString().trim().matches("")){
                        coursesAdapter.setCacheMenuRes(coursesModalArrayList);
                        return;
                    }
                }
                //On text changed in Edit text start filtering the list
                ArrayList<CoursesModal> tempList=new ArrayList<>();
                String heading="";
                CoursesModal coursesModal=null;
                for(CoursesModal c: coursesModalArrayList){
                    ArrayList<SubCategModal> subCategModals=c.getCourses();
                    for (SubCategModal s:subCategModals){
                        if(s.getSubCategory().toLowerCase(Locale.getDefault()).contains(charSequence.toString())
                                ||s.getSubCategory().toLowerCase(Locale.getDefault()).startsWith(charSequence.toString())){
                            Log.e("charSequence", String.valueOf(charSequence));
                            ArrayList<SubCategModal> tempSubList=new ArrayList<>();
                            heading=c.getCategoryHeading();
                            tempSubList.add(s);
                            coursesModal=new CoursesModal();
                            coursesModal.setCategoryHeading(heading);
                            coursesModal.setCategoryID(c.getCategoryID());
                            coursesModal.setCourses(tempSubList);
                            tempList.add(coursesModal);

                        }
                    }
                }
                if(coursesModal!=null){
                    coursesAdapter.setCacheMenuRes(tempList);
                    noCourse.setVisibility(View.GONE);
                    browseRecyclerView.setVisibility(View.VISIBLE);
                }else {
                    noCourse.setVisibility(View.VISIBLE);
                    browseRecyclerView.setVisibility(View.GONE);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        setClickListeners();

        return root;
    }

    private void getAllTags() {
        new GetMethod(Api.allTrainerTags+sharedPreferences.getString("lang",""), context).startmethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("allTags",data);
                try {
                    JSONObject dataObject = new JSONObject(data);
                    JSONArray tag = dataObject.optJSONArray("data");
                    tagsModalArrayList.clear();
                    for (int me=0; me<tag.length(); me++){
                        TagsModal tagsModal = new TagsModal();
                        JSONObject jsonObject = tag.getJSONObject(me);
                        tagsModal.setTagId(jsonObject.optString("id"));
                        tagsModal.setTagName(jsonObject.optString("tag"));
                        tagsModalArrayList.add(tagsModal);
                    }
                    TagsAdapter tagsAdapter = new TagsAdapter(context,tagsModalArrayList);
                    tagsRecyclerView.setAdapter(tagsAdapter);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });

    }

    private void getAllCourses() {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));

        new PostMethod(Api.BrowseCourses,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("Course List",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        coursesModalArrayList.clear();
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject object=jsonArray.getJSONObject(ar);
                            ArrayList<SubCategModal> subCategModalArrayList=new ArrayList<>();
                            for (int vk=0;vk<object.optJSONArray("sub_category").length();vk++){
                                SubCategModal subCategModal=new SubCategModal();
                                JSONObject object1=object.optJSONArray("sub_category").getJSONObject(vk);
                                subCategModal.setId(object1.optString("id"));
                                subCategModal.setSubCategory(object1.optString("subject"));
                                subCategModalArrayList.add(subCategModal);
                            }
                            CoursesModal coursesModal =new CoursesModal();
                            coursesModal.setCategoryHeading(object.optString("category"));
                            coursesModal.setCategoryID(object.optString("id"));
                            coursesModal.setCourses(subCategModalArrayList);

                            coursesModalArrayList.add(coursesModal);
                        }
                        coursesAdapter =new CoursesAdapter(context, coursesModalArrayList);
                        browseRecyclerView.setAdapter(coursesAdapter);
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void getLanguages() {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("page_filter","one-to-one");

        new PostMethod(Api.langFilter,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("Language List",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    languageList.clear();
                    if(jsonObject.optBoolean("status")){
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject object=jsonArray.getJSONObject(ar);
                            LanguageModal languageModal=new LanguageModal();
                            languageModal.setId(object.optString("id"));
                            languageModal.setLanguage(object.getString("language"));

                            languageList.add(languageModal);
                        }
                        languageAdapter=new LanguageAdapter(context,languageList);
                        languageRecyclerView.setAdapter(languageAdapter);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void getAllLocations() {
        Map<String, String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));

        Log.e("countryParam",""+param.toString());

        new PostMethod(Api.TrainerCountriesList,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("countryList",data);
                try {
                    locationList.clear();
                    JSONObject jsonObject=new JSONObject(data);
                    ArrayList<String> countryArray=new ArrayList<>();
                    if (jsonObject.optBoolean("status")) {
                        JSONArray countryList=jsonObject.optJSONArray("data");
                        for (int ar=0;ar<countryList.length();ar++){
                            JSONObject jsonObject1=countryList.getJSONObject(ar);
                            LocationModal locationModal=new LocationModal();
                            locationModal.setId(jsonObject1.optString("id"));
                            locationModal.setLanguage(jsonObject1.optString("country_name"));

                            locationList.add(locationModal);
                        }
                        locationAdapter=new LocationAdapter(context,locationList);
                        locationRecyclerView.setAdapter(locationAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });

    }

    private void getAllTrainers() {

        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("langg",langFilter);
        param.put("gender",genderFilter);
        param.put("cat_id",catIdFilter);
        param.put("min_price","");
        param.put("max_price",maxPriceFilter);
        param.put("country",countryFilter);
        param.put("teacher_name","");
        param.put("subject_id",subIdFilter);
        param.put("available_time",available_time);
        param.put("tag",tagFilter);

        Log.e("trainers Param",param.toString());

        new PostMethod(Api.TrainersList,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    Log.e("Trainer List",data);
                    JSONObject jsonObject=new JSONObject(data);
                    progressDialog.dismiss();
                    if(jsonObject.optBoolean("status")){
                        noTrainer.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        allTrainersModalArrayList.clear();
                        JSONObject priceFilter = jsonObject.optJSONObject("price_filter");
                        rangeBar.setTickEnd(priceFilter.optInt("highest_price"));
                        rangeBar.setTickStart(priceFilter.optInt("lowest_price"));

                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject object=jsonArray.getJSONObject(ar);

                            trainsFor=new ArrayList<>();

                            String []temp=object.optString("trains_for").split(",");

                            AllTrainersModal allTrainersModal =new AllTrainersModal();
                            allTrainersModal.setTeacherId(object.optString("teacher_id"));
                            allTrainersModal.setRating(object.optInt("average_rating"));
                            allTrainersModal.setImage(object.optString("profile_image"));
                            allTrainersModal.setName(object.optString("teacher_name"));
                            allTrainersModal.setLocation(object.optString("country_name"));
                            allTrainersModal.setLanguage(object.optString("language_name"));
                            allTrainersModal.setRate(object.optString("price_per_hour"));
                            allTrainersModal.setSessions(object.optJSONArray("courses"));
                            allTrainersModal.setSpecialized(object.optJSONArray("specialized"));
                            allTrainersModal.setIsMentor(object.optString("is_mentor"));

                            if (object.optJSONArray("tags_arr").length()>0){
                                allTrainersModal.setLabel(getResources().getString(R.string.tags));
                                for (int vk=0;vk<object.optJSONArray("tags_arr").length();vk++){
                                    trainsFor.add(object.optJSONArray("tags_arr").optJSONObject(vk).optString("tag").trim());
                                }
                            }
                            else if(object.optJSONArray("specialized").length()>0){
                                allTrainersModal.setLabel(getResources().getString(R.string.specializedIn));
                                for (int vk=0;vk<object.optJSONArray("specialized").length();vk++){
                                    trainsFor.add(object.optJSONArray("specialized").optJSONObject(vk).optString("category").trim());
                                }
                            }
                            allTrainersModal.setTrainsFor(trainsFor);

                            allTrainersModalArrayList.add(allTrainersModal);
                        }
                        allTrainersAdapter =new AllTrainersAdapter(context, allTrainersModalArrayList);
                        recyclerView.setAdapter(allTrainersAdapter);
                    }
                    else {
                        noTrainer.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
            }
        });
    }


    private void setClickListeners() {

        trainerSpeaks.setOnClickListener(v->{

            if (sharedPreferences.getString("lang","").equalsIgnoreCase("en")){
                if(languageLayout.getVisibility()==View.VISIBLE){
                    languageLayout.setVisibility(View.GONE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                }else {
                    selectGenderLayout.setVisibility(View.GONE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    rangeBarLayout.setVisibility(View.GONE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    availabilityLayout.setVisibility(View.GONE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    locationLayout.setVisibility(View.GONE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    languageLayout.setVisibility(View.VISIBLE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_down_24,0);
                }
            }
            else {

                if(languageLayout.getVisibility()==View.VISIBLE){
                    languageLayout.setVisibility(View.GONE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                }else {
                    selectGenderLayout.setVisibility(View.GONE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    rangeBarLayout.setVisibility(View.GONE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    availabilityLayout.setVisibility(View.GONE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    locationLayout.setVisibility(View.GONE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    languageLayout.setVisibility(View.VISIBLE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_down_24,0,0,0);
                }
            }
        });
        location.setOnClickListener(v->{
            if (sharedPreferences.getString("lang","").equalsIgnoreCase("en")){
                if(locationLayout.getVisibility()==View.VISIBLE){
                    locationLayout.setVisibility(View.GONE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                }else {
                    selectGenderLayout.setVisibility(View.GONE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    rangeBarLayout.setVisibility(View.GONE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    availabilityLayout.setVisibility(View.GONE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    locationLayout.setVisibility(View.VISIBLE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_down_24,0);

                    languageLayout.setVisibility(View.GONE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                }
            }
            else {
                if(locationLayout.getVisibility()==View.VISIBLE){
                    locationLayout.setVisibility(View.GONE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                }else {
                    selectGenderLayout.setVisibility(View.GONE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    rangeBarLayout.setVisibility(View.GONE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    availabilityLayout.setVisibility(View.GONE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    locationLayout.setVisibility(View.VISIBLE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_down_24,0,0,0);

                    languageLayout.setVisibility(View.GONE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                }
            }
        });
        radioGroup.setOnCheckedChangeListener((radioGroup, i) -> {
            if(i==R.id.male){
                genderFilter="male";
                applyFilter(genderFilter);
                //applyFilter("male");
            }
            if(i==R.id.female){
                genderFilter="female";
                applyFilter(genderFilter);
                //applyFilter("female");
            }
        });
        gender.setOnClickListener(v->{
            if (sharedPreferences.getString("lang","").equalsIgnoreCase("en")){
                if(selectGenderLayout.getVisibility()==View.VISIBLE){
                    selectGenderLayout.setVisibility(View.GONE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                }
                else {
                    rangeBarLayout.setVisibility(View.GONE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    selectGenderLayout.setVisibility(View.VISIBLE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_down_24,0);

                    locationLayout.setVisibility(View.GONE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    availabilityLayout.setVisibility(View.GONE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    languageLayout.setVisibility(View.GONE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                }
            }
            else {
                if(selectGenderLayout.getVisibility()==View.VISIBLE){
                    selectGenderLayout.setVisibility(View.GONE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                }
                else {
                    rangeBarLayout.setVisibility(View.GONE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    selectGenderLayout.setVisibility(View.VISIBLE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_down_24,0,0,0);

                    locationLayout.setVisibility(View.GONE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    availabilityLayout.setVisibility(View.GONE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    languageLayout.setVisibility(View.GONE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                }
            }
        });
        rate.setOnClickListener(v->{
            if (sharedPreferences.getString("lang","").equalsIgnoreCase("en")){
                if(rangeBarLayout.getVisibility()==View.VISIBLE){
                    rangeBarLayout.setVisibility(View.GONE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                }else {
                    selectGenderLayout.setVisibility(View.GONE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    rangeBarLayout.setVisibility(View.VISIBLE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_down_24,0);

                    locationLayout.setVisibility(View.GONE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    availabilityLayout.setVisibility(View.GONE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    languageLayout.setVisibility(View.GONE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                }
            }
            else {
                if(rangeBarLayout.getVisibility()==View.VISIBLE){
                    rangeBarLayout.setVisibility(View.GONE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                }else {
                    selectGenderLayout.setVisibility(View.GONE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    rangeBarLayout.setVisibility(View.VISIBLE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_down_24,0,0,0);

                    locationLayout.setVisibility(View.GONE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    availabilityLayout.setVisibility(View.GONE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    languageLayout.setVisibility(View.GONE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                }
            }
        });
        selectedRange.setText(rateText.getText().toString()+" < $"+rangBarRange);

        rangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                selectedRange.setText(rateText.getText().toString()+"  < $"+rightPinValue);
                maxPriceFilter=rightPinValue;
                /*if(allTrainersAdapter!=null) {
                    allTrainersAdapter.rangeFilter(rightPinValue);
                    if(allTrainersAdapter.getItemCount()==0) {
                        recyclerView.setVisibility(View.GONE);
                        noTrainer.setVisibility(View.VISIBLE);
                        return;
                    }
                    noTrainer.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }*/
            }

            @Override
            public void onTouchEnded(RangeBar rangeBar) {
                applyFilter("");
            }

            @Override
            public void onTouchStarted(RangeBar rangeBar) {}
        });
        close.setOnClickListener(v->{
            searchTrainerET.setText("");
            searchTrainerETLayout.setVisibility(View.GONE);
            filterText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            allFiltersLayout.setVisibility(View.GONE);
            headerLayout.setVisibility(View.VISIBLE);

        });
        searchTrainer.setOnClickListener(v->{
            searchTrainerET.setText("");
            searchTrainerETLayout.setVisibility(View.VISIBLE);
            filterText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            allFiltersLayout.setVisibility(View.GONE);
            if (sharedPreferences.getString("lang","").equalsIgnoreCase("en")){
                rateText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                genderText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                languageText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                locationText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
            }
            else {
                rateText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                genderText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                languageText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                locationText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

            }

            rangeBarLayout.setVisibility(View.GONE);

            selectGenderLayout.setVisibility(View.GONE);

            languageLayout.setVisibility(View.GONE);

            locationLayout.setVisibility(View.GONE);

            languageLayout.setVisibility(View.GONE);

            headerLayout.setVisibility(View.GONE);
        });
        searchTrainerET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(allTrainersAdapter!=null) {
                    allTrainersAdapter.filter(charSequence.toString());
                    if(allTrainersAdapter.getItemCount()==0) {
                        recyclerView.setVisibility(View.GONE);
                        noTrainer.setVisibility(View.VISIBLE);
                        return;
                    }
                    noTrainer.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        browseCourse.setOnClickListener(v->{
            if(allFiltersLayout.getVisibility()==View.VISIBLE){
                if(browseLayout.getVisibility()==View.VISIBLE){
                    filterLayout.setVisibility(View.GONE);
                    allFiltersLayout.setVisibility(View.GONE);
                    browseLayout.setVisibility(View.GONE);
                    browseCourseText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                }
                else {
                    tagsRecyclerView.setVisibility(View.GONE);
                    availabilityLayout.setVisibility(View.GONE);
                    filterLayout.setVisibility(View.GONE);
                    browseLayout.setVisibility(View.VISIBLE);
                    filterText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                    tagText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                    browseCourseText.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                    rangeBarLayout.setVisibility(View.GONE);
                    if (sharedPreferences.getString("lang","").equalsIgnoreCase("en")){
                        rateText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                        genderText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                        languageText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                        locationText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                    }
                    else {
                        rateText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                        genderText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                        languageText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                        locationText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    }

                    selectGenderLayout.setVisibility(View.GONE);
                    languageLayout.setVisibility(View.GONE);
                    locationLayout.setVisibility(View.GONE);
                }
            }else {
                tagsRecyclerView.setVisibility(View.GONE);
                availabilityLayout.setVisibility(View.GONE);
                filterLayout.setVisibility(View.GONE);
                browseLayout.setVisibility(View.VISIBLE);
                allFiltersLayout.setVisibility(View.VISIBLE);
                tagText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                browseCourseText.setTextColor(context.getResources().getColor(R.color.colorPrimary));

                filterText.setTextColor(context.getResources().getColor(R.color.dark_grey));

                rangeBarLayout.setVisibility(View.GONE);
                if (sharedPreferences.getString("lang","").equalsIgnoreCase("en")){
                    rateText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                }
                else {
                    rateText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                }



                selectGenderLayout.setVisibility(View.GONE);

                languageLayout.setVisibility(View.GONE);

                locationLayout.setVisibility(View.GONE);


            }
        });

        availability.setOnClickListener(v->{
            if (sharedPreferences.getString("lang","").equalsIgnoreCase("en")){
                if(availabilityLayout.getVisibility()==View.VISIBLE){
                    availabilityLayout.setVisibility(View.GONE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                }
                else {
                    rangeBarLayout.setVisibility(View.GONE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    selectGenderLayout.setVisibility(View.GONE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    availabilityLayout.setVisibility(View.VISIBLE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_down_24,0);

                    locationLayout.setVisibility(View.GONE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);

                    languageLayout.setVisibility(View.GONE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_baseline_keyboard_arrow_right_24,0);
                }
            }
            else {
                if(availabilityLayout.getVisibility()==View.VISIBLE){
                    availabilityLayout.setVisibility(View.GONE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                }
                else {
                    rangeBarLayout.setVisibility(View.GONE);
                    rateText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    availabilityLayout.setVisibility(View.VISIBLE);
                    availabilityText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_down_24,0,0,0);

                    selectGenderLayout.setVisibility(View.GONE);
                    genderText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0,0);

                    locationLayout.setVisibility(View.GONE);
                    locationText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);

                    languageLayout.setVisibility(View.GONE);
                    languageText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_keyboard_arrow_left_24,0,0 ,0);
                }
            }
            /**/

        });

        tags.setOnClickListener(v->{
            if(allFiltersLayout.getVisibility()==View.VISIBLE){
                if(tagsRecyclerView.getVisibility()==View.VISIBLE){
                    allFiltersLayout.setVisibility(View.GONE);
                    filterLayout.setVisibility(View.GONE);
                    tagsRecyclerView.setVisibility(View.GONE);
                    tagText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                }
                else {
                    filterLayout.setVisibility(View.GONE);
                    tagsRecyclerView.setVisibility(View.VISIBLE);
                    browseLayout.setVisibility(View.GONE);
                    filterText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                    browseCourseText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                    tagText.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                }
            }
            else {
                allFiltersLayout.setVisibility(View.VISIBLE);
                filterLayout.setVisibility(View.GONE);
                tagsRecyclerView.setVisibility(View.VISIBLE);
                browseLayout.setVisibility(View.GONE);
                filterText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                browseCourseText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                tagText.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            }
        });
        filter.setOnClickListener(v->{
            if(allFiltersLayout.getVisibility()==View.VISIBLE){
                if(filterLayout.getVisibility()==View.VISIBLE){
                    allFiltersLayout.setVisibility(View.GONE);
                    availabilityLayout.setVisibility(View.GONE);
                    tagsRecyclerView.setVisibility(View.GONE);
                    filterLayout.setVisibility(View.GONE);
                    filterText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                }
                else {
                    filterLayout.setVisibility(View.VISIBLE);
                    browseLayout.setVisibility(View.GONE);
                    tagsRecyclerView.setVisibility(View.GONE);
                    availabilityLayout.setVisibility(View.GONE);
                    browseCourseText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                    tagText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                    filterText.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                }
            }
            else {
                allFiltersLayout.setVisibility(View.VISIBLE);
                filterLayout.setVisibility(View.VISIBLE);
                tagsRecyclerView.setVisibility(View.GONE);
                availabilityLayout.setVisibility(View.GONE);
                browseLayout.setVisibility(View.GONE);
                browseCourseText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                availabilityText.setTextColor(context.getResources().getColor(R.color.dark_grey));
                filterText.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            }

        });

        clearFilter.setOnClickListener(v->{
            selectedCourse.setVisibility(View.GONE);
            selectedCourse.setText("");
            selectedRange.setText(rateText.getText().toString()+"  < $"+rangBarRange);
            rangeBar.setTickEnd(rangBarRange);
            clearAvailabilityFilters();
            radioGroup.clearCheck();
            langFilter="";
            genderFilter="";
            catIdFilter="";
            subIdFilter="";
            maxPriceFilter="";
            countryFilter="";
            available_time="";
            tagFilter="";
            getAllTrainers();
            getAllTags();

            languageAdapter=new LanguageAdapter(context,languageList);
            languageRecyclerView.setAdapter(languageAdapter);

            locationAdapter=new LocationAdapter(context,locationList);
            locationRecyclerView.setAdapter(locationAdapter);
        });
        done.setOnClickListener(v->{
            allFiltersLayout.setVisibility(View.GONE);
            rangeBarLayout.setVisibility(View.GONE);
            tagText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            browseCourseText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            filterText.setTextColor(context.getResources().getColor(R.color.dark_grey));
        });

        mor_sun.setOnClickListener(v->{
            String temp="sun-morning";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                mor_sun.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                mor_sun.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        mor_mon.setOnClickListener(v->{
            String temp="mon-morning";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                mor_mon.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                mor_mon.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        mor_tue.setOnClickListener(v->{
            String temp="tue-morning";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);

                mor_tue.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                mor_tue.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        mor_wed.setOnClickListener(v->{
            String temp="wed-morning";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);

                mor_wed.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                mor_wed.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        mor_thu.setOnClickListener(v->{
            String temp="thu-morning";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                mor_thu.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                mor_thu.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        mor_fri.setOnClickListener(v->{
            String temp="fri-morning";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                mor_fri.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                mor_fri.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        mor_sat.setOnClickListener(v->{
            String temp="sat-morning";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                mor_sat.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                mor_sat.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        aft_sun.setOnClickListener(v->{
            String temp="sun-afternoon";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                aft_sun.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                aft_sun.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        aft_mon.setOnClickListener(v->{
            String temp="mon-afternoon";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                aft_mon.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                aft_mon.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        aft_tue.setOnClickListener(v->{
            String temp="tue-afternoon";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                aft_tue.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                aft_tue.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        aft_wed.setOnClickListener(v->{
            String temp="wed-afternoon";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                aft_wed.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                aft_wed.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        aft_thu.setOnClickListener(v->{
            String temp="thu-afternoon";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                aft_thu.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                aft_thu.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        aft_fri.setOnClickListener(v->{
            String temp="fri-afternoon";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                aft_fri.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                aft_fri.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        aft_sat.setOnClickListener(v->{
            String temp="sat-afternoon";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                aft_sat.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                aft_sat.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        eve_sun.setOnClickListener(v->{
            String temp="sun-evening";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                eve_sun.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                eve_sun.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        eve_mon.setOnClickListener(v->{
            String temp="mon-evening";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                eve_mon.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                eve_mon.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        eve_tue.setOnClickListener(v->{
            String temp="tue-evening";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                eve_tue.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                eve_tue.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        eve_wed.setOnClickListener(v->{
            String temp="wed-evening";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                eve_wed.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                eve_wed.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        eve_thu.setOnClickListener(v->{
            String temp="thu-evening";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                eve_thu.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                eve_thu.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        eve_fri.setOnClickListener(v->{
            String temp="fri-evening";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                eve_fri.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                eve_fri.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
        eve_sat.setOnClickListener(v->{
            String temp="sat-evening";
            if(selectedDays.contains(temp)){
                selectedDays.remove(temp);
                eve_sat.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
            else {
                selectedDays.add(temp);
                eve_sat.setBackgroundColor(context.getResources().getColor(R.color.dark_grey1));
            }
            available_time= android.text.TextUtils.join(",", selectedDays);
            applyFilter("");
        });
    }

    private void applyFilter(String filter) {
       /* if(allTrainersAdapter!=null) {
            allTrainersAdapter.filter(filter);
            if(allTrainersAdapter.getItemCount()==0) {
                recyclerView.setVisibility(View.GONE);
                noTrainer.setVisibility(View.VISIBLE);
                return;
            }
            noTrainer.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }*/

       getAllTrainers();
    }

    private void clearAvailabilityFilters() {
        mor_sun.setBackgroundColor(context.getResources().getColor(R.color.white));
        mor_mon.setBackgroundColor(context.getResources().getColor(R.color.white));
        mor_tue.setBackgroundColor(context.getResources().getColor(R.color.white));
        mor_wed.setBackgroundColor(context.getResources().getColor(R.color.white));
        mor_thu.setBackgroundColor(context.getResources().getColor(R.color.white));
        mor_fri.setBackgroundColor(context.getResources().getColor(R.color.white));
        mor_sat.setBackgroundColor(context.getResources().getColor(R.color.white));
        aft_sun.setBackgroundColor(context.getResources().getColor(R.color.white));
        aft_mon.setBackgroundColor(context.getResources().getColor(R.color.white));
        aft_tue.setBackgroundColor(context.getResources().getColor(R.color.white));
        aft_wed.setBackgroundColor(context.getResources().getColor(R.color.white));
        aft_thu.setBackgroundColor(context.getResources().getColor(R.color.white));
        aft_fri.setBackgroundColor(context.getResources().getColor(R.color.white));
        aft_sat.setBackgroundColor(context.getResources().getColor(R.color.white));
        eve_sun.setBackgroundColor(context.getResources().getColor(R.color.white));
        eve_mon.setBackgroundColor(context.getResources().getColor(R.color.white));
        eve_tue.setBackgroundColor(context.getResources().getColor(R.color.white));
        eve_wed.setBackgroundColor(context.getResources().getColor(R.color.white));
        eve_thu.setBackgroundColor(context.getResources().getColor(R.color.white));
        eve_fri.setBackgroundColor(context.getResources().getColor(R.color.white));
        eve_sat.setBackgroundColor(context.getResources().getColor(R.color.white));
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(courseName,new IntentFilter("courseName"));
        context.registerReceiver(langLocBroadCast,new IntentFilter("langLocBroadCast"));
        context.registerReceiver(tagBroadCast,new IntentFilter("tagBroadCast"));
        getAllTrainers();

    }

    BroadcastReceiver courseName=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            search.setText("");
            selectedCourse.setVisibility(View.VISIBLE);
            selectedCourse.setText(intent.getStringExtra("name"));
            allFiltersLayout.setVisibility(View.GONE);
            browseLayout.setVisibility(View.GONE);
            browseCourseText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            searchTrainerET.setText("");
            clearAvailabilityFilters();
            if(intent.getStringExtra("type").equalsIgnoreCase("heading")){
                catIdFilter=intent.getStringExtra("id");
            }
            else
                subIdFilter=intent.getStringExtra("id");

            getAllTrainers();
        }
    };

    BroadcastReceiver langLocBroadCast =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("filterType").equalsIgnoreCase("lang")){
                langFilter=intent.getStringExtra("langLoc");

            }
            else{
                countryFilter=intent.getStringExtra("langLoc");

            }

            applyFilter(langFilter);
            //applyFilter(intent.getStringExtra("langLoc"));
        }
    };
    BroadcastReceiver tagBroadCast =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("filterType").equalsIgnoreCase("tag")){
                tagFilter=intent.getStringExtra("tagLoc");

            }
            applyFilter(tagFilter);
            //applyFilter(intent.getStringExtra("langLoc"));
        }
    };
}

package com.ecadema.fragments;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.EmojiFilter1;
import com.ecadema.MultipartUtility;
import com.ecadema.RealPathUtil;
import com.ecadema.adapter.EduAdapter;
import com.ecadema.adapter.ExpAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.GetMethod;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.BuildConfig;
import com.ecadema.app.FilePath;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.modal.EducationModal;
import com.ecadema.modal.ExpModal;
import com.ecadema.modal.LanguageModal;
import com.ecadema.modal.TagsModal;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class TrainerProfileFragment extends Fragment {
    Context context;
    SharedPreferences sharedPreferences;
    View root;
    ProgressDialog progressDialog;

    ImageView userImage,close,addExp,editAttachment;
    EditText firstName,lastName,languages,contactNumber,email,biography,certificateName;
    AutoCompleteTextView country;
    MultiAutoCompleteTextView langAutocomplete,tagsAutocomplete;
    RecyclerView educationRecycler,expRecycler;
    TextView viewResume,viewIDProof,savePD,saveBio,uploadResume,uploadID,resumeName,idProofName,updateAttachment,savePic;
    ImageView editProfileLayout;

    ArrayList<LanguageModal> languageList=new ArrayList<>();
    ArrayList<String> languageIds=new ArrayList<>();
    ArrayList<String> languageNames=new ArrayList<>();

    ArrayList<TagsModal> tagsModalArrayList=new ArrayList<>();
    ArrayList<String> tagIds=new ArrayList<>();
    ArrayList<String> tagNames=new ArrayList<>();
    ArrayList<String> tagTempNames=new ArrayList<>();

    ArrayList<ExpModal> expModalArrayList=new ArrayList<>();
    ArrayList<EducationModal> educationModalArrayList=new ArrayList<>();
    String langId="";
    ArrayList<String> tempList;
    ImageView editPD,editBio,addEducation;
    Calendar myCalendar;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;
    private android.app.AlertDialog dialog1;
    Uri mImageCaptureUri;
    File photoFile,file,resumeFile,IDFile,profileFile;
    public String photoFileName="image_" + System.currentTimeMillis()+ ".jpg",profilePicName="",add_edit="add",edit_resume,edit_id_proof;

    Bitmap thumbnail,rotatedBitmap;
    String fromDate,toDate,intentFrom="",intentTo="",intentDegree="",intentCertificateName="",intentURL="",intentID="",
            intentDesignation="",intentOrganisation="",resume_url="",id_proof_url="";
    private String realPath;
    CircleImageView profile_image;
    ScrollView scrollView;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_trainer_profile, container, false);

        context=getActivity();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

        progressDialog=new ProgressDialog(context);
        myCalendar = Calendar.getInstance();

        tagsAutocomplete=root.findViewById(R.id.tagsAutocomplete);
        savePic=root.findViewById(R.id.savePic);
        scrollView=root.findViewById(R.id.scrollView);
        profile_image=root.findViewById(R.id.profile_image);
        editProfileLayout=root.findViewById(R.id.editProfileLayout);
        editAttachment=root.findViewById(R.id.editAttachment);
        updateAttachment=root.findViewById(R.id.updateAttachment);
        langAutocomplete=root.findViewById(R.id.langAutocomplete);
        uploadResume=root.findViewById(R.id.uploadResume);
        uploadID=root.findViewById(R.id.uploadID);
        resumeName=root.findViewById(R.id.resumeName);
        idProofName=root.findViewById(R.id.idProofName);

        editPD=root.findViewById(R.id.editPD);
        savePD=root.findViewById(R.id.savePD);
        editBio=root.findViewById(R.id.editBio);
        saveBio=root.findViewById(R.id.saveBio);
        addEducation=root.findViewById(R.id.addEducation);
        addExp=root.findViewById(R.id.addExp);

        close=root.findViewById(R.id.close);
        userImage=root.findViewById(R.id.userImage);
        firstName=root.findViewById(R.id.firstName);
        lastName=root.findViewById(R.id.lastName);
        languages=root.findViewById(R.id.languages);
        contactNumber=root.findViewById(R.id.contactNumber);
        email=root.findViewById(R.id.email);
        biography=root.findViewById(R.id.biography);
        country=root.findViewById(R.id.country);
        educationRecycler=root.findViewById(R.id.educationRecycler);
        expRecycler=root.findViewById(R.id.expRecycler);
        viewResume=root.findViewById(R.id.viewResume);
        viewIDProof=root.findViewById(R.id.viewIDProof);

        educationRecycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        expRecycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        trainerDetail();

        langAutocomplete.setFilters(EmojiFilter1.getFilter());
        langAutocomplete.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        /*langAutocomplete.setOnTouchListener((v, event) -> {
            langAutocomplete.showDropDown();
            return true;
        });*/

        langAutocomplete.setOnItemClickListener((parent, view, position, id) -> {
            String selection = (String) parent.getItemAtPosition(position);
            int pos = -1;
            for (int i = 0; i < languageList.size(); i++) {
                if (selection.contains(languageList.get(i).getLanguage())) {
                    pos = i;
                    break;
                }
            }
            if(!languageIds.contains(languageList.get(pos).getId())) {
                languageIds.add(languageList.get(pos).getId());
            }
        });

        tagsAutocomplete.setFilters(EmojiFilter1.getFilter());
        tagsAutocomplete.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        tagsAutocomplete.setOnItemClickListener((parent, view, position, id) -> {
            String selection = (String) parent.getItemAtPosition(position);
            int pos = -1;
            for (int i = 0; i < tagsModalArrayList.size(); i++) {
                if (selection.contains(tagsModalArrayList.get(i).getTagName())) {
                    pos = i;
                    break;
                }
            }
            if(!tagIds.contains(tagsModalArrayList.get(pos).getTagId())) {
                tagIds.add(tagsModalArrayList.get(pos).getTagId());
            }
        });

        close.setOnClickListener(v->{
            if(savePD.getVisibility()==View.VISIBLE){
                quitDialog("pd");
                return;
            }
            if(saveBio.getVisibility()==View.VISIBLE){
                quitDialog("bio");
                return;
            }
            getParentFragmentManager().popBackStackImmediate();
        });

        biography.setFilters(EmojiFilter1.getFilter());
        editBio.setOnClickListener(v->{
            saveBio.setVisibility(View.VISIBLE);
            editBio.setVisibility(View.GONE);
            biography.setFocusableInTouchMode(true);
            biography.setSelection(biography.getText().length());
            biography.requestFocus();
        });

        saveBio.setOnClickListener(v->{
            saveBio.setVisibility(View.GONE);
            editBio.setVisibility(View.VISIBLE);
            biography.setFocusableInTouchMode(false);
            biography.setFocusable(false);

            updateBio();
        });
        editPD.setOnClickListener(v->{
            firstName.setEnabled(false);
            lastName.setEnabled(false);
            country.setEnabled(false);
            email.setEnabled(false);

            contactNumber.setFocusableInTouchMode(true);
            languages.setSelection(languages.getText().length());
            contactNumber.setSelection(contactNumber.getText().length());

            languages.setFocusableInTouchMode(true);
            languages.setFocusable(true);
            //editProfileLayout.setVisibility(View.VISIBLE);
            savePD.setVisibility(View.VISIBLE);
            editPD.setVisibility(View.GONE);
            langAutocomplete.setFocusableInTouchMode(true);
            langAutocomplete.setSelection(langAutocomplete.getText().length());
            langAutocomplete.requestFocus();

            tagsAutocomplete.setFocusableInTouchMode(true);
            tagsAutocomplete.setSelection(tagsAutocomplete.getText().length());
        });
        savePD.setOnClickListener(v->{

            String temp[] = langAutocomplete.getText().toString().split(", ");

            languageIds= new ArrayList<>();
            languageNames= new ArrayList<>();

            for (int ar=0;ar<temp.length;ar++){
                languageNames.add(temp[ar]);
            }
            for (int ar=0;ar<languageNames.size();ar++){
                for (int vk=0;vk<languageList.size();vk++){
                    if(languageNames.get(ar).equalsIgnoreCase(languageList.get(vk).getLanguage())){
                        languageIds.add(languageList.get(vk).getId());
                    }
                }
            }

            String temp1[] = tagsAutocomplete.getText().toString().split(", ");

            tagIds= new ArrayList<>();
            tagNames= new ArrayList<>();

            for (int ar=0;ar<temp1.length;ar++){
                tagNames.add(temp1[ar]);
            }
            for (int ar=0;ar<tagNames.size();ar++){
                for (int vk=0;vk<tagsModalArrayList.size();vk++){
                    if(tagNames.get(ar).equalsIgnoreCase(tagsModalArrayList.get(vk).getTagName())){
                        tagIds.add(tagsModalArrayList.get(vk).getTagId());
                    }
                }
            }

            if(contactNumber.getText().toString().trim().matches("")) {
                contactNumber.setError(getResources().getString(R.string.fieldRequired));
                return;
            }
            if(contactNumber.getText().toString().trim().length()<10) {
                contactNumber.setError(getResources().getString(R.string.numberMustBe));
                return;
            }
            if(languageIds.size()==0) {
                Toast.makeText(context, getResources().getString(R.string.selectLanguages), Toast.LENGTH_LONG).show();
                return;
            }
            if(tagIds.size()==0) {
                Toast.makeText(context, getResources().getString(R.string.enterTags), Toast.LENGTH_LONG).show();
                return;
            }
            UpdatePersonalInfo();
        });

        savePic.setOnClickListener(v-> UpdatePersonalInfo());
        captureImageInitialization();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissions();
        }

        editProfileLayout.setOnClickListener(v->{
            add_edit="profile";
            captureImageInitialization();
            dialog1.show();
        });
        addEducation.setOnClickListener(view -> {
            add_edit="add";
            captureImageInitialization();
            addNewEducation();
        });
        addExp.setOnClickListener(view -> {
            add_edit="add";
            captureImageInitialization();
            addNewExp();
        });

        editAttachment.setOnClickListener(v->{
            viewIDProof.setVisibility(View.GONE);
            viewResume.setVisibility(View.GONE);
            editAttachment.setVisibility(View.GONE);

            updateAttachment.setVisibility(View.VISIBLE);
            uploadID.setVisibility(View.VISIBLE);
            uploadResume.setVisibility(View.VISIBLE);
        });

        uploadResume.setOnClickListener(v->{
            add_edit="hide";
            captureImageInitialization();
            dialog1.show();
        });

        uploadID.setOnClickListener(v->{
            add_edit="hideID";
            captureImageInitialization();
            dialog1.show();
        });

        updateAttachment.setOnClickListener(v->{
            if(resumeFile==null && IDFile==null){
                Snackbar.make(updateAttachment,getResources().getString(R.string.pleaseSelectEitherResumeOrID),Snackbar.LENGTH_LONG).show();
                return;
            }
            ProgressDialog progressDialog=new ProgressDialog(context);
            progressDialog.setTitle(context.getResources().getString(R.string.processing));
            progressDialog.show();
            new Thread(() -> {
                try {
                    //first_name,last_name,contact_no,country,trainee_id,profile_photo_edit
                    MultipartUtility multipart = new MultipartUtility(Api.TrainerAttachmentsUpdate, "UTF-8");
                    multipart.addFormField("edit_resume", edit_resume);
                    multipart.addFormField("edit_id_proof", edit_id_proof);
                    multipart.addFormField("lang_token", sharedPreferences.getString("lang",""));
                    multipart.addFormField("traineer_id", sharedPreferences.getString("userID",""));
                    if(resumeFile==null)
                        multipart.addFormField("upload_resume", intentURL);
                    else
                        multipart.addFilePart("upload_resume", resumeFile);

                    if(IDFile==null)
                        multipart.addFormField("upload_photo_id", intentURL);
                    else
                        multipart.addFilePart("upload_photo_id", IDFile);

                    Log.e("IDFile",""+IDFile);
                    Log.e("resumeFile",""+resumeFile);

                    String response = multipart.finish();// response from server
                    Log.e("Multipart Response",response);
                    try {
                        final JSONObject jsonObject1 = new JSONObject(response);
                        boolean status = jsonObject1.optBoolean("status");
                        ((MainActivity)context).runOnUiThread(() -> {
                            progressDialog.cancel();;
                            if(status){
                                Intent intent=new Intent("updateTrainer");
                                context.sendBroadcast(intent);
                                updateAttachment.setVisibility(View.GONE);
                                editAttachment.setVisibility(View.VISIBLE);
                                uploadID.setVisibility(View.GONE);
                                uploadResume.setVisibility(View.GONE);

                                /*viewResume.setVisibility(View.VISIBLE);
                                viewIDProof.setVisibility(View.VISIBLE);*/
                                Toast.makeText(context, context.getResources().getString(R.string.attachmentUploaded), Toast.LENGTH_SHORT).show();
                            }
                            else
                                Toast.makeText(context, context.getResources().getString(R.string.somethingWentWrong), Toast.LENGTH_SHORT).show();
                        });


                    } catch (JSONException e) {
                        ((MainActivity)context).runOnUiThread(() -> {
                            progressDialog.cancel();;
                            // Stuff that updates the UI
                        });
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    e.printStackTrace();
                }
            }).start();
        });

        viewResume.setOnClickListener(v->{
            Log.e("resume_url",resume_url);
            openFile(resume_url);
        });
        viewIDProof.setOnClickListener(v->{
            Log.e("id_proof_url",id_proof_url);
            openFile(id_proof_url);

        });
        return root;
    }

    private void quitDialog(String type) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setMessage(context.getResources().getString(R.string.quitWithoutSaving));
        builder.setPositiveButton(context.getResources().getString(R.string.yes),
                (dialogInterface, i) -> getParentFragmentManager().popBackStackImmediate());
        builder.setNegativeButton(context.getResources().getString(R.string.no),
                (dialogInterface, i) -> dialogInterface.dismiss());

        builder.create().show();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void addNewExp() {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
        LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.exp_dialog, null);
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(false);
        Dialog dialog=alertDialog.create();
        ImageView close=dialogView.findViewById(R.id.close);
        CardView update=dialogView.findViewById(R.id.accept);
        CardView uploadCertificate=dialogView.findViewById(R.id.uploadCertificate);
        CardView cancel=dialogView.findViewById(R.id.cancel);
        EditText from,to,designation,company;
        from=dialogView.findViewById(R.id.from);
        to=dialogView.findViewById(R.id.to);
        designation=dialogView.findViewById(R.id.designation);
        company=dialogView.findViewById(R.id.company);
        certificateName=dialogView.findViewById(R.id.certificateName);

        designation.setFilters(EmojiFilter1.getFilter());
        company.setFilters(EmojiFilter1.getFilter());

        TextView add=dialogView.findViewById(R.id.add);
        TextView title=dialogView.findViewById(R.id.title);

        if(add_edit.equalsIgnoreCase("edit")) {
            add.setText(context.getResources().getString(R.string.update));
            title.setText(context.getResources().getString(R.string.editExperience));
            from.setText(intentFrom);
            to.setText(intentTo);
            designation.setText(intentDesignation);
            company.setText(intentOrganisation);
            certificateName.setText(intentCertificateName);
        }

        uploadCertificate.setOnClickListener(v->{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(checkAndRequestPermissions()) {
                    captureImageInitialization();
                    dialog1.show();
                }
            }
        });
        from.setOnTouchListener((v, event) -> {
            switch(event.getAction() & MotionEvent.ACTION_MASK){
                case MotionEvent.ACTION_DOWN:
                    //A pressed gesture has started, the motion contains the initial starting location.
                    Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);
                    @SuppressLint("ClickableViewAccessibility")
                    DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                            (DatePickerDialog.OnDateSetListener) (view, year, monthOfYear, dayOfMonth) -> {
                                view.setMaxDate(year);
                                from.setError(null);
                                from.setText(String.valueOf(year));
                                from.setSelection(from.getText().length());
                                to.setText("");
                                fromDate=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
                            }, mYear, mMonth, mDay);
                    datePickerDialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis());
                    datePickerDialog.show();

            }
            return true;
        });
        to.setOnTouchListener((v, event) -> {
            switch(event.getAction() & MotionEvent.ACTION_MASK){
                case MotionEvent.ACTION_DOWN:
                    //A pressed gesture has started, the motion contains the initial starting location.
                    Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                    SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("yyyy-MM-dd");
                    Date start = null;
                    try {
                        start = sdfOutPutToSend.parse(fromDate);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    @SuppressLint("ClickableViewAccessibility")
                    DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                            (DatePickerDialog.OnDateSetListener) (view, year, monthOfYear, dayOfMonth) -> {
                                view.setMaxDate(year);
                                to.setError(null);
                                to.setText(String.valueOf(year));
                                to.setSelection(from.getText().length());
                                toDate=year+"-"+monthOfYear+"-"+dayOfMonth;
                            }, mYear, mMonth, mDay);
                    Log.e("selectedTimeInMili",""+start.getTime());
                    datePickerDialog.getDatePicker().setMinDate(start.getTime());
                    datePickerDialog.show();

            }
            return true;
        });

        update.setOnClickListener(v1->{
            if(from.getText().toString().trim().matches("")) {
                from.setError(context.getResources().getString(R.string.fieldRequired));
                return;
            }
            if(to.getText().toString().trim().matches("")) {
                to.setError(context.getResources().getString(R.string.fieldRequired));
                return;
            }
            if(designation.getText().toString().trim().matches("")) {
                designation.setError(context.getResources().getString(R.string.fieldRequired));
                return;
            }
            if(company.getText().toString().trim().matches("")) {
                company.setError(context.getResources().getString(R.string.fieldRequired));
                return;
            }
            if(!add_edit.equalsIgnoreCase("edit")){
                if(photoFile==null) {
                    certificateName.setError(context.getResources().getString(R.string.pleaseUploadCertificate));
                    return;
                }
            }

            if (add_edit.equalsIgnoreCase("edit"))
                EditExperience(from.getText().toString(),to.getText().toString(),designation.getText().toString(),company.getText().toString(),dialog);
            else
                AddExperience(from.getText().toString(),to.getText().toString(),designation.getText().toString(),company.getText().toString(),dialog);
        });
        cancel.setOnClickListener(v1->{
            dialog.dismiss();
        });

        close.setOnClickListener(v1->dialog.dismiss());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.create();
        }
        dialog.show();
    }

    private void EditExperience(String from, String to, String designation, String company, Dialog dialog) {
        ProgressDialog progressDialog=new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        new Thread(() -> {
            try {
                //first_name,last_name,contact_no,country,trainee_id,profile_photo_edit
                MultipartUtility multipart = new MultipartUtility(Api.TrainerExperienceUpdate, "UTF-8");
                multipart.addFormField("date_from", from);
                multipart.addFormField("date_to", to);
                multipart.addFormField("upload_certificate_edit", intentCertificateName);
                multipart.addFormField("experience_id", intentID);
                multipart.addFormField("designation", designation);
                multipart.addFormField("organisation", company);
                multipart.addFormField("lang_token", sharedPreferences.getString("lang",""));
                if(photoFile==null)
                    multipart.addFormField("upload_certificate", intentURL);
                else
                    multipart.addFilePart("upload_certificate", photoFile);

                Log.e("photoFile",""+photoFile);

                String response = multipart.finish();// response from server
                Log.e("Multipart Response",response);
                try {
                    final JSONObject jsonObject1 = new JSONObject(response);
                    boolean status = jsonObject1.optBoolean("status");
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    if(status){
                        ((MainActivity)context).runOnUiThread(() -> {
                            dialog.dismiss();
                            Toast.makeText(context, jsonObject1.optString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent("updateTrainer");
                            context.sendBroadcast(intent);
                        });

                    }

                } catch (JSONException e) {
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    e.printStackTrace();
                }
            } catch (IOException e) {
                ((MainActivity)context).runOnUiThread(() -> {
                    progressDialog.cancel();;
                    // Stuff that updates the UI
                });
                e.printStackTrace();
            }
        }).start();
    }

    private void AddExperience(String from, String to, String designation, String company, Dialog dialog) {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        new Thread(() -> {
            try {
                //first_name,last_name,contact_no,country,trainee_id,profile_photo_edit
                MultipartUtility multipart = new MultipartUtility(Api.AddExperienceByTrainer, "UTF-8");
                multipart.addFormField("date_from", fromDate);
                multipart.addFormField("date_to", toDate);
                multipart.addFormField("designation", designation);
                multipart.addFormField("organisation", company);
                multipart.addFormField("traineer_id", sharedPreferences.getString("userID",""));
                multipart.addFormField("lang_token", sharedPreferences.getString("lang",""));
                multipart.addFilePart("upload_certificate", photoFile);

                Log.e("photoFile",""+photoFile);

                String response = multipart.finish();// response from server
                Log.e("Multipart Response",response);
                try {
                    final JSONObject jsonObject1 = new JSONObject(response);
                    boolean status = jsonObject1.optBoolean("status");
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    if(status){
                        ((MainActivity)context).runOnUiThread(() -> {
                            dialog.dismiss();
                            Toast.makeText(context, jsonObject1.optString("message"), Toast.LENGTH_SHORT).show();
                            trainerDetail();
                            // Stuff that updates the UI
                        });

                    }

                } catch (JSONException e) {
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    e.printStackTrace();
                }
            } catch (IOException e) {
                ((MainActivity)context).runOnUiThread(() -> {
                    progressDialog.cancel();;
                    // Stuff that updates the UI
                });
                e.printStackTrace();
            }
        }).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean checkAndRequestPermissions() {
        int cameraPermission = ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA);
        int gps = ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeExternal = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }

        if (gps != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (writeExternal != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 123);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults.length > 0) {
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equals( android.Manifest.permission.CAMERA)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {}
                    }
                    else if (permissions[i].equals(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {}
                    }
                    else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {}
                    }
                }
            }
        }
    }

    private void captureImageInitialization() {
        /**
         * a selector dialog to display two image source options, from camera
         * ‘Take from camera’ and from existing files ‘Select from gallery’
         */
        final String[] items = new String[]{"Take from camera", "Select from gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, items);
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        if(add_edit.equalsIgnoreCase("hide")){
            builder.setTitle(getResources().getString(R.string.uploadResume));
        }else if(add_edit.equalsIgnoreCase("hideID")){
            builder.setTitle(getResources().getString(R.string.uploadID));
        }else if(add_edit.equalsIgnoreCase("profile")){
            builder.setTitle(getResources().getString(R.string.uploadProfilePic));

        }else
            builder.setTitle(getResources().getString(R.string.uploadCertificate));

        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) { // pick from
                // camera
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    photoFile= getPhotoFileUri(photoFileName);
                    mImageCaptureUri= FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,mImageCaptureUri);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    try {
                        intent.putExtra("return-data", true);
                        startActivityForResult(intent, PICK_FROM_CAMERA);

                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    if(add_edit.equalsIgnoreCase("profile")){
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        // Start the Intent
                        startActivityForResult(galleryIntent, PICK_FROM_FILE);
                    }else {
                        Intent intent = new Intent();
                        intent.setType("application/*");
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setAction(Intent.ACTION_GET_CONTENT);

                        // Start the Intent
                        startActivityForResult(intent, PICK_FROM_FILE);
                    }

                }
            }
        });

        dialog1 = builder.create();
    }

    public File getPhotoFileUri(String fileName) {
        File mediaStorageDir = new File(String.valueOf(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)));

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
            Log.d("Error", "failed to create directory");
        }

        // Return the file target for the photo based on filename
        this.file = new File(mediaStorageDir.getPath() + File.separator + fileName);

        return this.file;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode !=RESULT_OK)
            return;

        switch (requestCode) {
            case PICK_FROM_CAMERA:
                //uri=data.getData();
                /**
                 * After taking a picture, do the crop
                 */
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    thumbnail = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(mImageCaptureUri), null, options);
                    Log.e("mImageCaptureUri",""+mImageCaptureUri);

                    if(add_edit.equalsIgnoreCase("hide")){
                        resumeFile=photoFile;
                        edit_resume=URLUtil.guessFileName(mImageCaptureUri.toString(), null, null);
                        viewResume.setVisibility(View.GONE);
                        resumeName.setText(context.getResources().getString(R.string.resume)+" - "+URLUtil.guessFileName(mImageCaptureUri.toString(), null, null));
                    }else if(add_edit.equalsIgnoreCase("hideID")){
                        edit_id_proof=URLUtil.guessFileName(mImageCaptureUri.toString(), null, null);
                        IDFile=photoFile;
                        viewIDProof.setVisibility(View.GONE);
                        idProofName.setText(context.getResources().getString(R.string.idProof)+" - "+URLUtil.guessFileName(mImageCaptureUri.toString(), null, null));
                    }else if(add_edit.equalsIgnoreCase("profile")){
                        CropImage();
                        /*profilePicName=URLUtil.guessFileName(mImageCaptureUri.toString(), null, null);
                        profileFile=photoFile;
                        getPicOrientation();*/

                    }else
                        certificateName.setText(URLUtil.guessFileName(mImageCaptureUri.toString(), null, null));

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;

            case PICK_FROM_FILE:
                /**
                 * After selecting image from files, save the selected path
                 */

                mImageCaptureUri = data.getData();
                Log.e("mImageCaptureUri",""+mImageCaptureUri);
                Bitmap b = null;
                try {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    // Get the cursor
                    Cursor cursor = context.getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    try {

                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String imgDecodableString = cursor.getString(columnIndex);
                        cursor.close();

                        b = BitmapFactory.decodeFile(imgDecodableString);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    if (mImageCaptureUri.toString().startsWith("content://")) {
                        try {
                            cursor = context.getContentResolver().query(mImageCaptureUri, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                String displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                if(add_edit.equalsIgnoreCase("hide")){
                                    edit_resume=displayName;
                                    viewResume.setVisibility(View.GONE);
                                    resumeName.setText(context.getResources().getString(R.string.resume)+" - "+displayName);
                                }else if(add_edit.equalsIgnoreCase("hideID")){
                                    edit_id_proof=displayName;
                                    viewIDProof.setVisibility(View.GONE);
                                    idProofName.setText(context.getResources().getString(R.string.idProof)+" - "+displayName);
                                }else if(add_edit.equalsIgnoreCase("profile")){
                                    profilePicName=displayName;
                                }else
                                    certificateName.setText(displayName);
                                Log.e("content",displayName);
                            }
                        } finally {
                            cursor.close();
                        }
                    } else if (mImageCaptureUri.toString().startsWith("file://")) {
                        File myFile = new File(mImageCaptureUri.toString());
                        String displayName = myFile.getName();
                        if(add_edit.equalsIgnoreCase("hide")){
                            edit_resume=displayName;
                            viewResume.setVisibility(View.GONE);
                            resumeName.setText(context.getResources().getString(R.string.resume)+" - "+displayName);
                        }else if(add_edit.equalsIgnoreCase("hideID")){
                            edit_id_proof=displayName;
                            viewIDProof.setVisibility(View.GONE);
                            idProofName.setText(context.getResources().getString(R.string.idProof)+" - "+displayName);
                        }else if(add_edit.equalsIgnoreCase("profile")){
                            profilePicName=displayName;
                        }else
                            certificateName.setText(displayName);
                        Log.e("displayName",displayName);
                    }


                    try {
                        // SDK < API11
                        if (Build.VERSION.SDK_INT < 11)
                            realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(context, data.getData());

                            // SDK >= 11 && SDK < 19
                        else if (Build.VERSION.SDK_INT < 19)
                            realPath = RealPathUtil.getRealPathFromURI_API11to18(context, data.getData());

                            // SDK > 19 (Android 4.4)
                        else
                            realPath = RealPathUtil.getRealPathFromURI_API19(context, data.getData());

                        Log.e("Real Path", "" + realPath);

                        String PdfPathHolder = null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                            try {
                                PdfPathHolder = FilePath.getPath(context, mImageCaptureUri);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        assert PdfPathHolder != null;
                        if (realPath == null)
                            photoFile = new File(PdfPathHolder);
                        else
                            photoFile = new File(realPath);

                        if(add_edit.equalsIgnoreCase("hide")){
                            resumeFile=photoFile;
                        }
                        if(add_edit.equalsIgnoreCase("hideID")){
                            IDFile=photoFile;
                        }
                        if(add_edit.equalsIgnoreCase("profile")){
                            CropImage();
                            /*profileFile=photoFile;
                            profile_image.setImageBitmap(b);*/
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                    /*File file = new File(imgDecodableString);
                    photoFile=file;*/

                    //
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //doCrop();
                break;

            case UCrop.REQUEST_CROP:
                handleUCropResult(data);
                break;

        }
    }

    private void handleUCropResult(Intent data) {
        if (data == null) {
            setResultCancelled();
            return;
        }
        final Uri resultUri = UCrop.getOutput(data);
        setResultOk(resultUri);
    }

    private void setResultOk(Uri imagePath) {
        Intent intent = new Intent();
        intent.putExtra("path", imagePath);
        ((MainActivity)context).setResult(Activity.RESULT_OK, intent);
        mImageCaptureUri=imagePath;
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(((MainActivity)context).getContentResolver(), imagePath);
            profile_image.setImageBitmap(bitmap);
            editProfileLayout.setVisibility(View.GONE);
            savePic.setVisibility(View.VISIBLE);

            try {
                // SDK < API11
                if (Build.VERSION.SDK_INT < 11)
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(context, imagePath);

                    // SDK >= 11 && SDK < 19
                else if (Build.VERSION.SDK_INT < 19)
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(context, imagePath);

                    // SDK > 19 (Android 4.4)
                else
                    realPath = RealPathUtil.getRealPathFromURI_API19(context,imagePath);

                Log.e("Real Path", "" + realPath);

                String PdfPathHolder = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    try {
                        PdfPathHolder = FilePath.getPath(context, mImageCaptureUri);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                assert PdfPathHolder != null;
                if (realPath == null)
                    photoFile = new File(PdfPathHolder);
                else
                    photoFile = new File(realPath);

                profileFile=photoFile;

            }
            catch (Exception e){
                e.printStackTrace();
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void setResultCancelled() {

    }
    private static String queryName(ContentResolver resolver, Uri uri) {
        Cursor returnCursor =
            resolver.query(uri, null, null, null, null);
        assert returnCursor != null;
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        returnCursor.close();
        return name;
    }
    private void CropImage() {
        try {
            Uri destinationUri = Uri.fromFile(new File(((MainActivity)context).getCacheDir(),
                queryName(((MainActivity)context).getContentResolver(), mImageCaptureUri)));
            UCrop.Options options = new UCrop.Options();
            options.setCompressionQuality(80);
            options.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary));
            options.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimary));
            options.setActiveWidgetColor(ContextCompat.getColor(context, R.color.colorPrimary));
            options.withAspectRatio(4, 4);
            options.withMaxResultSize(500, 500);

            UCrop.of(mImageCaptureUri, destinationUri)
                .withOptions(options)
                .start(getActivity());

        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(context, "Your device doesn't support the crop action!", Toast.LENGTH_SHORT).show();
        }
    }
    private void getPicOrientation() {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(String.valueOf(getPhotoFileUri(photoFileName)));
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            Log.e("orientation",""+orientation);
            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(thumbnail, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(thumbnail, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(thumbnail, 270);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                    rotatedBitmap = rotateImage(thumbnail, 0);
                    break;
                default:
                    rotatedBitmap = thumbnail;
            }

            profile_image.setImageBitmap(rotatedBitmap);
            userImage.setImageBitmap(rotatedBitmap);
            profileFile = new File(context.getCacheDir(), photoFileName);
            profileFile.createNewFile();

//Convert bitmap to byte array
            Bitmap bitmap =rotatedBitmap;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
            FileOutputStream fos = new FileOutputStream(profileFile);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ClickableViewAccessibility")
    private void addNewEducation() {
        fromDate="";
        toDate="";

        AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
        LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.education_dialog, null);
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(false);
        Dialog dialog=alertDialog.create();
        ImageView close=dialogView.findViewById(R.id.close);
        CardView update=dialogView.findViewById(R.id.accept);
        CardView uploadCertificate=dialogView.findViewById(R.id.uploadCertificate);
        CardView cancel=dialogView.findViewById(R.id.cancel);
        EditText from,to,degree;
        TextView add=dialogView.findViewById(R.id.add);
        TextView title=dialogView.findViewById(R.id.title);
        from=dialogView.findViewById(R.id.from);
        to=dialogView.findViewById(R.id.to);
        degree=dialogView.findViewById(R.id.degree);
        certificateName=dialogView.findViewById(R.id.certificateName);

        from.setFilters(EmojiFilter1.getFilter());
        to.setFilters(EmojiFilter1.getFilter());
        degree.setFilters(EmojiFilter1.getFilter());

        if(add_edit.equalsIgnoreCase("edit")) {
            add.setText(context.getResources().getString(R.string.update));
            title.setText(context.getResources().getString(R.string.editEducation));

            from.setText(intentFrom);
            to.setText(intentTo);
            degree.setText(intentDegree);
            certificateName.setText(intentCertificateName);
        }

        uploadCertificate.setOnClickListener(v->{
            if(checkAndRequestPermissions()) {
                captureImageInitialization();
                dialog1.show();
            }
        });
        from.setOnTouchListener((v, event) -> {
            switch(event.getAction() & MotionEvent.ACTION_MASK){
                case MotionEvent.ACTION_DOWN:
                    //A pressed gesture has started, the motion contains the initial starting location.
                    Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);
                    @SuppressLint("ClickableViewAccessibility")
                    DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                            (DatePickerDialog.OnDateSetListener) (view, year, monthOfYear, dayOfMonth) -> {
                                view.setMaxDate(year);
                                from.setError(null);
                                from.setText(String.valueOf(year));
                                from.setSelection(from.getText().length());
                                fromDate=year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
                            }, mYear, mMonth, mDay);
                    datePickerDialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis());
                    datePickerDialog.show();

            }
            return true;
        });
        to.setOnTouchListener((v, event) -> {
            switch(event.getAction() & MotionEvent.ACTION_MASK){
                case MotionEvent.ACTION_DOWN:
                    //A pressed gesture has started, the motion contains the initial starting location.
                    Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);

                    SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("yyyy-MM-dd");
                    Date start = null;
                    try {
                        start = sdfOutPutToSend.parse(fromDate);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                    @SuppressLint("ClickableViewAccessibility")
                    DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                            (DatePickerDialog.OnDateSetListener) (view, year, monthOfYear, dayOfMonth) -> {
                                view.setMaxDate(year);
                                to.setError(null);
                                to.setText(String.valueOf(year));
                                to.setSelection(from.getText().length());
                                toDate=year+"-"+monthOfYear+"-"+dayOfMonth;
                            }, mYear, mMonth, mDay);
                    datePickerDialog.getDatePicker().setMinDate(start.getTime());
                    datePickerDialog.show();

            }
            return true;
        });

        update.setOnClickListener(v1->{
            if(from.getText().toString().trim().matches("")) {
                from.setError(context.getResources().getString(R.string.fieldRequired));
                return;
            }
            if(to.getText().toString().trim().matches("")) {
                to.setError(context.getResources().getString(R.string.fieldRequired));
                return;
            }
            if(degree.getText().toString().trim().matches("")) {
                degree.setError(context.getResources().getString(R.string.fieldRequired));
                return;
            }
            if(!add_edit.equalsIgnoreCase("edit")){
                if(photoFile==null) {
                    certificateName.setError(context.getResources().getString(R.string.pleaseUploadCertificate));
                    return;
                }
            }
            if(add_edit.equalsIgnoreCase("edit"))
                editEducation(from.getText().toString(),to.getText().toString(),degree.getText().toString(),dialog,certificateName.getText().toString()
                        ,intentID,intentURL);
            else
                AddEducation(from.getText().toString(),to.getText().toString(),degree.getText().toString(),dialog);
        });
        cancel.setOnClickListener(v1->{
            dialog.dismiss();
        });

        close.setOnClickListener(v1->dialog.dismiss());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.create();
        }
        dialog.show();
    }

    private void editEducation(String from, String to, String degree, Dialog dialog, String certificate, String id, String url) {
        ProgressDialog progressDialog=new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        new Thread(() -> {
            try {
                //first_name,last_name,contact_no,country,trainee_id,profile_photo_edit
                MultipartUtility multipart = new MultipartUtility(Api.TrainerEducationUpdate, "UTF-8");
                multipart.addFormField("date_from", from);
                multipart.addFormField("date_to", to);
                multipart.addFormField("upload_certificate_edit", certificate);
                multipart.addFormField("education_id", id);
                multipart.addFormField("designation", degree);
                multipart.addFormField("traineer_id", sharedPreferences.getString("userID",""));
                multipart.addFormField("lang_token", sharedPreferences.getString("lang",""));
                if(photoFile==null)
                    multipart.addFormField("upload_certificate", url);
                else
                    multipart.addFilePart("upload_certificate", photoFile);

                Log.e("photoFile",""+photoFile);

                String response = multipart.finish();// response from server
                Log.e("Multipart Response",response);
                try {
                    final JSONObject jsonObject1 = new JSONObject(response);
                    boolean status = jsonObject1.optBoolean("status");
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    if(status){
                        ((MainActivity)context).runOnUiThread(() -> {
                            dialog.dismiss();
                            Toast.makeText(context, jsonObject1.optString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent("updateTrainer");
                            context.sendBroadcast(intent);
                        });

                    }

                } catch (JSONException e) {
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    e.printStackTrace();
                }
            } catch (IOException e) {
                ((MainActivity)context).runOnUiThread(() -> {
                    progressDialog.cancel();;
                    // Stuff that updates the UI
                });
                e.printStackTrace();
            }
        }).start();
    }

    private void AddEducation(String from, String to, String degree, Dialog dialog) {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        new Thread(() -> {
            try {
                //first_name,last_name,contact_no,country,trainee_id,profile_photo_edit
                MultipartUtility multipart = new MultipartUtility(Api.AddEducationByTrainer, "UTF-8");
                multipart.addFormField("date_from", fromDate);
                multipart.addFormField("date_to", toDate);
                multipart.addFormField("designation", degree);
                multipart.addFormField("traineer_id", sharedPreferences.getString("userID",""));
                multipart.addFormField("lang_token", sharedPreferences.getString("lang",""));
                multipart.addFilePart("upload_certificate", photoFile);

                Log.e("photoFile",""+photoFile);

                String response = multipart.finish();// response from server
                Log.e("Multipart Response",response);
                try {
                    final JSONObject jsonObject1 = new JSONObject(response);
                    boolean status = jsonObject1.optBoolean("status");
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    if(status){
                        ((MainActivity)context).runOnUiThread(() -> {
                            dialog.dismiss();
                            Toast.makeText(context, jsonObject1.optString("message"), Toast.LENGTH_SHORT).show();
                            trainerDetail();
                            // Stuff that updates the UI
                        });

                    }

                } catch (JSONException e) {
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    e.printStackTrace();
                }
            } catch (IOException e) {
                ((MainActivity)context).runOnUiThread(() -> {
                    progressDialog.cancel();;
                    // Stuff that updates the UI
                });
                e.printStackTrace();
            }
        }).start();

    }

    private void updateBio() {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.updating));
        progressDialog.show();

        Map<String,String> param = new HashMap<>();
        param.put("about",biography.getText().toString());
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("traineer_id",sharedPreferences.getString("userID",""));

        Log.e("bioParam",""+param);

        new PostMethod(Api.TrainerBiographyUpdate, param, context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    Log.e("bioData",data);
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        Toast.makeText(context, context.getResources().getString(R.string.biographyUpdated), Toast.LENGTH_LONG).show();
                        editBio.setFocusable(true);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    private void UpdatePersonalInfo() {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.updating));
        progressDialog.show();
        //progressDialog.setCancelable();

        new Thread(() -> {
            try {
                MultipartUtility multipart = new MultipartUtility(Api.TrainerPersonalDetailsUpdate, "UTF-8");
                multipart.addFormField("contact_no", contactNumber.getText().toString());
                multipart.addFormField("languages", TextUtils.join(",", languageIds));
                multipart.addFormField("tag", TextUtils.join(",", tagIds));
                multipart.addFormField("lang_token", sharedPreferences.getString("lang",""));
                multipart.addFormField("traineer_id", sharedPreferences.getString("userID",""));
                multipart.addFormField("profile_photo_edit", profilePicName);
                if(photoFile!=null) {
                    multipart.addFilePart("profile_photo", profileFile);
                }else
                    multipart.addFormField("profile_photo", "");
                Log.e("profileFile",""+photoFile);

                String response = multipart.finish();// response from server
                Log.e("Multipart Response",response);
                try {
                    final JSONObject jsonObject1 = new JSONObject(response);
                    boolean status = jsonObject1.optBoolean("status");
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    if(status){
                        ((MainActivity)context).runOnUiThread(() -> {
                            firstName.setEnabled(true);
                            lastName.setEnabled(true);
                            country.setEnabled(true);
                            email.setEnabled(true);
                            tagsAutocomplete.setFocusableInTouchMode(false);
                            langAutocomplete.setFocusableInTouchMode(false);
                            languages.setFocusableInTouchMode(false);
                            languages.setFocusable(false);
                            contactNumber.setFocusable(false);
                            langAutocomplete.setFocusable(false);
                            tagsAutocomplete.setFocusable(false);
                            contactNumber.setFocusableInTouchMode(false);
                            savePD.setVisibility(View.GONE);
                            editPD.setVisibility(View.VISIBLE);
                            editProfileLayout.setVisibility(View.VISIBLE);
                            savePic.setVisibility(View.GONE);

                            scrollView.fullScroll(View.FOCUS_UP);
                            trainerDetail();
                            sharedPreferences.edit().putString("profile_image",jsonObject1.optJSONObject("data").optString("profile_image_url")).apply();
                            ((MainActivity)context).showProfilePic();
                            Toast.makeText(context, getResources().getString(R.string.profileUpdated), Toast.LENGTH_SHORT).show();
                            // Stuff that updates the UI
                        });

                    }

                } catch (JSONException e) {
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    e.printStackTrace();
                }
            } catch (IOException e) {
                ((MainActivity)context).runOnUiThread(() -> {
                    progressDialog.cancel();;
                    // Stuff that updates the UI
                });
                e.printStackTrace();
            }
        }).start();



        /*Log.e("personalParam",""+param);

        new PostMethod(Api.TrainerPersonalDetailsUpdate, param, context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    Log.e("ppData",data);
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        trainerDetail();
                        Toast.makeText(context, context.getResources().getString(R.string.profileUpdated), Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });*/
    }

    private void getLanguages() {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));

        new PostMethod(Api.SpokenLanguages,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("Language List",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    languageList.clear();
                    if(jsonObject.optBoolean("status")){
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        tempList=new ArrayList<>();
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject object=jsonArray.getJSONObject(ar);
                            LanguageModal languageModal=new LanguageModal();
                            languageModal.setId(object.optString("id"));
                            languageModal.setLanguage(object.getString("language"));

                            tempList.add(object.getString("language"));
                            languageList.add(languageModal);
                        }
                        langAutocomplete.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, tempList));

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void trainerDetail() {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("trainer_id",sharedPreferences.getString("userID",""));

        Log.e("profileParam",""+param);

        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        new PostMethod(Api.TrainerProfile,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                educationModalArrayList.clear();
                expModalArrayList.clear();
                Log.e("trainerDetail",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        JSONObject dataObject=jsonObject.optJSONObject("data");
                        firstName.setText(dataObject.optString("first_name"));
                        lastName.setText(dataObject.optString("last_name"));
                        langAutocomplete.setText(dataObject.optString("language_name")+", ");
                        tagsAutocomplete.setText(dataObject.optString("tags_name")+", ");
                        contactNumber.setText(dataObject.optString("contact_no"));
                        email.setText(dataObject.optString("email"));
                        biography.setText(Html.fromHtml(dataObject.optString("about")));
                        country.setText(dataObject.optString("country_name"));
                        edit_id_proof=dataObject.optString("id_proof");
                        edit_resume=dataObject.optString("resume");
                        profilePicName=dataObject.getString("profile_image");

                        String tempList[]= dataObject.optString("language").split(",");
                        if(tempList.length>0){
                            for (int i=0;i<tempList.length;i++){
                                languageIds.add(tempList[i]);
                            }
                        }

                        String tempList1[]= dataObject.optString("language_name").split(",");
                        if(tempList1.length>0){
                            for (int i=0;i<tempList1.length;i++){
                                languageNames.add(tempList1[i]);
                            }
                        }

                        String tempList2[]= dataObject.optString("tags").split(",");
                        if(tempList2.length>0){
                            for (int i=0;i<tempList2.length;i++){
                                tagIds.add(tempList2[i]);
                            }
                        }

                        String tempList3[]= dataObject.optString("tags_name").split(",");
                        if(tempList3.length>0){
                            for (int i=0;i<tempList3.length;i++){
                                tagNames.add(tempList3[i]);
                            }
                        }

                        if(dataObject.optString("resume_url").equalsIgnoreCase("null") ||
                                dataObject.optString("resume_url").equalsIgnoreCase(""))
                            viewResume.setVisibility(View.GONE);

                        if(dataObject.optString("id_proof_url").equalsIgnoreCase("null") ||
                                dataObject.optString("id_proof_url").equalsIgnoreCase(""))
                            viewIDProof.setVisibility(View.GONE);

                        resume_url=dataObject.optString("resume_url");
                        id_proof_url=dataObject.optString("id_proof_url");

                        if(new URI(resume_url).getPath().substring(new URI(resume_url).getPath().lastIndexOf('/') + 1).equalsIgnoreCase("")
                                || new URI(resume_url).getPath().substring(new URI(resume_url).getPath().lastIndexOf('/') + 1).equalsIgnoreCase("null"))
                            viewResume.setVisibility(View.GONE);
                        else
                            viewResume.setVisibility(View.VISIBLE);

                        if(new URI(id_proof_url).getPath().substring(new URI(id_proof_url).getPath().lastIndexOf('/') + 1).equalsIgnoreCase("")
                                || new URI(id_proof_url).getPath().substring(new URI(id_proof_url).getPath().lastIndexOf('/') + 1).equalsIgnoreCase("null"))
                            viewIDProof.setVisibility(View.GONE);
                        else
                            viewIDProof.setVisibility(View.VISIBLE);

                        JSONArray eduArray=dataObject.getJSONArray("education");
                        for (int i=0;i<eduArray.length();i++){
                            JSONObject object=eduArray.optJSONObject(i);
                            EducationModal educationModal=new EducationModal();
                            educationModal.setCourse(object.optString("course"));
                            educationModal.setStartFrom(object.optString("start_from"));
                            educationModal.setCertificate(object.optString("certificate"));
                            educationModal.setId(object.optString("id"));
                            educationModal.setEndTo(object.optString("end_to"));
                            educationModal.setYear(object.optString("start_from")+" - "+object.optString("end_to"));
                            educationModal.setURL(object.optString("certificate_url"));

                            educationModalArrayList.add(educationModal);
                        }
                        educationRecycler.setAdapter(new EduAdapter(context,educationModalArrayList, new TrainerProfileFragment()));

                        JSONArray expArray=dataObject.getJSONArray("experience");
                        for (int i=0;i<expArray.length();i++){
                            JSONObject object=expArray.optJSONObject(i);
                            ExpModal expModal=new ExpModal();
                            expModal.setDesignation(object.optString("designation"));
                            expModal.setId(object.optString("id"));
                            expModal.setStart(object.optString("start_from"));
                            expModal.setTo(object.optString("end_to"));
                            expModal.setCertificate(object.optString("certificate"));
                            expModal.setYear(object.optString("start_from")+" - "+object.optString("end_to"));
                            expModal.setURL(object.optString("certificate_url"));
                            expModal.setOrganisation(object.optString("organisation"));

                            expModalArrayList.add(expModal);
                        }
                        expRecycler.setAdapter(new ExpAdapter(context,expModalArrayList));
                        Picasso.with(context).load(dataObject.optString("profile_image_url")).into(userImage);
                        Picasso.with(context).load(dataObject.optString("profile_image_url")).noFade().into(profile_image);
                        sharedPreferences.edit().putString("profile_image",dataObject.optString("profile_image_url")).apply();
                        ((MainActivity)context).showProfilePic();
                        getLanguages();
                        getAllTags();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    private void getAllTags() {
        new GetMethod(Api.allTags+sharedPreferences.getString("lang",""), context).startmethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("allTags",data);
                try {
                    JSONObject dataObject = new JSONObject(data);
                    JSONArray tag = dataObject.optJSONArray("data");
                    tagsModalArrayList.clear();
                    tagTempNames.clear();
                    for (int me=0; me<tag.length(); me++){
                        TagsModal tagsModal = new TagsModal();
                        JSONObject jsonObject = tag.getJSONObject(me);
                        tagsModal.setTagId(jsonObject.optString("id"));
                        tagsModal.setTagName(jsonObject.optString("tag"));
                        tagsModalArrayList.add(tagsModal);
                        tagTempNames.add(jsonObject.optString("tag"));
                    }
                    tagsAutocomplete.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, tagTempNames));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void openFile(String url) {

        try {

            Uri uri = Uri.parse(url);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (url.contains(".doc") || url.contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.contains(".ppt") || url.contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.contains(".xls") || url.contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.contains(".zip")) {
                // ZIP file
                intent.setDataAndType(uri, "application/zip");
            } else if (url.contains(".rar")){
                // RAR file
                intent.setDataAndType(uri, "application/x-rar-compressed");
            } else if (url.contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.contains(".wav") || url.contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.contains(".jpg") || url.contains(".jpeg") || url.contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.contains(".3gp") || url.contains(".mpg") ||
                    url.contains(".mpeg") || url.contains(".mpe") || url.contains(".mp4") || url.contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                intent.setDataAndType(uri, "*/*");
            }

            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            browserIntent.setDataAndType(Uri.parse(url), "text/html");

            Intent chooser = Intent.createChooser(browserIntent, context.getResources().getString(R.string.chooseApp));
            chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

            context.startActivity(chooser);

            Toast.makeText(context, context.getResources().getString(R.string.noApp), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(trainerProfileBroadcast,new IntentFilter("updateTrainer"));
        context.registerReceiver(updateEducation,new IntentFilter("updateEducation"));
        context.registerReceiver(updateExperience,new IntentFilter("updateExperience"));
        context.registerReceiver(exit,new IntentFilter("exit"));
    }

    BroadcastReceiver updateExperience= new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onReceive(Context context, Intent intent) {
            add_edit="edit";
            intentFrom=intent.getStringExtra("from");
            intentTo=intent.getStringExtra("to");
            intentDesignation=intent.getStringExtra("designation");
            intentCertificateName=intent.getStringExtra("certificate");
            intentID=intent.getStringExtra("id");
            intentURL=intent.getStringExtra("url");
            intentOrganisation=intent.getStringExtra("organisation");
            addNewExp();
        }
    };
    BroadcastReceiver updateEducation= new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onReceive(Context context, Intent intent) {
            add_edit="edit";
            intentFrom=intent.getStringExtra("start");
            intentTo=intent.getStringExtra("to");
            intentDegree=intent.getStringExtra("degree");
            intentCertificateName=intent.getStringExtra("certificate");
            intentID=intent.getStringExtra("id");
            intentURL=intent.getStringExtra("url");
            addNewEducation();
        }
    };
    BroadcastReceiver trainerProfileBroadcast= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            trainerDetail();
        }
    };
    BroadcastReceiver exit= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(savePD.getVisibility()==View.VISIBLE){
                quitDialog("pd");
                return;
            }
            if(saveBio.getVisibility()==View.VISIBLE){
                quitDialog("bio");
                return;
            }
            getParentFragmentManager().popBackStackImmediate();
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        context.unregisterReceiver(trainerProfileBroadcast);
        context.unregisterReceiver(exit);
        context.unregisterReceiver(updateEducation);
        context.unregisterReceiver(updateExperience);
    }
}


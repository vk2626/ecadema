package com.ecadema.fragments;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.androidbuts.multispinnerfilter.KeyPairBoolData;
import com.androidbuts.multispinnerfilter.MultiSpinnerSearch;
import com.ecadema.MultipartUtility;
import com.ecadema.adapter.MyPostAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.GetMethod;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.BuildConfig;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.modal.CoursesModal;
import com.ecadema.modal.PostModal;

import net.dankito.richtexteditor.android.RichTextEditor;
import net.dankito.richtexteditor.android.toolbar.AllCommandsEditorToolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MyPostFragment extends Fragment {

  Context context;
  SharedPreferences sharedPreferences;

  public RecyclerView recyclerView;
  TextView post, createPost,submittedPost, publishedPost,noData;

  LinearLayout postLayout;
  ScrollView createPostLayout;
  ProgressDialog progressDialog;
  ImageView close;
  TextView readMore,submitPost,youtubeText,chooseFile,uploadImageOptional,tagText;
  Uri mImageCaptureUri;
  File photoFile,file;
  public String photoFileName="image_" + System.currentTimeMillis()+ ".jpg",profilePicName="";
  private static final int PICK_FROM_FILE = 3;

  private static final int PICK_FROM_CAMERA = 1;
  Bitmap thumbnail;
  android.app.AlertDialog dialog;

  ArrayList<PostModal> postModalArrayList = new ArrayList<>();
  EditText fileName,title,keywords,youTubeURL;
  //AutoCompleteTextView categoryList;
  ArrayList<CoursesModal> coursesModalArrayList =new ArrayList<>();
  ArrayList<String> coursesArrayList =new ArrayList<>();

  WebView mWebView;
  FrameLayout flAction;
  LinearLayout llActionBarContainer;
  ImageView iv_action_insert_link;

  private String htmlContent = "";

  MultiSpinnerSearch multiSelectSpinnerWithSearch;

  List<KeyPairBoolData> listArray1 = new ArrayList<>();

  View root;
  LayoutInflater layoutInflater;
  ViewGroup viewGroup;
  private String[] idArray;
  private String postID;
  private String uploadedImage;
  private final ArrayList<String> tagId=new ArrayList<>();
  TextView tagError,blogError;

  public RichTextEditor editorDescription;
  private AllCommandsEditorToolbar editorToolbarDescription;

  @RequiresApi(api = VERSION_CODES.LOLLIPOP)
  @SuppressLint("ClickableViewAccessibility")
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    context = getActivity();
    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

    Resources activityRes = context.getResources();
    Configuration activityConf = activityRes.getConfiguration();
    Locale newLocale = new Locale(sharedPreferences.getString("lang",""));
    activityConf.setLocale(newLocale);
    activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

    Resources res=getResources();
    DisplayMetrics dr=res.getDisplayMetrics();
    Configuration configuration=res.getConfiguration();
    if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
      configuration.setLocale(new Locale(sharedPreferences.getString("lang","").toLowerCase()));
    }
    else
    {
      configuration.locale=new Locale(sharedPreferences.getString("lang","").toLowerCase());
    }
    res.updateConfiguration(configuration,dr);
    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    root = inflater.inflate(R.layout.my_post_frag, container, false);

    this.layoutInflater=inflater;
    this.viewGroup=container;
    progressDialog=new ProgressDialog(context);
    editorToolbarDescription = root.findViewById(R.id.editorToolbarDescription);
    editorDescription = root.findViewById(R.id.editorDescription);

    editorToolbarDescription.setEditor(editorDescription);

    editorDescription.setEditorFontSize(14);
    editorDescription.setPadding((int) (4 * getResources().getDisplayMetrics().density));


    tagText = root.findViewById(R.id.tagText);
    uploadImageOptional = root.findViewById(R.id.uploadImageOptional);
    chooseFile = root.findViewById(R.id.chooseFile);
    youtubeText = root.findViewById(R.id.youtubeText);
    fileName = root.findViewById(R.id.fileName);
    title = root.findViewById(R.id.title);
    keywords = root.findViewById(R.id.keywords);
    youTubeURL = root.findViewById(R.id.youTubeURL);
    flAction = root.findViewById(R.id.fl_action);
    //categoryList = root.findViewById(R.id.categoryList);
    llActionBarContainer = root.findViewById(R.id.ll_action_bar_container);
    submitPost = root.findViewById(R.id.submitPost);
    close = root.findViewById(R.id.close);
    createPostLayout = root.findViewById(R.id.createPostLayout);
    postLayout = root.findViewById(R.id.postLayout);
    publishedPost = root.findViewById(R.id.publishedPost);
    submittedPost = root.findViewById(R.id.submittedPost);
    post = root.findViewById(R.id.post);
    createPost = root.findViewById(R.id.createPost);
    recyclerView = root.findViewById(R.id.recyclerView);
    noData = root.findViewById(R.id.noData);
    tagError = root.findViewById(R.id.tagError);
    blogError = root.findViewById(R.id.blogError);

    multiSelectSpinnerWithSearch = root.findViewById(R.id.multipleItemSelectionSpinner);
    multiSelectSpinnerWithSearch.setSearchEnabled(true);
    multiSelectSpinnerWithSearch.setShowSelectAllButton(true);

    checkAndRequestPermissions();

    new Handler().postDelayed(()->{
      tagText.setText(getResources().getString(R.string.tags));
      uploadImageOptional.setText(getResources().getString(R.string.uploadImage));
      chooseFile.setText(getResources().getString(R.string.chooseFile));
      youtubeText.setText(getResources().getString(R.string.youtube));
      submitPost.setText(getResources().getString(R.string.submit));
      blogError.setText(getResources().getString(R.string.fieldRequired1));
      tagError.setText(getResources().getString(R.string.fieldRequired1));
      multiSelectSpinnerWithSearch.setHintText(context.getResources().getString(R.string.selectTagsForPost));
      multiSelectSpinnerWithSearch.setSearchHint(context.getResources().getString(R.string.search));
      multiSelectSpinnerWithSearch.setEmptyTitle(context.getResources().getString(R.string.noMatchFound));
      multiSelectSpinnerWithSearch.setClearText(context.getResources().getString(R.string.close));
      multiSelectSpinnerWithSearch.setLimit(3, data -> Toast.makeText(context,
              context.getResources().getString(R.string.selectOnlyTHree), Toast.LENGTH_LONG).show());
      captureImageInitialization();
    },2000);


    close.setOnClickListener(v-> {
      if (createPostLayout.getVisibility()==View.VISIBLE){
        createPostLayout.setVisibility(View.GONE);
        postLayout.setVisibility(View.VISIBLE);

        youTubeURL.setFocusableInTouchMode(true);
        youTubeURL.setFocusable(true);
        youTubeURL.requestFocus();

        new Handler().postDelayed(()->{
          InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
          imm.hideSoftInputFromWindow(youTubeURL.getWindowToken(), 0);
        },500);
        return;
      }
      ((MainActivity)context).onBackPressed();
    });

    chooseFile.setOnClickListener(v -> dialog.show());

    submitPost.setOnClickListener(v -> {
      progressDialog = new ProgressDialog(context);
      progressDialog.show();
      progressDialog.setMessage(context.getResources().getString(R.string.processing));

      youTubeURL.setFocusableInTouchMode(true);
      youTubeURL.setFocusable(true);
      youTubeURL.requestFocus();

      new Handler().postDelayed(()->{
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(youTubeURL.getWindowToken(), 0);
      },500);

      htmlContent = editorDescription.getCachedHtml();

      new Handler().postDelayed(() -> {
        progressDialog.dismiss();

        if (htmlContent.trim().matches("<p><br></p>")){
          tagError.setVisibility(View.GONE);
          blogError.setVisibility(View.VISIBLE);
          return;
        }

        if (htmlContent.trim().matches("")){
          blogError.setVisibility(View.VISIBLE);
          return;
        }

        if (tagId.size()==0){
          tagError.requestFocus();
          tagError.setVisibility(View.VISIBLE);
          return;
        }
        if (multiSelectSpinnerWithSearch.getSelectedItems().size()==0){
          tagError.requestFocus();
          tagError.setVisibility(View.VISIBLE);
          tagId.clear();
          return;
        }
        tagError.setVisibility(View.GONE);
        blogError.setVisibility(View.GONE);

        addCommuntiy();
        // close this activity
      }, 2000);

    });

    getPosts("1");

    return root;
  }

  private void addCommuntiy() {
    progressDialog.setTitle(getResources().getString(R.string.processing));
    progressDialog.show();
    new Thread(() -> {
      try {
        //first_name,last_name,contact_no,country,trainee_id,profile_photo_edit
        MultipartUtility multipart = new MultipartUtility(Api.AddCommunity, "UTF-8");
        multipart.addFormField("trainer_id",sharedPreferences.getString("userID",""));
        multipart.addFormField("blog",htmlContent);
        multipart.addFormField("tags",TextUtils.join(",", tagId));
        multipart.addFormField("video_link",youTubeURL.getText().toString());
        multipart.addFormField("community_id",postID);
        multipart.addFormField("upload_image_edit",uploadedImage);
        if(photoFile!=null) {
          multipart.addFilePart("upload_image", photoFile);
        }else
          multipart.addFormField("upload_image", "");

        String response = multipart.finish();// response from server
        Log.e("AddCommunityResponsee",response);
        try {
          final JSONObject jsonObject1 = new JSONObject(response);
          boolean status = jsonObject1.optBoolean("status");
          ((MainActivity)context).runOnUiThread(() -> {
            progressDialog.cancel();

            // Stuff that updates the UI
            if(status){
              progressDialog.cancel();
              post.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
              post.setTextColor(context.getResources().getColor(R.color.white));
              createPost.setBackgroundColor(context.getResources().getColor(R.color.white));
              createPost.setTextColor(context.getResources().getColor(R.color.dark_grey));
              postLayout.setVisibility(View.VISIBLE);
              createPostLayout.setVisibility(View.GONE);

              publishedPost.setTextColor(context.getResources().getColor(R.color.dark_grey));
              submittedPost.setTextColor(context.getResources().getColor(R.color.colorPrimary));
              title.getText().clear();
              keywords.getText().clear();
              youTubeURL.getText().clear();

              photoFile = null;
              fileName.getText().clear();

              getPosts("1");

            }
            Toast.makeText(context, jsonObject1.optString("message"), Toast.LENGTH_SHORT).show();

          });

        } catch (JSONException e) {
          ((MainActivity)context).runOnUiThread(() -> {
            progressDialog.cancel();
            // Stuff that updates the UI
          });
          e.printStackTrace();
        }
      } catch (IOException e) {
        ((MainActivity)context).runOnUiThread(() -> {
          progressDialog.cancel();
          // Stuff that updates the UI
        });
        e.printStackTrace();
      }
    }).start();
  }

  private void getCategoryList() {
    Map<String,String> param=new HashMap<>();
    param.put("lang_token",sharedPreferences.getString("lang",""));

    new GetMethod(Api.allTags+sharedPreferences.getString("lang",""),context).startmethod(new ResponseData() {
      @Override
      public void response(String data) {
        Log.e("Course List",data);
        try {
          JSONObject jsonObject=new JSONObject(data);
          if(jsonObject.optBoolean("status")){
            listArray1.clear();
            coursesModalArrayList.clear();
            JSONArray jsonArray=jsonObject.optJSONArray("data");
            for (int ar=0;ar<jsonArray.length();ar++){
              JSONObject object=jsonArray.getJSONObject(ar);
              CoursesModal coursesModal =new CoursesModal();
              coursesModal.setCategoryHeading(object.optString("category"));
              coursesModal.setCategoryID(object.optString("id"));
              coursesModalArrayList.add(coursesModal);
              coursesArrayList.add(object.optString("category"));

              KeyPairBoolData h = new KeyPairBoolData();
              h.setId(Long.parseLong(object.optString("id")));
              h.setName(object.optString("tag"));
              if (idArray!=null){
                for (String s : idArray) {
                  if (object.optString("id").equalsIgnoreCase(s))
                    h.setSelected(true);
                }
              }
              //h.setSelected(me < 5);
              listArray1.add(h);
            }
            /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item,coursesArrayList);
            categoryList.setAdapter(adapter);*/

            multiSelectSpinnerWithSearch.setItems(listArray1, items -> {
              tagId.clear();
              for (int i = 0; i < items.size(); i++) {
                if (items.get(i).isSelected()) {
                  if (!tagId.contains(""+items.get(i).getId()))
                    tagId.add(""+items.get(i).getId());
                }
              }
              Log.e("SelectedTags",""+tagId);
            });
          }

        }catch (Exception e){
          e.printStackTrace();
        }
      }

      @Override
      public void error(VolleyError error) {
        error.printStackTrace();
      }
    });
  }

  private void captureImageInitialization() {
    /**
     * a selector dialog to display two image source options, from camera
     * ‘Take from camera’ and from existing files ‘Select from gallery’
     */
    final String[] items = new String[]{getResources().getString(R.string.Takefromcamera),getResources().getString(R.string.Selectfromgallery)};
    ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, items);
    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

    builder.setTitle(getResources().getString(R.string.SelectImage));
    builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int item) { // pick from
        // camera
        if (item == 0) {
          Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
          photoFile= getPhotoFileUri(photoFileName);
          mImageCaptureUri= FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", photoFile);
          intent.putExtra(MediaStore.EXTRA_OUTPUT,mImageCaptureUri);
          intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
          try {
            intent.putExtra("return-data", true);
            startActivityForResult(intent, PICK_FROM_CAMERA);

          } catch (ActivityNotFoundException e) {
            e.printStackTrace();
          }
        } else {
          Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
          startActivityForResult(galleryIntent, PICK_FROM_FILE);
        }
      }
    });

    dialog = builder.create();
  }

  private boolean checkAndRequestPermissions() {
    int cameraPermission = ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA);
    int gps = ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE);
    int writeExternal = ContextCompat.checkSelfPermission(context,
            Manifest.permission.WRITE_EXTERNAL_STORAGE);

    List<String> listPermissionsNeeded = new ArrayList<>();
    if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
      listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
    }

    if (gps != PackageManager.PERMISSION_GRANTED) {
      listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
    }
    if (writeExternal != PackageManager.PERMISSION_GRANTED) {
      listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }
    if (!listPermissionsNeeded.isEmpty()) {
      requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 123);
      return false;
    }
    return true;
  }

  public File getPhotoFileUri(String fileName) {
    File mediaStorageDir = new File(String.valueOf(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)));
    // Create the storage directory if it does not exist
    if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
      Log.d("Error", "failed to create directory");
    }
    // Return the file target for the photo based on filename
    this.file = new File(mediaStorageDir.getPath() + File.separator + fileName);
    return this.file;
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode !=RESULT_OK)
      return;

    switch (requestCode) {
      case PICK_FROM_CAMERA:
        //uri=data.getData();
        /**
         * After taking a picture, do the crop
         */
        try {
          BitmapFactory.Options options = new BitmapFactory.Options();

          thumbnail = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(mImageCaptureUri), null, options);
          fileName.setText( URLUtil.guessFileName(mImageCaptureUri.toString(), null, null));

        } catch (FileNotFoundException e) {
          e.printStackTrace();
        }
        break;

      case PICK_FROM_FILE:
        /**
         * After selecting image from files, save the selected path
         */

        mImageCaptureUri = data.getData();

        try {
          Uri selectedImage = data.getData();
          String[] filePathColumn = {MediaStore.Images.Media.DATA};

          // Get the cursor
          Cursor cursor = context.getContentResolver().query(selectedImage,
                  filePathColumn, null, null, null);
          // Move to first row
          cursor.moveToFirst();

          int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
          String imgDecodableString = cursor.getString(columnIndex);
          cursor.close();

          //filePath = imgDecodableString;

          Bitmap b = BitmapFactory.decodeFile(imgDecodableString);
          if (mImageCaptureUri.toString().startsWith("content://")) {
            try {
              cursor = context.getContentResolver().query(mImageCaptureUri, null, null, null, null);
              if (cursor != null && cursor.moveToFirst()) {
                String displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                fileName.setText(displayName);
                Log.e("content",displayName);
              }
            } finally {
              cursor.close();
            }
          }else if (mImageCaptureUri.toString().startsWith("file://")) {
            File myFile = new File(mImageCaptureUri.toString());
            String displayName = myFile.getName();
            fileName.setText(displayName);
            Log.e("displayName",displayName);
          }

          File file = new File(imgDecodableString);
          photoFile=file;
        } catch (Exception e) {
          e.printStackTrace();
        }
        //doCrop();
        break;

    }
  }

  public void editPost(String shortDecription, String tagIds, String image, String videoUrl, String id){
    Resources activityRes = context.getResources();
    Configuration activityConf = activityRes.getConfiguration();
    Locale newLocale = new Locale(sharedPreferences.getString("lang",""));
    activityConf.setLocale(newLocale);
    activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

    idArray = null;
    tagId.clear();
    tagError.setVisibility(View.GONE);
    blogError.setVisibility(View.GONE);

    postLayout.setVisibility(View.GONE);
    createPostLayout.setVisibility(View.VISIBLE);
    fileName.setText(shortDecription);
    htmlContent=fileName.getText().toString();
    editorDescription.setHtml(htmlContent);
    editorDescription.focusEditorAndShowKeyboard();

   /* mRichEditorAction.insertHtml(htmlContent);
    mRichEditorAction.refreshHtml(mRichEditorCallback, onGetHtmlListener);*/

    if (!tagIds.equalsIgnoreCase("")){
      idArray = tagIds.split(",");

      tagId.addAll(Arrays.asList(idArray));
    }

    postID = id;
    uploadedImage = URLUtil.guessFileName(image, null, null);
    fileName.setText(uploadedImage);
    youTubeURL.setText(videoUrl);
    getCategoryList();

  }
  public void getPosts(String status) {
    Resources activityRes = context.getResources();
    Configuration activityConf = activityRes.getConfiguration();
    Locale newLocale = new Locale(sharedPreferences.getString("lang",""));
    activityConf.setLocale(newLocale);
    activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

    progressDialog.setTitle(context.getResources().getString(R.string.processing));
    progressDialog.show();

    Map<String,String> param = new HashMap<>();
    param.put("trainer_id",sharedPreferences.getString("userID",""));
    param.put("lang",sharedPreferences.getString("lang",""));
    param.put("status","1");
    new PostMethod(Api.GetTrainerPublicSubmitted,param,context).startPostMethod(new ResponseData() {
      @Override
      public void response(final String data) {
        progressDialog.dismiss();
        Log.e("PublishedSubmit",data);
        try {
          postModalArrayList.clear();
          JSONObject jsonObject = new JSONObject(data);
          if(jsonObject.optBoolean("status")){
            noData.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            JSONArray jsonArray = jsonObject.optJSONArray("data");
            for (int i=0; i<jsonArray.length(); i++){
              JSONObject object = jsonArray.getJSONObject(i);
              PostModal postModal = new PostModal();
              postModal.setId(object.optString("id"));
              postModal.setName(object.optString("teacher_name"));
              postModal.setImage(object.optString("profile_image"));
              postModal.setTitle(object.optString("title"));
              postModal.setDate(object.optString("create_date"));
              postModal.setShortDecription(object.optString("short_description"));
              postModal.setView(object.optString("views"));
              postModal.setShareUrl(object.optString("share_url"));
              postModal.setLikes(object.optString("likes"));
              postModal.setDislike(object.optString("dislikes"));
              postModal.setCategoryName(object.optString("categories_name"));
              postModal.setTagIds(object.optString("tags"));
              postModal.setTagName(object.optString("tags_name"));
              postModal.setPostImage(object.optString("image"));
              postModal.setVideoUrl(object.optString("url"));
              postModal.setVideoCode(object.optString("youtube_code"));

              postModalArrayList.add(postModal);
            }
            MyPostAdapter myPostAdapter = new MyPostAdapter(context,postModalArrayList,status,MyPostFragment.this);
            recyclerView.setAdapter(myPostAdapter);
          }else {
            noData.setText(jsonObject.optString("message"));
            noData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
          }
        }catch (Exception e){
          e.printStackTrace();
        }
      }

      @Override
      public void error(final VolleyError error) {
        progressDialog.dismiss();
        error.printStackTrace();
      }
    });
  }

  @Override public void onResume() {
    super.onResume();

    context = getActivity();
    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    Resources res=getResources();
    DisplayMetrics dr=res.getDisplayMetrics();
    Configuration configuration=res.getConfiguration();
    if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
      configuration.setLocale(new Locale(sharedPreferences.getString("lang","").toLowerCase()));
    }
    else
    {
      configuration.locale=new Locale(sharedPreferences.getString("lang","").toLowerCase());
    }
    res.updateConfiguration(configuration,dr);
  }

  @Override public void onPause() {
    super.onPause();
    progressDialog.dismiss();
  }

  @Override public void onDestroy() {
    super.onDestroy();
  }

  private void setAppLocale(String LocaleCode) {
    Resources res = getResources();
    DisplayMetrics dr = res.getDisplayMetrics();
    Configuration configuration = res.getConfiguration();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
      configuration.setLocale(new Locale(LocaleCode.toLowerCase()));
    } else {
      configuration.locale = new Locale(LocaleCode.toLowerCase());
    }
    res.updateConfiguration(configuration, dr);
  }
}

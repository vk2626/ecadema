package com.ecadema.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.adapter.MyTrainerAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.modal.TrainerModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyTrainersFragment extends Fragment {

    Context context;
    SharedPreferences sharedPreferences;
    RecyclerView recyclerView;
    ArrayList<TrainerModal> trainerModalArrayList=new ArrayList<>();
    MyTrainerAdapter myTrainerAdapter;
    ProgressDialog progressDialog;
    TextView noTrainer;
    ImageView close;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_my_trainer, container, false);

        context=getContext();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        progressDialog= new ProgressDialog(context);

        close=root.findViewById(R.id.close);
        noTrainer=root.findViewById(R.id.noTrainer);
        recyclerView=root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));

        close.setOnClickListener(v->{
            ((MainActivity)context).getSupportFragmentManager().popBackStackImmediate();
        });
        getAllTrainers();

        return root;
    }

    private void getAllTrainers() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("trainee_id",sharedPreferences.getString("userID",""));

        new PostMethod(Api.MyTrainers,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        recyclerView.setVisibility(View.VISIBLE);
                        noTrainer.setVisibility(View.GONE);
                        JSONArray jsonArray= jsonObject.optJSONArray("data");
                        trainerModalArrayList.clear();
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject dataObj = jsonArray.getJSONObject(ar);
                            TrainerModal trainerModal=new TrainerModal();
                            trainerModal.setTeacherID(dataObj.optString("teacher_id"));
                            trainerModal.setImage(dataObj.optString("profile_image"));
                            trainerModal.setName(dataObj.optString("first_name")+" "+dataObj.optString("last_name"));
                            trainerModal.setRating(dataObj.optString("rating"));

                            trainerModalArrayList.add(trainerModal);
                        }
                        myTrainerAdapter=new MyTrainerAdapter(context,trainerModalArrayList);
                        recyclerView.setAdapter(myTrainerAdapter);
                    }else{
                        recyclerView.setVisibility(View.GONE);
                        noTrainer.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                Log.e("My Trainers",data);
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(updateTrainer,new IntentFilter("trainerUpdate"));
    }

    BroadcastReceiver updateTrainer = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getAllTrainers();
        }
    };
}

package com.ecadema.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.VerticalSpaceItemDecoration;
import com.ecadema.adapter.LanguageAdapter;
import com.ecadema.adapter.PastAdapter;
import com.ecadema.adapter.TagsAdapter;
import com.ecadema.adapter.UpcomingAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.GetMethod;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.R;
import com.ecadema.modal.LanguageModal;
import com.ecadema.modal.PastModal;
import com.ecadema.modal.TagsModal;
import com.ecadema.modal.UpcomingModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AllSessionFrag extends Fragment {

    RecyclerView upcomingRecyclerView,pastRecyclerView;
    UpcomingAdapter upcomingAdapter;
    PastAdapter pastAdapter;

    ArrayList<UpcomingModal> upcomingModalArrayList=new ArrayList<>();
    ArrayList<PastModal> pastModalArrayList=new ArrayList<>();

    Context context;
    SharedPreferences sharedPreferences;
    TextView upcoming,past,noPastText,noUpcomingText;

    RelativeLayout upcomingSessionView,pastSessionView;

    CardView allFiltersLayout,clearFilterLayout,languageLayout,typeLayout;
    LinearLayout tagLayout,courseType,language,tags,searchMainLayout,searchLayout;
    EditText search,searchTrainerAndCourse;
    RecyclerView tagRecyclerView,languageRecyclerView;
    TextView done,clearFilter,browseTypeText,languageText,tagText,searchTrainerText;
    CheckBox workshopCheckBox,courseCheckBox;
    ImageView closeSearchTrainer;
    String typeFilter="";
    ArrayList<TagsModal> tagsModalArrayList=new ArrayList<>();
    String tagFilter="",langFilter="";
    ArrayList<LanguageModal> languageList=new ArrayList<>();
    LanguageAdapter languageAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_all_sessions, container, false);

        context= getContext();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

        searchTrainerText= root.findViewById(R.id.searchTrainerText);
        closeSearchTrainer= root.findViewById(R.id.closeSearchTrainer);
        searchTrainerAndCourse= root.findViewById(R.id.searchTrainerAndCourse);
        searchLayout= root.findViewById(R.id.searchLayout);
        searchMainLayout= root.findViewById(R.id.searchTrainerLayout);
        tags= root.findViewById(R.id.tags);
        courseType= root.findViewById(R.id.courseType);
        language= root.findViewById(R.id.language);
        allFiltersLayout= root.findViewById(R.id.allFiltersLayout);
        clearFilterLayout= root.findViewById(R.id.clearFilterLayout);
        languageLayout= root.findViewById(R.id.languageLayout);
        typeLayout= root.findViewById(R.id.typeLayout);
        tagLayout= root.findViewById(R.id.tagLayout);
        search= root.findViewById(R.id.search);
        tagRecyclerView= root.findViewById(R.id.tagRecyclerView);
        languageRecyclerView= root.findViewById(R.id.languageRecyclerView);
        done= root.findViewById(R.id.done);
        clearFilter= root.findViewById(R.id.clearFilter);
        browseTypeText= root.findViewById(R.id.browseTypeText);
        languageText= root.findViewById(R.id.languageText);
        tagText= root.findViewById(R.id.tagText);
        workshopCheckBox= root.findViewById(R.id.workshopCheckBox);
        courseCheckBox= root.findViewById(R.id.courseCheckBox);

        upcomingSessionView= root.findViewById(R.id.upcomingSessionView);
        pastSessionView= root.findViewById(R.id.pastSessionView);
        noUpcomingText= root.findViewById(R.id.noUpcomingText);
        noPastText= root.findViewById(R.id.noPastText);
        upcoming= root.findViewById(R.id.upcoming);
        past= root.findViewById(R.id.past);

        upcomingRecyclerView= root.findViewById(R.id.upcomingRecyclerView);
        pastRecyclerView= root.findViewById(R.id.pastRecyclerView);

        upcomingRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        pastRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        VerticalSpaceItemDecoration verticalSpaceItemDecoration=new VerticalSpaceItemDecoration(0,0,30,10);
        upcomingRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(0,0,20,10));
        pastRecyclerView.addItemDecoration(verticalSpaceItemDecoration);

        upcomingSessions("show");

        upcoming.setOnClickListener(view -> {
            upcoming.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            past.setTextColor(context.getResources().getColor(R.color.dark_grey));
            pastSessionView.setVisibility(View.GONE);
            upcomingSessionView.setVisibility(View.VISIBLE);
        });

        pastSessions("show");

        past.setOnClickListener(view -> {
            past.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            upcoming.setTextColor(context.getResources().getColor(R.color.dark_grey));
            pastSessionView.setVisibility(View.VISIBLE);
            upcomingSessionView.setVisibility(View.GONE);
        });

        searchTrainerAndCourse.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(upcomingAdapter!=null) {
                    upcomingAdapter.filter(charSequence.toString());
                    if(upcomingAdapter.getItemCount()==0) {
                        upcomingRecyclerView.setVisibility(View.GONE);
                        noUpcomingText.setVisibility(View.VISIBLE);
                        return;
                    }
                    noUpcomingText.setVisibility(View.GONE);
                    upcomingRecyclerView.setVisibility(View.VISIBLE);
                }
                if(pastAdapter!=null) {
                    pastAdapter.filter(charSequence.toString());
                    if(pastAdapter.getItemCount()==0) {
                        pastRecyclerView.setVisibility(View.GONE);
                        noPastText.setVisibility(View.VISIBLE);
                        return;
                    }
                    noPastText.setVisibility(View.GONE);
                    pastRecyclerView.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        searchMainLayout.setOnClickListener(v->{
            allFiltersLayout.setVisibility(View.GONE);
            languageText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            searchTrainerText.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            tagText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            browseTypeText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            searchLayout.setVisibility(View.VISIBLE);
        });
        closeSearchTrainer.setOnClickListener(v->{
            upcomingSessions("hide");
            pastSessions("hide");
            languageText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            searchTrainerText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            tagText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            browseTypeText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            searchLayout.setVisibility(View.GONE);
        });

        done.setOnClickListener(v-> {
            allFiltersLayout.setVisibility(View.GONE);
            languageText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            searchTrainerText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            tagText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            browseTypeText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            searchLayout.setVisibility(View.GONE);

        });
        clearFilter.setOnClickListener(v->{
            typeFilter="";
            tagFilter="";
            langFilter="";
            workshopCheckBox.setChecked(false);
            courseCheckBox.setChecked(false);
            getAllTags();
            getLanguages();
            upcomingSessions("show");
            pastSessions("show");
        });
        courseType.setOnClickListener(v->{
            languageText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            searchTrainerText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            tagText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            browseTypeText.setTextColor(context.getResources().getColor(R.color.colorPrimary));

            if (allFiltersLayout.getVisibility()==View.GONE)
                allFiltersLayout.setVisibility(View.VISIBLE);

            searchLayout.setVisibility(View.GONE);
            typeLayout.setVisibility(View.VISIBLE);
            tagLayout.setVisibility(View.GONE);
            languageLayout.setVisibility(View.GONE);
        });
        tags.setOnClickListener(v->{
            languageText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            searchTrainerText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            browseTypeText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            tagText.setTextColor(context.getResources().getColor(R.color.colorPrimary));

            if (allFiltersLayout.getVisibility()==View.GONE)
                allFiltersLayout.setVisibility(View.VISIBLE);

            searchLayout.setVisibility(View.GONE);
            typeLayout.setVisibility(View.GONE);
            tagLayout.setVisibility(View.VISIBLE);
            languageLayout.setVisibility(View.GONE);
        });
        language.setOnClickListener(v->{
            browseTypeText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            searchTrainerText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            tagText.setTextColor(context.getResources().getColor(R.color.dark_grey));
            languageText.setTextColor(context.getResources().getColor(R.color.colorPrimary));

            if (allFiltersLayout.getVisibility()==View.GONE)
                allFiltersLayout.setVisibility(View.VISIBLE);

            searchLayout.setVisibility(View.GONE);
            typeLayout.setVisibility(View.GONE);
            tagLayout.setVisibility(View.GONE);
            languageLayout.setVisibility(View.VISIBLE);
        });

        workshopCheckBox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (typeFilter.equalsIgnoreCase(""))
                    typeFilter="workshop";
                else if (courseCheckBox.isChecked()){
                    typeFilter="course"+","+"workshop";
                }
            }else {
                if (courseCheckBox.isChecked())
                    typeFilter = "course";
                else
                    typeFilter="";
            }
            upcomingSessions("hide");
            pastSessions("hide");
        });
        courseCheckBox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                if (typeFilter.equalsIgnoreCase(""))
                    typeFilter="course";
                else if (workshopCheckBox.isChecked()){
                    typeFilter="workshop"+","+"course";
                }
            }
            else {
                if (workshopCheckBox.isChecked())
                    typeFilter = "workshop";
                else
                    typeFilter="";
            }
            upcomingSessions("hide");
            pastSessions("hide");
        });

        getAllTags();
        getLanguages();
        return root;
    }

    private void getLanguages() {
        Map<String,String> param = new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("page_filter","group-session");

        new PostMethod(Api.langFilter,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("lang",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    languageList.clear();
                    if(jsonObject.optBoolean("status")){
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        for (int ar=0;ar<jsonArray.length();ar++){
                            JSONObject object=jsonArray.getJSONObject(ar);
                            LanguageModal languageModal=new LanguageModal();
                            languageModal.setId(object.optString("id"));
                            languageModal.setLanguage(object.getString("language"));

                            languageList.add(languageModal);
                        }
                        languageAdapter=new LanguageAdapter(context,languageList);
                        languageRecyclerView.setAdapter(languageAdapter);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void getAllTags() {
        new GetMethod(Api.allSessionsTags+sharedPreferences.getString("lang",""), context).startmethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("allTags",data);
                try {
                    JSONObject dataObject = new JSONObject(data);
                    JSONArray tag = dataObject.optJSONArray("data");
                    tagsModalArrayList.clear();
                    for (int me=0; me<tag.length(); me++){
                        TagsModal tagsModal = new TagsModal();
                        JSONObject jsonObject = tag.getJSONObject(me);
                        tagsModal.setTagId(jsonObject.optString("id"));
                        tagsModal.setTagName(jsonObject.optString("tag"));
                        tagsModalArrayList.add(tagsModal);
                    }
                    TagsAdapter tagsAdapter = new TagsAdapter(context,tagsModalArrayList);
                    tagRecyclerView.setAdapter(tagsAdapter);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });

    }


    private void pastSessions(String progress) {
        new GetMethod(Api.PastGroupSessions+sharedPreferences.getString("lang","")+"&type="+typeFilter
                +"&tags="+tagFilter+"&language="+langFilter,getContext()).startmethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    Log.e("pastSessions",data);
                    pastModalArrayList.clear();
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        for (int ar=0;ar<jsonObject.optJSONArray("data").length();ar++){
                            JSONObject object=jsonObject.optJSONArray("data").getJSONObject(ar);
                            PastModal pastModal=new PastModal();
                            pastModal.setID(object.optString("id"));
                            pastModal.setImage(object.optString("profile_image"));
                            pastModal.setName(object.optString("teacher_name"));
                            pastModal.setSessionType(object.optString("type"));
                            pastModal.setAbout(object.optString("about"));
                            pastModal.setBanner(object.optString("image"));
                            pastModal.setTicket("$ "+object.optString("ticket_cost"));

                            pastModalArrayList.add(pastModal);
                        }
                        pastAdapter=new PastAdapter(context,pastModalArrayList,"All");
                        pastRecyclerView.setAdapter(pastAdapter);

                        pastRecyclerView.setVisibility(View.VISIBLE);
                        noPastText.setVisibility(View.GONE);
                    }else {
                        noPastText.setText(jsonObject.optString("message"));
                        pastRecyclerView.setVisibility(View.GONE);
                        noPastText.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });

    }

    private void upcomingSessions(String progress) {
        ProgressDialog progressDialog=new ProgressDialog(getContext());
        if (progress.equalsIgnoreCase("show"))
            progressDialog.show();
        progressDialog.setMessage(getResources().getString(R.string.loading));
        new GetMethod(Api.UpGroupSessions+sharedPreferences.getString("lang","")+"&type="+typeFilter
                +"&tags="+tagFilter+"&language="+langFilter,getContext()).startmethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    progressDialog.dismiss();
                    Log.e("UpGroupSessionsZ",data);
                    upcomingModalArrayList.clear();
                    JSONObject jsonObject=new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        for (int ar=0;ar<jsonObject.optJSONArray("data").length();ar++){
                            JSONObject object=jsonObject.optJSONArray("data").getJSONObject(ar);
                            UpcomingModal upcomingModal=new UpcomingModal();
                            upcomingModal.setID(object.optString("id"));
                            upcomingModal.setImage(object.optString("profile_image"));
                            upcomingModal.setName(object.optString("teacher_name"));
                            upcomingModal.setSessionType(object.optString("type"));
                            upcomingModal.setTeacherID(object.optString("teacher_id"));
                            upcomingModal.setAbout(object.optString("about"));
                            upcomingModal.setBanner(object.optString("image"));
                            upcomingModal.setTicket("$ "+object.optString("ticket_cost"));

                            upcomingModalArrayList.add(upcomingModal);
                        }
                        upcomingRecyclerView.setVisibility(View.VISIBLE);
                        noUpcomingText.setVisibility(View.GONE);

                        upcomingAdapter=new UpcomingAdapter(context,upcomingModalArrayList,"All");
                        upcomingRecyclerView.setAdapter(upcomingAdapter);
                    }else {
                        noUpcomingText.setText(jsonObject.optString("message"));
                        upcomingRecyclerView.setVisibility(View.GONE);
                        noUpcomingText.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.dismiss();
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(langLocBroadCast,new IntentFilter("langLocBroadCast"));
        context.registerReceiver(tagBroadCast,new IntentFilter("tagBroadCast"));
    }

    BroadcastReceiver tagBroadCast =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("filterType").equalsIgnoreCase("tag")){
                tagFilter=intent.getStringExtra("tagLoc");

            }
            upcomingSessions("hide");
            pastSessions("hide");
        }
    };

    BroadcastReceiver langLocBroadCast =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("filterType").equalsIgnoreCase("lang")){
                langFilter=intent.getStringExtra("langLoc");

            }

            upcomingSessions("hide");
            pastSessions("hide");
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        context.unregisterReceiver(tagBroadCast);
        context.unregisterReceiver(langLocBroadCast);
    }
}

package com.ecadema.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class CompanyDetailFragment extends Fragment {

    EditText email,editBusiness,edRegisterName,edcountry,edWebsite,edTelName,edemailCompany;
    EditText edFirstname,edLastname,edDepartment,edseniority,edmobile,edemail;
    TextView tvPersonalDetail,tvCompanyDetail,sendInvitation,sendRequest;
    ScrollView scrollPersonal,scrollCompany;
    LinearLayout optionLayout;
    ImageView close;
    SharedPreferences sharedPreferences;
    Context context;
    String requested_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_company_detail, container, false);

        context = getActivity();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

        edFirstname=view.findViewById(R.id.edFirstname);
        edLastname=view.findViewById(R.id.edLastname);
        edDepartment=view.findViewById(R.id.edDepartment);
        edseniority=view.findViewById(R.id.edseniority);
        edmobile=view.findViewById(R.id.edmobile);
        edemail=view.findViewById(R.id.edemail);
        sendRequest=view.findViewById(R.id.sendRequest);

        edFirstname.setText(sharedPreferences.getString("firstName",""));
        edLastname.setText(sharedPreferences.getString("lastName",""));
        edmobile.setText(sharedPreferences.getString("contact_no",""));
        edemail.setText(sharedPreferences.getString("email",""));

        edRegisterName=view.findViewById(R.id.edRegisterName);
        editBusiness=view.findViewById(R.id.editBusiness);
        edcountry=view.findViewById(R.id.edcountry);
        edWebsite=view.findViewById(R.id.edWebsite);
        edTelName=view.findViewById(R.id.edTelName);
        edemailCompany=view.findViewById(R.id.edemailCompany);
        close=view.findViewById(R.id.close);
        email=view.findViewById(R.id.email);
        optionLayout=view.findViewById(R.id.optionLayout);
        tvPersonalDetail=view.findViewById(R.id.tvPersonalDetail);
        scrollCompany=view.findViewById(R.id.scrollCompany);
        scrollPersonal=view.findViewById(R.id.scrollPersonal);
        tvCompanyDetail=view.findViewById(R.id.tvCompanyDetail);
        sendInvitation=view.findViewById(R.id.sendInvitation);

        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        close.setOnClickListener(v-> getParentFragmentManager().popBackStackImmediate());
        sendInvitation.setOnClickListener(v->{
            if(email.getText().toString().matches("")) {
                email.setError(context.getResources().getString(R.string.fieldRequired));
                return;
            }
            if (!email.getText().toString().matches(emailPattern)){
                email.setError(context.getResources().getString(R.string.entervalidemail));
                return;
            }
            sendInvitationData(email.getText().toString());
        });
        sendRequest.setOnClickListener(v->{
            if (edDepartment.getText().toString().trim().matches(""))
                edDepartment.setError(context.getResources().getString(R.string.fieldRequired));
            else if (edseniority.getText().toString().trim().matches(""))
                edseniority.setError(context.getResources().getString(R.string.fieldRequired));
            else
                sendReqToCompany();
        });
        tvPersonalDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollCompany.setVisibility(View.GONE);
                tvPersonalDetail.setTextColor(getResources().getColor(R.color.colorPrimary));
                tvCompanyDetail.setTextColor(getResources().getColor(R.color.dark_grey));
                scrollPersonal.setVisibility(View.VISIBLE);
            }
        });
        tvCompanyDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollPersonal.setVisibility(View.GONE);
                scrollCompany.setVisibility(View.VISIBLE);
                tvPersonalDetail.setTextColor(getResources().getColor(R.color.dark_grey));
                tvCompanyDetail.setTextColor(getResources().getColor(R.color.colorPrimary));

            }
        });
        return view;
    }

    private void sendReqToCompany() {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("requested_id",requested_id);
        param.put("department",edDepartment.getText().toString());
        param.put("seniority",edseniority.getText().toString());
        Log.e("TeammatesRequest",""+param);

        new PostMethod(Api.TeammatesRequest,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("TeammatesRequestData",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONObject Data = jsonObject.optJSONObject("data");
                    if(Data.optInt("status")==1){
                        email.getText().clear();
                        scrollCompany.setVisibility(View.GONE);
                        scrollPersonal.setVisibility(View.GONE);
                        optionLayout.setVisibility(View.GONE);
                    }
                    Toast.makeText(context, Data.optString("msg"), Toast.LENGTH_SHORT).show();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    private void sendInvitationData(String emailAdd) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("email",emailAdd);
        Log.e("TeammatesInvitation",""+param);

        new PostMethod(Api.TeammatesInvitation,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                Log.e("sendInvitation",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONObject Data = jsonObject.optJSONObject("data");
                    if(Data.optInt("status")==1){
                        JSONObject compData = Data.optJSONObject("data");
                        editBusiness.setText(compData.optString("business_name"));
                        edRegisterName.setText(compData.optString("registration_no"));
                        edcountry.setText(compData.optString("country_name"));
                        edWebsite.setText(compData.optString("website_url"));
                        edTelName.setText(compData.optString("tel_number"));
                        edemailCompany.setText(compData.optString("email"));
                        requested_id = compData.optString("id");

                        optionLayout.setVisibility(View.VISIBLE);
                        scrollCompany.setVisibility(View.VISIBLE);
                    }else {
                        email.setError(jsonObject.optString("message"));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }
}
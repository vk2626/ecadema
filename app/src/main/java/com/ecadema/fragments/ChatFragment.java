package com.ecadema.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.adapter.ChatListAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.modal.ChatListModal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ChatFragment extends Fragment {

    Context context;
    SharedPreferences sharedPreferences;
    RecyclerView recyclerView;
    ArrayList<ChatListModal> chatListModalArrayList =new ArrayList<>();
    ChatListAdapter chatListAdapter;
    ProgressDialog progressDialog;
    TextView noChat;
    EditText search;
    String api="";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context= getContext();

        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

        Resources res=getResources();
        DisplayMetrics dr=res.getDisplayMetrics();
        Configuration configuration=res.getConfiguration();
        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.JELLY_BEAN_MR1){
            configuration.setLocale(new Locale(sharedPreferences.getString("lang","").toLowerCase()));
        }
        else
        {
            configuration.locale=new Locale(sharedPreferences.getString("lang","").toLowerCase());
        }
        res.updateConfiguration(configuration,dr);
        View root = inflater.inflate(R.layout.fragment_chat, container, false);

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();

        search=root.findViewById(R.id.search);
        noChat=root.findViewById(R.id.noChat);
        recyclerView=root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(chatListAdapter!=null) {
                    chatListAdapter.filter(charSequence.toString());
                    if(chatListAdapter.getItemCount()==0) {
                        recyclerView.setVisibility(View.GONE);
                        noChat.setVisibility(View.VISIBLE);
                        return;
                    }
                    noChat.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        return root;
    }

    private void getList() {
        Map<String,String> param = new HashMap<>();
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("lang_token",sharedPreferences.getString("lang",""));

        Log.e("chatList",""+param);
        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("3"))
            api=Api.ChatTraineeList;
        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("1"))
            api=Api.ChatTrainerList;
       if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2"))
            api=Api.ChatCorporateList;

        new PostMethod(api,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if (jsonObject.optBoolean("status")){
                        noChat.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        chatListModalArrayList.clear();
                        //JSONObject dataObj= jsonObject.optJSONObject("data");
                        JSONArray dataArray= jsonObject.optJSONArray("data");
                        for(int ar=0;ar<dataArray.length();ar++){
                            JSONObject object = dataArray.getJSONObject(ar);
                            ChatListModal chatListModal =new ChatListModal();
                            chatListModal.setChatId(""+(ar+1));
                            chatListModal.setAccountActive(object.optString("is_active"));

                            if(sharedPreferences.getString("user_type","").equalsIgnoreCase("3")) {
                                chatListModal.setReceiversID(object.optString("student_id"));
                                chatListModal.setUserName(object.optString("student_name"));
                            }

                            if(sharedPreferences.getString("user_type","").equalsIgnoreCase("1")) {
                                chatListModal.setReceiversID(object.optString("teacher_id"));
                                chatListModal.setUserName(object.optString("teacher_name"));
                                chatListModal.setCourses(object.optJSONArray("courses"));
                            }
                            if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")) {
                                chatListModal.setReceiversID(object.optString("teacher_id"));
                                chatListModal.setUserName(object.optString("teacher_name"));
                                chatListModal.setCourses(object.optJSONArray("courses"));
                            }

                            chatListModal.setDescription("Description "+(ar+1));
                            chatListModal.setUnread(object.optInt("unread"));

                            if(object.optJSONObject("last_msg")!=null) {
                                chatListModal.setDate(object.optJSONObject("last_msg").optString("timestamp"));
                                try {
                                    String Title = URLDecoder.decode(object.optJSONObject("last_msg").optString("chat_message"), "UTF-8");
                                    chatListModal.setLastMessageString(Title);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }else {
                                chatListModal.setDate("");
                                chatListModal.setLastMessageString("");
                            }
                            chatListModal.setDP(object.optString("image_url"));
                            chatListModal.setLastMessage(object.optJSONObject("last_msg"));
                            chatListModal.setOnlineStatus(object.optString("status"));
                            chatListModalArrayList.add(chatListModal);
                        }
                        chatListAdapter =new ChatListAdapter(context, chatListModalArrayList);
                        recyclerView.setAdapter(chatListAdapter);
                    }else {
                        noChat.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                Log.e("chatFragResp",data);
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getList();
        context.registerReceiver(update,new IntentFilter("update"));
        ((MainActivity)context).getChatCount();
        sharedPreferences.edit().putString("chatRoom","0").apply();
    }

    BroadcastReceiver update =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getList();
            ((MainActivity)context).getChatCount();
        }
    };
}

package com.ecadema.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.ecadema.app.ChatRoom;
import com.ecadema.app.LoginActivity;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.adapter.CustomAdapter;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.Web_View;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

public class SessionDetails extends Fragment implements YouTubePlayer.OnInitializedListener{

    Context context;
    SharedPreferences sharedPreferences;

    TextView viewProfile,chat,video,bookNow;
    ProgressDialog progressDialog;

    CircleImageView profile_pic;
    ImageView header_pic;
    TextView name,sessionType,about,ticket,dateTime,totalHours,maxParticipants,languages,overview,whyAttend,wst;
    ScrollView mainLayout;
    private String teacher_id="",youtube_url="";
    TimeZone tz = TimeZone.getDefault();
    ArrayList<String>dateArray;
    private String imageurl;
    MultiAutoCompleteTextView autoComplete;

    public static final String DEVELOPER_KEY = "AIzaSyA0dSJ53Fvxhg7T4FSmgeKHkNuK5EM3Pnc";

    // YouTube video id
    public String YOUTUBE_VIDEO_CODE = "PTrNHW4BhSo";
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    YouTubePlayer YPlayer;
    private YouTubePlayerSupportFragment youTubePlayerFragment;
    RelativeLayout videoView;
    ArrayList<String> emailID = new ArrayList<>();
    boolean booking =true;
    JSONArray coursesArray;
    CardView chatCard,bookCard;
    String wizIQURL="";

    @SuppressLint("ClickableViewAccessibility")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_seesion_details, container, false);

        context= getContext();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

        progressDialog=new ProgressDialog(context);

        bookCard= root.findViewById(R.id.bookCard);
        chatCard= root.findViewById(R.id.chatCard);
        mainLayout= root.findViewById(R.id.mainLayout);
        header_pic= root.findViewById(R.id.headerPic);
        profile_pic= root.findViewById(R.id.profile_pic);
        name= root.findViewById(R.id.name);
        sessionType= root.findViewById(R.id.sessionType);
        about= root.findViewById(R.id.about);
        ticket= root.findViewById(R.id.ticket);
        dateTime= root.findViewById(R.id.dateTime);
        totalHours= root.findViewById(R.id.totalHours);
        maxParticipants= root.findViewById(R.id.maxParticipants);
        languages= root.findViewById(R.id.languages);
        overview= root.findViewById(R.id.overview);
        whyAttend= root.findViewById(R.id.whyAttend);
        wst= root.findViewById(R.id.wst);

        videoView= root.findViewById(R.id.videoView);
        viewProfile= root.findViewById(R.id.viewProfile);
        chat= root.findViewById(R.id.chat);
        video= root.findViewById(R.id.video);
        bookNow= root.findViewById(R.id.bookNow);

        getSessionDetails();
        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")) {
            getTeammates();
        }
        youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_fragment, youTubePlayerFragment);
        transaction.commit();

        ImageView close1=root.findViewById(R.id.close);
        close1.setOnClickListener(v1-> videoView.setVisibility(View.GONE));
        video.setOnClickListener(v->{
            videoView.setVisibility(View.VISIBLE);
        });



        chat.setOnClickListener(v->{
            if (sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
                return;
            }

            Intent intent=new Intent(context, ChatRoom.class);
            intent.putExtra("chatID","");
            intent.putExtra("userName",name.getText().toString());
            intent.putExtra("receiverID",teacher_id);
            intent.putExtra("userImage",imageurl);
            intent.putExtra("courses",coursesArray.toString());
            context.startActivity(intent);

        });
        viewProfile.setOnClickListener(v->{
            Bundle bundle=new Bundle();
            bundle.putString("trainerID",getArguments().getString("trainerID"));
            ViewProfile viewProfile=new ViewProfile();
            viewProfile.setArguments(bundle);
            ((MainActivity) context).addFragment2(viewProfile, true, false, 0);
        });
        bookNow.setOnClickListener(v->{
            if(sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                Intent intent=new Intent(context, LoginActivity.class);
                startActivity(intent);
                return;
            }

            if (bookNow.getText().toString().equalsIgnoreCase(getResources().getString(R.string.attend))){
                Intent intent = new Intent(context, Web_View.class);
                intent.putExtra("url",wizIQURL);
                context.startActivity(intent);
                /*Intent intent = new Intent(context, ClassroomActivity.class);
                intent.putExtra(ClassroomActivity.URL, wizIQURL);
                context.startActivity(intent);*/
                return;
            }
            if(!booking){
                Toast.makeText(context, getResources().getString(R.string.cannotBookThisSessionNow), Toast.LENGTH_SHORT).show();
                return;
            }
            AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                    inflater1.inflate(R.layout.booking_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            LinearLayout sessionTimes=dialogView.findViewById(R.id.sessionTimes);
            autoComplete = dialogView.findViewById(R.id.autoComplete);
            if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")) {
               autoComplete.setVisibility(View.VISIBLE);
               autoComplete.setAdapter(new CustomAdapter(context,R.layout.checkbox_layout,emailID));

                autoComplete.setOnTouchListener((view, motionEvent) -> {
                    if(motionEvent.getAction()== MotionEvent.ACTION_DOWN)
                        autoComplete.showDropDown();
                    return true;
                });

                ArrayList<String> arrayList =new ArrayList<>();
                //List of attendees
                autoComplete.setOnItemClickListener((parent, view, i, id) -> {
                    String selection = (String) parent.getItemAtPosition(i);
                    if(arrayList.contains(selection))
                        arrayList.remove(selection);
                    else
                        arrayList.add(selection);
                    autoComplete.setText(android.text.TextUtils.join(",", arrayList));
                    autoComplete.setAdapter(new CustomAdapter(context,R.layout.checkbox_layout,emailID));
                });
            }
            ImageView close=dialogView.findViewById(R.id.close);
            TextView book=dialogView.findViewById(R.id.book);
            TextView rate=dialogView.findViewById(R.id.rate);
            rate.setText(getResources().getString(R.string.ticket)+ticket.getText().toString());
            close.setOnClickListener(v1-> dialog.dismiss());

            book.setOnClickListener(v1->{
                if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")){
                    if(autoComplete.getText().toString().trim().matches("")){
                        Snackbar.make(v1,getResources().getString(R.string.pleaseSelectMember),Snackbar.LENGTH_LONG).show();
                        return;
                    }
                    if(autoComplete.getText().toString().equalsIgnoreCase(getResources().getString(R.string.allReadyBookedForAllMembers))){
                        Snackbar.make(v1,getResources().getString(R.string.allReadyBookedForAllMembers),Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                savebooking(dialog);

            });
            for (int ar=0;ar<dateArray.size();ar++){
                CheckBox checkBox=new CheckBox(context);
                checkBox.setText(dateArray.get(ar));
                checkBox.setChecked(true);
                checkBox.setClickable(false);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0,0,0,10);
                checkBox.setLayoutParams(lp);
                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/montserrat_medium.ttf");
                checkBox.setTypeface(font);
                checkBox.setTextSize(TypedValue.COMPLEX_UNIT_SP,15);
                checkBox.setTextColor(context.getResources().getColor(R.color.dark_grey));
                sessionTimes.addView(checkBox);
            }

            dialog.create();
            dialog.show();

        });
        return root;
    }

    private void getTeammates() {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("user_id",sharedPreferences.getString("userID",""));
        param.put("workshop_id",getArguments().getString("sessionId"));

        new PostMethod(Api.Teammates,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("Teammates",data);
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){

                        emailID.clear();
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        if(jsonArray.length()==0){
                            emailID.add(getResources().getString(R.string.allReadyBookedForAllMembers));
                            return;
                        }
                        for(int ar=0;ar<jsonArray.length();ar++){
                            JSONObject response = jsonArray.optJSONObject(ar);
                            emailID.add(response.optString("email"));
                        }
                    }
                    else
                        emailID.add(getResources().getString(R.string.allReadyBookedForAllMembers));

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void savebooking(Dialog dialog) {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> map=new HashMap<>();
        map.put("user_id",sharedPreferences.getString("userID",""));
        map.put("workshop_id",getArguments().getString("sessionId"));
        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2"))
            map.put("emails[]",autoComplete.getText().toString().replace(",\n",","));
        else
            map.put("emails[]",sharedPreferences.getString("email",""));
        map.put("rate",ticket.getText().toString().replace("$ ",""));
        map.put("timezone",tz.getID());
        map.put("lang_token",sharedPreferences.getString("lang",""));
        map.put("teacher_id",teacher_id);
        Log.e("savebookingparam",""+map);
        new PostMethod(Api.SaveWorkshopBooking,map,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("savebookingresponse",data);
                try {
                    progressDialog.cancel();;
                    JSONObject jsonObject=new JSONObject(data);
                    if (jsonObject.optBoolean("status")){

                        Bundle bundle=new Bundle();
                        if(sharedPreferences.getString("user_type","").equalsIgnoreCase("2")) {
                            String [] temp = autoComplete.getText().toString().split(",\n");
                            bundle.putInt("qty",temp.length);
                        }
                        else
                            bundle.putInt("qty",1);

                        bundle.putString("sessionType",sessionType.getText().toString());
                        bundle.putString("about",about.getText().toString());
                        bundle.putString("name",name.getText().toString());
                        bundle.putString("image",imageurl);
                        bundle.putString("trainerID",teacher_id);
                        bundle.putString("from","booking");
                        bundle.putString("walletAmount",jsonObject.optString("wallet_amount"));
                        bundle.putString("trans_id",jsonObject.optString("booking_code"));
                        bundle.putString("price",ticket.getText().toString().replace("$ ",""));
                        bundle.putString("payable_amount",ticket.getText().toString().replace("$ ",""));
                        CheckOut checkOut=new CheckOut();
                        checkOut.setArguments(bundle);
                        ((MainActivity) context).addFragment2(checkOut, true, false, 0);

                    }
                    else {
                        Toast.makeText(context, jsonObject.optJSONObject("message").optString("failed"), Toast.LENGTH_SHORT).show();
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    progressDialog.cancel();;
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
                progressDialog.cancel();;
            }
        });
    }

    private void getSessionDetails() {
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));
        param.put("workshop_id",getArguments().getString("sessionId"));
        param.put("user_id",sharedPreferences.getString("userID",""));

        Log.e("sessionDetailsParam",""+param);

        new PostMethod(Api.ViewWorkshop,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("sessionDetailsResp",data);
                try {
                    JSONObject jsonObject=new JSONObject(data);
                    if (jsonObject.optBoolean("status")) {

                        if (!jsonObject.optString("student_url").equalsIgnoreCase("")) {
                            bookNow.setText(getResources().getString(R.string.attend));
                            wizIQURL=jsonObject.optString("student_url");
                        }

                        JSONObject dataObject=jsonObject.optJSONObject("data");
                        name.setText(dataObject.optString("trainer_name"));
                        sessionType.setText(dataObject.optString("type"));
                        about.setText(dataObject.optString("about"));
                        ticket.setText("$ "+dataObject.optString("ticket_cost"));
                        teacher_id=dataObject.optString("teacher_id");
                        coursesArray=dataObject.optJSONArray("courses");

                        if(teacher_id.equalsIgnoreCase(sharedPreferences.getString("userID",""))) {
                            chatCard.setVisibility(View.GONE);
                            bookCard.setVisibility(View.GONE);
                        }

                        if(!dataObject.optString("youtube_url").equalsIgnoreCase("null")){
                            youtube_url = dataObject.optString("youtube_url");
                            Log.e("youtube_url",URLUtil.guessFileName(youtube_url, null, null).replace(".bin",""));
                            youTubePlayerFragment.initialize(DEVELOPER_KEY, new YouTubePlayer.OnInitializedListener() {
                                @Override
                                public void onInitializationSuccess(YouTubePlayer.Provider arg0, YouTubePlayer youTubePlayer, boolean b) {
                                    if (!b) {
                                        YPlayer = youTubePlayer;
                                        YPlayer.setFullscreen(false);
                                        YPlayer.cueVideo(URLUtil.guessFileName(youtube_url, null, null).replace(".bin",""));
                                        YPlayer.setFullscreenControlFlags(0);
                                        YPlayer.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
                                            @Override
                                            public void onFullscreen(boolean b) {
                                                YPlayer.setFullscreen(false);
                                                Toast.makeText(getContext(), "Landscape mode is not supported.",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        //YPlayer.play();
                                    }
                                }

                                @Override
                                public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) {
                                    // TODO Auto-generated method stub

                                }
                            });

                        }else
                            video.setVisibility(View.GONE);

                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
                        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

                        SimpleDateFormat sdfOutPutToSend = new SimpleDateFormat("hh:mm a");
                        sdfOutPutToSend.setTimeZone(TimeZone.getDefault());
                        String start_time="";
                        String time="";
                        try {
                            Date start  = sdf.parse(dataObject.optString("start_time"));
                             start_time = sdfOutPutToSend.format(start);

                            Date end  = sdf.parse(dataObject.optString("end_time"));
                            String end_time = sdfOutPutToSend.format(end);

                            time=start_time+" - "+ end_time;
                            //Log.e("time",time);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                        //sdf1.setTimeZone(TimeZone.getTimeZone("UTC"));

                        SimpleDateFormat sdfOutPutToSend1 = new SimpleDateFormat("MMMM-EEEE dd, yyyy");
                        //sdfOutPutToSend1.setTimeZone(TimeZone.getDefault());

                        JSONArray workshopDates=dataObject.optJSONArray("workshop_date");
                        dateArray=new ArrayList<>();
                        for(int ar=0;ar<workshopDates.length();ar++){
                            try {
                                JSONObject object=workshopDates.optJSONObject(ar);
                                Date start  = sdf1.parse(object.optString("workshope_time"));
                                String workshopDate = sdfOutPutToSend1.format(start);
                                if(ar==0){
                                    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd,hh:mm a");
                                    Date tempDate  = sdf2.parse(object.optString("workshope_time")+","+start_time);
                                    Calendar cal=Calendar.getInstance();
                                    Date currentDate = cal.getTime();
                                    long timeInMilliseconds = tempDate.getTime();
                                    int dateDifference = (int) TimeUnit.HOURS.convert(tempDate.getTime()-currentDate.getTime(),TimeUnit.MILLISECONDS);
                                    booking= dateDifference > 24;
                                }

                                dateArray.add(workshopDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        String csv = time+"\n"+android.text.TextUtils.join("\n", dateArray);
                        dateTime.setText(csv);

                        totalHours.setText((Integer.parseInt(dataObject.optString("hours"))*dateArray.size())+" "+context.getResources().getString(R.string.hours));
                        maxParticipants.setText(dataObject.optString("max_participants"));
                        languages.setText(dataObject.optString("lang_name"));

                        overview.setText(Html.fromHtml(dataObject.optString("overview")));
                        whyAttend.setText(Html.fromHtml(dataObject.optString("why_attend")));
                        wst.setText(Html.fromHtml(dataObject.optString("who_should_attend")));

                        Glide.with(context).load(dataObject.optString("image_url")).into(header_pic);
                        Glide.with(context).load(dataObject.optString("profile_image_url")).into(profile_pic);
                        imageurl=dataObject.optString("profile_image_url");

                        mainLayout.setVisibility(View.VISIBLE);
                        progressDialog.cancel();;
                    }else {
                        Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    progressDialog.cancel();;
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean b) {
        if (!b) {

            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically
            player.cueVideo(YOUTUBE_VIDEO_CODE);

            // Hiding player controls
            player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(getActivity(), RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = errorReason.toString();
            Toast.makeText(getContext(), errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(AutoCompleteReceiver, new IntentFilter("CheckBox"));
    }

    BroadcastReceiver AutoCompleteReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("csv",intent.getStringExtra("csv"));
            if(null!=autoComplete)
                autoComplete.setText(intent.getStringExtra("csv"));
        }
    };
}

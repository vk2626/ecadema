package com.ecadema.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.VerticalSpaceItemDecoration;
import com.ecadema.adapter.MyClassPastAdapter;
import com.ecadema.adapter.MyClassUpcomingAdapter;
import com.ecadema.adapter.SessionsPastAdapter;
import com.ecadema.adapter.SessionsUpcomingAdapter;
import com.ecadema.adapter.TrainerPastBookingAdapter;
import com.ecadema.app.R;
import com.ecadema.modal.AttendeesModal;
import com.ecadema.modal.PastModal;
import com.ecadema.modal.SubmittedModal;
import com.ecadema.modal.UpcomingModal;

import java.util.ArrayList;

public class TrainerBookingFragment extends Fragment {

    RecyclerView upcomingRecyclerView,pastRecyclerView;
    MyClassUpcomingAdapter upcomingAdapter;
    MyClassPastAdapter pastAdapter;
    SessionsPastAdapter sessionsPastAdapter;
    SessionsUpcomingAdapter sessionsUpcomingAdapter;

    ArrayList<UpcomingModal> upcomingModalArrayList=new ArrayList<>();
    ArrayList<PastModal> pastModalArrayList=new ArrayList<>();
    ArrayList<SubmittedModal> submittedModalArrayList=new ArrayList<>();

    Context context;
    SharedPreferences sharedPreferences;
    TextView upcoming,past,noPastText,noUpcomingText,header;

    RelativeLayout upcomingSessionView,pastSessionView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.trainer_booking_fragment, container, false);

        context= getContext();
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

        upcomingSessionView= root.findViewById(R.id.upcomingSessionView);
        header= root.findViewById(R.id.header);
        pastSessionView= root.findViewById(R.id.pastSessionView);
        noUpcomingText= root.findViewById(R.id.noUpcomingText);
        noPastText= root.findViewById(R.id.noPastText);
        upcoming= root.findViewById(R.id.upcoming);
        past= root.findViewById(R.id.past);

        upcomingRecyclerView= root.findViewById(R.id.upcomingRecyclerView);
        pastRecyclerView= root.findViewById(R.id.pastRecyclerView);

        upcomingRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        pastRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        VerticalSpaceItemDecoration verticalSpaceItemDecoration=new VerticalSpaceItemDecoration(0,0,30,10);
        upcomingRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(0,0,20,10));
        pastRecyclerView.addItemDecoration(verticalSpaceItemDecoration);

        Upcoming(sharedPreferences.getString("user_type",""));

        upcoming.setOnClickListener(view -> {
            Upcoming(sharedPreferences.getString("user_type",""));
        });

        past.setOnClickListener(view -> {
            Past(sharedPreferences.getString("user_type",""));
        });


        return root;
    }


    private void Past(String user_type) {
        past.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        upcoming.setTextColor(context.getResources().getColor(R.color.dark_grey));
        pastSessionView.setVisibility(View.VISIBLE);
        upcomingSessionView.setVisibility(View.GONE);

        pastModalArrayList.clear();
        for (int ar=0;ar<5;ar++){
            PastModal pastModal=new PastModal();
            pastModal.setID(""+ar);
            pastModal.setImage("");
            pastModal.setName("Name "+(ar+1));
            pastModal.setSessionType("Session Type "+(ar+1));
            pastModal.setSessionName("Session Name "+(ar+1));
            pastModal.setTimer(172800000*(ar+1));

            ArrayList<AttendeesModal> attendees=new ArrayList<>();
            for(int vk=0;vk<ar+1;vk++){
                AttendeesModal attendeesModal=new AttendeesModal();
                attendeesModal.setName("Attendees "+(vk+1));
                if((vk+1)%2==0)
                    attendeesModal.setAttendance("1");
                else
                    attendeesModal.setAttendance("2");

                attendees.add(attendeesModal);
            }
            pastModal.setAttendeesList(attendees);
            pastModalArrayList.add(pastModal);
        }
        TrainerPastBookingAdapter trainerPastAdapter=new TrainerPastBookingAdapter(context,pastModalArrayList);
        pastRecyclerView.setAdapter(trainerPastAdapter);
    }

    private void Upcoming(String user_type) {
        upcoming.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        past.setTextColor(context.getResources().getColor(R.color.dark_grey));
        pastSessionView.setVisibility(View.GONE);
        upcomingSessionView.setVisibility(View.VISIBLE);


        upcomingModalArrayList.clear();
        for (int ar=0;ar<5;ar++){
            ArrayList<AttendeesModal> attendees=new ArrayList<>();
            UpcomingModal upcomingModal=new UpcomingModal();
            upcomingModal.setID(""+ar);
            upcomingModal.setImage("");
            upcomingModal.setName("Name "+(ar+1));
            upcomingModal.setSessionType("Session Type "+(ar+1));
            upcomingModal.setSessionName("Session Name "+(ar+1));
            upcomingModal.setTimer(172800000*(ar+1));

            for(int vk=0;vk<ar+1;vk++){
                AttendeesModal attendeesModal=new AttendeesModal();
                attendeesModal.setName("Attendees "+(vk+1));
                attendeesModal.setAttendance("3");

                attendees.add(attendeesModal);
            }
            upcomingModal.setAttendeesList(attendees);
            upcomingModalArrayList.add(upcomingModal);
        }
        /*TrainerUpcomingBookingAdapter trainerUpcomingAdapter =new TrainerUpcomingBookingAdapter(context,upcomingModalArrayList, MyClassesFragment.this);
        upcomingRecyclerView.setAdapter(trainerUpcomingAdapter);*/
    }
}

package com.ecadema.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.ChatRoom;
import com.ecadema.app.LoginActivity;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Rahul Mishra on 2/21/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    SharedPreferences sharedPreferences;
    String show_hide;
    String image="";
    String type;
    Intent intent;
    Notification notification;
    Context context;
    JSONObject jsonObject;

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("deviceToken",token).apply();
    }
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Log.e("User notify message", String.valueOf(remoteMessage.getData().get("message")));
            Log.e("User notification body", String.valueOf(remoteMessage.getData().get("body")));
            //sendNotification(String.valueOf(remoteMessage.getData()));
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            sendNotification(remoteMessage.getData().get("message"));

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(String messageBody) {
        String title = null, textMessage=null;
        try{
            context=this;
            JSONObject jsonObject1=new JSONObject(messageBody);
            jsonObject = jsonObject1.optJSONObject("details");
            title=jsonObject.optString("name");
            textMessage= URLDecoder.decode(jsonObject.optString("msg"), "UTF-8");
            image=jsonObject.optString("image_url");
            type=jsonObject.optString("notification_type");
            show_hide=jsonObject.optString("show_hide");
            intent = new Intent(this, MainActivity.class);

        }catch (Exception e){
            e.printStackTrace();
        }
        if(type.equalsIgnoreCase("AdminLogout")) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(() -> ShowToast(), 500 );

            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            sharedPreferences.edit().putString("userID","-1").apply();
            sharedPreferences.edit().remove("firstName").apply();
            sharedPreferences.edit().remove("lastName").apply();
            sharedPreferences.edit().remove("email").apply();
            sharedPreferences.edit().remove("contact_no").apply();
            sharedPreferences.edit().remove("country").apply();

            sharedPreferences.edit().remove("profile_image").apply();
            sharedPreferences.edit().remove("user_type").apply();


            sharedPreferences.edit().putString("socialLogin","-1").apply();
            intent=new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            TaskStackBuilder.create(this).addNextIntentWithParentStack(intent).startActivities();
            return;
        }

        else if(type.equalsIgnoreCase("Request to Reschedule")) {
            intent = new Intent(this, MainActivity.class);
            intent.putExtra("type","Reschedule");

            if(show_hide.equalsIgnoreCase("hide_notification"))
                return;
        }
        else if(type.equalsIgnoreCase("rating")) {
            Intent broadCast = new Intent("rating");
            sendBroadcast(broadCast);
            return;
        }
        else if(type.equalsIgnoreCase("home")) {
            intent = new Intent(this, MainActivity.class);
            intent.putExtra("type",type);
            if(show_hide.equalsIgnoreCase("hide_notification"))
                return;
        }
        else if(type.equalsIgnoreCase("Chat History")) {
            if (sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
                return;
            }

            intent = new Intent(this, ChatRoom.class);
            intent.putExtra("userName",jsonObject.optString("name"));
            intent.putExtra("receiverID",jsonObject.optString("id"));
            intent.putExtra("userImage",jsonObject.optString("profile_image"));
            intent.putExtra("type",type);

            Intent broadCast = new Intent("newMessage");
            sendBroadcast(broadCast);

            Intent broadCast1 = new Intent("update");
            sendBroadcast(broadCast1);

            Intent broadCast2 = new Intent("mainActivity");
            sendBroadcast(broadCast2);

            if(sharedPreferences.getString("chatRoom","0").equalsIgnoreCase("1")){
                return;
            }

            if(show_hide.equalsIgnoreCase("hide_notification"))
                return;
        }


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_layout);

        Bitmap bmp = null;
        try {
            if(!image.equalsIgnoreCase("")) {
                //bmp = Picasso.with(getApplicationContext()).load(image).get();
                bmp = Picasso.with(context).load(image).get();
                contentView.setImageViewBitmap(R.id.extraImage, bmp);
                Log.e("Uri.parse(extraImage)", String.valueOf(Uri.parse(image)));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        contentView.setTextViewText(R.id.title, title);
        contentView.setTextViewText(R.id.detail, textMessage);

        String channelId = "Default";
        NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.guest_user)
                .setContent(contentView)
                .setCustomBigContentView(contentView)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent);
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.canShowBadge();
            notificationChannel.setLockscreenVisibility(RECEIVER_VISIBLE_TO_INSTANT_APPS);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

            assert manager != null;
            manager.createNotificationChannel(notificationChannel);
        }
        assert manager != null;
        manager.notify(0, builder.build());
    }

    private void ShowToast() {
        try{
            GoogleSignInOptions gso = new GoogleSignInOptions.
                    Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                    build();

            GoogleSignInClient googleSignInClient= GoogleSignIn.getClient(this,gso);
            googleSignInClient.signOut();
        }catch (Exception e){
            e.printStackTrace();
        }

        try{
            FacebookSdk.sdkInitialize(this);
            if (AccessToken.getCurrentAccessToken() != null) {
                LoginManager.getInstance().logOut();
                new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/",
                        null, HttpMethod.DELETE, response -> LoginManager.getInstance().logOut()).executeAsync();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        FirebaseAuth.getInstance().signOut();
        Map<String, String> params=new HashMap<>();
        params.put("user_id",sharedPreferences.getString("userID",""));
        params.put("lang_token",sharedPreferences.getString("lang",""));
        params.put("device_id",sharedPreferences.getString("device_id",""));
        Log.e("logoutParam",params.toString());
        new PostMethod(Api.LogOut,params,this).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                Log.e("logoutResp",data);
                //Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void error(VolleyError error) {

                error.printStackTrace();
            }
        });
    }
}
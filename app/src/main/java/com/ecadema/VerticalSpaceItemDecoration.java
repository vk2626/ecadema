package com.ecadema;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

    private final int right;
    private final int left;
    private final int top;
    private final int bottom;

    public VerticalSpaceItemDecoration(int right, int left, int top, int bottom) {
        this.right = right;
        this.left = left;
        this.top = top;
        this.bottom = bottom;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.right = right;
        outRect.left = left;
        outRect.top = top;
        outRect.bottom = bottom;
    }
}
package com.ecadema.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.R;
import com.ecadema.app.Web_View;
import com.ecadema.fragments.MyClassesFragment;
import com.ecadema.modal.UpcomingModal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TrainerUpcomingAdapter extends RecyclerView.Adapter<TrainerUpcomingAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<UpcomingModal> upcomingModalArrayList;
    private final MyClassesFragment myClassesFragment;
    SharedPreferences sharedPreferences;

    public TrainerUpcomingAdapter(Context context, ArrayList<UpcomingModal> upcomingModalArrayList, MyClassesFragment myClassesFragment) {
        this.context=context;
        this.upcomingModalArrayList=upcomingModalArrayList;
        this.myClassesFragment=myClassesFragment;
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.my_class_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {

        UpcomingModal upcomingModal=upcomingModalArrayList.get(position);
        String text = "<font color=#f16667>"+context.getResources().getString(R.string.with)+" </font> <font color=#757575>"+upcomingModal.getName()+"</font>";

        holder.sessionType.setText(upcomingModal.getSessionType());
        holder.name.setText(Html.fromHtml(text));
        holder.sessionName.setText(upcomingModal.getSessionName());
        holder.dateTime.setText(upcomingModal.getDateTime());
        holder.duration.setText(upcomingModal.getDuration()+" "+context.getResources().getString(R.string.hours));
        holder.rate.setText("$ "+upcomingModal.getRate());

        try {
            Picasso.with(context).load(upcomingModal.getBannerImage()).into(holder.headerPic);

        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            Picasso.with(context).load(upcomingModal.getImage()).into(holder.profilePic);

        }catch (Exception e){
            e.printStackTrace();
        }

        new CountDownTimer(upcomingModal.getTimer(), 1000){
            @Override
            public void onTick(long millisUntilFinished) {

                /* converting the milliseconds into days, hours, minutes and seconds and displaying it in textviews */
                holder.days.setText(TimeUnit.HOURS.toDays(TimeUnit.MILLISECONDS.toHours(millisUntilFinished))+" d");
                holder.hours.setText((TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)))+" h");
                holder.mins.setText((TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)))+" m");
                holder.seconds.setText((TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))+" s");
            }

            @Override

            public void onFinish() {
                /* clearing all fields and displaying countdown finished message  */
                holder.days.setText("00 d");
                holder.hours.setText("00 h");
                holder.mins.setText("00 m");
                holder.seconds.setText("00 s");
                myClassesFragment.RemoveGroupSession(upcomingModal.getBookedDateId());
            }
        }.start();

        holder.profileCard.setVisibility(View.GONE);
        holder.constraint.setVisibility(View.VISIBLE);

        if(upcomingModal.getAttend().equalsIgnoreCase("1") && !upcomingModal.getIsReschedule()){
            holder.attend.setVisibility(View.VISIBLE);
        }
        else
            holder.attend.setVisibility(View.GONE);

        holder.attend.setOnClickListener(v->{
            if(!upcomingModal.getWizIQ().equalsIgnoreCase("null") &&
                    !upcomingModal.getTeacherID().equalsIgnoreCase(sharedPreferences.getString("userID",""))){
                Intent intent = new Intent(context, Web_View.class);
                intent.putExtra("url",upcomingModal.getWizIQ());
                context.startActivity(intent);
               /* Intent intent = new Intent(context, ClassroomActivity.class);
                intent.putExtra(ClassroomActivity.URL, upcomingModal.getWizIQ());
                context.startActivity(intent);*/

                if(upcomingModal.getIsSendPDF()){
                    Map<String,String> param = new HashMap<>();
                    param.put("id",upcomingModal.getWizIQ());
                    param.put("lang_token",sharedPreferences.getString("lang",""));

                    Log.e("sendPdfPara",""+param);
                    new PostMethod(Api.CheckingToSendPdfMentor,param,context).startPostMethod(new ResponseData() {
                        @Override
                        public void response(String data) {
                            Log.e("sendPdfResult",data);
                        }

                        @Override
                        public void error(VolleyError error) {
                            error.printStackTrace();
                        }
                    });
                }

            }
        });

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return upcomingModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView profilePic,headerPic;
        TextView name,sessionType,sessionName,days,hours,mins,seconds,dateTime,duration,rate;
        LinearLayout ll1;
        ConstraintLayout constraint;
        CardView profileCard,mainLayout,attend;

        public RowHolder(@NonNull View itemView) {
            super(itemView);

            attend=itemView.findViewById(R.id.attend);
            mainLayout=itemView.findViewById(R.id.mainLayout);
            profileCard=itemView.findViewById(R.id.profileCard);
            headerPic=itemView.findViewById(R.id.headerPic);
            rate=itemView.findViewById(R.id.rate);
            duration=itemView.findViewById(R.id.duration);
            dateTime=itemView.findViewById(R.id.dateTime);
            constraint=itemView.findViewById(R.id.constraint);
            profilePic=itemView.findViewById(R.id.profile_pic);
            ll1=itemView.findViewById(R.id.ll1);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);
            sessionName=itemView.findViewById(R.id.sessionName);

            days = itemView.findViewById(R.id.days);
            hours = itemView.findViewById(R.id.hours);
            mins = itemView.findViewById(R.id.minutes);
            seconds = itemView.findViewById(R.id.seconds);

            ll1.setVisibility(View.GONE);
        }
    }
}

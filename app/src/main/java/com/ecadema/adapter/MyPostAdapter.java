package com.ecadema.adapter;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.fragments.BlogDetailsFrag;
import com.ecadema.fragments.MyPostFragment;
import com.ecadema.modal.PostModal;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyPostAdapter extends RecyclerView.Adapter<MyPostAdapter.RowHolder> {

  private final Context context;
  private final ArrayList<PostModal> postModalArrayList;
  private final String status;
  private final MyPostFragment myPostFragment;
  private int lineCount;
  private int selectedPosition=-1;
  private int scrollToPosition=-1;
  ProgressDialog progressDialog;
  SharedPreferences sharedPreferences;

  public MyPostAdapter(final Context context, final ArrayList<PostModal> postModalArrayList,
                       final String status, MyPostFragment myPostFragment) {
    this.context = context;
    this.postModalArrayList = postModalArrayList;
    this.myPostFragment = myPostFragment;
    this.status = status;
    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
  }

  @NonNull
  @Override
  public RowHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
    Resources activityRes = context.getResources();
    Configuration activityConf = activityRes.getConfiguration();
    Locale newLocale = new Locale(sharedPreferences.getString("lang",""));
    activityConf.setLocale(newLocale);
    activityRes.updateConfiguration(activityConf, activityRes.getDisplayMetrics());

    Resources applicationRes = context.getResources();
    Configuration applicationConf = applicationRes.getConfiguration();
    applicationConf.setLocale(newLocale);
    applicationRes.updateConfiguration(applicationConf, applicationRes.getDisplayMetrics());

    View view= LayoutInflater.from(context).inflate(R.layout.my_post_adapter,null);
    return new RowHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull final RowHolder holder, @SuppressLint("RecyclerView") final int position) {
    PostModal postModal = postModalArrayList.get(position);
    holder.name.setText(postModal.getName());
    holder.date.setText(postModal.getDate());
    holder.views.setText(context.getResources().getString(R.string.views)+" "+postModal.getView());
    holder.title.setText(postModal.getTitle());
    holder.description.setText(Html.fromHtml(postModal.getShortDecription()));
    holder.likeCount.setText(postModal.getLikes());
    holder.dislikeCount.setText(postModal.getDislike());
    holder.catName.setText(postModal.getCategoryName());

    if (postModal.getLikes().equalsIgnoreCase("0"))
      holder.likeImage.setColorFilter(context.getResources().getColor(R.color.dark_grey));
    else
      holder.likeImage.setColorFilter(context.getResources().getColor(R.color.colorPrimary));

    if (postModal.getDislike().equalsIgnoreCase("0"))
      holder.disLikeImage.setColorFilter(context.getResources().getColor(R.color.dark_grey));
    else
      holder.disLikeImage.setColorFilter(context.getResources().getColor(R.color.colorPrimary));

    lineCount = holder.description.getText().toString().split(System.getProperty("line.separator")).length;
    // Use lineCount here
    if (lineCount>3){
      holder.readMoreTV.setVisibility(View.VISIBLE);
    }else
      holder.readMoreTV.setVisibility(View.GONE);

    if (selectedPosition==position){
      holder.readMoreTV.setText(R.string.readLess);
      holder.description.setMaxLines(Integer.MAX_VALUE);
      holder.description.setEllipsize(null);
    }
    else {
      if (scrollToPosition !=-1){
        myPostFragment.recyclerView.scrollToPosition(scrollToPosition);
        scrollToPosition=-1;
      }
      holder.readMoreTV.setText(context.getResources().getString(R.string.ReadMore));
      if (lineCount>3){
        holder.readMoreTV.setVisibility(View.VISIBLE);
      }else {
        holder.readMoreTV.setVisibility(View.GONE);
      }
      holder.description.setMaxLines(3);
      holder.readMoreTV.setText(context.getResources().getString(R.string.ReadMore));
      holder.description.setEllipsize(TextUtils.TruncateAt.END);
    }


    if (postModal.getTagName().equalsIgnoreCase("")) {
      holder.catName.setText(postModal.getCategoryName());
      holder.tagTV.setText(context.getResources().getString(R.string.categories));
    }
    else {
      holder.catName.setText(postModal.getTagName());
      holder.tagTV.setText(context.getResources().getString(R.string.tags));
    }

    try {
      Glide.with(context).load(postModal.getImage()).into(holder.profile_pic);
    }catch (Exception e){
      e.printStackTrace();
    }

     try {
      Glide.with(context).load(postModal.getPostImage()).into(holder.image);
    }catch (Exception e){
      e.printStackTrace();
    }

    if (postModal.getVideoUrl().equalsIgnoreCase("")){
      holder.youTubePlayerView.setVisibility(View.GONE);
    }else {
      holder.youTubePlayerView.setVisibility(View.VISIBLE);
    }

    holder.share.setTag(position);
    holder.share.setOnClickListener(v->{
      int pos = (int)v.getTag();
      Intent sendIntent = new Intent();
      sendIntent.setAction(Intent.ACTION_SEND);
      sendIntent.putExtra(Intent.EXTRA_TEXT,postModalArrayList.get(pos).getShareUrl());
      sendIntent.setType("text/plain");

      Intent shareIntent = Intent.createChooser(sendIntent, null);
      startActivity(shareIntent);
    });

    holder.cueVideo(postModal.getVideoCode());

    holder.readMore.setOnClickListener(v->{
      Bundle bundle=new Bundle();
      bundle.putString("id",postModal.getId());
      BlogDetailsFrag blogDetailsFrag=new BlogDetailsFrag();
      blogDetailsFrag.setArguments(bundle);
      ((MainActivity) context).addFragment2(blogDetailsFrag, true, false, 0);
    });

    holder.delete.setTag(position);
    holder.delete.setOnClickListener(v->{
      AlertDialog.Builder alertbuilder = new AlertDialog.Builder(context);
      alertbuilder.setCancelable(false);
      alertbuilder.setTitle(context.getResources().getString(R.string.deletePostTitle));
      alertbuilder.setIcon(context.getResources().getDrawable(R.mipmap.app_icon));
      alertbuilder.setMessage(context.getResources().getString(R.string.deletePost));
      alertbuilder.setPositiveButton(context.getResources().getString(R.string.yes),
              (dialog, which) -> {
                progressDialog = new ProgressDialog(context);
                progressDialog.show();
                progressDialog.setMessage(context.getResources().getString(R.string.processing));
                int pos = (int)v.getTag();
                deletePost(postModalArrayList.get(pos).getId());
              });
      alertbuilder.setNegativeButton(context.getResources().getString(R.string.no),
              (dialog, which) -> dialog.dismiss());

      AlertDialog alertDialog = alertbuilder.create();
      alertDialog.show();
    });
    holder.readMoreTV.setOnClickListener(v->{
      if (holder.readMoreTV.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.ReadMore))){
        selectedPosition=position;
        scrollToPosition = -1;
        //isSelected.add(position);
      }else {
        selectedPosition=-1;
        scrollToPosition = position;
      }
      notifyDataSetChanged();
    });
    holder.edit.setTag(position);
    holder.edit.setOnClickListener(v->{
      int pos = (int)v.getTag();
      PostModal tempPostModal = postModalArrayList.get(pos);

      myPostFragment.editPost(tempPostModal.getShortDecription(),tempPostModal.getTagIds(),tempPostModal.getPostImage(),
              tempPostModal.getVideoUrl(),tempPostModal.getId());
      
    });

  }

  private void deletePost(String id) {
    Map<String,String> param = new HashMap<>();
    param.put("removed_id",id);
    param.put("lan",sharedPreferences.getString("lang",""));
    Log.e("deleteCommunity",""+param);
    new PostMethod(Api.deletePost,param,context).startPostMethod(new ResponseData() {
      @Override
      public void response(String data) {
        progressDialog.dismiss();
        Log.e("deleteResp",data);
        try {
          JSONObject jsonObject = new JSONObject(data);
          if (jsonObject.optBoolean("status"))
            myPostFragment.getPosts("1");
          Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
          e.printStackTrace();
        }

      }

      @Override
      public void error(VolleyError error) {
        progressDialog.dismiss();
        error.printStackTrace();
      }
    });
  }

  @Override
  public int getItemCount() {
    return postModalArrayList.size();
  }

  public class RowHolder extends ViewHolder {
    ImageView image,likeImage,disLikeImage,share;
    CardView readMore;
    TextView name, date, views, title, description, likeCount, dislikeCount, catName,readMoreTV,tagTV;
    CircleImageView profile_pic;
    YouTubePlayerView youTubePlayerView;
    private YouTubePlayer youTubePlayer;
    private String currentVideoId;
    LinearLayout delete,edit;

    public RowHolder(@NonNull final View itemView) {
      super(itemView);

      profile_pic = (itemView).findViewById(R.id.profile_pic);

      share = itemView.findViewById(R.id.share);
      tagTV = itemView.findViewById(R.id.tagTV);
      delete = itemView.findViewById(R.id.delete);
      edit = itemView.findViewById(R.id.edit);
      image = itemView.findViewById(R.id.image);
      youTubePlayerView = itemView.findViewById(R.id.youTubePlayerView);
      disLikeImage = (itemView).findViewById(R.id.disLikeImage);
      likeImage = (itemView).findViewById(R.id.likeImage);
      readMoreTV = (itemView).findViewById(R.id.readMoreTV);
      name = (itemView).findViewById(R.id.name);
      date = (itemView).findViewById(R.id.date);
      views = (itemView).findViewById(R.id.view);
      title = (itemView).findViewById(R.id.title);
      description = (itemView).findViewById(R.id.description);
      readMore = (itemView).findViewById(R.id.readMore);
      likeCount = (itemView).findViewById(R.id.likeCount);
      dislikeCount = (itemView).findViewById(R.id.dislikeCount);
      catName = (itemView).findViewById(R.id.catName);

      youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
        @Override
        public void onReady(@NonNull YouTubePlayer initializedYouTubePlayer) {
          youTubePlayer = initializedYouTubePlayer;
          youTubePlayer.cueVideo(currentVideoId, 0);
        }
      });
    }
    void cueVideo(String videoId) {
      currentVideoId = videoId;

      if(youTubePlayer == null)
        return;

      youTubePlayer.cueVideo(videoId, 0);
    }

  }
}

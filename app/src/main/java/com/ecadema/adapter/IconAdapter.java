package com.ecadema.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.IconModal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class IconAdapter extends RecyclerView.Adapter<IconAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<IconModal> iconModalArrayList;
    private final String type;

    public IconAdapter(Context context, ArrayList<IconModal> iconModalArrayList, String type) {
        this.context=context;
        this.iconModalArrayList=iconModalArrayList;
        this.type=type;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.icons,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        IconModal iconModal = iconModalArrayList.get(position);

        Log.e("Image",""+iconModal.getImage());
        Picasso.with(context).load(iconModal.getImage()).into(holder.icon);
        holder.name.setText(iconModal.getName());
        holder.mainLayout.setOnClickListener(v->{
            Intent intent = new Intent(type);
            if(holder.count.getText().toString().equalsIgnoreCase("0")) {
                intent.putExtra("count","1");
                holder.count.setText("1");
            }else {
                intent.putExtra("count","0");
                holder.count.setText("0");
            }

            intent.putExtra("id",iconModal.getId());
            context.sendBroadcast(intent);
        });
    }

    @Override
    public int getItemCount() {
        return iconModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView name,count;
        RelativeLayout mainLayout;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            mainLayout=itemView.findViewById(R.id.mainLayout);
            icon=itemView.findViewById(R.id.icon);
            name=itemView.findViewById(R.id.name);
            count=itemView.findViewById(R.id.count);

        }
    }
}

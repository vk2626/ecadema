package com.ecadema.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.bumptech.glide.Glide;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.fragments.BlogDetailsFrag;
import com.ecadema.modal.CommunityModal;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import java.util.ArrayList;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.RowHolder> {

  private final Context context;
  private final ArrayList<CommunityModal> articleModalArrayList;

  public ArticleAdapter(final Context context, final ArrayList<CommunityModal> articleModalArrayList) {
    this.context = context;
    this.articleModalArrayList = articleModalArrayList;
  }

  @NonNull
  @Override
  public RowHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
    View view= LayoutInflater.from(context).inflate(R.layout.article_adapter,null);
    return new RowHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull final RowHolder holder, final int position) {
    CommunityModal communityModal=articleModalArrayList.get(position);
    Glide.with(context).load(communityModal.getImage()).into(holder.image);
    holder.userName.setText(communityModal.getName());
    holder.date.setText(communityModal.getDate());
    holder.title.setText(communityModal.getTitle());
    holder.description.setText(Html.fromHtml(communityModal.getDescription()));

    try {
      Picasso.with(context).load(communityModal.getImage()).into(holder.image);
    }catch (Exception e){
      e.printStackTrace();
    }

    holder.cardView.setOnClickListener(v->{
      Bundle bundle=new Bundle();
      bundle.putString("id",communityModal.getId());
      BlogDetailsFrag blogDetailsFrag=new BlogDetailsFrag();
      blogDetailsFrag.setArguments(bundle);
      ((MainActivity) context).addFragment2(blogDetailsFrag, true, false, 0);
    });
  }

  @Override
  public int getItemCount() {
    return articleModalArrayList.size();
  }

  public class RowHolder extends ViewHolder {
    CircleImageView image;
    TextView userName,title,description,date;
    CardView cardView;
    public RowHolder(@NonNull final View itemView) {
      super(itemView);
      cardView=itemView.findViewById(R.id.cardView);
      image=itemView.findViewById(R.id.image);
      userName=itemView.findViewById(R.id.userName);
      title=itemView.findViewById(R.id.title);
      date=itemView.findViewById(R.id.date);
      description=itemView.findViewById(R.id.description);
    }
  }
}

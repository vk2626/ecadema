  package com.ecadema.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.android.volley.VolleyError;
import com.ecadema.CardValidator;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.R;
import com.ecadema.modal.SavedCardModal;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SavedCardsBillingAdapter extends RecyclerView.Adapter<SavedCardsBillingAdapter.RowHolder> {

  private final Context context;
  private final ArrayList<SavedCardModal> savedCardModalArrayList;

  public SavedCardsBillingAdapter(final ArrayList<SavedCardModal> savedCardModalArrayList,
      final Context context) {
    this.context=context;
    this.savedCardModalArrayList=savedCardModalArrayList;
  }

  @NonNull
  @Override
  public RowHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
    View view= LayoutInflater.from(context).inflate(R.layout.saved_card_adapter,null);
    return new RowHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull final RowHolder holder, final int position) {
    SavedCardModal savedCardModal = savedCardModalArrayList.get(position);

    String ccNum = savedCardModal.getCardNumber();
    Log.e("cardValidator",""+ CardValidator.getCardImage(ccNum));

    holder.image.setImageResource(CardValidator.getCardImage(ccNum));

    holder.cardNumber.setText(String.format("xxxx-%s", savedCardModal.getLastFour()));

    holder.expiry.setText(String.format("%s : %s/%s", context.getResources().getString(R.string.expiresIn),
        savedCardModal.getMonth(), savedCardModal.getYear()));

    holder.delete.setOnClickListener(v->{
      AlertDialog.Builder alertbuilder = new AlertDialog.Builder(context);
      alertbuilder.setCancelable(false);
      alertbuilder.setTitle(context.getResources().getString(R.string.deleteCard));
      alertbuilder.setIcon(context.getResources().getDrawable(R.mipmap.app_icon));
      alertbuilder.setMessage(context.getResources().getString(R.string.confirmDelete));
      alertbuilder.setPositiveButton(context.getResources().getString(R.string.yes),
          (dialog, which) -> {
            ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(context.getResources().getString(R.string.processing));
            progressDialog.setCancelable(false);
            progressDialog.show();

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            Map<String,String> param = new HashMap<>();
            param.put("lang_token",sharedPreferences.getString("lang",""));
            param.put("user_id",sharedPreferences.getString("userID",""));
            param.put("card_id",savedCardModal.getId());

            Log.e("deleteCardParam",""+param);
            new PostMethod(Api.RemoveStripeCardById,param,context).startPostMethod(
                new ResponseData() {
                  @Override
                  public void response(final String data) {
                    Log.e("deleteCard",data);
                    progressDialog.dismiss();
                    dialog.dismiss();
                    try {
                      JSONObject jsonObject = new JSONObject(data);
                      if (jsonObject.optBoolean("status")){
                        savedCardModalArrayList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, getItemCount() - position);
                        Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                      }
                    }catch (Exception e){
                      e.printStackTrace();
                    }
                  }

                  @Override
                  public void error(final VolleyError error) {
                    progressDialog.dismiss();
                    error.printStackTrace();
                  }
                });
          });
      alertbuilder.setNegativeButton(context.getResources().getString(R.string.no),
          (dialog, which) -> dialog.dismiss());

      AlertDialog alertDialog = alertbuilder.create();
      alertDialog.show();

    });
  }

  @Override
  public int getItemCount() {
    return savedCardModalArrayList.size();
  }

  public class RowHolder extends ViewHolder {
    RadioButton radio;
    TextView cardNumber, expiry;
    ImageView image;
    CardView cardView;
    ImageView delete;
    public RowHolder(@NonNull final View itemView) {
      super(itemView);
      radio = (itemView).findViewById(R.id.radio);
      cardNumber = (itemView).findViewById(R.id.cardNumber);
      expiry = (itemView).findViewById(R.id.expiry);
      image = (itemView).findViewById(R.id.image);
      cardView = (itemView).findViewById(R.id.cardView);
      delete = (itemView).findViewById(R.id.delete);
      delete.setVisibility(View.VISIBLE);
      radio.setVisibility(View.GONE);
    }
  }
}

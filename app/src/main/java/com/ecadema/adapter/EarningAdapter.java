package com.ecadema.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.EarningModal;

import java.util.ArrayList;

public class EarningAdapter extends RecyclerView.Adapter<EarningAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<EarningModal> earningModalArrayList;

    public EarningAdapter(Context context, ArrayList<EarningModal> earningModalArrayList) {
        this.context=context;
        this.earningModalArrayList=earningModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.earning_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {

        EarningModal earningModal = earningModalArrayList.get(position);
        if(earningModal.getStatus().equalsIgnoreCase(context.getResources().getString(R.string.Rejected)))
            holder.status.setTextColor(Color.RED);
        else if(earningModal.getStatus().equalsIgnoreCase(context.getResources().getString(R.string.Processed)))
            holder.status.setTextColor(context.getResources().getColor(R.color.green));
        else
            holder.status.setTextColor(context.getResources().getColor(R.color.colorAccent));

        holder.status.setText(earningModal.getStatus());
        holder.amount.setText(earningModal.getAmount());
        holder.method.setText(earningModal.getMethod());
        holder.time.setText(earningModal.getDateTime());
    }

    @Override
    public int getItemCount() {
        return earningModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView status,amount,method,time;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            status=itemView.findViewById(R.id.status);
            amount=itemView.findViewById(R.id.amount);
            method=itemView.findViewById(R.id.method);
            time=itemView.findViewById(R.id.time);
        }
    }
}

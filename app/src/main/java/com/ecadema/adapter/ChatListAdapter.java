package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.ChatRoom;
import com.ecadema.app.LoginActivity;
import com.ecadema.app.R;
import com.ecadema.modal.ChatListModal;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<ChatListModal> chatListModalArrayList;
    private ArrayList<ChatListModal> filterArrayList;

    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat format5 = new SimpleDateFormat("dd MMM, yyyy");
    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat1 = new SimpleDateFormat("yyyy-MM-dd");
    @SuppressLint("SimpleDateFormat") SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm " + "a");
    Calendar cal;
    public ChatListAdapter(Context context, ArrayList<ChatListModal> chatListModalArrayList) {
        this.context=context;
        this.chatListModalArrayList = chatListModalArrayList;

        this.filterArrayList = new ArrayList<>();//initiate filter list
        this.filterArrayList.addAll(chatListModalArrayList);//add all items of array list to filter list
        cal = Calendar.getInstance();
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.chat_list_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        ChatListModal chatListModal=chatListModalArrayList.get(position);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        format5.setTimeZone(TimeZone.getDefault());
        timeFormat2.setTimeZone(TimeZone.getDefault());
        timeFormat1.setTimeZone(TimeZone.getDefault());

        //Picasso.with(context).load(chatListModal.getDP()).into(holder.dp);
        Picasso.with(context).load(chatListModal.getDP()).noFade().into(holder.profile_image);

        if(chatListModal.getOnlineStatus().equalsIgnoreCase(context.getResources().getString(R.string.Offline)))
            holder.online.setVisibility(View.GONE);
        else
            holder.online.setVisibility(View.VISIBLE);

        if(chatListModal.getUnread()>0) {
            String text = chatListModal.getUserName()+"<font color=#f16667> ("+chatListModal.getUnread()+")</font></a>";
            holder.name.setText(Html.fromHtml(text));
        }
        else
            holder.name.setText(chatListModal.getUserName());
        holder.message.setText(chatListModal.getLastMessageString());
        try {
            if(!chatListModal.getDate().equalsIgnoreCase("")){
                Date tempDate  = format.parse(chatListModal.getDate());
                //Log.e("time: ","chatTime=>"+timeFormat1.format(tempDate)+"======"+"systemTime=>"+timeFormat1.format(cal.getTime()));
                if(timeFormat1.format(tempDate).compareTo(timeFormat1.format(cal.getTime()))==0)
                    holder.date.setText(timeFormat2.format(tempDate));
                else
                    holder.date.setText(format5.format(tempDate));
            }
            else {
                holder.date.setText("");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.parentLayout.setOnClickListener(v->{
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            if (sharedPreferences.getString("userID","").equalsIgnoreCase("-1")){
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
                return;
            }

            Intent intent=new Intent(context, ChatRoom.class);
            intent.putExtra("chatID",chatListModal.getChatId());
            intent.putExtra("userName",chatListModal.getUserName());
            intent.putExtra("student_id",chatListModal.getStudentID());
            intent.putExtra("receiverID",chatListModal.getReceiversID());
            intent.putExtra("userImage",chatListModal.getDP());
            intent.putExtra("accountActive",chatListModal.getAccountActive());
            if(null!=chatListModal.getCourses())
                intent.putExtra("courses",chatListModal.getCourses().toString());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return chatListModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView name,message,date;
        LinearLayout parentLayout;
        CircleImageView profile_image;
        ImageView dp;
        RelativeLayout online;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            online = itemView.findViewById(R.id.online);
            profile_image = itemView.findViewById(R.id.profile_image);
            name = itemView.findViewById(R.id.name);
            message = itemView.findViewById(R.id.message);
            date = itemView.findViewById(R.id.date);
            parentLayout = itemView.findViewById(R.id.parentLayout);

            dp = itemView.findViewById(R.id.dp);
        }
    }

    public void filter(String charText) {

        Log.e("filters",charText);
        //If Filter type is NAME and EMAIL then only do lowercase, else in case of NUMBER no need to do lowercase because of number format
        charText = charText.toLowerCase(Locale.getDefault());
        chatListModalArrayList.clear();//Clear the main ArrayList

        //If search query is null or length is 0 then add all filterList items back to attachmentList
        if (charText.length() == 0) {
            chatListModalArrayList.addAll(filterArrayList);
        } else {

            //Else if search query is not null do a loop to all filterList items
            for (ChatListModal model : filterArrayList) {

                //Now check the type of search filter
                if (model.getUserName().toLowerCase(Locale.getDefault()).startsWith(charText)||
                        model.getDescription().toLowerCase(Locale.getDefault()).contains(charText))
                    chatListModalArrayList.add(model);
            }
        }
        notifyDataSetChanged();
    }

}

package com.ecadema.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.SubCategModal;

import java.util.ArrayList;

public class SubCatAdapter extends RecyclerView.Adapter <SubCatAdapter.RowHolder>{
    private final Context context;
    public final ArrayList<SubCategModal> courses;
    private ArrayList<SubCategModal> filterArrayList;

    public SubCatAdapter(Context context, ArrayList<SubCategModal> courses) {
        this.context=context;
        this.courses=courses;
        this.filterArrayList = new ArrayList<>();//initiate filter list
        this.filterArrayList.addAll(courses);//add all items of array list to filter list
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.sub_cat_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        holder.courseName.setText(courses.get(position).getSubCategory());
        holder.courseName.setOnClickListener(v->{
            Intent broadCast=new Intent("courseName");
            broadCast.putExtra("type","subHeading");
            broadCast.putExtra("id",courses.get(position).getId());
            broadCast.putExtra("name",courses.get(position).getSubCategory());
            context.sendBroadcast(broadCast);
        });
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView courseName;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            courseName=itemView.findViewById(R.id.courseName);
        }
    }

}


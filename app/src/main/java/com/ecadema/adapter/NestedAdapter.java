package com.ecadema.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;

import java.util.ArrayList;

public class NestedAdapter extends RecyclerView.Adapter<NestedAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<String> trainsFor;

    public NestedAdapter(Context context, ArrayList<String> trainsFor) {
        this.context=context;
        this.trainsFor=trainsFor;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.nested_adapter,null);
        return new RowHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        holder.name.setText(trainsFor.get(position));
    }

    @Override
    public int getItemCount() {
        return trainsFor.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView name;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
        }
    }
}

package com.ecadema.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.ChatRoomModal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter{
    private final Context context;
    private final ArrayList<ChatRoomModal> maliciousModelArrayList;
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    SharedPreferences sharedPreferences;

    public ChatAdapter(Context context, ArrayList<ChatRoomModal> maliciousModelArrayList) {
        this.context=context;
        this.maliciousModelArrayList=maliciousModelArrayList;
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public int getItemViewType(int position) {
        ChatRoomModal maliciousModel=maliciousModelArrayList.get(position);
        if (maliciousModel.getUserId().equals(sharedPreferences.getString("userID",""))) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view;

        if (i == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
            return new SentMessageHolder(view);
        } else if (i == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
       /* View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.malicious_adapter_view, null);
        return new ItemRowHolder(v);*/
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder itemRowHolder, int i) {
        ChatRoomModal maliciousModel=maliciousModelArrayList.get(i);
        switch (itemRowHolder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                if(i!=0)
                    ((SentMessageHolder) itemRowHolder).bind(maliciousModel,maliciousModelArrayList.get(i-1).getDate(),i);
                else
                    ((SentMessageHolder) itemRowHolder).bind(maliciousModel, maliciousModelArrayList.get(i).getDate(),i);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                if(i!=0)
                    ((ReceivedMessageHolder) itemRowHolder).bind(maliciousModel,maliciousModelArrayList.get(i-1).getDate(),i);
                else
                    ((ReceivedMessageHolder) itemRowHolder).bind(maliciousModel, maliciousModelArrayList.get(i).getDate(),i);
        }
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText,text_message_date;

        SentMessageHolder(View itemView) {
            super(itemView);

            text_message_date = (TextView) itemView.findViewById(R.id.text_message_date);
            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
        }

        void bind(ChatRoomModal message, String date, int i) {
            //messageText.setAutoLinkMask(0);
            messageText.setLinksClickable(true);
            messageText.setMovementMethod(LinkMovementMethod.getInstance());
            messageText.setText(Html.fromHtml(message.getMessage()));
            timeText.setText(message.getTime());
            Linkify.addLinks(messageText, Linkify.WEB_URLS);
            messageText.setLinkTextColor(Color.parseColor("#1D60EF"));

            Log.e("i",""+i);
            if(null!=message.getDate()){
                if(i==0){
                    text_message_date.setVisibility(View.VISIBLE);
                    text_message_date.setText(message.getDate());
                    return;
                }
                if(message.getDate().equalsIgnoreCase(date))
                    text_message_date.setVisibility(View.GONE);
                else {
                    text_message_date.setVisibility(View.VISIBLE);
                    text_message_date.setText(message.getDate());
                }

            }
            // Format the stored timestamp into a readable String using method.
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, text_message_date;
        ImageView profileImage;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
            text_message_date = (TextView) itemView.findViewById(R.id.text_message_date);
            profileImage = (ImageView) itemView.findViewById(R.id.image_message_profile);
        }

        void bind(ChatRoomModal message, String date, int i) {
            //messageText.setAutoLinkMask(0);
            messageText.setLinksClickable(true);
            messageText.setMovementMethod(LinkMovementMethod.getInstance());
            messageText.setText(Html.fromHtml(message.getMessage()));
            Linkify.addLinks(messageText, Linkify.WEB_URLS);
            messageText.setLinkTextColor(Color.parseColor("#1D60EF"));

            Log.e("i",""+i);
            // Format the stored timestamp into a readable String using method.
            timeText.setText(message.getTime());
            if(null!=message.getDate()){
                if(i==0){
                    text_message_date.setVisibility(View.VISIBLE);
                    text_message_date.setText(message.getDate());
                    return;
                }
                if(message.getDate().equalsIgnoreCase(date))
                    text_message_date.setVisibility(View.GONE);
                else {
                    text_message_date.setVisibility(View.VISIBLE);
                    text_message_date.setText(message.getDate());
                }

            }
            Picasso.with(context).load(message.getImage()).into(profileImage);
           /* // Insert the profile image from the URL into the ImageView.
            */
        }
    }
    @Override
    public int getItemCount() {
        return maliciousModelArrayList.size();
    }
}
package com.ecadema.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ecadema.app.R;
import com.ecadema.modal.TimeSlotModal;

import java.util.ArrayList;

public class TextAdapter extends BaseAdapter {
    private final ArrayList<String> selectedTimeDate;
    private final String from;
    Context context;
    ArrayList<TimeSlotModal> logos;
    LayoutInflater inflter;
    ArrayList<String> selectedTime = new ArrayList<>();
    String csv;
    private int selectedPosition=-1;

    public TextAdapter(Context applicationContext, ArrayList<TimeSlotModal> logos, ArrayList<String> selectedTimeDate, String from) {
        this.context = applicationContext;
        this.logos = logos;
        this.selectedTimeDate = selectedTimeDate;
        this.from = from;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return logos.size();
    }

    @Override
    public Object getItem(int position) {
        return logos.size();
    }

    @Override
    public long getItemId(int position) {
        return logos.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.timeslotlayout, null); // inflate the layout
        TextView tvslot=convertView.findViewById(R.id.tvslot);
        tvslot.setText(logos.get(position).getTime());

        Log.e("Position",""+position);
        Log.e("selectedPosition",""+selectedPosition);
         if (selectedPosition == position) {
             tvslot.setBackground(context.getResources().getDrawable(R.drawable.selected_bg));
             tvslot.setTextColor(context.getResources().getColor(R.color.white));
        }
        else {
             tvslot.setBackground(context.getResources().getDrawable(R.drawable.cardlayout));
             tvslot.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }
        //final int finalPosition = position;

        if(selectedTimeDate.size()>0){
            for (int ar=0;ar<selectedTimeDate.size();ar++){
                if(selectedTimeDate.contains(logos.get(position).getDate())){
                    tvslot.setBackground(context.getResources().getDrawable(R.drawable.selected_bg));
                    tvslot.setTextColor(context.getResources().getColor(R.color.white));
                    selectedTime.add(logos.get(position).getDate());
                }
            }

        }
        tvslot.setOnClickListener(v->{
            selectedPosition= position;
            if(from.equalsIgnoreCase("booking")){
                if (selectedTime.contains(logos.get(position).getDate())){
                    tvslot.setBackground(context.getResources().getDrawable(R.drawable.cardlayout));
                    tvslot.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    selectedTime.remove(logos.get(position).getDate());
                    csv = android.text.TextUtils.join(",", selectedTime);
                    Intent intent = new Intent("timeSlotArray");
                    intent.putExtra("type","remove");
                    intent.putExtra("selectedTime",logos.get(position).getDate());
                    context.sendBroadcast(intent);
                    return;
                }
                selectedTime.add(logos.get(position).getDate());
                tvslot.setBackground(context.getResources().getDrawable(R.drawable.selected_bg));
                tvslot.setTextColor(context.getResources().getColor(R.color.white));
                csv = android.text.TextUtils.join(",", selectedTime);
                Intent intent = new Intent("timeSlotArray");
                intent.putExtra("type","add");
                intent.putExtra("selectedTime",logos.get(position).getDate());
                context.sendBroadcast(intent);
            }else {
                /*if (selectedTime.size()>0){
                    selectedTime.clear();
                    tvslot.setBackground(context.getResources().getDrawable(R.drawable.cardlayout));
                    tvslot.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                }
                tvslot.setBackground(context.getResources().getDrawable(R.drawable.selected_bg));
                tvslot.setTextColor(context.getResources().getColor(R.color.white));*/
                selectedTime.add(logos.get(position).getDate());
                csv = android.text.TextUtils.join(",", selectedTime);
                Intent intent = new Intent("timeSlotArray");
                intent.putExtra("type","add");
                intent.putExtra("selectedTime",logos.get(position).getDate());
                context.sendBroadcast(intent);
                notifyDataSetChanged();
            }
        });
        return convertView;
    }
}

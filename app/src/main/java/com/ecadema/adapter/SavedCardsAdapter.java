package com.ecadema.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.ecadema.CardValidator;
import com.ecadema.app.R;
import com.ecadema.app.StripePaymentActivity;
import com.ecadema.modal.SavedCardModal;
import java.util.ArrayList;

public class SavedCardsAdapter extends RecyclerView.Adapter<SavedCardsAdapter.RowHolder> {

  private final StripePaymentActivity stripePaymentActivity;
  private final ArrayList<SavedCardModal> savedCardModalArrayList;
  private final Context context;
  int lastSelectedPosition = -1;

  public SavedCardsAdapter(final StripePaymentActivity stripePaymentActivity,
      final ArrayList<SavedCardModal> savedCardModalArrayList,
      final Context context) {
    this.stripePaymentActivity = stripePaymentActivity;
    this.savedCardModalArrayList = savedCardModalArrayList;
    this.context = context;
  }

  @NonNull
  @Override
  public RowHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
    View view= LayoutInflater.from(context).inflate(R.layout.saved_card_adapter,null);
    return new RowHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull final RowHolder holder, final int position) {
    SavedCardModal savedCardModal = savedCardModalArrayList.get(position);

    String ccNum = savedCardModal.getCardNumber();
    Log.e("cardValidator",""+CardValidator.getCardImage(ccNum));

    holder.image.setImageResource(CardValidator.getCardImage(ccNum));
    holder.radio.setChecked(lastSelectedPosition == position);

    holder.cardNumber.setText(String.format("xxxx-%s", savedCardModal.getLastFour()));

    holder.expiry.setText(String.format("%s : %s/%s", stripePaymentActivity.getResources().getString(R.string.expiresIn),
            savedCardModal.getMonth(), savedCardModal.getYear()));

    holder.cardView.setOnClickListener(v->{
      lastSelectedPosition = position;
      stripePaymentActivity.cardInputWidget.clear();
      stripePaymentActivity.cardInputWidget.setCardNumber(savedCardModal.getCardNumber());
      stripePaymentActivity.cardInputWidget.setCvcCode(savedCardModal.getCVC());
      stripePaymentActivity.cardInputWidget.setExpiryDate(Integer.parseInt(savedCardModal.getMonth()), Integer.parseInt(savedCardModal.getYear()));
      notifyDataSetChanged();
    });
    holder.radio.setOnClickListener(v->{
      lastSelectedPosition = position;
      stripePaymentActivity.cardInputWidget.clear();
      stripePaymentActivity.cardInputWidget.setCardNumber(savedCardModal.getCardNumber());
      stripePaymentActivity.cardInputWidget.setCvcCode(savedCardModal.getCVC());
      stripePaymentActivity.cardInputWidget.setExpiryDate(Integer.parseInt(savedCardModal.getMonth()), Integer.parseInt(savedCardModal.getYear()));
      notifyDataSetChanged();
    });
  }

  @Override
  public int getItemCount() {
    return savedCardModalArrayList.size();
  }

  public class RowHolder extends ViewHolder {

    RadioButton radio;
    TextView cardNumber, expiry;
    ImageView image;
    CardView cardView;
    public RowHolder(@NonNull final View itemView) {
      super(itemView);

      radio = (itemView).findViewById(R.id.radio);
      cardNumber = (itemView).findViewById(R.id.cardNumber);
      expiry = (itemView).findViewById(R.id.expiry);
      image = (itemView).findViewById(R.id.image);
      cardView = (itemView).findViewById(R.id.cardView);
    }
  }
}

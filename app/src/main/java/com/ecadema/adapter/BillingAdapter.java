package com.ecadema.adapter;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.app.R;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.modal.BillingModal;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BillingAdapter extends RecyclerView.Adapter<BillingAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<BillingModal> billingModalArrayList;

    public BillingAdapter(Context context, ArrayList<BillingModal> billingModalArrayList) {
        this.context=context;
        this.billingModalArrayList=billingModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.billing_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        BillingModal billingModal = billingModalArrayList.get(position);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        holder.date.setText(billingModal.getDate());
        holder.price.setText("$ "+billingModal.getPrice());
        holder.item.setText(billingModal.getItem());

        holder.viewInvoice.setOnClickListener(v->{
            ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(context.getResources().getString(R.string.processing));
            progressDialog.show();
            Map<String,String> param = new HashMap<>();

            param.put("row_id",billingModal.getID());
            param.put("lang_token",sharedPreferences.getString("lang",""));
            param.put("billing_type",billingModal.getBillingType());
            param.put("user_id",sharedPreferences.getString("userID",""));
            param.put("booking_code",billingModal.getBookingCode());

            Log.e("PDF Param",""+param);

            new PostMethod(Api.BillingPdf,param,context).startPostMethod(new ResponseData() {
                @Override
                public void response(String data) {
                    progressDialog.cancel();;
                    try {
                        Log.e("PDF Resp",data);
                        JSONObject jsonObject = new JSONObject(data);
                        if(jsonObject.optBoolean("status")){
                            Uri uri = Uri.parse(jsonObject.optString("data"));
                            try{
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(uri, "application/pdf");
                                context.startActivity(intent);
                            }catch (ActivityNotFoundException e){
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                                browserIntent.setDataAndType(uri, "text/html");

                                Intent chooser = Intent.createChooser(browserIntent, context.getResources().getString(R.string.chooseApp));
                                chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

                                context.startActivity(chooser);
                            }

                        }
                        else
                            Toast.makeText(context, jsonObject.optJSONObject("message").optString("failed"), Toast.LENGTH_SHORT).show();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void error(VolleyError error) {
                    progressDialog.cancel();;
                    error.printStackTrace();
                }
            });
        });
    }

    @Override
    public int getItemCount() {
        return billingModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView item,price,date,viewInvoice;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            viewInvoice=itemView.findViewById(R.id.viewInvoice);
            item=itemView.findViewById(R.id.item);
            price=itemView.findViewById(R.id.price);
            date=itemView.findViewById(R.id.date);
        }
    }
}

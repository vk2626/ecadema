package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.RealPathUtil;
import com.ecadema.app.BuildConfig;
import com.ecadema.app.FilePath;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.fragments.TrainerProfileFragment;
import com.ecadema.modal.EducationModal;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;

import static android.app.Activity.RESULT_OK;

public class EduAdapter extends RecyclerView.Adapter<EduAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<EducationModal> educationModalArrayList;
    private final TrainerProfileFragment trainerProfileFragment;
    Calendar myCalendar;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;
    private android.app.AlertDialog dialog1;
    Uri mImageCaptureUri;
    File photoFile,file;
    public String photoFileName="image_" + System.currentTimeMillis()+ ".jpg",profilePicName="";
    Bitmap thumbnail,rotatedBitmap;
    String fromDate,toDate;
    private String realPath;
    EditText certificateName;

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode !=RESULT_OK)
            return;

        switch (requestCode) {
            case PICK_FROM_CAMERA:
                //uri=data.getData();
                /**
                 * After taking a picture, do the crop
                 */
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    thumbnail = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(mImageCaptureUri), null, options);
                    Log.e("mImageCaptureUri",""+mImageCaptureUri);
                    certificateName.setText(URLUtil.guessFileName(mImageCaptureUri.toString(), null, null));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;

            case PICK_FROM_FILE:
                /**
                 * After selecting image from files, save the selected path
                 */

                mImageCaptureUri = data.getData();
                Log.e("mImageCaptureUri",""+mImageCaptureUri);

                try {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    // Get the cursor
                    Cursor cursor = context.getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();

                    Bitmap b = BitmapFactory.decodeFile(imgDecodableString);

                    if (mImageCaptureUri.toString().startsWith("content://")) {
                        try {
                            cursor = context.getContentResolver().query(mImageCaptureUri, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                String displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                certificateName.setText(displayName);
                                Log.e("content",displayName);
                            }
                        } finally {
                            cursor.close();
                        }
                    } else if (mImageCaptureUri.toString().startsWith("file://")) {
                        File myFile = new File(mImageCaptureUri.toString());
                        String displayName = myFile.getName();
                        certificateName.setText(displayName);
                        Log.e("displayName",displayName);
                    }


                    try {
                        // SDK < API11
                        if (Build.VERSION.SDK_INT < 11)
                            realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(context, data.getData());

                            // SDK >= 11 && SDK < 19
                        else if (Build.VERSION.SDK_INT < 19)
                            realPath = RealPathUtil.getRealPathFromURI_API11to18(context, data.getData());

                            // SDK > 19 (Android 4.4)
                        else
                            realPath = RealPathUtil.getRealPathFromURI_API19(context, data.getData());

                        Log.e("Real Path", "" + realPath);

                        String PdfPathHolder = null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                            try {
                                PdfPathHolder = FilePath.getPath(context, mImageCaptureUri);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        assert PdfPathHolder != null;
                        if (realPath == null)
                            photoFile = new File(PdfPathHolder);
                        else
                            photoFile = new File(realPath);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                    /*File file = new File(imgDecodableString);
                    photoFile=file;*/

                    //
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //doCrop();
                break;

        }
    }

    public File getPhotoFileUri(String fileName) {
        File mediaStorageDir = new File(String.valueOf(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)));

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
            Log.d("Error", "failed to create directory");
        }

        // Return the file target for the photo based on filename
        this.file = new File(mediaStorageDir.getPath() + File.separator + fileName);

        return this.file;
    }

    private void captureImageInitialization() {
        /**
         * a selector dialog to display two image source options, from camera
         * ‘Take from camera’ and from existing files ‘Select from gallery’
         */
        final String[] items = new String[]{"Take from camera", "Select from gallery"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_item, items);
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        builder.setTitle("Upload Certificate");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) { // pick from
                // camera
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    photoFile= getPhotoFileUri(photoFileName);
                    mImageCaptureUri= FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,mImageCaptureUri);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    try {
                        intent.putExtra("return-data", true);
                        ((MainActivity) context).startActivityForResult(intent, PICK_FROM_CAMERA);

                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                } else {
                    //Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    Intent intent = new Intent();
                    intent.setType("application/*");
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setAction(Intent.ACTION_GET_CONTENT);

                    // Start the Intent
                    ((MainActivity) context).startActivityForResult(intent, PICK_FROM_FILE);
                }
            }
        });

        dialog1 = builder.create();
    }

    public EduAdapter(Context context, ArrayList<EducationModal> educationModalArrayList, TrainerProfileFragment trainerProfileFragment) {
        this.context=context;
        this.educationModalArrayList=educationModalArrayList;
        this.trainerProfileFragment=trainerProfileFragment;
        captureImageInitialization();
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.edu_adapter,null);
        return new RowHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        EducationModal educationModal=educationModalArrayList.get(position);
        holder.course.setText(educationModal.getCourse());
        holder.year.setText(educationModal.getYear());
        holder.view.setOnClickListener(v-> openFile(educationModal.getURL()));

        holder.edit.setOnClickListener(v12->{
            fromDate="";
            toDate="";

            Intent intent = new Intent("updateEducation");
            intent.putExtra("start",educationModal.getStartFrom());
            intent.putExtra("to",educationModal.getEndTo());
            intent.putExtra("degree",educationModal.getCourse());
            intent.putExtra("certificate",educationModal.getCertificate());
            intent.putExtra("id",educationModal.getId());
            intent.putExtra("url",educationModal.getURL());

            context.sendBroadcast(intent);

            /*AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.education_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            CardView update=dialogView.findViewById(R.id.accept);
            TextView add=dialogView.findViewById(R.id.add);
            CardView uploadCertificate=dialogView.findViewById(R.id.uploadCertificate);
            CardView cancel=dialogView.findViewById(R.id.cancel);
            EditText from,to,degree;
            from=dialogView.findViewById(R.id.from);
            to=dialogView.findViewById(R.id.to);
            degree=dialogView.findViewById(R.id.degree);
            certificateName=dialogView.findViewById(R.id.certificateName);

            add.setText(context.getResources().getString(R.string.update));
            from.setText(educationModal.getStartFrom());
            to.setText(educationModal.getEndTo());
            degree.setText(educationModal.getCourse());
            certificateName.setText(educationModal.getCertificate());

            uploadCertificate.setOnClickListener(v->{
                dialog1.show();

            });
            from.setOnTouchListener((v, event) -> {
                switch(event.getAction() & MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_DOWN:
                        //A pressed gesture has started, the motion contains the initial starting location.
                        Calendar c = Calendar.getInstance();
                        int mYear = c.get(Calendar.YEAR);
                        int mMonth = c.get(Calendar.MONTH);
                        int mDay = c.get(Calendar.DAY_OF_MONTH);
                        @SuppressLint("ClickableViewAccessibility")
                        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                                (DatePickerDialog.OnDateSetListener) (view, year, monthOfYear, dayOfMonth) -> {
                                    view.setMaxDate(year);
                                    from.setError(null);
                                    from.setText(String.valueOf(year));
                                    from.setSelection(from.getText().length());
                                    fromDate=year+"-"+monthOfYear+"-"+dayOfMonth;
                                }, mYear, mMonth, mDay);
                        datePickerDialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis());
                        datePickerDialog.show();

                }
                return true;
            });
            to.setOnTouchListener((v, event) -> {
                switch(event.getAction() & MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_DOWN:
                        //A pressed gesture has started, the motion contains the initial starting location.
                        Calendar c = Calendar.getInstance();
                        int mYear = c.get(Calendar.YEAR);
                        int mMonth = c.get(Calendar.MONTH);
                        int mDay = c.get(Calendar.DAY_OF_MONTH);
                        @SuppressLint("ClickableViewAccessibility")
                        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                                (DatePickerDialog.OnDateSetListener) (view, year, monthOfYear, dayOfMonth) -> {
                                    view.setMaxDate(year);
                                    to.setError(null);
                                    to.setText(String.valueOf(year));
                                    to.setSelection(from.getText().length());
                                    toDate=year+"-"+monthOfYear+"-"+dayOfMonth;
                                }, mYear, mMonth, mDay);
                        //datePickerDialog.getDatePicker().setMaxDate(myCalendar.getTimeInMillis());
                        datePickerDialog.show();

                }
                return true;
            });


            update.setOnClickListener(v1->{
                if(from.getText().toString().trim().matches("")) {
                    from.setError(context.getResources().getString(R.string.fieldRequired));
                    return;
                }
                if(to.getText().toString().trim().matches("")) {
                    to.setError(context.getResources().getString(R.string.fieldRequired));
                    return;
                }
                if(degree.getText().toString().trim().matches("")) {
                    degree.setError(context.getResources().getString(R.string.fieldRequired));
                    return;
                }

                updateEducation(from.getText().toString(),to.getText().toString(),degree.getText().toString(),dialog,certificateName.getText().toString()
                        ,educationModal.getId(),educationModal.getURL());
            });
            cancel.setOnClickListener(v1->{
                dialog.dismiss();
            });

            close.setOnClickListener(v1->dialog.dismiss());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dialog.create();
            }
            dialog.show();*/
        });
    }

    /*private void updateEducation(String from, String to, String degree, Dialog dialog, String certificateName, String id, String url) {
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        ProgressDialog progressDialog=new ProgressDialog(context);
        progressDialog.showProgress(context.getResources().getString(R.string.processing));
        new Thread(() -> {
            try {
                //first_name,last_name,contact_no,country,trainee_id,profile_photo_edit
                MultipartUtility multipart = new MultipartUtility(Api.TrainerEducationUpdate, "UTF-8");
                multipart.addFormField("date_from", from);
                multipart.addFormField("date_to", to);
                multipart.addFormField("upload_certificate_edit", certificateName);
                multipart.addFormField("education_id", id);
                multipart.addFormField("designation", degree);
                multipart.addFormField("traineer_id", sharedPreferences.getString("userID",""));
                multipart.addFormField("lang_token", sharedPreferences.getString("lang",""));
                if(photoFile==null)
                    multipart.addFormField("upload_certificate", url);
                else
                    multipart.addFilePart("upload_certificate", photoFile);

                Log.e("photoFile",""+photoFile);

                String response = multipart.finish();// response from server
                Log.e("Multipart Response",response);
                try {
                    final JSONObject jsonObject1 = new JSONObject(response);
                    boolean status = jsonObject1.optBoolean("status");
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    if(status){
                        ((MainActivity)context).runOnUiThread(() -> {
                            dialog.dismiss();
                            Intent intent=new Intent("updateTrainer");
                            context.sendBroadcast(intent);
                        });

                    }

                } catch (JSONException e) {
                    ((MainActivity)context).runOnUiThread(() -> {
                        progressDialog.cancel();;
                        // Stuff that updates the UI
                    });
                    e.printStackTrace();
                }
            } catch (IOException e) {
                ((MainActivity)context).runOnUiThread(() -> {
                    progressDialog.cancel();;
                    // Stuff that updates the UI
                });
                e.printStackTrace();
            }
        }).start();
    }*/

    @Override
    public int getItemCount() {
        return educationModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView course,year,view;
        ImageView edit;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            edit=itemView.findViewById(R.id.edit);
            view=itemView.findViewById(R.id.view);
            course=itemView.findViewById(R.id.course);
            year=itemView.findViewById(R.id.year);
        }
    }


    private void openFile(String url) {

        try {

            Uri uri = Uri.parse(url);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip")) {
                // ZIP file
                intent.setDataAndType(uri, "application/zip");
            } else if (url.toString().contains(".rar")){
                // RAR file
                intent.setDataAndType(uri, "application/x-rar-compressed");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") ||
                    url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                intent.setDataAndType(uri, "*/*");
            }

            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW);
            browserIntent.setDataAndType(Uri.parse(url), "text/html");

            Intent chooser = Intent.createChooser(browserIntent, context.getResources().getString(R.string.chooseApp));
            chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

            context.startActivity(chooser);

            Toast.makeText(context, context.getResources().getString(R.string.noApp), Toast.LENGTH_SHORT).show();
        }
    }

}

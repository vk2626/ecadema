package com.ecadema.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.fragments.BlogDetailsFrag;
import com.ecadema.modal.BlogModal;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class CommunityAdapter extends RecyclerView.Adapter<CommunityAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<BlogModal> blogModalArrayList;

    public CommunityAdapter(Context context, ArrayList<BlogModal> blogModalArrayList) {
        this.context=context;
        this.blogModalArrayList=blogModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.my_post_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        BlogModal postModal = blogModalArrayList.get(position);
        holder.name.setText(postModal.getName());
        holder.date.setText(postModal.getDate());
        holder.views.setText(context.getResources().getString(R.string.views)+" "+postModal.getView());
        holder.title.setText(postModal.getTitle());
        holder.description.setText(Html.fromHtml(postModal.getShortDecription()));
        holder.likeCount.setText(postModal.getLikes());
        holder.dislikeCount.setText(postModal.getDislike());
        holder.catName.setText(postModal.getCategoryName());

        try {
            Picasso.with(context).load(postModal.getImage()).into(holder.profile_pic);
        }catch (Exception e){
            e.printStackTrace();
        }

        holder.cardView.setOnClickListener(v->{

            Bundle bundle=new Bundle();
            bundle.putString("id",postModal.getId());
            BlogDetailsFrag blogDetailsFrag=new BlogDetailsFrag();
            blogDetailsFrag.setArguments(bundle);
            ((MainActivity) context).addFragment2(blogDetailsFrag, true, false, 0);

            /*Intent intent = new Intent("showDetail");
            intent.putExtra("id",postModal.getId());
            context.sendBroadcast(intent);*/
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return blogModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView profile_pic;
        TextView name, date, views, title, description, likeCount, dislikeCount, catName;
        CardView readMore,cardView;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            profile_pic = (itemView).findViewById(R.id.profile_pic);

            name = (itemView).findViewById(R.id.name);
            date = (itemView).findViewById(R.id.date);
            cardView = (itemView).findViewById(R.id.cardView);
            views = (itemView).findViewById(R.id.view);
            title = (itemView).findViewById(R.id.title);
            description = (itemView).findViewById(R.id.description);
            readMore = (itemView).findViewById(R.id.readMore);
            likeCount = (itemView).findViewById(R.id.likeCount);
            dislikeCount = (itemView).findViewById(R.id.dislikeCount);
            catName = (itemView).findViewById(R.id.catName);

            readMore.setVisibility(View.GONE);
        }
    }
}

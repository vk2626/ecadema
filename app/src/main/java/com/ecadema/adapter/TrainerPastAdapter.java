package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.R;
import com.ecadema.modal.IconModal;
import com.ecadema.modal.PastModal;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TrainerPastAdapter extends RecyclerView.Adapter<TrainerPastAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<PastModal> pastModalArrayList;


    ArrayList<IconModal> iconModalArrayList = new ArrayList<>();
    ArrayList<String> selectedSkills= new ArrayList<>();
    String starRating="", teacherID="",bookingID="";
    SharedPreferences sharedPreferences;

    BroadcastReceiver trainerSkills =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(selectedSkills.contains(intent.getStringExtra("id")))
                selectedSkills.remove(intent.getStringExtra("id"));
            else
                selectedSkills.add(intent.getStringExtra("id"));

            Log.e("TAG", "trainerAdapter: "+ selectedSkills.toString());
        }
    };

    private void getAllRatings() {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));

        new PostMethod(Api.TraineerSkills,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.optJSONArray("data");
                    for (int ar=0;ar<dataArray.length();ar++){
                        JSONObject object = dataArray.getJSONObject(ar);
                        IconModal iconModal = new IconModal();
                        iconModal.setCount(object.optString("id"));
                        iconModal.setName(object.optString("skill"));
                        iconModal.setId(object.optString("id"));
                        iconModal.setImage(object.optString("icon_url"));
                        iconModalArrayList.add(iconModal);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }


    public TrainerPastAdapter(Context context, ArrayList<PastModal> pastModalArrayList) {
        this.context=context;
        this.pastModalArrayList=pastModalArrayList;
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        getAllRatings();
        context.registerReceiver(trainerSkills,new IntentFilter("trainer"));
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.my_class_adapter,null);
        return new RowHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        PastModal pastModal=pastModalArrayList.get(position);
        String text = "<font color=#f16667>"+context.getResources().getString(R.string.with) +" </font> <font color=#757575>"+pastModal.getName()+"</font>";

        holder.sessionType.setText(pastModal.getSessionType());
        holder.name.setText(Html.fromHtml(text));
        holder.sessionName.setText(pastModal.getSessionName());
        holder.dateTime.setText(pastModal.getDateTime());
        holder.duration.setText(pastModal.getDuration()+" "+context.getResources().getString(R.string.hours));
        holder.rate.setText("$ "+pastModal.getRate());

        holder.ll1.setVisibility(View.GONE);
        holder.ll2.setVisibility(View.GONE);
        holder.profileCard.setVisibility(View.GONE);
        holder.rating.setVisibility(View.VISIBLE);
        holder.expired.setVisibility(View.VISIBLE);

        holder.constraint.setVisibility(View.VISIBLE);

        try {
            Picasso.with(context).load(pastModal.getBanner()).into(holder.headerPic);

        }catch (Exception e){
            e.printStackTrace();
        }

        if (pastModal.getTeacherID().equalsIgnoreCase(""))
            holder.rating.setVisibility(View.GONE);
        else {
            if (pastModal.getButtonName().equalsIgnoreCase(""))
                holder.rating.setVisibility(View.GONE);
            else
                holder.rating.setVisibility(View.VISIBLE);
        }

        holder.rating.setText(pastModal.getButtonName());
        holder.rating.setTag(position);
        holder.rating.setOnClickListener(v-> {

            int pos = (int)v.getTag();
            if (holder.rating.getText().toString().equalsIgnoreCase(context.getString(R.string.rated)))
                return;

            if (!pastModalArrayList.get(pos).getURL().equalsIgnoreCase("")){
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(pastModalArrayList.get(pos).getURL()), "application/pdf");

                Intent chooser = Intent.createChooser(browserIntent, context.getString(R.string.chooser_title));
                chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

                context.startActivity(chooser);
                return;
            }

            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView =
                    inflater1.inflate(R.layout.feedback_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            RecyclerView skills=dialogView.findViewById(R.id.skills);
            CardView update=dialogView.findViewById(R.id.update);
            RatingBar ratings = dialogView.findViewById(R.id.ratings);
            EditText comment = dialogView.findViewById(R.id.comment);


            skills.setLayoutManager(new GridLayoutManager(context,3));
            IconAdapter iconAdapter= new IconAdapter(context,iconModalArrayList,"trainer");
            skills.setAdapter(iconAdapter);
            update.setOnClickListener(v1->{
                starRating=""+ratings.getRating();
                    /*if(comment.getText().toString().trim().matches("")){
                        comment.setError(context.getResources().getString(R.string.fieldRequired));
                        return;
                    }*/
                if(selectedSkills.size()==0){
                    Toast.makeText(context, context.getResources().getString(R.string.selectOneSkill), Toast.LENGTH_SHORT).show();
                    return;
                }
                AddRatings(dialog,comment.getText().toString(),starRating,pastModal.getTeacherID(),
                        pastModal.getBookingID(),pos);
            });

            close.setOnClickListener(v1->dialog.dismiss());
            //cancel.setOnClickListener(v1->dialog.dismiss());

            dialog.create();
            dialog.show();
        });
    }

    private void AddRatings(Dialog dialog, String comment, String starRating, String teacherID, String id, int pos) {
        String csv = android.text.TextUtils.join(",", selectedSkills);

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String > param = new HashMap<>();
        param.put("teacher_id",teacherID);
        param.put("student_id",sharedPreferences.getString("userID",""));
        param.put("rating",starRating);
        param.put("comment",comment);
        param.put("review_type","workshop");
        param.put("skill",csv);
        param.put("booking_id",id);

        Log.e("addRatingParam",""+param);
        new PostMethod(Api.AddReviews,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        Intent intent = new Intent("trainerWorkshopUpdate");
                        intent.putExtra("pos",pos);
                        context.sendBroadcast(intent);
                        dialog.dismiss();
                    }
                    Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        return pastModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView profilePic,expired,headerPic;
        TextView name,sessionType,sessionName,rating,dateTime,duration,rate;
        LinearLayout ll1,ll2;
        ConstraintLayout constraint;
        CardView profileCard;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            headerPic=itemView.findViewById(R.id.headerPic);
            profileCard=itemView.findViewById(R.id.profileCard);
            dateTime=itemView.findViewById(R.id.dateTime);
            duration=itemView.findViewById(R.id.duration);
            rate=itemView.findViewById(R.id.rate);
            constraint=itemView.findViewById(R.id.constraint);
            profilePic=itemView.findViewById(R.id.profile_pic);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);
            ll1=itemView.findViewById(R.id.ll1);
            ll2=itemView.findViewById(R.id.ll2);
            sessionName=itemView.findViewById(R.id.sessionName);
            rating=itemView.findViewById(R.id.rating);
            expired=itemView.findViewById(R.id.expired);
        }
    }
}

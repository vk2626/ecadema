package com.ecadema.adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.fragments.SessionDetails;
import com.ecadema.modal.UpcomingModal;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class UpcomingAdapter extends RecyclerView.Adapter<UpcomingAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<UpcomingModal> upcomingModalArrayList;
    private final String type;
    private ArrayList<UpcomingModal> filterArrayList;

    public UpcomingAdapter(Context context, ArrayList<UpcomingModal> upcomingModalArrayList, String type) {
        this.context=context;
        this.upcomingModalArrayList=upcomingModalArrayList;
        this.type=type;

        this.filterArrayList = new ArrayList<>();//initiate filter list
        this.filterArrayList.addAll(upcomingModalArrayList);
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.session_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        UpcomingModal upcomingModal=upcomingModalArrayList.get(position);

        holder.sessionType.setText(upcomingModal.getSessionType());
        holder.name.setText(upcomingModal.getName());
        holder.about.setText(upcomingModal.getAbout());
        holder.ticket.setText(upcomingModal.getTicket());
        Glide.with(context).load(upcomingModal.getImage()).into(holder.profilePic);
        Glide.with(context).load(upcomingModal.getBanner()).into(holder.banner);

        holder.readMore.setOnClickListener(view -> {
            Bundle bundle=new Bundle();
            bundle.putString("sessionId",upcomingModal.getID());
            bundle.putString("trainerID",upcomingModal.getTeacherID());
            SessionDetails sessionDetails=new SessionDetails();
            sessionDetails.setArguments(bundle);
            ((MainActivity) context).addFragment2(sessionDetails, true, false, 0);
        });
    }

    @Override
    public int getItemCount() {
        return upcomingModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {

        CircleImageView profilePic;
        ImageView banner;
        TextView name,sessionType,about,ticket,readMore;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            profilePic=itemView.findViewById(R.id.profile_pic);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);
            about=itemView.findViewById(R.id.about);
            ticket=itemView.findViewById(R.id.ticket);
            readMore=itemView.findViewById(R.id.readMore);
            banner=itemView.findViewById(R.id.banner);
        }
    }

    public void filter(String charText) {

        Log.e("filters",charText);
        //If Filter type is NAME and EMAIL then only do lowercase, else in case of NUMBER no need to do lowercase because of number format
        charText = charText.toLowerCase(Locale.getDefault());
        upcomingModalArrayList.clear();//Clear the main ArrayList

        //If search query is null or length is 0 then add all filterList items back to attachmentList
        if (charText.length() == 0) {
            upcomingModalArrayList.addAll(filterArrayList);
        } else {

            //Else if search query is not null do a loop to all filterList items
            for (UpcomingModal model : filterArrayList) {

                //Now check the type of search filter
                if (model.getName().toLowerCase(Locale.getDefault()).startsWith(charText)||
                        model.getName().toLowerCase(Locale.getDefault()).contains(charText)||
                        model.getAbout().toLowerCase(Locale.getDefault()).contains(charText))
                    upcomingModalArrayList.add(model);

            }
        }
        notifyDataSetChanged();
    }

}

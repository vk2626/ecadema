package com.ecadema.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.fragments.SessionDetails;
import com.ecadema.modal.GSModal;

import java.util.ArrayList;

public class GSAdapter extends RecyclerView.Adapter<GSAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<GSModal> gsModalArrayList;

    public GSAdapter(Context context, ArrayList<GSModal> gsModalArrayList) {
        this.context=context;
        this.gsModalArrayList=gsModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.gs_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        GSModal gsModal=gsModalArrayList.get(position);

        holder.sessionType.setText(gsModal.getSessionType());
        holder.name.setText(gsModal.getName());
        holder.about.setText(gsModal.getAbout());
        holder.ticket.setText(gsModal.getTicket());
        Glide.with(context).load(gsModal.getBanner()).into(holder.banner);

        holder.cardView.setOnClickListener(view -> {
            Bundle bundle=new Bundle();
            bundle.putString("sessionId",gsModal.getID());
            bundle.putString("trainerID",gsModal.getTrainerId());
            SessionDetails sessionDetails=new SessionDetails();
            sessionDetails.setArguments(bundle);
            ((MainActivity) context).addFragment2(sessionDetails, true, false, 0);
        });
    }

    @Override
    public int getItemCount() {
        return gsModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView banner;
        TextView name,sessionType,about,ticket;
        CardView cardView;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            cardView=itemView.findViewById(R.id.cardView);
            banner=itemView.findViewById(R.id.banner);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);
            about=itemView.findViewById(R.id.about);
            ticket=itemView.findViewById(R.id.ticket);
        }
    }
}

package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.app.R;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.modal.IconModal;
import com.ecadema.modal.PastModal;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyPastAdapter extends RecyclerView.Adapter<MyPastAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<PastModal> pastModalArrayList;

    SharedPreferences sharedPreferences;
    ArrayList<IconModal> iconModalArrayList = new ArrayList<>();
    ArrayList<String> selectedSkills= new ArrayList<>();
    String starRating="", teacherID="",bookingID="";

    BroadcastReceiver skills =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(selectedSkills.contains(intent.getStringExtra("id")))
                selectedSkills.remove(intent.getStringExtra("id"));
            else
                selectedSkills.add(intent.getStringExtra("id"));

            Log.e("TAG", "sessionAdapter: "+ selectedSkills.toString());
        }
    };

    private void getAllRatings() {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));

        new PostMethod(Api.TraineerSkills,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.optJSONArray("data");
                    for (int ar=0;ar<dataArray.length();ar++){
                        JSONObject object = dataArray.getJSONObject(ar);
                        IconModal iconModal = new IconModal();
                        iconModal.setCount(object.optString("id"));
                        iconModal.setName(object.optString("skill"));
                        iconModal.setId(object.optString("id"));
                        iconModal.setImage(object.optString("icon_url"));
                        iconModalArrayList.add(iconModal);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    public MyPastAdapter(Context context, ArrayList<PastModal> pastModalArrayList) {
        this.context=context;
        this.pastModalArrayList=pastModalArrayList;

        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        getAllRatings();
        context.registerReceiver(skills,new IntentFilter("sessions"));
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.my_session_adapter,null);
        return new RowHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        PastModal pastModal=pastModalArrayList.get(position);
        String text = "<font color=#f16667>"+ context.getResources().getString(R.string.with) +" </font> <font color=#757575>"+pastModal.getName()+"</font>";

        holder.sessionType.setText(pastModal.getSessionType());
        holder.name.setText(Html.fromHtml(text));
        holder.about.setText(pastModal.getSessionName());
        holder.dateTime.setText(pastModal.getDateTime());
        holder.totalHours.setText(pastModal.getDuration()+" "+context.getResources().getString(R.string.hours));
        holder.languages.setText(pastModal.getLanguage());

        holder.ll2.setVisibility(View.GONE);

        if(pastModal.getRateByTrainee().equalsIgnoreCase("1")){
            holder.rating.setText(context.getResources().getString(R.string.rated));
        }
        if (pastModal.getButtonName().equalsIgnoreCase(""))
            holder.rating.setVisibility(View.GONE);
        else
            holder.rating.setVisibility(View.VISIBLE);

        holder.rating.setText(pastModal.getButtonName());

        holder.rating.setTag(position);
        holder.rating.setOnClickListener(v->{
            int pos = (int)v.getTag();
            teacherID= pastModalArrayList.get(pos).getTeacherID();
            bookingID= pastModalArrayList.get(pos).getID();

            if (!pastModalArrayList.get(pos).getURL().equalsIgnoreCase("")){
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(pastModalArrayList.get(pos).getURL()), "application/pdf");

                Intent chooser = Intent.createChooser(browserIntent, context.getString(R.string.chooser_title));
                chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

                context.startActivity(chooser);
                return;
            }

            if (!holder.rating.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.rated))){
                androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
                LayoutInflater inflater1 =
                        (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                @SuppressLint("InflateParams") final View dialogView =
                        inflater1.inflate(R.layout.feedback_dialog, null);
                alertDialog.setView(dialogView);
                alertDialog.setCancelable(false);
                Dialog dialog=alertDialog.create();
                ImageView close=dialogView.findViewById(R.id.close);
                RecyclerView skills=dialogView.findViewById(R.id.skills);
                CardView update=dialogView.findViewById(R.id.update);
                RatingBar ratings = dialogView.findViewById(R.id.ratings);
                EditText comment = dialogView.findViewById(R.id.comment);


                skills.setLayoutManager(new GridLayoutManager(context,3));
                IconAdapter iconAdapter= new IconAdapter(context,iconModalArrayList,"sessions");
                skills.setAdapter(iconAdapter);
                update.setOnClickListener(v1->{
                    starRating=""+ratings.getRating();
                    /*if(comment.getText().toString().trim().matches("")){
                        comment.setError(context.getResources().getString(R.string.fieldRequired));
                        return;
                    }*/
                    if(selectedSkills.size()==0){
                        Toast.makeText(context, context.getResources().getString(R.string.selectOneSkill), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    AddRatings(dialog,comment.getText().toString(),starRating,teacherID,bookingID);
                });

                close.setOnClickListener(v1->dialog.dismiss());
                //cancel.setOnClickListener(v1->dialog.dismiss());

                dialog.create();
                dialog.show();
            }
        });

        try {
            Picasso.with(context).load(pastModal.getBanner()).into(holder.headerPic);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void AddRatings(Dialog dialog, String comment, String starRating, String teacherID, String id) {
        String csv = android.text.TextUtils.join(",", selectedSkills);

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String > param = new HashMap<>();
        param.put("teacher_id",teacherID);
        param.put("student_id",sharedPreferences.getString("userID",""));
        param.put("rating",starRating);
        param.put("comment",comment);
        param.put("review_type","workshop");
        param.put("skill",csv);
        param.put("booking_id",id);

        Log.e("addRatingParam",""+param);
        new PostMethod(Api.AddReviews,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        Intent intent = new Intent("updateWorkshop");
                        context.sendBroadcast(intent);
                        dialog.dismiss();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        return pastModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView profilePic,headerPic;
        TextView name,sessionType,about,days,hours,mins,seconds,dateTime,totalHours,languages,rating;
        LinearLayout ll2;
        CardView rate;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            headerPic=itemView.findViewById(R.id.headerPic);
            profilePic=itemView.findViewById(R.id.profile_pic);
            rating=itemView.findViewById(R.id.rating);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);
            about=itemView.findViewById(R.id.about);
            dateTime=itemView.findViewById(R.id.dateTime);
            totalHours=itemView.findViewById(R.id.totalHours);
            languages=itemView.findViewById(R.id.languages);

            days = itemView.findViewById(R.id.days);
            hours = itemView.findViewById(R.id.hours);
            mins = itemView.findViewById(R.id.minutes);
            seconds = itemView.findViewById(R.id.seconds);
            rate = itemView.findViewById(R.id.rate);
            ll2 = itemView.findViewById(R.id.ll2);

            ll2.setVisibility(View.GONE);
            rate.setVisibility(View.VISIBLE);
        }
    }

}

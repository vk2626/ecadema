package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.R;
import com.ecadema.fragments.TeamMateFragment;
import com.ecadema.modal.TeammateModal;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class EmployeesRequestListAdapter extends RecyclerView.Adapter<EmployeesRequestListAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<TeammateModal> teammateModalArrayList;
    private final TeamMateFragment teamMateFragment;

    public EmployeesRequestListAdapter(Context context, ArrayList<TeammateModal> teammateModalArrayList, TeamMateFragment teamMateFragment) {
        this.context=context;
        this.teammateModalArrayList=teammateModalArrayList;
        this.teamMateFragment=teamMateFragment;
    }

    @NonNull
    @Override
    public EmployeesRequestListAdapter.RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.teammate_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeesRequestListAdapter.RowHolder holder, int position) {
        TeammateModal teammateModal=teammateModalArrayList.get(position);

        holder.name.setText(teammateModal.getName());
        holder.email.setText(teammateModal.getEmail());

        if (teammateModal.getMobile().equalsIgnoreCase(""))
            holder.mobile.setText("-");
        else
            holder.mobile.setText(teammateModal.getMobile());

        if (teammateModal.getDepartment().equalsIgnoreCase(""))
            holder.department.setText("-");
        else
            holder.department.setText(teammateModal.getDepartment());

        if (teammateModal.getSeniority().equalsIgnoreCase(""))
            holder.seniority.setText("-");
        else
            holder.seniority.setText(teammateModal.getSeniority());

        Picasso.with(context).load(teammateModal.getImage()).into(holder.profile_pic);

        holder.accept.setTag(position);
        holder.accept.setOnClickListener(v->{
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.teammate_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            TextView cancel=dialogView.findViewById(R.id.cancel);
            TextView message=dialogView.findViewById(R.id.message);
            TextView yes=dialogView.findViewById(R.id.yes);
            TextView title=dialogView.findViewById(R.id.title);
            CardView deactivate=dialogView.findViewById(R.id.deactivate);

            yes.setText(context.getResources().getString(R.string.yes));
            cancel.setText(context.getResources().getString(R.string.no));
            message.setText(context.getResources().getString(R.string.want_to_accept));
            title.setText(context.getResources().getString(R.string.accept_emplpoyee));
            close.setOnClickListener(v1->dialog.dismiss());
            cancel.setOnClickListener(v1->dialog.dismiss());

            deactivate.setOnClickListener(v1->{
                int pos = (int) v.getTag();
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setTitle(context.getResources().getString(R.string.processing));
                progressDialog.show();
                Map<String,String> param=new HashMap<>();
                param.put("lang_token",sharedPreferences.getString("lang",""));
                param.put("removed_id",teammateModalArrayList.get(pos).getTeammateId());
                param.put("status","1");
                Log.e("AcceptParam",""+param);

                new PostMethod(Api.RemoveTeammates,param,context).startPostMethod(new ResponseData() {
                    @Override
                    public void response(String data) {
                        progressDialog.cancel();;
                        Log.e("AcceptResp",data);
                        try {
                            teamMateFragment.getTeamMates(2);
                            JSONObject jsonObject = new JSONObject(data);
                            Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void error(VolleyError error) {
                        progressDialog.cancel();;
                        error.printStackTrace();
                    }
                });
                dialog.dismiss();

            });
            dialog.create();
            dialog.show();

        });

        holder.reject.setTag(position);
        holder.reject.setOnClickListener(v->{
            androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
            LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.teammate_dialog, null);
            alertDialog.setView(dialogView);
            alertDialog.setCancelable(false);
            Dialog dialog=alertDialog.create();
            ImageView close=dialogView.findViewById(R.id.close);
            TextView cancel=dialogView.findViewById(R.id.cancel);
            TextView message=dialogView.findViewById(R.id.message);
            TextView yes=dialogView.findViewById(R.id.yes);
            TextView title=dialogView.findViewById(R.id.title);
            CardView deactivate=dialogView.findViewById(R.id.deactivate);

            yes.setText(context.getResources().getString(R.string.yes));
            cancel.setText(context.getResources().getString(R.string.no));
            message.setText(context.getResources().getString(R.string.want_to_reject));
            title.setText(context.getResources().getString(R.string.reject_emplpoyee));
            close.setOnClickListener(v1->dialog.dismiss());
            cancel.setOnClickListener(v1->dialog.dismiss());

            deactivate.setOnClickListener(v1->{
                int pos = (int) v.getTag();
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                ProgressDialog progressDialog = new ProgressDialog(context);
                progressDialog.setTitle(context.getResources().getString(R.string.processing));
                progressDialog.show();
                Map<String,String> param=new HashMap<>();
                param.put("lang_token",sharedPreferences.getString("lang",""));
                param.put("removed_id",teammateModalArrayList.get(pos).getTeammateId());
                param.put("status","2");
                Log.e("RejectedParam",""+param);

                new PostMethod(Api.RemoveTeammates,param,context).startPostMethod(new ResponseData() {
                    @Override
                    public void response(String data) {
                        progressDialog.cancel();;
                        Log.e("RejectedResp",data);
                        try {
                            teamMateFragment.getTeamMates(2);
                            teammateModalArrayList.remove(pos);
                            notifyDataSetChanged();
                            JSONObject jsonObject = new JSONObject(data);
                            Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void error(VolleyError error) {
                        progressDialog.cancel();;
                        error.printStackTrace();
                    }
                });
                dialog.dismiss();

            });
            dialog.create();
            dialog.show();
        });
    }

    @Override
    public int getItemCount() {
        return teammateModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView reject,accept,name,email,mobile,department,seniority;
        CircleImageView profile_pic;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            seniority=itemView.findViewById(R.id.seniority);
            department=itemView.findViewById(R.id.department);
            reject=itemView.findViewById(R.id.remove);
            accept=itemView.findViewById(R.id.chat);
            name=itemView.findViewById(R.id.name);
            email=itemView.findViewById(R.id.email);
            mobile=itemView.findViewById(R.id.mobile);
            profile_pic=itemView.findViewById(R.id.profile_pic);

            reject.setText(context.getResources().getString(R.string.reject));
            accept.setText(context.getResources().getString(R.string.accept));
        }
    }
}

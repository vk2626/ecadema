package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.fragments.ViewProfile;
import com.ecadema.modal.IconModal;
import com.ecadema.modal.TrainerModal;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyTrainerAdapter extends RecyclerView.Adapter<MyTrainerAdapter.RowHolder>{
    private final Context context;
    private final ArrayList<TrainerModal> trainerModalArrayList;
    SharedPreferences sharedPreferences;

    ArrayList<IconModal> iconModalArrayList = new ArrayList<>();
    ArrayList<String> selectedSkills= new ArrayList<>();
    String starRating="", teacherID="",bookingID="";

    BroadcastReceiver skills =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(selectedSkills.contains(intent.getStringExtra("id")))
                selectedSkills.remove(intent.getStringExtra("id"));
            else
                selectedSkills.add(intent.getStringExtra("id"));

            Log.e("TAG", "trainerAdapter: "+ selectedSkills.toString());
        }
    };

    private void getAllRatings() {
        Map<String,String> param=new HashMap<>();
        param.put("lang_token",sharedPreferences.getString("lang",""));

        new PostMethod(Api.TraineerSkills,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.optJSONArray("data");
                    for (int ar=0;ar<dataArray.length();ar++){
                        JSONObject object = dataArray.getJSONObject(ar);
                        IconModal iconModal = new IconModal();
                        iconModal.setCount(object.optString("id"));
                        iconModal.setName(object.optString("skill"));
                        iconModal.setId(object.optString("id"));
                        iconModal.setImage(object.optString("icon_url"));
                        iconModalArrayList.add(iconModal);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                error.printStackTrace();
            }
        });
    }
    public MyTrainerAdapter(Context context, ArrayList<TrainerModal> trainerModalArrayList) {
        this.context=context;
        this.trainerModalArrayList=trainerModalArrayList;

        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        getAllRatings();
        context.registerReceiver(skills,new IntentFilter("trainer"));
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.my_trainer_adapter,null);
        return new RowHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        TrainerModal trainerModal=trainerModalArrayList.get(position);
        holder.name.setText(trainerModal.getName());
        holder.rating.setRating(Float.parseFloat(trainerModal.getRating()));

        holder.viewProfile.setOnClickListener(v->{
            Bundle bundle=new Bundle();
            bundle.putString("trainerID",trainerModal.getTeacherID());
            ViewProfile viewProfile=new ViewProfile();
            viewProfile.setArguments(bundle);
            ((MainActivity) context).addFragment2(viewProfile, true, false, 0);
        });

        holder.rate.setTag(position);

        try {
            Picasso.with(context).load(trainerModal.getImage()).into(holder.profile_pic);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void AddRatings(Dialog dialog, String comment, String starRating, String teacherID, String id) {
        String csv = android.text.TextUtils.join(",", selectedSkills);

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();
        Map<String,String > param = new HashMap<>();
        param.put("teacher_id",teacherID);
        param.put("student_id",sharedPreferences.getString("userID",""));
        param.put("rating",starRating);
        param.put("comment",comment);
        param.put("review_type","");
        param.put("skill",csv);
        param.put("booking_id",id);

        Log.e("addRatingParam",""+param);
        new PostMethod(Api.AddReviews,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    JSONObject jsonObject = new JSONObject(data);
                    if(jsonObject.optBoolean("status")){
                        Intent intent = new Intent("trainerUpdate");
                        context.sendBroadcast(intent);
                        dialog.dismiss();
                    }
                    Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        return trainerModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView name,rate,viewProfile;
        RatingBar rating;
        ImageView profile_pic;
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);
            rating=itemView.findViewById(R.id.ratings);
            profile_pic=itemView.findViewById(R.id.profile_pic);
            viewProfile=itemView.findViewById(R.id.viewProfile);
            rate=itemView.findViewById(R.id.rate);

            rate.setOnClickListener(v->{
                int pos = (int)v.getTag();
                teacherID= trainerModalArrayList.get(pos).getTeacherID();
                bookingID= "";
                androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
                LayoutInflater inflater1 =
                        (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                @SuppressLint("InflateParams") final View dialogView =
                        inflater1.inflate(R.layout.feedback_dialog, null);
                alertDialog.setView(dialogView);
                alertDialog.setCancelable(false);
                Dialog dialog=alertDialog.create();
                ImageView close=dialogView.findViewById(R.id.close);
                RecyclerView skills=dialogView.findViewById(R.id.skills);
                CardView update=dialogView.findViewById(R.id.update);
                RatingBar ratings = dialogView.findViewById(R.id.ratings);
                EditText comment = dialogView.findViewById(R.id.comment);


                skills.setLayoutManager(new GridLayoutManager(context,3));
                IconAdapter iconAdapter= new IconAdapter(context,iconModalArrayList,"trainer");
                skills.setAdapter(iconAdapter);
                update.setOnClickListener(v1->{
                    starRating=""+ratings.getRating();
                    /*if(comment.getText().toString().trim().matches("")){
                        comment.setError(context.getResources().getString(R.string.fieldRequired));
                        return;
                    }*/
                    if(selectedSkills.size()==0){
                        Toast.makeText(context, context.getResources().getString(R.string.selectOneSkill), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    AddRatings(dialog,comment.getText().toString(),starRating,teacherID,bookingID);
                });

                close.setOnClickListener(v1->dialog.dismiss());
                //cancel.setOnClickListener(v1->dialog.dismiss());

                dialog.create();
                dialog.show();
            });

        }
    }
}

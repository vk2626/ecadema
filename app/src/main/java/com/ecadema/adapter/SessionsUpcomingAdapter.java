package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.app.Web_View;
import com.ecadema.fragments.MyClassesFragment;
import com.ecadema.modal.UpcomingModal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SessionsUpcomingAdapter extends RecyclerView.Adapter<SessionsUpcomingAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<UpcomingModal> upcomingModalArrayList;

    public SessionsUpcomingAdapter(Context context, ArrayList<UpcomingModal> upcomingModalArrayList, MyClassesFragment myClassesFragment) {
        this.context=context;
        this.upcomingModalArrayList=upcomingModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.corporate_session_adapter,null);
        return new RowHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        UpcomingModal upcomingModal=upcomingModalArrayList.get(position);
        String text = "<font color=#f16667>"+  context.getResources().getString(R.string.By) +" </font> <font color=#757575>"+upcomingModal.getName()+"</font>";

        holder.sessionType.setText(upcomingModal.getSessionType());
        holder.name.setText(Html.fromHtml(text));
        holder.sessionName.setText(upcomingModal.getSessionName());

        holder.dateTime.setText(upcomingModal.getDateTime());
        holder.rate.setText("$ "+upcomingModal.getRate()+" /"+context.getResources().getString(R.string.hour));
        holder.duration.setText(upcomingModal.getDuration()+" "+context.getResources().getString(R.string.hours));

        holder.listOfAttendees.setOnTouchListener((view, motionEvent) -> {
            if(motionEvent.getAction()== MotionEvent.ACTION_DOWN)
                holder.listOfAttendees.showDropDown();
            return true;
        });

        holder.wizIQ.setTag(position);
        if (upcomingModal.getURL().equalsIgnoreCase("")){
            holder.wizIQ.setVisibility(View.GONE);
        }else {
            holder.wizIQ.setVisibility(View.VISIBLE);
            holder.wizIQ.setOnClickListener(v->{
                int pos = (int)v.getTag();
                Intent intent = new Intent(context, Web_View.class);
                intent.putExtra("url",upcomingModalArrayList.get(pos).getURL());
                context.startActivity(intent);
                /*Intent intent = new Intent(context, ClassroomActivity.class);
                intent.putExtra(ClassroomActivity.URL, upcomingModalArrayList.get(pos).getURL());
                context.startActivity(intent);*/
            });
        }

        Picasso.with(context).load(upcomingModal.getBanner()).into(holder.headerPic);
        //List of attendees
        holder.listOfAttendees.setOnItemClickListener((parent, view, i, id) -> {
            holder.listOfAttendees.setText(context.getResources().getString(R.string.listOfAttendees));
            holder.listOfAttendees.setAdapter(new AutocompleteCustomAdapter(context,R.layout.custom_adapter,upcomingModal.getAttendeesList()));
        });
        holder.listOfAttendees.setAdapter(new AutocompleteCustomAdapter(context,R.layout.custom_adapter,upcomingModal.getAttendeesList()));
    }

    @Override
    public int getItemCount() {
        return upcomingModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView profilePic,headerPic,wizIQ;
        TextView name,sessionType,sessionName,duration,dateTime,rate;
        AutoCompleteTextView listOfAttendees;
        CardView complete;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            wizIQ=itemView.findViewById(R.id.wizIQ);
            headerPic=itemView.findViewById(R.id.headerPic);
            dateTime=itemView.findViewById(R.id.dateTime);
            rate=itemView.findViewById(R.id.rate);
            profilePic=itemView.findViewById(R.id.profile_pic);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);
            duration=itemView.findViewById(R.id.duration);
            sessionName=itemView.findViewById(R.id.sessionName);
            listOfAttendees=itemView.findViewById(R.id.listOfAttendees);
            complete=itemView.findViewById(R.id.complete);

        }
    }
}

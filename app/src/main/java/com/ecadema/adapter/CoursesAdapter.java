package com.ecadema.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.CoursesModal;

import java.util.ArrayList;

public class CoursesAdapter extends RecyclerView.Adapter<CoursesAdapter.RowHolder> {
    private final Context context;
    private ArrayList<CoursesModal> coursesModalArrayList;
    SubCatAdapter subCatAdapter;
    public CoursesAdapter(Context context, ArrayList<CoursesModal> coursesModalArrayList) {
        this.context=context;
        this.coursesModalArrayList=coursesModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.courses_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        CoursesModal coursesModal=coursesModalArrayList.get(position);
        holder.heading.setText(coursesModal.getCategoryHeading());

        holder.subCatRecyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        subCatAdapter=new SubCatAdapter(context,coursesModal.getCourses());
        holder.subCatRecyclerView.setAdapter(subCatAdapter);
        holder.heading.setOnClickListener(v->{
            Intent broadCast=new Intent("courseName");
            broadCast.putExtra("type","heading");
            broadCast.putExtra("id",coursesModal.getCategoryID());
            broadCast.putExtra("name",coursesModal.getCategoryHeading());
            context.sendBroadcast(broadCast);
        });

    }

    @Override
    public int getItemCount() {
        return coursesModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView heading;
        public RecyclerView subCatRecyclerView;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            heading=itemView.findViewById(R.id.heading);
            subCatRecyclerView=itemView.findViewById(R.id.subCatRecyclerView);
        }
    }

    public void setCacheMenuRes(ArrayList<CoursesModal> cacheMenuRes)
    {
        this.coursesModalArrayList = cacheMenuRes;
        notifyDataSetChanged();
    }

}

package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.ecadema.apiMethods.Api;
import com.ecadema.apiMethods.PostMethod;
import com.ecadema.apiMethods.ResponseData;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.app.Web_View;
import com.ecadema.fragments.CalendarFragment;
import com.ecadema.fragments.MyClassesFragment;
import com.ecadema.modal.UpcomingModal;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MyClassUpcomingAdapter extends RecyclerView.Adapter<MyClassUpcomingAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<UpcomingModal> upcomingModalArrayList;
    private final MyClassesFragment myClassesFragment;
    SharedPreferences sharedPreferences;

    public MyClassUpcomingAdapter(Context context, ArrayList<UpcomingModal> upcomingModalArrayList, MyClassesFragment myClassesFragment) {
        this.context=context;
        this.upcomingModalArrayList=upcomingModalArrayList;
        this.myClassesFragment=myClassesFragment;
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.my_class_booking_adapter,null);
        return new RowHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {

        UpcomingModal upcomingModal=upcomingModalArrayList.get(position);
        String text = "<font color=#f16667>"+ context.getResources().getString(R.string.with) +" </font> <font color=#757575>"+upcomingModal.getName()+"</font>";

        holder.rate.setText("$ "+upcomingModal.getRate()+" /"+context.getResources().getString(R.string.hour));
        holder.dateTime.setText(upcomingModal.getDateTime());
        holder.duration.setText(upcomingModal.getDuration() +" "+context.getResources().getString(R.string.hours));
        holder.sessionType.setText(upcomingModal.getSessionType());

        holder.name.setText(Html.fromHtml(text));

        if(upcomingModal.getRescheduleRequest().equalsIgnoreCase("0")){
            holder.attentionReq.setVisibility(View.VISIBLE);
        }
        else{
            holder.attentionReq.setVisibility(View.GONE);
        }

        if(upcomingModal.getTraineeIsReschedule()){
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        }
        else{
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.fade));
        }

        if(upcomingModal.getIsReschedule()){
            holder.attend.setVisibility(View.GONE);
        }
        else{
            holder.attend.setVisibility(View.VISIBLE);
        }
        holder.attend.setOnClickListener(v->{
            if(sharedPreferences.getString("user_type","").equalsIgnoreCase("1")){
                if(!upcomingModal.getWizIQ().equalsIgnoreCase("null")){
                    /*Intent intent = new Intent(context,ClassroomActivity.class);
                    intent.putExtra(ClassroomActivity.URL, upcomingModal.getWizIQ());
                    context.startActivity(intent);*/
                    Intent intent = new Intent(context, Web_View.class);
                    intent.putExtra("url",upcomingModal.getWizIQ());
                    context.startActivity(intent);
                }
            }
        });
        holder.reschedule.setOnClickListener(v->{
            if(upcomingModal.getRescheduleRequest().equalsIgnoreCase("0")){
                ShowRescheduleReqPopUp(upcomingModal.getRescheduleReason(),upcomingModal.getOldDate(),upcomingModal.getID(),
                        upcomingModal.getTeacherID(),upcomingModal.getName(),upcomingModal.getRate(),upcomingModal.getImage(),upcomingModal.getCourseID(),
                        upcomingModal.getBookingSlotID(),upcomingModal.getOldDate());

            }else if (upcomingModal.getTraineeIsReschedule()){
                Bundle bundle=new Bundle();
                bundle.putString("trainerID",upcomingModal.getTeacherID());
                bundle.putString("name",upcomingModal.getName());
                bundle.putString("courseID",upcomingModal.getCourseID());
                bundle.putString("bookingID",upcomingModal.getID());
                bundle.putString("bookingSlotID",upcomingModal.getBookingSlotID());
                bundle.putString("imageurl",upcomingModal.getImage());
                bundle.putString("ticketPrice",upcomingModal.getRate());
                bundle.putString("oldDate",upcomingModal.getOldDate());
                bundle.putString("from","reschedule");
                CalendarFragment calendarFragment =new CalendarFragment();
                calendarFragment.setArguments(bundle);
                FragmentManager fragmentManager=((MainActivity)context).getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.frag_container, calendarFragment, calendarFragment.getClass().getSimpleName());
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        holder.cancelSession.setOnClickListener(v->{
            cancelSessionPopup(upcomingModal.getOldDate(),upcomingModal.getID());
        });

        new CountDownTimer(upcomingModal.getTimer(), 1000){
            @Override
            public void onTick(long millisUntilFinished) {

                //holder.rate.setText("$ "+upcomingModal.getRate()+" /Hour");

                /* converting the milliseconds into days, hours, minutes and seconds and displaying it in textviews */
                holder.days.setText(TimeUnit.HOURS.toDays(TimeUnit.MILLISECONDS.toHours(millisUntilFinished))+" "+context.getResources().getString(R.string.D));
                holder.hours.setText((TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)))+" "+context.getResources().getString(R.string.H));
                holder.mins.setText((TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)))+" "+context.getResources().getString(R.string.M));
                holder.seconds.setText((TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)))+" "+context.getResources().getString(R.string.S));
            }

            @Override

            public void onFinish() {
                /* clearing all fields and displaying countdown finished message  */
                holder.days.setText("00 "+context.getResources().getString(R.string.D));
                holder.hours.setText("00 "+context.getResources().getString(R.string.H));
                holder.mins.setText("00 "+context.getResources().getString(R.string.M));
                holder.seconds.setText("00 "+context.getResources().getString(R.string.S));
                myClassesFragment.RemoveOneToOne(upcomingModal.getBookingSlotID());
            }
        }.start();

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void ShowRescheduleReqPopUp(String rescheduleReason, String cancelDate, String id, String teacherID, String name, String rate, String image, String courseID, String bookingSlotID, String oldDate) {
        androidx.appcompat.app.AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
        LayoutInflater inflater1 =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") final View dialogView =
                inflater1.inflate(R.layout.reschedule_trainee_dialog, null);
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(false);
        Dialog dialog=alertDialog.create();
        ImageView close=dialogView.findViewById(R.id.close);
        CardView update=dialogView.findViewById(R.id.accept);
        CardView cancel=dialogView.findViewById(R.id.cancel);
        EditText reason=dialogView.findViewById(R.id.reason);

        reason.setText(rescheduleReason);
        update.setOnClickListener(v1->{
            Bundle bundle=new Bundle();
            bundle.putString("trainerID",teacherID);
            bundle.putString("name",name);
            bundle.putString("courseID",courseID);
            bundle.putString("bookingID",id);
            bundle.putString("bookingSlotID",bookingSlotID);
            bundle.putString("imageurl",image);
            bundle.putString("ticketPrice",rate);
            bundle.putString("oldDate",oldDate);
            bundle.putString("from","reschedule");
            CalendarFragment calendarFragment =new CalendarFragment();
            calendarFragment.setArguments(bundle);
            FragmentManager fragmentManager=((MainActivity)context).getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.frag_container, calendarFragment, calendarFragment.getClass().getSimpleName());
            ft.addToBackStack(null);
            ft.commit();
            dialog.dismiss();
            //Reschedule(upcomingModal.getID(),,reason.getText().toString(),dialog,upcomingModal.getStudentID(),holder.rescheduleRequest);
        });
        cancel.setOnClickListener(v1->{
            cancelSessionPopup(cancelDate,id);
            dialog.dismiss();
        });

        close.setOnClickListener(v1->dialog.dismiss());
        dialog.create();
        dialog.show();
    }

    private void cancelSessionPopup(String cancelDate, String id) {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
        LayoutInflater inflater1 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") final View dialogView = inflater1.inflate(R.layout.reschedule_dialog, null);
        alertDialog.setView(dialogView);
        alertDialog.setCancelable(false);
        Dialog dialog=alertDialog.create();
        ImageView close=dialogView.findViewById(R.id.close);
        CardView update=dialogView.findViewById(R.id.update);
        CardView cancel=dialogView.findViewById(R.id.cancel);
        EditText reason=dialogView.findViewById(R.id.reason);
        TextView header=dialogView.findViewById(R.id.header);
        TextView cancelText=dialogView.findViewById(R.id.cancelText);

        cancelText.setVisibility(View.VISIBLE);
        //Linkify.addLinks(cancelText, Linkify.WEB_URLS);
        String text = context.getResources().getString(R.string.cancelText)+" <a href=https://support.ecadema.com/how-do-i-cancel-my-session/><font color=#f16667>"+context.getResources().getString(R.string.cancellationPolicy)+"</font></a>";
        cancelText.setText(Html.fromHtml(text));
        cancelText.setMovementMethod(LinkMovementMethod.getInstance());
        header.setText(context.getResources().getString(R.string.cancelSession));
        update.setOnClickListener(v1->{
            if(reason.getText().toString().trim().matches("")) {
                reason.setError(context.getResources().getString(R.string.fieldRequired));
                return;
            }
            CancelBooking(id,cancelDate,reason.getText().toString(),dialog);
        });
        cancel.setOnClickListener(v1->{
            dialog.dismiss();
        });

        close.setOnClickListener(v1->dialog.dismiss());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.create();
        }
        dialog.show();
    }

    private void CancelBooking(String id, String cancelDate, String reason, Dialog dialog) {
        ProgressDialog progressDialog=new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(R.string.processing));
        progressDialog.show();

        Map<String,String> param = new HashMap<>();
        param.put("booking_id",id);
        param.put("reason",reason);
        param.put("booking_date",cancelDate);
        param.put("lang_token",sharedPreferences.getString("lang",""));

        Log.e("CancelBooking",""+param);
        new PostMethod(Api.TraineeCancelSession,param,context).startPostMethod(new ResponseData() {
            @Override
            public void response(String data) {
                progressDialog.cancel();;
                try {
                    Log.e("cancelResponse",data);
                    JSONObject jsonObject=new JSONObject(data);
                    Toast.makeText(context, jsonObject.optString("message"), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    Intent intent = new Intent("traineeCancelBooking");
                    context.sendBroadcast(intent);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void error(VolleyError error) {
                progressDialog.cancel();;
                error.printStackTrace();
            }
        });
    }

    @Override
    public int getItemCount() {
        return upcomingModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView profilePic;
        TextView name,sessionType,days,hours,mins,seconds,dateTime,duration,rate,reschedule,cancelSession;
        LinearLayout attentionReq;
        CardView cardView,parentCardView,attend;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            cancelSession=itemView.findViewById(R.id.cancelSession);
            attentionReq=itemView.findViewById(R.id.attentionReq);
            reschedule=itemView.findViewById(R.id.reschedule);
            dateTime=itemView.findViewById(R.id.dateTime);
            duration=itemView.findViewById(R.id.duration);
            rate=itemView.findViewById(R.id.rate);

            attend=itemView.findViewById(R.id.attend);
            profilePic=itemView.findViewById(R.id.profile_pic);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);

            parentCardView = itemView.findViewById(R.id.parentCardView);
            cardView = itemView.findViewById(R.id.cardView);
            days = itemView.findViewById(R.id.days);
            hours = itemView.findViewById(R.id.hours);
            mins = itemView.findViewById(R.id.minutes);
            seconds = itemView.findViewById(R.id.seconds);

        }
    }
}

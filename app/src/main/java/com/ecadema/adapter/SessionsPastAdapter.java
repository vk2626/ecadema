package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.PastModal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SessionsPastAdapter extends RecyclerView.Adapter<SessionsPastAdapter.RowHolder> {
    private final ArrayList<PastModal> upcomingModalArrayList;
    private final Context context;

    public SessionsPastAdapter(Context context, ArrayList<PastModal> upcomingModalArrayList) {
        this.context=context;
        this.upcomingModalArrayList=upcomingModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.corporate_session_adapter,null);
        return new RowHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        PastModal pastModal=upcomingModalArrayList.get(position);
        String text = "<font color=#f16667>"+ context.getResources().getString(R.string.By) +" </font> <font color=#757575>"+pastModal.getName()+"</font>";

        holder.sessionType.setText(pastModal.getSessionType());
        holder.name.setText(Html.fromHtml(text));
        holder.sessionName.setText(pastModal.getSessionName());

        holder.dateTime.setText(pastModal.getDateTime());
        holder.rate.setText("$ "+pastModal.getRate()+" /"+context.getResources().getString(R.string.hour));
        holder.duration.setText(pastModal.getDuration()+" "+context.getResources().getString(R.string.hours));

        holder.listOfAttendees.setOnTouchListener((view, motionEvent) -> {
            if(motionEvent.getAction()== MotionEvent.ACTION_DOWN)
                holder.listOfAttendees.showDropDown();
            return true;
        });
        holder.listOfAttendees.setOnItemClickListener((parent, view, i, id) -> {
            holder.listOfAttendees.setText(context.getResources().getString(R.string.listOfAttendees));
            holder.listOfAttendees.setAdapter(new AutocompleteCustomAdapter(context,R.layout.custom_adapter,pastModal.getAttendeesList()));
        });
        holder.listOfAttendees.setAdapter(new AutocompleteCustomAdapter(context,R.layout.custom_adapter,pastModal.getAttendeesList()));
        Picasso.with(context).load(pastModal.getBanner()).into(holder.headerPic);
    }

    @Override
    public int getItemCount() {
        return upcomingModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView profilePic,headerPic;
        TextView name,sessionType,sessionName,duration,dateTime,rate;
        AutoCompleteTextView listOfAttendees;
        CardView complete;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            dateTime=itemView.findViewById(R.id.dateTime);
            headerPic=itemView.findViewById(R.id.headerPic);
            rate=itemView.findViewById(R.id.rate);
            duration=itemView.findViewById(R.id.duration);

            profilePic=itemView.findViewById(R.id.profile_pic);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);
            sessionName=itemView.findViewById(R.id.sessionName);
            listOfAttendees=itemView.findViewById(R.id.listOfAttendees);
            complete=itemView.findViewById(R.id.complete);

            complete.setVisibility(View.VISIBLE);

        }
    }
}

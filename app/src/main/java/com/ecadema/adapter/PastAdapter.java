package com.ecadema.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ecadema.app.R;
import com.ecadema.modal.PastModal;

import java.util.ArrayList;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class PastAdapter extends RecyclerView.Adapter<PastAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<PastModal> pastModalArrayList;
    private final String type;
    private ArrayList<PastModal> filterArrayList;

    public PastAdapter(Context context, ArrayList<PastModal> pastModalArrayList, String type) {
        this.context=context;
        this.pastModalArrayList=pastModalArrayList;
        this.type=type;

        this.filterArrayList = new ArrayList<>();//initiate filter list
        this.filterArrayList.addAll(pastModalArrayList);
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.session_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        PastModal pastModal=pastModalArrayList.get(position);
        holder.expired.setVisibility(View.VISIBLE);
        holder.readMore.setVisibility(View.GONE);

        holder.sessionType.setText(pastModal.getSessionType());
        holder.name.setText(pastModal.getName());
        holder.about.setText(pastModal.getAbout());
        holder.ticket.setText(pastModal.getTicket());
        Glide.with(context).load(pastModal.getImage()).into(holder.profilePic);
        Glide.with(context).load(pastModal.getBanner()).into(holder.banner);

    }

    @Override
    public int getItemCount() {
        return pastModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        ImageView expired,banner;
        TextView name,sessionType,about,ticket,readMore;
        CircleImageView profilePic;

        public RowHolder(@NonNull View itemView) {
            super(itemView);

            expired=itemView.findViewById(R.id.expired);
            profilePic=itemView.findViewById(R.id.profile_pic);
            name=itemView.findViewById(R.id.name);
            sessionType=itemView.findViewById(R.id.sessionType);
            about=itemView.findViewById(R.id.about);
            ticket=itemView.findViewById(R.id.ticket);
            readMore=itemView.findViewById(R.id.readMore);
            banner=itemView.findViewById(R.id.banner);

        }
    }
    public void filter(String charText) {

        Log.e("filters",charText);
        //If Filter type is NAME and EMAIL then only do lowercase, else in case of NUMBER no need to do lowercase because of number format
        charText = charText.toLowerCase(Locale.getDefault());
        pastModalArrayList.clear();//Clear the main ArrayList

        //If search query is null or length is 0 then add all filterList items back to attachmentList
        if (charText.length() == 0) {
            pastModalArrayList.addAll(filterArrayList);
        } else {

            //Else if search query is not null do a loop to all filterList items
            for (PastModal model : filterArrayList) {

                //Now check the type of search filter
                if (model.getName().toLowerCase(Locale.getDefault()).startsWith(charText)||
                        model.getName().toLowerCase(Locale.getDefault()).contains(charText)||
                        model.getAbout().toLowerCase(Locale.getDefault()).contains(charText))
                    pastModalArrayList.add(model);

            }
        }
        notifyDataSetChanged();
    }

}

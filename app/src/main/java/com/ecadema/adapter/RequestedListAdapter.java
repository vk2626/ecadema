package com.ecadema.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.fragments.MyClassesFragment;
import com.ecadema.modal.RequestedModel;

import java.util.ArrayList;

public class RequestedListAdapter extends RecyclerView.Adapter<RequestedListAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<RequestedModel> requestedModelArrayList;
    private final MyClassesFragment myClassesFragment;

    public RequestedListAdapter(Context context, ArrayList<RequestedModel> requestedModelArrayList, MyClassesFragment myClassesFragment) {
        this.context = context;
        this.requestedModelArrayList = requestedModelArrayList;
        this.myClassesFragment = myClassesFragment;
    }

    @NonNull
    @Override
    public RequestedListAdapter.RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.requested_list_view,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestedListAdapter.RowHolder holder, int position) {
        RequestedModel requestedModel = requestedModelArrayList.get(position);
        holder.about.setText(requestedModel.getAbout());
        holder.language.setText(requestedModel.getLanguage());
        holder.noOfParticipants.setText(requestedModel.getMaxPart());
        holder.totalPrice.setText("$"+requestedModel.getPrice());
        if (requestedModel.getStatus().equalsIgnoreCase("1"))
            holder.status.setText(context.getResources().getString(R.string.active));
        else
            holder.status.setText(context.getResources().getString(R.string.in_active));

        holder.re_request.setTag(position);
        holder.re_request.setOnClickListener(v1->{
            int pos = (int) v1.getTag();
            RequestedModel requestedModel_ = requestedModelArrayList.get(pos);
            myClassesFragment.sendRequestForWorkshop(requestedModel_.getAbout(),requestedModel_.getWhen(),requestedModel_.getLanguage()
                    ,requestedModel_.getPrice(),requestedModel_.getDetails(),requestedModel_.getMaxPart());
        });
    }

    @Override
    public int getItemCount() {
        return requestedModelArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView about,language,noOfParticipants,totalPrice,status,re_request;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            about = itemView.findViewById(R.id.about);
            language = itemView.findViewById(R.id.language);
            noOfParticipants = itemView.findViewById(R.id.noOfParticipants);
            totalPrice = itemView.findViewById(R.id.totalPrice);
            status = itemView.findViewById(R.id.status);
            re_request = itemView.findViewById(R.id.re_request);
        }
    }
}

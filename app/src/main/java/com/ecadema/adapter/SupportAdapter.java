package com.ecadema.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.DescriptionWebView;
import com.ecadema.app.R;
import com.ecadema.modal.SupportModal;

import java.util.ArrayList;
import java.util.Locale;

public class SupportAdapter extends RecyclerView.Adapter<SupportAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<SupportModal> supportModalArrayList;
    private ArrayList<SupportModal> filterArrayList;

    public SupportAdapter(Context context, ArrayList<SupportModal> supportModalArrayList) {
        this.context=context;
        this.supportModalArrayList=supportModalArrayList;
        this.filterArrayList = new ArrayList<>();//initiate filter list
        this.filterArrayList.addAll(supportModalArrayList);//add all items of array list to filter list
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.support_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        SupportModal supportModal = supportModalArrayList.get(position);
        String text = context.getResources().getString(R.string.writtenBy)+" </font> <font color=#f16667>"+supportModal.getWrittenBy()+"</font>";

        holder.writtenBy.setText(Html.fromHtml(text));
        holder.title.setText(Html.fromHtml(supportModal.getTitle()));

        Spanned spanned = HtmlCompat.fromHtml(supportModal.getContent(), HtmlCompat.FROM_HTML_MODE_LEGACY);

        holder.content.setText(spanned);
        holder.date.setText(context.getResources().getString(R.string.postedOn)+" "+supportModal.getDate());

        //webView.getSettings().setJavaScriptEnabled(true);
        holder.webView.getSettings().setAllowFileAccess(true);
        //wv.loadUrl(doc);
        //holder.webView.getSettings().setMinimumFontSize(40);

        holder.webView.getSettings().setLoadWithOverviewMode(true);
        holder.webView.getSettings().setUseWideViewPort(true);
        holder.webView.loadData( supportModal.getContent() , "text/html; charset=UTF-8", null);
        //holder.webView.loadDataWithBaseURL("",supportModal.getContent(), "text/html", "utf-8","");

        holder.cardView.setOnClickListener(v->{
            Intent intent = new Intent(context, DescriptionWebView.class);
            intent.putExtra("link",supportModal.getDetails());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return supportModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView title,content,writtenBy, date;
        WebView webView;
        CardView cardView;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            webView = itemView.findViewById(R.id.webView);
            cardView = itemView.findViewById(R.id.cardView);
            title = itemView.findViewById(R.id.title);
            content = itemView.findViewById(R.id.content);
            writtenBy = itemView.findViewById(R.id.writtenBy);
            date = itemView.findViewById(R.id.date);
        }
    }

    public void filter(String charText) {

        //If Filter type is NAME and EMAIL then only do lowercase, else in case of NUMBER no need to do lowercase because of number format
        charText = charText.toLowerCase(Locale.getDefault());
        supportModalArrayList.clear();//Clear the main ArrayList

        //If search query is null or length is 0 then add all filterList items back to attachmentList
        if (charText.length() == 0) {
            supportModalArrayList.addAll(filterArrayList);
        } else {

            //Else if search query is not null do a loop to all filterList items
            for (SupportModal model : filterArrayList) {

                //Now check the type of search filter
                if (model.getContent().toLowerCase(Locale.getDefault()).contains(charText)||
                        model.getTitle().toLowerCase(Locale.getDefault()).contains(charText)||
                        model.getDate().toLowerCase(Locale.getDefault()).contains(charText)/*||
                        model.getVariants().optJSONObject(0).optString("").toLowerCase(Locale.getDefault()).contains(charText)*/)
                    supportModalArrayList.add(model);
            }
        }
        notifyDataSetChanged();
    }

}

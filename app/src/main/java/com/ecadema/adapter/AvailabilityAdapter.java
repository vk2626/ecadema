package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.AvailabilityModal;

import java.util.ArrayList;

public class AvailabilityAdapter extends RecyclerView.Adapter<AvailabilityAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<AvailabilityModal> availabilityModalArrayList;
    private final ArrayList<String> selectedTimeDate;
    private final String type;
    private final String dayOfTheWeek;
    private int selectedPosition=-1;
    ArrayList<String> selectedTime = new ArrayList<>();

    public AvailabilityAdapter(Context context, ArrayList<AvailabilityModal> availabilityModalArrayList, ArrayList<String> selectedTimeDate, String type, String dayOfTheWeek) {
        this.context=context;
        this.availabilityModalArrayList=availabilityModalArrayList;
        this.selectedTimeDate=selectedTimeDate;
        this.type=type;
        this.dayOfTheWeek=dayOfTheWeek;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.timeslotlayout,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, @SuppressLint("RecyclerView") int position) {

        if (dayOfTheWeek.equalsIgnoreCase(availabilityModalArrayList.get(position).getDay())){
            holder.ll.setVisibility(View.VISIBLE);
            holder.tvslot.setVisibility(View.VISIBLE);
            holder.tvslot.setText(availabilityModalArrayList.get(position).getTimings());

            if (availabilityModalArrayList.get(position).getSlots().equals("1")) {
                holder.tvslot.setBackground(context.getResources().getDrawable(R.drawable.selected_bg));
                holder.tvslot.setTextColor(context.getResources().getColor(R.color.white));
            }
            else {
                holder.tvslot.setBackground(context.getResources().getDrawable(R.drawable.grey_border_layout));
                holder.tvslot.setTextColor(context.getResources().getColor(R.color.dark_grey));
            }

            if(type.equalsIgnoreCase("edit")){
                if(selectedTimeDate.size()>0){
                    //selectedTime = new ArrayList<>();
                    for (int ar=0;ar<selectedTimeDate.size();ar++){
                        if(selectedTimeDate.contains(availabilityModalArrayList.get(position).getID())){
                            holder.tvslot.setBackground(context.getResources().getDrawable(R.drawable.selected_bg));
                            holder.tvslot.setTextColor(context.getResources().getColor(R.color.white));
                            selectedTime.add(selectedTimeDate.get(ar));
                        }
                    }
                }
                holder.tvslot.setTag(position);
                holder.tvslot.setOnClickListener(v->{
                    selectedPosition= (int)v.getTag();
                    if (selectedTimeDate.contains(availabilityModalArrayList.get(selectedPosition).getID())){
                        holder.tvslot.setBackground(context.getResources().getDrawable(R.drawable.grey_border_layout));
                        holder.tvslot.setTextColor(context.getResources().getColor(R.color.dark_grey));
                        selectedTime.remove(availabilityModalArrayList.get(selectedPosition).getID());
                        Intent intent = new Intent("editSlot");
                        intent.putExtra("type","remove");
                        intent.putExtra("selectedTime",availabilityModalArrayList.get(selectedPosition).getID());
                        context.sendBroadcast(intent);
                        return;
                    }
                    selectedTime.add(availabilityModalArrayList.get(selectedPosition).getID());
                    holder.tvslot.setBackground(context.getResources().getDrawable(R.drawable.selected_bg));
                    holder.tvslot.setTextColor(context.getResources().getColor(R.color.white));
                    Intent intent = new Intent("editSlot");
                    intent.putExtra("type","add");
                    intent.putExtra("selectedTime",availabilityModalArrayList.get(selectedPosition).getID());
                    context.sendBroadcast(intent);
                });
            }
        }
        else {
            holder.tvslot.setVisibility(View.GONE);
            holder.ll.setVisibility(View.GONE);
        }
       Log.e("SelectedArray",""+selectedTime.size());
    }

    @Override
    public int getItemCount() {
        return availabilityModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView tvslot;
        LinearLayout ll;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            ll= itemView.findViewById(R.id.ll);
            tvslot= itemView.findViewById(R.id.tvslot);
        }
    }
}

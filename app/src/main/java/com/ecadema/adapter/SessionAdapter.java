package com.ecadema.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;
import com.ecadema.modal.SessionModal;

import java.util.ArrayList;

public class SessionAdapter extends RecyclerView.Adapter<SessionAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<SessionModal> sessionModalArrayList;
    private int lastCheckedPosition = 0;

    public SessionAdapter(Context context, ArrayList<SessionModal> sessionModalArrayList) {
        this.context=context;
        this.sessionModalArrayList=sessionModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.select_course,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.session.setText(sessionModalArrayList.get(position).getSubject());
        holder.session.setChecked(position == lastCheckedPosition);

        holder.session.setOnClickListener(v->{
            int copyOfLastCheckedPosition = lastCheckedPosition;
            lastCheckedPosition = position;
            notifyItemChanged(copyOfLastCheckedPosition);
            notifyItemChanged(lastCheckedPosition);

            Intent intent=new Intent("courseSelected");
            intent.putExtra("coursePrice", sessionModalArrayList.get(position).getCoursePrices());
            intent.putExtra("courseID", sessionModalArrayList.get(position).getCourseId());
            context.sendBroadcast(intent);
        });
    }

    @Override
    public int getItemCount() {
        return sessionModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        RadioButton session;
        public RowHolder(@NonNull View itemView) {
            super(itemView);
            session=itemView.findViewById(R.id.session);
        }
    }
}

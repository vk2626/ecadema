package com.ecadema.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ecadema.app.R;

import java.util.ArrayList;

public class EducationAdapter extends RecyclerView.Adapter<EducationAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<String> educationArrayList;

    public EducationAdapter(Context context, ArrayList<String> educationArrayList) {
        this.context=context;
        this.educationArrayList=educationArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.education_layout,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        holder.education.setText(educationArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return educationArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView education;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            education=itemView.findViewById(R.id.education);
        }
    }
}

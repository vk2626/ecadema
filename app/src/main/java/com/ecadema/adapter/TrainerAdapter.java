package com.ecadema.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ecadema.app.MainActivity;
import com.ecadema.app.R;
import com.ecadema.fragments.ViewProfile;
import com.ecadema.modal.TrainerModal;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class TrainerAdapter extends RecyclerView.Adapter<TrainerAdapter.RowHolder> {
    private final Context context;
    private final ArrayList<TrainerModal> trainerModalArrayList;

    public TrainerAdapter(Context context, ArrayList<TrainerModal> trainerModalArrayList) {
        this.context=context;
        this.trainerModalArrayList=trainerModalArrayList;
    }

    @NonNull
    @Override
    public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.trainer_adapter,null);
        return new RowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RowHolder holder, int position) {
        TrainerModal trainerModal=trainerModalArrayList.get(position);

        holder.ratings.setRating(trainerModal.getRatings());
        holder.trainerName.setText(trainerModal.getName());
        holder.rate.setText(trainerModal.getRates());
        Glide.with(context).load(trainerModal.getImage()).into(holder.profilePic);

        if(trainerModal.getIsMentor().equalsIgnoreCase("1"))
            holder.mentor.setVisibility(View.VISIBLE);
        else
            holder.mentor.setVisibility(View.GONE);

        holder.cardView.setOnClickListener(v->{
            Bundle bundle=new Bundle();
            bundle.putString("trainerID",trainerModal.getTeacherID());
            ViewProfile viewProfile=new ViewProfile();
            viewProfile.setArguments(bundle);
            ((MainActivity) context).addFragment2(viewProfile, true, false, 0);
        });
    }

    @Override
    public int getItemCount() {
        return trainerModalArrayList.size();
    }

    public class RowHolder extends RecyclerView.ViewHolder {
        TextView trainerName,rate;
        RatingBar ratings;
        ImageView mentor;
        CircleImageView profilePic;
        CardView cardView;
        public RowHolder(@NonNull View itemView) {
            super(itemView);

            cardView=itemView.findViewById(R.id.cardView);
            profilePic=itemView.findViewById(R.id.profile_pic);
            ratings=itemView.findViewById(R.id.ratings);
            mentor=itemView.findViewById(R.id.mentor);
            trainerName=itemView.findViewById(R.id.trainerName);
            rate=itemView.findViewById(R.id.rate);
        }
    }
}

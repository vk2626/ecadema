package com.ecadema.payPal;

public class TokenData {
  String clienttoken;

  public String getClienttoken() {
    return clienttoken;
  }

  public void setClienttoken(String clienttoken) {
    this.clienttoken = clienttoken;
  }
}

package com.ecadema.modal;

import java.util.ArrayList;

public class CoursesModal {
    private String categoryID;
    private ArrayList<SubCategModal> courses;
    private String categoryHeading;

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCourses(ArrayList<SubCategModal> courses) {
        this.courses = courses;
    }

    public ArrayList<SubCategModal> getCourses() {
        return courses;
    }

    public void setCategoryHeading(String categoryHeading) {
        this.categoryHeading = categoryHeading;
    }

    public String getCategoryHeading() {
        return categoryHeading;
    }
}

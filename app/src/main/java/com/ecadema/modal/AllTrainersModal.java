package com.ecadema.modal;

import org.json.JSONArray;

import java.util.ArrayList;

public class AllTrainersModal {
    private String ID;
    private String image;
    private String name;
    private String location;
    private String language;
    private String rate;
    private ArrayList<String> trainsFor;
    private int rating;
    private String gender;
    private String teacherId;
    private JSONArray sessions;
    private String isMentor;
    private JSONArray specialized;
    private String label;

    public void setID(String id) {
        this.ID = id;
    }

    public String getID() {
        return ID;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRate() {
        return rate;
    }

    public void setTrainsFor(ArrayList<String> trainsFor) {
        this.trainsFor = trainsFor;
    }

    public ArrayList<String> getTrainsFor() {
        return trainsFor;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setSessions(JSONArray sessions) {
        this.sessions = sessions;
    }

    public JSONArray getSessions() {
        return sessions;
    }

    public void setIsMentor(String isMentor) {
        this.isMentor = isMentor;
    }

    public String getIsMentor() {
        return isMentor;
    }

    public void setSpecialized(JSONArray specialized) {
        this.specialized = specialized;
    }

    public JSONArray getSpecialized() {
        return specialized;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}

package com.ecadema.modal;

import org.json.JSONArray;

public class AvailabilityModal {
    private String ID;
    private String timings;
    private String slots;
    private JSONArray availability;
    private String day;

    public void setID(String id) {
        this.ID = id;
    }

    public String getID() {
        return ID;
    }

    public void setTimings(String timings) {
        this.timings = timings;
    }

    public String getTimings() {
        return timings;
    }

    public void setSlots(String slots) {
        this.slots = slots;
    }

    public String getSlots() {
        return slots;
    }

    public void setAvailability(JSONArray availability) {
        this.availability = availability;
    }

    public JSONArray getAvailability() {
        return availability;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDay() {
        return day;
    }
}

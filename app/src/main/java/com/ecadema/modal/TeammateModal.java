package com.ecadema.modal;

import org.json.JSONArray;

public class TeammateModal {
    private String id;
    private String image;
    private String name;
    private String mobile;
    private String email;
    private String teammateId;
    private JSONArray course;
    private String userType;
    private String department;
    private String seniority;
    private String status;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setTeammateId(String teammateId) {
        this.teammateId = teammateId;
    }

    public String getTeammateId() {
        return teammateId;
    }

    public void setCourse(JSONArray course) {
        this.course = course;
    }

    public JSONArray getCourse() {
        return course;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return userType;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    public void setSeniority(String seniority) {
        this.seniority = seniority;
    }

    public String getSeniority() {
        return seniority;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}

package com.ecadema.modal;

import java.util.ArrayList;

public class UpcomingModal {
    private String ID;
    private String image;
    private String name;
    private String sessionType;
    private String about;
    private String ticket;
    private String sessionName;
    private long timer;
    private ArrayList<AttendeesModal> attendeesList;
    private String banner;
    private String bannerImage;
    private String dateTime;
    private String duration;
    private String rate;
    private boolean isReschedule;
    private String teacherID;
    private String studentID;
    private String isRescheduleResp;
    private String rescheduleRequest;
    private String rescheduleReason;
    private String cancelDate;
    private String language;
    private String bookingSlotID;
    private String courseID;
    private String oldDate;
    private String wizIQ;
    private boolean traineeIsReschedule;
    private String attend;
    private long sortingDate;
    private boolean isSendPDF;
    private String bookingId;
    private String bookedDateId;
    private String URL;
    private String bookingType;
    private String tagUrl;
    private boolean showAttend;

    public void setID(String id) {
        this.ID = id;
    }

    public String getID() {
        return ID;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSessionType(String sessionType) {
        this.sessionType = sessionType;
    }

    public String getSessionType() {
        return sessionType;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAbout() {
        return about;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getTicket() {
        return ticket;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setTimer(long timer) {
        this.timer = timer;
    }

    public long getTimer() {
        return timer;
    }

    public void setAttendeesList(ArrayList<AttendeesModal> attendeesList) {
        this.attendeesList = attendeesList;
    }

    public ArrayList<AttendeesModal> getAttendeesList() {
        return attendeesList;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getBanner() {
        return banner;
    }

    public void setBannerImage(String bannerImage) {
        this.bannerImage = bannerImage;
    }

    public String getBannerImage() {
        return bannerImage;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration() {
        return duration;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRate() {
        return rate;
    }

    public void setIsReschedule(boolean isReschedule) {
        this.isReschedule = isReschedule;
    }

    public boolean getIsReschedule() {
        return isReschedule;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setIsRescheduleResp(String isRescheduleResp) {
        this.isRescheduleResp = isRescheduleResp;
    }

    public String getIsRescheduleResp() {
        return isRescheduleResp;
    }

    public void setRescheduleRequest(String rescheduleRequest) {
        this.rescheduleRequest = rescheduleRequest;
    }

    public String getRescheduleRequest() {
        return rescheduleRequest;
    }

    public void setRescheduleReason(String rescheduleReason) {
        this.rescheduleReason = rescheduleReason;
    }

    public String getRescheduleReason() {
        return rescheduleReason;
    }

    public void setCancelDate(String cancelDate) {
        this.cancelDate = cancelDate;
    }

    public String getCancelDate() {
        return cancelDate;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setBookingSlotID(String bookingSlotID) {
        this.bookingSlotID = bookingSlotID;
    }

    public String getBookingSlotID() {
        return bookingSlotID;
    }

    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    public String getCourseID() {
        return courseID;
    }

    public void setOldDate(String oldDate) {
        this.oldDate = oldDate;
    }

    public String getOldDate() {
        return oldDate;
    }

    public void setWizIQ(String wizIQ) {
        this.wizIQ = wizIQ;
    }

    public String getWizIQ() {
        return wizIQ;
    }

    public void setTraineeIsReschedule(boolean traineeIsReschedule) {
        this.traineeIsReschedule = traineeIsReschedule;
    }

    public boolean getTraineeIsReschedule() {
        return traineeIsReschedule;
    }

    public void setAttend(String attend) {
        this.attend = attend;
    }

    public String getAttend() {
        return attend;
    }

    public void setSortingDate(long sortingDate) {
        this.sortingDate = sortingDate;
    }

    public long getSortingDate() {
        return sortingDate;
    }

    public void setIsSendPDF(boolean isSendPDF) {
        this.isSendPDF = isSendPDF;
    }

    public boolean getIsSendPDF() {
        return isSendPDF;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookedDateId(String bookedDateId) {
        this.bookedDateId = bookedDateId;
    }

    public String getBookedDateId() {
        return bookedDateId;
    }

    public void setURL(String url) {
        this.URL = url;
    }

    public String getURL() {
        return URL;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setTagUrl(String tagUrl) {
        this.tagUrl = tagUrl;
    }

    public String getTagUrl() {
        return tagUrl;
    }

    public void setShowAttend(boolean showAttend) {

        this.showAttend = showAttend;
    }

    public boolean getShowAttend() {
        return showAttend;
    }
}

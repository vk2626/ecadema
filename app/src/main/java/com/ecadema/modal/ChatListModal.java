package com.ecadema.modal;

import org.json.JSONArray;
import org.json.JSONObject;

public class ChatListModal {
    private String chatId;
    private String userName;
    private String description;
    private String date;
    private String DP;
    private String receiversID;
    private JSONObject lastMessage;
    private String lastMessageString;
    private String studentID;
    private int unread;
    private JSONArray courses;
    private String onlineStatus;
    private String accountActive;

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getChatId() {
        return chatId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDP(String dp) {
        this.DP = dp;
    }

    public String getDP() {
        return DP;
    }

    public void setReceiversID(String receiversID) {
        this.receiversID = receiversID;
    }

    public String getReceiversID() {
        return receiversID;
    }

    public void setLastMessage(JSONObject lastMessage) {
        this.lastMessage = lastMessage;
    }

    public JSONObject getLastMessage() {
        return lastMessage;
    }

    public void setLastMessageString(String lastMessageString) {
        this.lastMessageString = lastMessageString;
    }

    public String getLastMessageString() {
        return lastMessageString;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setUnread(int unread) {
        this.unread = unread;
    }

    public int getUnread() {
        return unread;
    }

    public void setCourses(JSONArray courses) {
        this.courses = courses;
    }

    public JSONArray getCourses() {
        return courses;
    }

    public void setOnlineStatus(String onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public String getOnlineStatus() {
        return onlineStatus;
    }

    public void setAccountActive(String accountActive) {
        this.accountActive = accountActive;
    }

    public String getAccountActive() {
        return accountActive;
    }
}

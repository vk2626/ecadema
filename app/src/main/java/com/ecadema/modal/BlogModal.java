package com.ecadema.modal;

public class BlogModal {
    private String blogId;
    private String authorName;
    private String title;
  private String id;
  private String name;
  private String image;
  private String date;
  private String shortDecription;
  private String view;
  private String likes;
  private String dislike;
  private String categoryName;
  private String profileImage;
  private String video;
  private String shareUrl;
  private String likeDislike;
  private String teacherId;
  private String tagIDs;
  private String videoLink;
  private String tagName;

  public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getBlogId() {
        return blogId;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

  public void setId(final String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setImage(final String image) {
    this.image = image;
  }

  public String getImage() {
    return image;
  }

  public void setDate(final String date) {
    this.date = date;
  }

  public String getDate() {
    return date;
  }

  public void setShortDecription(final String shortDecription) {
    this.shortDecription = shortDecription;
  }

  public String getShortDecription() {
    return shortDecription;
  }

  public void setView(final String view) {
    this.view = view;
  }

  public String getView() {
    return view;
  }

  public void setLikes(final String likes) {
    this.likes = likes;
  }

  public String getLikes() {
    return likes;
  }

  public void setDislike(final String dislike) {
    this.dislike = dislike;
  }

  public String getDislike() {
    return dislike;
  }

  public void setCategoryName(final String categoryName) {
    this.categoryName = categoryName;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setProfileImage(String profileImage) {
    this.profileImage = profileImage;
  }

  public String getProfileImage() {
    return profileImage;
  }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideo() {
        return video;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public String getShareUrl() {
        return shareUrl;
    }

  public void setLikeDislike(String likeDislike) {
    this.likeDislike = likeDislike;
  }

  public String getLikeDislike() {
    return likeDislike;
  }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTagIDs(String tagIDs) {
        this.tagIDs = tagIDs;
    }

    public String getTagIDs() {
        return tagIDs;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTagName() {
        return tagName;
    }
}

package com.ecadema.modal;

public class SavedCardModal {

  private String cardNumber;
  private String lastFour;
  private String year;
  private String CVC;
  private String brand;
  private String month;
  private String id;

  public void setCardNumber(final String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public String getCardNumber() {
    return cardNumber;
  }

  public void setLastFour(final String lastFour) {
    this.lastFour = lastFour;
  }

  public String getLastFour() {
    return lastFour;
  }

  public void setYear(final String year) {
    this.year = year;
  }

  public String getYear() {
    return year;
  }

  public void setCVC(final String cvc) {
    this.CVC = cvc;
  }

  public String getCVC() {
    return CVC;
  }

  public void setBrand(final String brand) {
    this.brand = brand;
  }

  public String getBrand() {
    return brand;
  }

  public void setMonth(final String month) {
    this.month = month;
  }

  public String getMonth() {
    return month;
  }

  public void setId(final String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }
}

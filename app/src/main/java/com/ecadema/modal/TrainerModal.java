package com.ecadema.modal;

public class TrainerModal {
    private String ID;
    private String image;
    private String name;
    private int ratings;
    private String rates;
    private String teacherID;
    private String rating;
    private String isMentor;


    public void setID(String id) {
        this.ID = id;
    }

    public String getID() {
        return ID;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setRatings(int ratings) {
        this.ratings = ratings;
    }

    public int getRatings() {
        return ratings;
    }

    public void setRates(String rates) {
        this.rates = rates;
    }

    public String getRates() {
        return rates;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

    public void setIsMentor(String isMentor) {
        this.isMentor = isMentor;
    }

    public String getIsMentor() {
        return isMentor;
    }
}

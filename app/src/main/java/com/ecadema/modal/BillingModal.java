package com.ecadema.modal;

public class BillingModal {
    private String ID;
    private String item;
    private String price;
    private String date;
    private String bookingCode;
    private String billingType;

    public void setID(String id) {
        this.ID = id;
    }

    public String getID() {
        return ID;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getItem() {
        return item;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getBillingType() {
        return billingType;
    }
}

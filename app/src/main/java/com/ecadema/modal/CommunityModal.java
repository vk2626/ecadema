package com.ecadema.modal;

public class CommunityModal {

  private String name;
  private String image;
  private String date;
  private String title;
  private String description;
  private String id;

  public void setName(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setImage(final String image) {
    this.image = image;
  }

  public String getImage() {
    return image;
  }

  public void setDate(final String date) {
    this.date = date;
  }

  public String getDate() {
    return date;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  public String getTitle() {
    return title;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  public void setId(final String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }
}

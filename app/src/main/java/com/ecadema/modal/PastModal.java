package com.ecadema.modal;

import java.util.ArrayList;

public class PastModal {
    private String id;
    private String image;
    private String name;
    private String sessionType;
    private String about;
    private String ticket;
    private String sessionName;
    private long timer;
    private ArrayList<AttendeesModal> attendeesList;
    private String banner;
    private String dateTime;
    private String duration;
    private String rate;
    private String rateByTrainee;
    private String language;
    private String teacherID;
    private long sortingDate;
    private String buttonName;
    private String URL;
    private String bookingID;
    private String time;
    private String bookingType;
    private String tagUrl;


    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSessionType(String sessionType) {
        this.sessionType = sessionType;
    }

    public String getSessionType() {
        return sessionType;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAbout() {
        return about;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public String getTicket() {
        return ticket;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setTimer(long timer) {
        this.timer = timer;
    }

    public long getTimer() {
        return timer;
    }

    public void setAttendeesList(ArrayList<AttendeesModal> attendeesList) {
        this.attendeesList = attendeesList;
    }

    public ArrayList<AttendeesModal> getAttendeesList() {
        return attendeesList;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getID() {
        return id;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getBanner() {
        return banner;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration() {
        return duration;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRate() {
        return rate;
    }

    public void setRateByTrainee(String rateByTrainee) {
        this.rateByTrainee = rateByTrainee;
    }

    public String getRateByTrainee() {
        return rateByTrainee;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public void setSortingDate(long sortingDate) {
        this.sortingDate = sortingDate;
    }

    public long getSortingDate() {
        return sortingDate;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    public String getButtonName() {
        return buttonName;
    }

    public void setURL(String url) {
        this.URL = url;
    }

    public String getURL() {
        return URL;
    }

    public void setBookingID(String bookingID) {
        this.bookingID = bookingID;
    }

    public String getBookingID() {
        return bookingID;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setTagUrl(String tagUrl) {
        this.tagUrl = tagUrl;
    }

    public String getTagUrl() {
        return tagUrl;
    }
}

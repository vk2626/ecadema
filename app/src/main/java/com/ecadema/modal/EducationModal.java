package com.ecadema.modal;

public class EducationModal {
    private String course;
    private String year;
    private String URL;
    private String institute;
    private String startFrom;
    private String endTo;
    private String certificate;
    private String id;

    public void setCourse(String course) {
        this.course = course;
    }

    public String getCourse() {
        return course;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public void setURL(String url) {
        this.URL = url;
    }

    public String getURL() {
        return URL;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getInstitute() {
        return institute;
    }

    public void setStartFrom(String startFrom) {
        this.startFrom = startFrom;
    }

    public String getStartFrom() {
        return startFrom;
    }

    public void setEndTo(String endTo) {
        this.endTo = endTo;
    }

    public String getEndTo() {
        return endTo;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}

package com.even.rich.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import butterknife.ButterKnife;
import com.even.rich.R;

/**
 * Edit Hyperlink Activity
 *
 * @author even.wu
 * @date 10/8/17
 */

public class EditHyperlinkFragment extends Fragment implements View.OnClickListener {
    EditText etAddress;
   EditText etDisplayText;
   ImageView iv_back;
   Button btn_ok;
    private OnHyperlinkListener mOnHyperlinkListener;

    @Nullable @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit_hyperlink, null);
        ButterKnife.bind(this, rootView);
        etAddress=rootView.findViewById(R.id.et_address);
        etDisplayText=rootView.findViewById(R.id.et_display_text);
        iv_back=rootView.findViewById(R.id.iv_back);
        btn_ok=rootView.findViewById(R.id.btn_ok);
        return rootView;
    }

 /*   @OnClick(R.id.iv_back) void onClickBack() {
        getFragmentManager().beginTransaction().remove(this).commit();
    }

    @OnClick(R.id.btn_ok) void onClickOK() {
        if (mOnHyperlinkListener != null) {
            mOnHyperlinkListener.onHyperlinkOK(etAddress.getText().toString(),
                etDisplayText.getText().toString());
            onClickBack();
        }
    }*/

    public void setOnHyperlinkListener(OnHyperlinkListener mOnHyperlinkListener) {
        this.mOnHyperlinkListener = mOnHyperlinkListener;
    }

    @Override
    public void onClick(View v) {
        int j=v.getId();
        if (j==R.id.btn_ok){
            if (mOnHyperlinkListener != null) {
                mOnHyperlinkListener.onHyperlinkOK(etAddress.getText().toString(),
                        etDisplayText.getText().toString());
                getFragmentManager().beginTransaction().remove(this).commit();
            }

        }
        if (j==R.id.iv_back){
            getFragmentManager().beginTransaction().remove(this).commit();
        }

    }

    public interface OnHyperlinkListener {
        void onHyperlinkOK(String address, String text);
    }
}
